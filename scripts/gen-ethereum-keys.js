// const Web3 = require('web3').default;
import Web3 from 'web3';
const web3 = new Web3();
const account = web3.eth.accounts.create();
const {
  address,
  privateKey,
} = account;
console.log(JSON.stringify({
  address,
  privateKey,
}, null, 2));
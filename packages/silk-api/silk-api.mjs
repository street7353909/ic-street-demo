import {
  ethers,
} from 'ethers';

//

const headers = [
  {
    "key": "Access-Control-Allow-Origin",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Methods",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Expose-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Private-Network",
    "value": "true"
  }
];
const headersObject = {};
for (const header of headers) {
  headersObject[header.key] = header.value;
}

//

// Cloudflare Worker
export default {
  async fetch(request, env, ctx) {
    const u = new URL(request.url);
    const pathname = u.pathname;
    console.log('got request', {
      method: request.method,
      url: request.url,
      pathname,
      // env,
    });

    // if OPTIONS, send headers
    if (request.method === 'OPTIONS') {
      return new Response(null, {
        headers: headersObject,
      });
    }

    const privateKey = env.ETHEREUM_PRIVATE_KEY;
    const p = (async () => {
      // console.log('got method', request.method);

      if (request.method === 'POST') {
        // Generate a random private key (replace this with the signer's private key)
        // const privateKey = ethers.Wallet.createRandom().privateKey;

        // Create a new wallet instance
        const wallet = new ethers.Wallet(privateKey);

        // The message you want to sign
        // const messageBytes = new TextEncoder().encode('Hello, world!');
        const arrayBuffer = await request.arrayBuffer();
        const messageBytes = new Uint8Array(arrayBuffer);

        // Sign the message
        const signature = await wallet.signMessage(messageBytes);
        console.log('sign message bytes', {
          messageBytes,
          signature,
        });
        // console.log('got sig', signature);

        // Parse the signature into r, s, v
        // const signatureParts = ethers.splitSignature(signature);
        // const signatureParts = ethers.utils.splitSignature(signature);
        const splitSig = ethers.Signature.from(signature)
        // const sigBytes = ethers.Signature.from(splitSig).serialized
        const {
          r,
          s,
          v,
        } = splitSig;

        return new Response(JSON.stringify({
          r,
          s,
          v,
        }), {
          headers: {
            ...headersObject,
            'Content-Type': 'application/json',
          },
        });
      }

      /* const supabase = makeSupabase(env);

      if (request.method === 'GET' && pathname === '/goal') {
        const j = await ensureGenerateGoal({
          supabase,
        });

        // respond with json
        return new Response(JSON.stringify(j), {
          headers: {
            ...headersObject,
            'Content-Type': 'application/json',
          },
        });
      } else if (request.method === 'GET' && pathname === '/generate_goal') {
        try {
          const j = await generateGoal();
          return new Response(JSON.stringify(j), {
            headers: {
              ...headersObject,
              'Content-Type': 'application/json',
            },
          });
        } catch(err) {
          return new Response(err.stack, {
            headers: {
              ...headersObject,
              'Content-Type': 'text/plain',
            },
          });
        }
      } */

      // return 404
      return new Response(null, {
        status: 404,
        statusText: 'Not Found',
      });
    })();
    // console.log('waiting for p 1');
    ctx.waitUntil(p);
    // console.log('waiting for p 2');
    return await p;
  },
};
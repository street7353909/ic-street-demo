// const ethers = require('ethers');
import {ethers} from 'ethers';

async function signMessage() {
  // Generate a random private key (replace this with the signer's private key)
  // const privateKey = ethers.Wallet.createRandom().privateKey;
  const privateKey = "0x66d7ff92d0fbbccc3950a164dc5a3123768ce162343d265ad52059777a7c5e15";

  // Create a new wallet instance
  const wallet = new ethers.Wallet(privateKey);

  // The message you want to sign
  const message = "Hello, world!";

  // Sign the message
  const signature = await wallet.signMessage(message);

  // Parse the signature into r, s, v
  console.log('got sig', signature);
  // const signatureParts = ethers.splitSignature(signature);
  // const signatureParts = ethers.utils.splitSignature(signature);
  const splitSig = ethers.Signature.from(signature)
  // const sigBytes = ethers.Signature.from(splitSig).serialized
  const {
    r,
    s,
    v,
  } = splitSig;

  console.log(`sig`, splitSig);
  console.log(`r: ${r}`);
  console.log(`s: ${s}`);
  console.log(`v: ${v}`);
}
signMessage(); // .catch(console.error);
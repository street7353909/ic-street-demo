export const getDefaultUser = (id = crypto.randomUUID()) => {
  return {
    id,
    name: 'Anon ' + Math.floor(Math.random() * Number.MAX_SAFE_INTEGER),
    playerSpec: getDefaultPlayerSpec(),
    // default: true,
  };
};
export const getDefaultPlayerSpec = () => {
  return (Math.random() < 0.5 ?
    {
      name: 'Vipe 2185',
      bio: 'A blond-haired boy',
      // voiceEndpoint: 'elevenlabs:scillia',
      // voiceEndpoint: 'tiktalknet:Trixie',
      // voiceEndpoint: 'tiktalknet:Shining Armor',
      voiceEndpoint: 'elevenlabs:kaido:YkP683vAWY3rTjcuq2hX',
      voicePack: 'Griffin voice pack',
      // avatarUrl: '/avatars/default_2195.vrm',
      // avatarUrl: '/avatars/default_2194.vrm',
      // avatarUrl: '/avatars/Yoll2.vrm',
      // avatarUrl: '/avatars/ann_liskwitch_v3.3_gulty.vrm',
      // avatarUrl: '/avatars/CornetVRM.vrm',
      // avatarUrl: '/avatars/Buster_Rabbit_V1.1_Guilty.vrm',
      // avatarUrl: '/avatars/Scilly_FaceTracking_v1_Darling.vrm',
      // avatarUrl: '/avatars/Scilly_FaceTracking_v3_Darling_2.vrm',
      // avatarUrl: '/avatars/Scilly_FaceTracking_v5_Darling.vrm',
      // avatarUrl: '/avatars/Scilly_FaceTracking_v6_Darling.vrm',
      // avatarUrl: '/avatars/scilly_psx.vrm',
      // avatarUrl: '/avatars/scilly_drophunter_v31.10_Guilty.vrm',
      avatarUrl: '/avatars/default_2185.vrm',
    }
  :
    {
      name: 'Vipe 1614',
      bio: 'A brown-haired girl',
      voiceEndpoint: 'elevenlabs:scillia:kNBPK9DILaezWWUSHpF9',
      voicePack: 'ShiShi voice pack',
      // avatarUrl: '/avatars/default_1933.vrm',
      // avatarUrl: '/avatars/default_2194.vrm',
      avatarUrl: '/avatars/default_1934.vrm',
    }
  );
};
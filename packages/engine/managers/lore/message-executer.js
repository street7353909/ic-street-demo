import {
  QueueManager,
} from '../queue/queue-manager.js';
import {
  Message,
} from './message.js';

//

/* const advanceModeExecuteFilterFns = {
  'RPG': (message, messageExecuter) => {
    return false;
  },
  'Wait': (message, messageExecuter) => {
    return false;
  },
  'Director': (message, messageExecuter) => {
    return false;
  },
  'Auto': (message, messageExecuter) => {
    return true;
  },
}; */

//

export class MessageExecuter extends EventTarget {
  constructor({
    engine,
    aiClient,
    loreManager,
    storyAdvance,
    signal,
    messages = [],
    // preloadedMessages = [],
    // minBufferedMessages = 0,
  } = {}) {
    super();

    if (!engine || !aiClient || !loreManager || !storyAdvance || !signal) {
      console.warn('missing args', {
        engine,
        aiClient,
        loreManager,
        storyAdvance,
        signal,
      });
      debugger;
      throw new Error('missing args');
    }

    this.#engine = engine;
    // this.#aiClient = aiClient;
    // this.#loreManager = loreManager;
    // this.#storyAdvance = storyAdvance;
    this.#signal = signal;
    // this.#executeAbortController = null;
    // this.#preloadAbortController = null;
    this.#queueManager = new QueueManager();
    this.#queuedMessages = messages;
    this.#pendingMessages = 0;
    // this.#preloadedMessages = preloadedMessages;
    // this.minBufferedMessages = minBufferedMessages;
    // this.#executingMessage = null;

    this.#queueManager.addEventListener('idlechange', e => {
      const {
        idle,
      } = e.data;
      // console.log('queue manager idle change', {
      //   idle,
      // });
      this.dispatchEvent(new MessageEvent('runningchange', {
        data: {
          running: !idle,
        },
      }));
    });
  }
  #engine;
  // #aiClient;
  // #loreManager;
  // #storyAdvance;
  #signal;
  // #executeAbortController;
  // #preloadAbortController;
  #queueManager;
  #queuedMessages;
  #pendingMessages;
  // #preloadedMessages;
  #executingMessage;

  isRunning() {
    return !this.#queueManager.isIdle();
  }
  /* isExecuting() {
    return !!this.#executeAbortController;
  }
  isPreloading() {
    return !!this.#preloadAbortController;
  } */
  /* executeFilter({
    advanceMode,
    lastMessage,
  }) {
    const filterFn = advanceModeExecuteFilterFns[advanceMode];
    if (filterFn) {
      return filterFn(lastMessage, this);
    } else {
      console.warn('missing filterFn for', {
        advanceMode,
      });
      return false;
    }
  }
  preloadFilter() {
    return this.#preloadedMessages.length < this.minBufferedMessages;
  } */
  // pump the preload -> queue message
  /* async startExecute(forceStart = false) {
    console.log('start execute', {
      forceStart,
      executing: this.isExecuting(),
    });
    const executeAbortController = new AbortController();
    this.#executeAbortController = executeAbortController;
    this.dispatchEvent(new MessageEvent('executingchange', {
      data: {
        executing: !!this.#executeAbortController,
      },
    }));

    for (;;) {
      // if it's time to stop, stop
      const advanceMode = this.#storyAdvance.getMode();
      if (!forceStart && !this.executeFilter({
        advanceMode,
        lastMessage: this.#queuedMessages[this.#queuedMessages.length - 1],
        // conversation: this,
      })) {
        this.stopExecute(); // XXX do not call this unless we actually want to stop the execution
        break;
      } else {
        forceStart = false;

        // wait for a preloaded message
        const queuedMessages = this.#queuedMessages.slice();
        console.log('wait for preload 1', {
          queuedMessages,
          aborted: executeAbortController.signal.aborted,
        });
        const syncFn = await this.pullPreloadMessageFn();
        console.log('wait for preload 2', {
          queuedMessages,
          aborted: executeAbortController.signal.aborted,
        });
        // if aborted
        if (executeAbortController.signal.aborted) break;
        console.log('wait for preload 3', {
          queuedMessages,
          aborted: executeAbortController.signal.aborted,
        });

        const nextPreloadMessage = syncFn();
        console.log('queue next preload message', {
          queuedMessages,
          nextPreloadMessage,
          aborted: executeAbortController.signal.aborted,
        });
        this.queueMessage(nextPreloadMessage);

        if (!this.isPreloading()) {
          this.startPreload();
        }
      }
    }
  }
  stopExecute() {
    if (this.#executeAbortController) {
      this.#executeAbortController.abort();
      this.#executeAbortController = null;

      // console.log('call stop execute', new Error().stack);

      this.dispatchEvent(new MessageEvent('executingchange', {
        data: {
          executing: !!this.#executeAbortController,
        },
      }));
    } else {
      throw new Error('not executing!');
    }
  } */
  /* async startPreload() {
    const preloadAbortController = new AbortController();
    this.#preloadAbortController = preloadAbortController;
    this.dispatchEvent(new MessageEvent('preloadingchange', {
      data: {
        preloading: !!this.#preloadAbortController,
      },
    }));

    for (;;) {
      // if it's time to stop, stop
      if (!this.preloadFilter()) {
        this.stopPreload();
        break;
      } else {
        const messages = [
          ...this.#queuedMessages,
          ...this.#preloadedMessages,
        ];
        const conversation = new Conversation({
          aiClient: this.#aiClient,
          loreManager: this.#loreManager,
          messages,
        });
        const nextMessage = await conversation.getNextMessage({
          signal: this.#preloadAbortController.signal,
        });
        if (preloadAbortController.signal.aborted) break;

        if (nextMessage) {
          // ensure it's preloading
          nextMessage.preload({
            engine: this.#engine,
          });

          // queue the message for execution
          this.pushPreloadMessage(nextMessage);
        } else {
          console.warn('failed to get next message in conversation', conversation, new Error().stack);
        }
      }
    }
  }
  stopPreload() {
    if (this.#preloadAbortController) {
      this.#preloadAbortController.abort();
      this.#preloadAbortController = null;

      this.dispatchEvent(new MessageEvent('preloadingchange', {
        data: {
          preloading: !!this.#preloadAbortController,
        },
      }));
    } else {
      throw new Error('not preloading');
    }
  } */
  ensurePreloading() {
    if (!this.isPreloading()) {
      this.startPreload();
    }
  }

  // queue message for execution
  async queueMessage(message) {
    // if (!(message instanceof Message)) {
    //   console.warn('message is not a Message', message);
    //   debugger;
    //   throw new Error('message is not a Message');
    // }

    // latch the message as executing
    this.#queuedMessages.push(message);

    const rawMessage = message.getRaw();
    const isRunnable = rawMessage.role === 'assistant';

    // ensure the message is preloading
    if (isRunnable && !message.isPreloaded() && !message.isPreloading()) {
      message.preload({
        engine: this.#engine,
      });
    }

    let isFinished = false;
    const finishPending = () => {
      if (!isFinished) {
        isFinished = true;
        this.#pendingMessages--;
        // console.log('pending remove', {
        //   pendingMessages: this.#pendingMessages,
        // });
        this.dispatchEvent(new MessageEvent('pendingremove'));
      }
    };

    // wait for our turn to execute, minding aborts
    this.#pendingMessages++;
    this.dispatchEvent(new MessageEvent('pendingadd'));

    message.addEventListener('abort', e => {
      finishPending();
    }, {
      once: true,
    });

    await this.#queueManager.waitForTurn(async () => {
      this.#executingMessage = message;

      try {
        if (isRunnable) {
          await message.waitForLoad();
          if (this.#signal.aborted) return;
        }

        this.dispatchEvent(new MessageEvent('messageplay', {
          data: {
            message,
          },
        }));

        if (isRunnable) {
          await message.execute({
            engine: this.#engine,
          });
          if (this.#signal.aborted) return;
        }

        this.dispatchEvent(new MessageEvent('messagestop', {
          data: {
            message,
          },
        }));
      } finally {
        if (this.#executingMessage === message) {
          this.#executingMessage = null;
        }
      }
    });
    finishPending();
  }
  /* // queue message for preload
  async pushPreloadMessage(message) {
    this.#preloadedMessages.push(message);

    this.dispatchEvent(new MessageEvent('preloadpush'));
  }
  async pullPreloadMessageFn() {
    const syncFn = () => {
      const message = this.#preloadedMessages.shift();
      this.dispatchEvent(new MessageEvent('preloadpull'));
      return message;
    };

    if (this.#preloadedMessages.length > 0) {
      console.log('preload do not wait');
      return syncFn;
    } else {
      this.ensurePreloading();

      for (;;) {
        await new Promise((accept, reject) => {
          console.log('preload push wait 1');
          this.addEventListener('preloadpush', (e) => {
            console.log('preload push wait 2');
            // if (this.#preloadedMessages.length > 0) {
              accept();
            // } else {
            //   console.warn('did not have a message to preload');
            //   accept(null);
            // }
          }, {
            once: true,
          });
        });
        // if (syncFn) {
          return syncFn;
        // } else {
        //   continue;
        // }
      }
    }
  } */
  interrupt() {
    for (const message of this.#queuedMessages) {
      message.abort();
    }
  }
  interruptSoft() {
    for (const message of this.#queuedMessages) {
      if (message !== this.#executingMessage) {
        message.abort();
      }
    }
  }
  async next() {
    if (!this.isExecuting()) {
      await this.startExecute(true);
    } else {
      console.warn('already executing!');
    }
  }
  start(nextFn, {
    minBuffer = 3,
  } = {}) {
    const pred = () => this.#pendingMessages < minBuffer;

    (async () => {
      while (!this.#signal.aborted) {
        if (pred()) {
          console.log('next 1');
          await nextFn();
          console.log('next 2');
        } else {
          console.log('wait for pending remove 1');
          await new Promise((accept, reject) => {
            this.addEventListener('pendingremove', e => {
              // if (pred()) {
                accept();
              // }
            }, {
              once: true,
            });
          });
          console.log('wait for pending remove 2');
        }
      }
      console.log('message executer aborted');
    })();
  }
}
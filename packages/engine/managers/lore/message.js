import {
  Parser,
} from '../../ai-agent/utils/avatarml-stream-parser.js';
import {
  messageTypes,
  aiMessageTypes,

  messageToName,
  // messageToCharacterName,
  messageToCommand,
  // messageToCommandArgument,
  messageToReact,
  messageToText,
  messageToFullText,
  // messageToEmbeddingString,
} from './messages.jsx';
import {
  jsonParse,
} from '../../util.js';

export class Message extends EventTarget {
  constructor(opts) {
    super();

    const {
      raw,
      bindings = {},
    } = opts;
    if (!raw) {
      console.warn('missing raw messsage', opts);
      debugger;
      throw new Error('missing raw message');
    }
    if (!(/*raw?.id && */raw?.role && raw?.content)) {
      console.warn('invalid raw message', {
        opts,
        raw,
        // id: raw?.id,
        name: raw?.name,
        content: raw?.content,
      });
      debugger;
      throw new Error('invalid raw message');
    }

    this.#raw = raw;
    this.#bindings = bindings;

    this.#preloadPromise = null;
    this.#preloaded = false;
    this.#abortController = new AbortController();
  }
  #raw;
  #bindings;
  #preloadPromise;
  #preloaded;
  #abortController;

  getRaw() {
    return this.#raw;
  }
  // getId() {
  //   return this.#raw?.id;
  // }
  getRole() {
    return this.#raw?.role;
  }
  getContent() {
    return this.#raw?.content;
  }

  getParsed() {
    const array = jsonParse(this.#raw?.content);
    if (Array.isArray(array)) {
      const [
        name,
        command,
        args,
        message,
      ] = array;
      if (!name) {
        debugger;
      }
      return {
        name,
        command,
        args,
        message,
      };
    } else {
      return null;
    }
  }
  getBindings() {
    return this.#bindings;
  }

  getName() {
    return messageToName(this);
  }
  getCommand() {
    return messageToCommand(this);
  }

  toReact(opts) {
    return messageToReact(this, opts);
  }
  toText() {
    return messageToText(this);
  }
  toFullText() {
    return messageToFullText(this);
  }

  bindLore(lore) {
    // const raw = this.getRaw();
    // if (raw.role === 'assistant') {
      const parsed = this.getParsed();
      /* if (!parsed) {
        console.warn('missing parsed', {
          raw,
          parsed,
        });
        debugger;
      } */
      const {
        command,
      } = parsed;
      const messageType = messageTypes[command];
      if (messageType) {
        return messageType.bindLore(this, lore);
      } else {
        return false;
      }
    // } else {
    //   return false;
    // }
  }

  static fromRaw(raw) {
    return new Message({
      raw,
    });
  }
  static fromParsed({
    name = '',
    command = '',
    args = [],
    message = '',
  }) {
    // const role = aiMessageTypes[command] ? 'assistant' : 'user';
    const role = 'assistant';
    const parser = new Parser();
    const content = parser.format({
      name,
      command,
      args,
      message,
    });
    return new Message({
      raw: {
        role,
        content,
      },
    });
  }

  isPreloaded() {
    return this.#preloaded;
  }
  isPreloading() {
    return !!this.#preloadPromise;
  }

  async preload({
    engine,
  }) {
    if (!engine) {
      debugger;
      throw new Error('missing engine argument');
    }

    if (!this.#preloadPromise) {
      this.#preloadPromise = (async () => {
        const parsed = this.getParsed();
        const {
          command,
          message,
        } = parsed;
        const bindings = this.getBindings();
        const args = {
          message: this,
          parsed,
          bindings,
          engine,
        };

        const result = {};

        const preloadCommand = async command => {
          const messageType = messageTypes[command];
          if (messageType) {
            if (messageType.preload) {
              const preloadResult = await messageType.preload(args);
              result[command] = preloadResult;
            // } else {
              // return null;
            }
          } else {
            console.warn('unknown command to preload', command);
            // return null;
          }
        };

        const sayPromise = preloadCommand('SAY');
        const commandPromise = preloadCommand(command);
        await Promise.all([
          sayPromise,
          commandPromise,
        ]);
        this.#preloaded = true;
        return result;
      })();
    }
    return await this.#preloadPromise;
  }
  async waitForLoad() {
    if (this.#preloadPromise) {
      return await this.#preloadPromise;
    } else {
      throw new Error('not preloading');
    }
  }
  async execute({
    engine,
  }) {
    const parsed = this.getParsed();
    const {
      command,
      message,
    } = parsed;
    const bindings = this.getBindings();
    const {signal} = this.#abortController;
    const args = {
      message: this,
      parsed,
      bindings,
      engine,
      signal,
    };

    const promises = [];
    // always perform a SAY action if there is a message
    if (message) {
      const p = messageTypes.SAY.execute(args);
      promises.push(p);
    }
    // perform non-SAY actions
    if (command !== 'SAY') {
      const messageType = messageTypes[command];
      if (messageType) {
        const p = messageType.execute(args);
        promises.push(p);
      } else {
        console.warn('unknown command', command);
      }
    }

    await Promise.all(promises);
  }
  getSignal() {
    return this.#abortController.signal;
  }
  abort() {
    this.#abortController.abort();
  }
}
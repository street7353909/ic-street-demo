import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';

import {
  jsonParse,
  shuffle,
  // cleanLowercase,
  uniqueArray,
} from '../../util.js';
import {
  aiProxyHost,
} from '../../endpoints.js';
import {
  Message,
} from './message.js';
// import {
//   Lore,
// } from './lore.js';
import {
  aiMessageTypesArray,
  aiMessageTypes,

  characterMessageTypesArray,
  characterMessageTypes,

  objectMessageTypesArray,
  objectMessageTypes,
} from './messages.jsx';

import {
  createThread,
  // createThreadAndRun,
  pushThreadMessage,
  continueThread,
} from '../../agent-threads.js';
import {
  embed,
} from '../../embedding.js';
// import {
//   advanceModeExecuteFilterFns,
// } from '../story/story-manager.js';
// import {
//   QueueManager,
// } from '../../managers/queue/queue-manager.js'
// import {
//   createAssistant,
// } from '../../agents.js';
import { ReadWriteQueueManager } from '../queue/queue-manager.js';

//

TimeAgo.addDefaultLocale(en);
const timeAgo = new TimeAgo('en-US');
// const maxNumRetries = 5;
// const fakeMessages = [
//   `Hey, what's up?`,
//   `Wanna come over to my place?`,
//   `Have you heard the news?`,
// ];

//

export class Conversation extends EventTarget {
  constructor({
    // id,
    messages = [],
    loreManager,
    aiClient,
  }) {
    super();

    if (/*!id || */!loreManager || !aiClient) {
      console.warn('missing arguments', {
        // id,
        loreManager,
        aiClient,
      });
    }

    // this.#id = id;
    this.#messages = messages;
    this.#loreManager = loreManager;
    this.#aiClient = aiClient;

    this.#threadQueue = new ReadWriteQueueManager();
    this.#abortController = new AbortController();

    // this.#listen();
  }
  // #id;
  #messages;
  #loreManager;
  #aiClient;
  #threadQueue;
  #abortController;

  /* #listen() {
    const lore = this.#loreManager.getLore();
    lore.addEventListener('message', async e => {
      const {message} = e.data;
      this.addRawMessage(message, {
        errorFeedback: false,
      });
    });
  } */

  static #getInstructions() {
    return `\
You are an AvatarML script writer for a TV show.
AvatarML is a JSONL-based machine parseable language for script writing.
AvatarML is rendered using a game engine to produce a show for an audience of users.
I will tell you about changes to the scene. In response, you will write the AvatarML script.
You must reply only in AvatarML format. Do not reply with anything else.

# AvatarML specification
Each line of AvatarML is an array of JSON values.
Lines are separated by a newline character.
Each line specifies an action performed by an actor in the scene.
Actors in the scene can perform different actions.

The general format of AvatarML is:
\`\`\`
[actor_name_1,action_name_1,[arg1,arg2,...],dialogue_message_1]
[actor_name_2,action_name_2,[arg1,arg2,...],dialogue_message_2]
\`\`\`

Each line is a single JSON array WITHOUT additional whitespace or formatting, i.e. /^\[.+\]$/

AvatarML scenes contain actors which perform action commands.
Actors include characters, objects (props), and other entities.
Here are the allowed commands for each type of actor. Note that actors have different commands available.

${characterMessageTypesArray.length > 0 ? `\
## Character actor actions API

${characterMessageTypesArray.map(type => `\
### ${type.name}
${type.description}
Ex:
\`\`\`
${type.example()}
\`\`\`
${(() => {
  if (type.args) {
    const args = type.args();
    if (args) {
      return args.map((args2, i) => `\
arg${i + 1} = {${args2.map(a => JSON.stringify(a)).join(',')}}
`).join('');
    } else {
      return '';
    }
  } else {
    return '';
  }
})()}\
`).join('\n')}\
` : ''}\

${objectMessageTypesArray.length > 0 ? `\
## Object actor actions API

${objectMessageTypesArray.map(type => `\
### ${type.name}
${type.description}
Ex:
\`\`\`
${type.example()}
\`\`\`
${(() => {
  if (type.args) {
    const args = type.args();
    if (args) {
      return args.map((args2, i) => `\
arg${i + 1} = {${args2.map(a => JSON.stringify(a)).join(',')}}
`).join('');
    } else {
      return '';
    }
  } else {
    return '';
  }
})()}\
`).join('\n')}\
` : ''}\

## Additional requirements
Chracters must follow their personality. They should demonstrate that they can have an interesting and accurate conversation on any topic.
Characters in the script should not repeat themselves. If they have already said something, they should say something new instead.
Do not add any additional notes or commentary.
`;
  }
  /* static async list({
    name,
    supabaseClient,
  }) {
    const result = await supabaseClient.supabase
      .from('conversations')
      .select('*')
      .eq('name', name)
      .order('created_at', {
        ascending: true,
      });
    console.log('got conversation list result', result);
    debugger;
    return result.data;
  } */
  /* static async create({
    name,
    instructions = this.#getDefaultInstructions(),
    loreManager,
    supabaseClient,
  }) {
    if (!name || !loreManager || !supabaseClient) {
      console.warn('missing arguments', {
        name,
        loreManager,
        supabaseClient,
      });
      debugger;
      throw new Error('missing arguments');
    }

    const result = await supabaseClient.supabase
      .from('conversations')
      .upsert({
        name,
        // instructions,
      });
    console.log('got conversation create result', result);
    debugger;

    return new Conversation2({
      id,
      instructions,
      loreManager,
    });
  }
  static load({
    id,
    loreManager,
  }) {
    
  } */

  getMessages() {
    return this.#messages;
  }
  getSignal() {
    return this.#abortController.signal;
  }

  /* async startInspectPlayerMessage({
    localPlayerId,
    playerId,
  }, opts) {
    const lore = this.#loreManager.getLore();
    const localPlayerSpec = lore.getActorById(localPlayerId);
    const remotePlayerSpec = lore.getActorById(playerId);

    const messageRaw = {
      role: 'user',
      content: `\
${localPlayerSpec.spec.name} starts a conversation with ${remotePlayerSpec.spec.name}
Make the response something that is likely to lead to an interesting conversation.
`,
    };
    const triggerMessage = Message.fromRaw(messageRaw, this)
    const completionMessage = await this.#getCompletion([], triggerMessage.getRaw(), opts);
    if (completionMessage) {
      // this.startMessage(completionMessage);
      return completionMessage;
    } else {
      console.warn('failed to get completion message');
    }
  }
  async getInspectTargetMessage({
    localPlayerId,
    targetActor,
  }, opts) {
    const lore = this.#loreManager.getLore();
    const localPlayerSpec = lore.getActorById(localPlayerId);

    const messageRaw = {
      role: 'user',
      content: `\
${localPlayerSpec.spec.name} comments on ${targetActor.spec.name}
`,
    };
    const triggerMessage = Message.fromRaw(messageRaw, this);
    const completionMessage = await this.#getCompletion([], triggerMessage.getRaw(), opts);
    if (completionMessage) {
      // this.startMessage(completionMessage);
      return completionMessage;
    } else {
      console.warn('failed to get completion message');
    }
  } */
  async inspectSelf() {
    const loreManager = this.#loreManager;
    const {
      playersManager,
    } = loreManager;
    const lore = loreManager.getLore();
    const localPlayer = playersManager.getLocalPlayer();
    const localPlayerSpec = lore.getActorById(localPlayer.playerId);

    const messageRaw = {
      role: 'user',
      content: `\
${localPlayerSpec.spec.name} thinks to themselves a new topic in line with their personality.
`,
    };
    const completionMessage = await this.#getCompletion(messageRaw);
    if (completionMessage) {
      return completionMessage;
    } else {
      console.warn('failed to get completion message');
    }
  }
  async inspectTarget(sourceActor, targetActor) {
    // const loreManager = this.#loreManager;
    // const {
    //   playersManager,
    // } = loreManager;
    // const lore = loreManager.getLore();
    // const localPlayer = playersManager.getLocalPlayer();
    // const localPlayerSpec = lore.getActorById(localPlayer.playerId);

    const messageRaw = {
      role: 'user',
      content: `\
${sourceActor.spec.name} interacts with ${targetActor.spec.name}
`,
    };
    const completionMessage = await this.#getCompletion(messageRaw);
    if (completionMessage) {
      return completionMessage;
    } else {
      console.warn('failed to get completion message');
    }
  }
  async next() {
    // const newMessages = await continueThread({
    //   // threadId: this.#id,
    //   messages: this.#messages,
    //   queue: this.#threadQueue,
    // });
    // for (const message of newMessages) {
    //   this.addRawMessage(message);
    // }
    // return newMessages;

    const newMessage = await this.#getCompletion();
    this.addRawMessage(newMessage);
  }

  /* #makeExampleOpts() {
    const lore = this.#loreManager.getLore();
    const actors = lore.getActors();

    //

    const characterActorsArray = actors.filter(actor => actor.type === 'character');
    const objectActorsArray = actors.filter(actor => actor.type === 'object');

    //

    let characterNames = characterActorsArray.map((actor, i) => actor.spec.name);
    let objectNames = objectActorsArray.map((actor, i) => actor.spec.name);
    let targetNames = [
      ...characterNames,
      ...objectNames,
    ];
    let messages = fakeMessages.slice();
    characterNames = shuffle(characterNames);
    objectNames = shuffle(objectNames);
    targetNames = shuffle(targetNames);
    messages = shuffle(messages);
    return {
      characterNames,
      objectNames,
      targetNames,
      messages,
    };
  } */
  /* #getContextMessages(opts = {}) {
    const lore = this.#loreManager.getLore();
    const {
      actors = lore.getActors(),
      loreItems = lore.getLoreItems(),
      setting = lore.getSetting(),
      director = lore.getDirector(),
      error,
    } = opts;

    //

    const characterActorsArray = actors.filter(actor => actor.type === 'character');
    const objectActorsArray = actors.filter(actor => actor.type === 'object');
    console.log('object actors', {actors, objectActorsArray});

    //

const systemMessageChunks = [

// system message

`\
AvatarML is a JSON-based language for scripting an AI generated TV shows.
AvatarML is rendered using a game engine to produce the stream for an audience of users. User can interact with the show via chat to steer its direction. I will enter chat messages as they are received.
Your job is to write AvatarML scripts.

# AvatarML specification
Each line of AvatarML is a comma separated list of JSON values.
Lines are separated by a newline character.
Each line specifies an action performed by an actor in the scene.
Actors in the scene can perform different actions.

The general format is:
_actor_name,_action_name,[arg1,arg2,...],_dialogue_message

`,

// actors

`\
## Actors
Actors in the scene include characters, objects (props), and others entities.
ONLY the below actors can appear in the AvatarML script. Users in the chat are NOT actors in the scene.

`,

// character actors

characterActorsArray.length > 0 ? `\
## Character actors

${characterActorsArray.map(({spec: {name = '', description = ''} = {}, memories = []}, i) => `\
Character Name: ${JSON.stringify(name)} 
${description ? `\
Bio: ${description}
` : ''}\
${memories.length > 0 ? `\
Memories:
${memories.map(s => `\
>${s.text} [${s.timestamp}]
`).join('')}\
` : ''}\
`).join('\n')}\

${characterMessageTypesArray.length > 0 ? `\
## Character actor actions API

${characterMessageTypesArray.map(type => `\
### ${type.name}
${type.description}
Ex:
\`\`\`
${type.example(this.#makeExampleOpts())}
\`\`\`
${(() => {
  if (type.args) {
    const args = type.args(this.#makeExampleOpts());
    if (args) {
      return args.map((args2, i) => `\
arg${i + 1} = {${args2.map(a => JSON.stringify(a)).join(',')}}
`).join('');
    } else {
      return '';
    }
  } else {
    return '';
  }
})()}\
`).join('\n')}\
` : ''}\

` : null,

// object actors

objectActorsArray.length > 0 ? `\
## Object actors
These are objects that can be interacted with in the scene.

${objectActorsArray.map(({spec: {name = '', description = ''} = {}}, i) => `\
Object Name: ${JSON.stringify(name)}
${description ? `\
Description: ${description}
` : ''}\
`).join('\n')}\

${objectMessageTypesArray.length > 0 ? `\
## Object actor actions API

${objectMessageTypesArray.map(type => `\
### ${type.name}
${type.description}
Ex:
\`\`\`
${type.example(this.#makeExampleOpts())}
\`\`\`
${(() => {
  if (type.args) {
    const args = type.args(this.#makeExampleOpts());
    if (args) {
      return args.map((args2, i) => `\
arg${i + 1} = {${args2.map(a => JSON.stringify(a)).join(',')}}
`).join('');
    } else {
      return '';
    }
  } else {
    return '';
  }
})()}\
`).join('\n')}\
` : ''}\
` : null,

// setting

setting ? `\
## Setting

${setting}

` : null,

// director

director ? `\
## Director's instructions
${director}

` : null,

// lore

loreItems.length > 0 ? `\
## Lore
Use this lore when worldbuilding:
${loreItems.join('\n')}

` : null,

// additional instructions

`\
# Important notes
- The user will prompt you to generate the next line of AvatarML. Treat the user's messages as direction for the script.
- However, you MUST ALWAYS continue in AvatarML format, as specified above. Do not respond with null.
- DO NOT REPEAT lines or memories that characters have already said.
- If you cannot generate a response, make the character say something witty instead. Your response must adhere to AvatarML format.
`,

].filter(chunk => chunk !== null);
    const systemMessage = systemMessageChunks.join('');
    
    const messages = [
      {
        role: 'system',
        content: systemMessage,
      },
    ]
    .map(raw => Message.fromRaw(raw, this))
    // .concat(this.#messages);
    return messages;
  } */

  //

  /* async #getApiCompletion(messages) {
    const {
      // modelType,
      modelName,
    } = this.aiClient;

    const response = await fetch(`https://${aiProxyHost}/api/ai/chat/completions`, {
      method: 'POST',

      headers: {
        'Content-Type': 'application/json',
        'OpenAI-Beta': 'assistants=v1',
      },

      body: JSON.stringify({
        model: modelName,
        messages,
        stop: ['\n'],
        // response_format: 'json_object',
      }),
    });

    const j = await response.json();
    const message = j.choices[0].message.content;
    return message;
  } */
  async #getActorsWithMemories() {
    const lore = this.#loreManager.getLore();
    const actors = lore.getActors();
    const characterActors = actors.filter(a => a.type === 'character');
    const objectActors = actors.filter(a => a.type === 'object');

    const cleanMemories = memories => {
      // remove empty memories
      memories = memories.map(memory => {
        return {
          // text: Message.fromParsed(memory).toFullText(),
          // text: Message.fromParsed(memory).toText(),
          text: memory.message,
          // created_at: memory.created_at,
          timestamp: timeAgo.format(new Date(memory.created_at)),
        };
      });

      // remove duplicate memories
      memories = uniqueArray(memories, m => m.text);

      // filter out memories with the exact same content as current messages
      const messageContentsSet = new Set();
      for (const m of this.#messages) {
        const parsed = m.getParsed();
        const {message} = parsed;
        messageContentsSet.add(message);
      }
      memories = memories.filter(memory => !messageContentsSet.has(memory.text));

      // return result
      return memories;
    };

    if (this.#messages.length > 0) {
      const embeddingString = this.#messages.map(m => m.getRaw().content).join('\n');
      const embedding = await embed(embeddingString);
      const promises = characterActors.map(async actor => {
        const {
          // id,
          // type,
          spec,
          // object,
        } = actor;
        // console.log('get actor spec name', actor);
        // debugger;
        const name = spec?.name ?? null;
        let memories = await this.#loreManager.characterMemoriesManager.searchMemoriesByEmbedding(embedding, {
          name,
        });
        memories = cleanMemories(memories);
        console.log('actor memories 1', {
          messages: this.#messages.slice(),
          actor,
          memories,
        });
        return {
          ...actor,
          memories,
        };
      });
      const characterActorsWithMemories = await Promise.all(promises);
      return [
        ...characterActorsWithMemories,
        ...objectActors,
      ];
    } else {
      const promises = characterActors.map(async actor => {
        const {
          // id,
          // type,
          spec,
          // object,
        } = actor;
        // console.log('get actor spec name', actor);
        // debugger;
        const name = spec?.name ?? null;
        let memories = await this.#loreManager.characterMemoriesManager.getMemoriesByName(name);
        memories = cleanMemories(memories);
        console.log('actor memories 0', {
          messages: this.#messages.slice(),
          actor,
          memories,
        });
        return {
          ...actor,
          memories,
        };
      });
      const characterActorsWithMemories = await Promise.all(promises);
      return [
        ...characterActorsWithMemories,
        ...objectActors,
      ];
    }
  }
  #getContextMessages() {
    const messages = [];

    //

    // collect user/assistant messages
    const lore = this.#loreManager.getLore();
    const userMessages = [];
    const assistantMessages = [];
    for (let i = 0; i < this.#messages.length; i++) {
      const message = this.#messages[i];
      const role = message.getRole();
      if (role === 'user') {
        userMessages.push(message);
      } else if (role === 'assistant') {
        assistantMessages.push(message);
      } else {
        console.warn('invalid message role', message);
      }
    }

    //

    const instructionMessage = {
      role: 'system',
      content: Conversation.#getInstructions(),
    };
    messages.push(instructionMessage);

    //

    let initialUserContent = `\
Here is a description of the scene:
${lore.getContent()}

`;
    if (userMessages.length > 0) {
      const s = userMessages.map(m => m.getContent()).join('\n');
      initialUserContent += `\
Here are some messages from the audience chat. This is NOT part of the show, but you can use this to help you write the script.
${s}

`;
    }
    const initialUserMessage = {
      role: 'user',
      content: initialUserContent,
    };
    messages.push(initialUserMessage);

    //

    if (assistantMessages.length > 0) {
      const s = assistantMessages.map(m => m.getContent()).join('\n');
      const initialAssistantMessage = {
        role: 'assistant',
        content: s,
      };
      messages.push(initialAssistantMessage);
    }

    //

    const characterActors = lore.getActors().filter(a => a.type === 'character');
    const finalUserMessage = {
      role: 'user',
      content: `\
${assistantMessages.length > 0 ? 'Continue' : 'Start'} the show.

The only characters who may act are:
${characterActors.map(actor => JSON.stringify(actor.spec.name)).join('\n')}
`
    };
    messages.push(finalUserMessage);

    // collect all of the existing messages into a request/response format
    /* let index = 0;
    while (index < this.#messages.length) {
      const nextMessage = this.#messages[index];
      switch (nextMessage.getRole()) {
        case 'user': {
          // collect the upcoming user messages
          const userMessages = [];
          while (index < this.#messages.length && this.#messages[index].getRole() === 'user') {
            userMessages.push(this.#messages[index]);
            index++;
          }

          // collect into a single message
          const newMessage = {
            role: 'user',
            content: userMessages.map(m => m.getContent()).join('\n'),
          };
          messages.push(newMessage);

          break;
        }
        case 'assistant': {
          // collect the upcoming assistant messages
          const assistantMessages = [];
          while (index < this.#messages.length && this.#messages[index].getRole() === 'assistant') {
            assistantMessages.push(this.#messages[index]);
            index++;
          }

          // collect into a single message
          const newMessage = {
            role: 'assistant',
            content: assistantMessages.map(m => m.getContent()).join('\n'),
          };
          messages.push(newMessage);

          break;
        }
        default: {
          console.warn('invalid message role', nextMessage);
          debugger;
          break;
        }
      }
    } */

    return messages;
  }
  async #getCompletion(nextPromptMessage) {
    const {
      modelName,
    } = this.#aiClient;

    //

    const messages = [
      ...this.#getContextMessages(),
    ];
    if (nextPromptMessage) {
      if (typeof nextPromptMessage === 'string') {
        nextPromptMessage = {
          role: 'user',
          content: nextPromptMessage,
        };
      }
      messages.push(nextPromptMessage);
    }
    // console.log('get completion messages', {
    //   nextPromptMessage,
    //   messages,
    // });
    // debugger;

    const res = await fetch(`https://${aiProxyHost}/api/ai/chat/completions`, {
      method: 'POST',

      headers: {
        'Content-Type': 'application/json',
        // 'OpenAI-Beta': 'assistants=v1',
      },

      body: JSON.stringify({
        model: modelName,
        messages,
        
        // stop: ['\n'],
        
        // response_format: {
        //   type: 'json_object',
        // },
      }),
    });
    if (res.ok) {
      const j = await res.json();
      // console.log('got response', j);
      // debugger;
      const message = j.choices[0].message;
      return message;
    } else {
      throw new Error('bad response status code: ' + res.status);
    }
  }

  // add raw messages, splitting as necessary first
  addRawMessage(message) {
    const newMessages = [];
    if (message.role === 'assistant') {
      // latch the content
      const content = message.content;
      if (typeof content !== 'string') {
        console.warn('invalid lines string', {content});
        debugger;
        throw new Error('invalid lines string');
      }

      // clean the content
      const cleanContent = content
        .replace(/^\s*`*\s*/, '')
        .replace(/\s*`*\s*$/, '');

      // message needs to be split if it has multiple lines
      const linesString = cleanContent;
      const lines = linesString.split(/\n+\,?/);
      for (const line of lines) {
        if (line) {
          const newMessage = structuredClone(message);
          newMessage.content = line;
          newMessages.push(newMessage);
        }
      }
    } else {
      newMessages.push(message);
    }

    console.log('split message', {message, newMessages, length: newMessages.length});
    this.#addRawMessagesInternal(newMessages);
  }
  #addRawMessagesInternal(rawMessages) {
    for (const rawMessage of rawMessages) {
      // console.log('message', rawMessage, new Error().stack);
      const message = Message.fromRaw(rawMessage);
      this.#messages.push(message);
      this.dispatchEvent(new MessageEvent('message', {
        data: {
          message,
        },
      }));

      if (rawMessage.role === 'assistant') {
        const parsed = message.getParsed();
        if (parsed) {
          const lore = this.#loreManager.getLore();
          if (message.bindLore(lore)) {
            this.dispatchEvent(new MessageEvent('parsedmessage', {
              data: {
                parsedMessage: message,
              },
            }));
          } else {
            console.warn('could not bind lore to message', {
              message,
            });
            // debugger;
            break;
          }
        } else {
          console.warn('could not parse AvatarML message', {
            rawMessage,
          });
          // debugger;
          break;
        }
      }
    }
  }
  close() {
    this.#abortController.abort();
    this.dispatchEvent(new MessageEvent('close'));
  }
}
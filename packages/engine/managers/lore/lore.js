import {
  Actor,
} from './actor.js';

export class Lore extends EventTarget {
  #actors;
  #settings;
  #directors;
  #loreItems;

  constructor({
    actors = new Map(),
    settings = [],
    directors = [],
    loreItems = [],
  } = {}) {
    super();

    this.#actors = structuredClone(actors);
    this.#settings = structuredClone(settings);
    this.#directors = structuredClone(directors);
    this.#loreItems = structuredClone(loreItems);
  }
  clone() {
    return new Lore({
      actors: this.#actors,
      settings: this.#settings,
      directors: this.#directors,
      loreItems: this.#loreItems,
    });
  }

  static #getActorMessage(actor) {
    if (actor.type === 'character') {
      return {
        role: 'user',
        content: `\
Character actor added:
Name: ${JSON.stringify(actor.spec.name)}
${actor.spec.description ? `Description: ${actor.spec.description}` : ''}
${actor.spec.personality ? `Personality: ${actor.spec.personality}` : ''}
`,
      };
    } else if (actor.type === 'object') {
      return {
        role: 'user',
        content: `\
Object actor added:
Name: ${JSON.stringify(actor.spec.name)}
${actor.spec.description ? `Description: ${actor.spec.description}` : ''}
`,
      };
    } else {
      throw new Error('invalid actor type');
    }
  }
  static #getSettingMessage(setting) {
    return {
      role: 'user',
      content: `\
Setting changed:
${setting}
`,
    };
  }
  static #getDirectorMessage(director) {
    console.log('get director message', setting);
    debugger;
    return {
    };
  }
  static #getLoreItemMessage(loreItem) {
    return {
      role: 'user',
      content: `\
Lore added:
${loreItem}
`,
    };
  }
  getContent() {
    let result = '';

    const characterActors = Array.from(this.#actors.values()).filter(a => a.type === 'character');
    for (const actor of characterActors) {
      result += `\
Character name: ${JSON.stringify(actor.spec.name)}
${actor.spec.description ? `Description: ${actor.spec.description}` : ''}\
${actor.spec.personality ? `Personality: ${actor.spec.personality}` : ''}\
${actor.spec.first_mes ? `First message: ${actor.spec.first_mes}` : ''}\
`;
    }

    const propActors = Array.from(this.#actors.values()).filter(a => a.type === 'object');
    for (const actor of propActors) {
      result += `\
Prop name: ${JSON.stringify(actor.spec.name)}
${actor.spec.description ? `Description: ${actor.spec.description}` : ''}
`;
    }

    return result;
  }
  /* getInitialLoreMessages() {
    const messages = [];

    for (const actor of this.#actors.values()) {
      const m = Lore.#getActorMessage(actor);
      messages.push(m);
    }
    for (const setting of this.#settings) {
      const m = Lore.#getSettingMessage(setting);
      messages.push(m);
    }
    for (const director of this.#directors) {
      const m = Lore.#getDirectorMessage(director);
      messages.push(m);
    }
    for (const loreItem of this.#loreItems) {
      const m = Lore.#getLoreItemMessage(loreItem);
      messages.push(m);
    }

    return messages;
  } */

  /* toJSON() {
    return {
      actors: Array.from(this.#actors.values())
        .map(actor => actor.toJSON()),
      settings: this.#settings,
      loreItems: this.#loreItems,
    };
  } */

  //

  getActorById(actorId) {
    return this.#actors.get(actorId) ?? null;
  }
  getActorByName(name) {
    const actors = this.getActors();
    const actor = actors.find(actor => actor.spec.name === name);
    if (actor) {
      return actor;
    } else {
      return null;
    }
  }
  getActors() {
    return Array.from(this.#actors.values());
  }
  addActor(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('invalid actor');
    }

    this.#actors.set(actor.id, actor);

    this.dispatchEvent(new MessageEvent('update'));
  }
  removeActor(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('invalid actor');
    }

    if (this.#actors.has(actor.id)) {
      this.#actors.delete(actor.id);
    } else {
      throw new Error('actor not found');
    }

    // const index = this.#loreItems.indexOf(loreItem);
    // if (index !== -1) {
    //   this.#loreItems.splice(index, 1);
    // } else {
    //   console.warn('lore item not found', loreItem);
    // }

    this.dispatchEvent(new MessageEvent('update'));
  }

  //

  getLoreItems() {
    return this.#loreItems;
  }
  addLoreItem(loreItem) {
    this.#loreItems.push(loreItem);

    this.dispatchEvent(new MessageEvent('update'));
  }
  removeLoreItem(loreItem) {
    const index = this.#loreItems.indexOf(loreItem);
    if (index !== -1) {
      this.#loreItems.splice(index, 1);
    } else {
      console.warn('lore item not found', loreItem);
    }

    this.dispatchEvent(new MessageEvent('update'));
  }

  //

  addSetting(setting) {
    this.#settings.push(setting);
  }
  getSetting() {
    return this.#settings[this.#settings.length - 1] ?? null;
  }
  removeSetting(setting) {
    const index = this.#settings.indexOf(setting);
    if (index !== -1) {
      this.#settings.splice(index, 1);
    } else {
      console.warn('setting not found', setting);
    }
  }

  //

  addDirector(director) {
    this.#directors.push(director);
  }
  getDirector() {
    return this.#directors[this.#directors.length - 1] ?? null;
  }
  removeDirector(director) {
    const index = this.#directors.indexOf(director);
    if (index !== -1) {
      this.#directors.splice(index, 1);
    } else {
      console.warn('director not found', director);
    }
  }
}
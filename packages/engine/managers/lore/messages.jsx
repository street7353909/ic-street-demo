import * as THREE from 'three';
import React from 'react';
import classnames from 'classnames';
import {
  emotes,
} from '../emote/emotes.js';
import {
  emotions,
} from '../emote/emotions.js';
// import {
//   Message,
// } from './message.js';
import {
  storyCameraShots,
  storyCameraFrames,
  storyCameraAngles,
} from '../story/story-manager.js';
import {
  AutoVoiceEndpoint,
  VoiceEndpointVoicer,
} from '../../audio/voice-output/voice-endpoint-voicer.js';
import {
  AudioInjectWorkletNode,
} from '../../audio/inject-worklet-node.js';

//

//

const bindCharacterLore = (m, lore) => {
  const parsed = m.getParsed();
  const bindings = m.getBindings();
  const actor = lore.getActorByName(parsed.name);
  if (actor?.type === 'character') {
    bindings.player = actor.object;
  } else {
    return false;
  }
  return true;
};
const bindCharacterTargetLore = (m, lore) => {
  const parsed = m.getParsed();
  const bindings = m.getBindings();
  const actor = lore.getActorByName(parsed.name);
  if (actor?.type === 'character') {
    bindings.player = actor.object;
  } else {
    return false;
  }

  const actor2 = lore.getActorByName(parsed.args[0]);
  if (actor2) {
    bindings.target = actor2.object;
  } else {
    return false;
  }

  return true;
};

// character message types are actions that can be performed by characters (.e.g say)
export const characterMessageTypesArray = [
  {
    name: 'SAY',
    // format: `:{character_name}::SPEAK:::{chat_message}`,
    description: 'Character says something. The dialogue should not be repetitive. If the conversation is dragging on, change the topic.',
    example: () => {
      return JSON.stringify([
        'character_name_1',
        'SAY',
        [],
        'The dialogue text goes here. Try to aim for at least two sentences of content, so the conversation has enough entropy to be interesting.',
      ]) + '\n' + JSON.stringify([
        'character_name_2',
        'SAY',
        [],
        'More dialogue goes here. Note that you can output multiple related lines at a time to keep the flow of dialogue natural.',
      ]);
    },
    bindLore: bindCharacterLore,
    render: ({
      styles,
      key,
      name,
      message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.say,
      )} key={key}>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}>: {message}</span>
      </div>
    ),
    text: ({
      name,
      message,
    }) => `${name}: ${message}`,
    preload: ({
      message,
      parsed,
      bindings,
      engine,
    }) => {
      const {
        player,
      } = bindings;
      const signal = message.getSignal();
      const stream = player?.voicer.getStream(parsed.message, {
        signal,
      });
      return stream;
    },
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        player,
      } = bindings;
      const {
        voiceQueueManager,
      } = engine;
      const {
        'SAY': stream,
      } = await message.waitForLoad();

      // console.log('voice queue 1');
      await voiceQueueManager.waitForTurn(async () => {
        const playStart = () => {
          message.dispatchEvent(new MessageEvent('playStart', {
            data: {
              player,
            },
          }));
        };
        const playEnd = () => {
          message.dispatchEvent(new MessageEvent('playEnd', {
            data: {
              player,
            },
          }));
        };

        if (stream) {
          await player.playAudioStream(stream, {
            onStart: () => {
              // console.log('voice queue start');
              playStart();
            },
            onEnd: () => {
              // console.log('voice queue end');
              playEnd();
            },
            signal,
          });
        } else {
          // console.log('fake play start');
          playStart();

          await new Promise((accept, reject) => {
            const timeout = setTimeout(() => {
              accept();
              cleanup();
            }, 50 * parsed.message.length);

            const abort = () => {
              clearTimeout(timeout);
              accept();
              cleanup();
            };
            signal.addEventListener('abort', abort);
            const cleanup = () => {
              signal.removeEventListener('abort', abort);
            };
          });

          // console.log('fake play end');
          playEnd();
        }
      });
      // console.log('voice queue 2');
    },
  },
  {
    name: 'EMOTE',
    // format: `:{character_name}::EMOTE={emote}:::{chat_message}`,
    description: 'Perform an emote action and say something. Not to be confused with EMOTION, which has different arguments.',
    example: () => {
      return JSON.stringify([
        'character_name',
        'EMOTE',
        ['emote_name'],
        'Optional chat message goes here',
      ]);
    },
    args: () => [emotes],
    bindLore: bindCharacterLore,
    render: ({
      styles,
      key,
      name,
      args,
      // message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.emote,
      )} key={key}>
        <span className={styles.text}>*</span>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}> emotes </span>
        <span className={styles.value}>{args[0]}</span>
        <span className={styles.text}>*</span>
      </div>
    ),
    text: ({
      name,
      args,
    }) => `${name} emotes ${args[0]}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        args,
      } = parsed;
      const {
        player,
      } = bindings;

      const emote = args[0];
      if (emote) {
        engine.emoteManager.triggerEmote(emote, player);
      } else {
        console.warn('invalid emote', {
          emote,
        });
      }
    },
  },
  {
    name: 'EMOTION',
    // format: `:{character_name}::EMOTION={emotion}:::{chat_message}`,
    description: 'Change the character\'s current mood and say something. Not to be confused with EMOTE, which has different argum',
    example: () => {
      return JSON.stringify([
        'character_name',
        'EMOTION',
        ['emotion_name'],
        'Optional chat message goes here',
      ]);
    },
    args: () => [emotions],
    bindLore: bindCharacterLore,
    render: ({
      styles,
      key,
      name,
      args,
      // message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.emote,
      )} key={key}>
        <span className={styles.text}>*</span>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}> change emotion to </span>
        <span className={styles.value}>{args[0]}</span>
        <span className={styles.text}>*</span>
      </div>
    ),
    text: ({
      name,
      args,
    }) => `${name} change emotion to ${args[0]}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        args,
      } = parsed;
      const {
        player,
      } = bindings;

      const emotion = args[0];
      if (emotions.includes(emotion)) {
        const faceposeAction = player.actionManager.addAction({
          type: 'facepose',
          emotion,
          value: 1,
        });

        setTimeout(() => {
          if (player.actionManager.hasActionId(faceposeAction.actionId)) {
            player.actionManager.removeAction(faceposeAction);
          }
        }, 2000);
      } else {
        console.warn('invalid emotion', {
          emotion,
          emotions,
        });
      }
    },
  },
  {
    name: 'FACETOWARD',
    // format: `:{character_name}::FACETOWARD={target}:::{chat_message}`,
    description: 'Turn to face a target and optionally say something',
    example: () => {
      return JSON.stringify([
        "$character_name",
        'FACETOWARD',
        ["$character_or_object"],
        "Optional chat message",
      ]);
    },
    bindLore: bindCharacterTargetLore,
    render: ({
      styles,
      key,
      name,
      args,
      // message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.emote,
      )} key={key}>
        <span className={styles.text}>*</span>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}> faces toward </span>
        <span className={styles.value}>{args[0]}</span>
        <span className={styles.text}>*</span>
      </div>
    ),
    text: ({
      name,
      args,
    }) => `${name} faces toward ${args[0]}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        player,
        target,
      } = bindings;

      const targetPosition = target.position;
      const timestamp = performance.now();
      player.characterBehavior.addFaceTowardAction(
        targetPosition,
        timestamp,
      );
    },
  },
  {
    name: 'MOVETO',
    // format: `:{character_name}::MOVETO={target}:::{chat_message}`,
    description: 'Move to a target object and optionally say something',
    example: () => {
      return JSON.stringify([
        'character_name',
        'MOVETO',
        ['character_or_object'],
        'Optional chat message goes here',
      ]);
    },
    bindLore: bindCharacterTargetLore,
    render: ({
      styles,
      key,
      name,
      args,
      // message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.emote,
      )} key={key}>
        <span className={styles.text}>*</span>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}> moves to </span>
        <span className={styles.value}>{args[0]}</span>
        <span className={styles.text}>*</span>
      </div>
    ),
    text: ({
      name,
      args,
    }) => `${name} moves to ${args[0]}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        player,
        target,
      } = bindings;
      
      // console.log('move to', target);
      // if (target) {
        // console.log('got', target);
        const targetPosition = target.position;
        const timestamp = performance.now();
        const bbox2 = target.physicsMesh ?
          new THREE.Box3()
            .setFromBufferAttribute(target.physicsMesh.geometry.attributes.position)
            .applyMatrix4(target.physicsMesh.matrixWorld)
        :
          null;

        /* const {scene} = this.engineRenderer;

        const hitMap = getHitMap({
          localPlayer: player,
          playersManager: this.playersManager,
          npcManager: this.npcManager,
        });

        const mesh = makeHitMesh(hitMap);
        scene.add(mesh);
        mesh.updateMatrixWorld();

        const floorPosition = player.position.clone()
          .add(new THREE.Vector3(0, -player.avatar.height, 0));
        // console.log('got target object', targetObject);
        
        targetObject.physicsMesh.visible = true;
        scene.add(targetObject);
        targetObject.updateMatrixWorld();

        const size2 = bbox2.getSize(new THREE.Vector3());
        // mesh for the bounding box
        const bboxMesh2 = new THREE.Mesh(
          new THREE.BoxBufferGeometry(1, 1, 1),
          new THREE.MeshPhongMaterial({
            color: 0xff0000,
          }),
        );
        bboxMesh2.position.set(
          (bbox2.min.x + bbox2.max.x)/2,
          (bbox2.min.y + bbox2.max.y)/2,
          (bbox2.min.z + bbox2.max.z)/2,
        );
        bboxMesh2.scale.copy(size2);
        scene.add(bboxMesh2);
        bboxMesh2.updateMatrixWorld();

        const line = getLine(
          hitMap,
          floorPosition,
          targetPosition,
        );

        if (line.length > 0) {
          const geometry = makePathLineGeometry(line);
          const map = new THREE.TextureLoader()
            .load(`/images/arrowtail.png`);
          const material = new THREE.MeshBasicMaterial({
            map,
            side: THREE.DoubleSide,
          });
          const mesh = new THREE.Mesh(geometry, material);
          mesh.frustumCulled = false;
          mesh.visible = geometry.attributes.position.array.length > 0;
          scene.add(mesh);
          mesh.updateMatrixWorld();
        } */

        player.characterBehavior.addWaypointAction(
          targetPosition,
          timestamp,
          {
            boundingBox: bbox2,
          },
        );
      // }
    },
  },
  {
    name: 'LOOKAT',
    // format: `:{character_name}::LOOKAT={target}:::{chat_message}`,
    description: 'Look at a target object and optionally say something',
    example: () => {
      return JSON.stringify([
        'character_name',
        'LOOKAT',
        ['character_or_object'],
        'Optional chat message goes here',
      ]);
    },
    bindLore: bindCharacterTargetLore,
    render: ({
      styles,
      key,
      name,
      args,
      // message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        styles.emote,
      )} key={key}>
        <span className={styles.text}>*</span>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}> looks at </span>
        <span className={styles.value}>{args[0]}</span>
        <span className={styles.text}>*</span>
      </div>
    ),
    text: ({
      name,
      args,
    }) => `${name} looks at ${args[0]}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      const {
        player,
        target,
      } = bindings;

      // if (target) {
        const targetPosition = target.position;
        const timestamp = performance.now();
        const bbox2 = target.physicsMesh ?
          new THREE.Box3()
            .setFromBufferAttribute(target.physicsMesh.geometry.attributes.position)
            .applyMatrix4(target.physicsMesh.matrixWorld)
        :
          null;

        player.characterBehavior.addWaypointAction(
          targetPosition,
          timestamp,
          {
            boundingBox: bbox2,
          },
        );
      // }
    },
  },
];
export const directorMessageTypesArray = [
  /* {
    name: 'CAMERA',
    description: 'Change the camera to focus on something. The first value represents the actor (characeter/object) to focus on. The method arguments represent the camera settings to use.',
    example: () => {
      const targetName = targetNames[Math.floor(Math.random() * targetNames.length)];
      // const message = messages[Math.floor(Math.random() * messages.length)];
      const storyCameraShot = storyCameraShots[Math.floor(Math.random() * storyCameraShots.length)];
      const storyCameraFrame = storyCameraFrames[Math.floor(Math.random() * storyCameraFrames.length)];
      const storyCameraAngle = storyCameraAngles[Math.floor(Math.random() * storyCameraAngles.length)];
      return [
        'focal_character_or_object_name',
        'CAMERASHOT',
        [
          storyCameraShot,
          storyCameraFrame,
          storyCameraAngle,
        ],
        `${storyCameraShot.toUpperCase()} on _character_or_object_name`,
      ].map(a => JSON.stringify(a)).join(',');
    },
    args: () => [
      storyCameraShots,
      storyCameraFrames,
      storyCameraAngles,
    ],
    bindLore: bindCharacterLore,
    render: ({
      styles,
      key,
      name,
      command,
      args,
      message,
    }) => {
      const [
        storyCameraShot,
        storyCameraFrame,
        storyCameraAngle,
      ] = args;

      return (
        <div className={classnames(
          styles.chatMessage,
          styles.say,
        )} key={key}>
          <span className={styles.name}>{storyCameraShot.toUpperCase()}</span>
          <span className={styles.text}>on ${targetName}</span>
        </div>
      );
    },
    text: ({
      name,
      command,
      args,
      message,
    }) => {
      const [
        storyCameraShot,
        storyCameraFrame,
        storyCameraAngle,
      ] = args;

      return `${storyCameraShot.toUpperCase()} on ${targetName}`;
    },
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      console.log('execute camera shot', parsed);
      const {
        name,
        command,
        args,
      } = parsed;
      const [
        storyCameraShot,
        storyCameraFrame,
        storyCameraAngle,
      ] = args;

      // XXX camera
      console.warn('not implemented');

      // constants
      const narratorVoiceId = `1Cg9Oc_K9UDe5WgVDAcaCSbbBoo-Npj1E`; // 'Discord'
      const model = 'tiktalknet';
      const voiceId = narratorVoiceId;

      // narrator
      const {audioManager} = engine;
      const {audioContext} = audioManager;

      const voiceEndpoint = new AutoVoiceEndpoint({
        model,
        voiceId,
      });

      const voicer = new VoiceEndpointVoicer({
        voiceEndpoint,
        audioManager,
      });

      const narratorMessage = `${storyCameraShot.toUpperCase()} on ${name} (${storyCameraFrame.toUpperCase()}, ${storyCameraAngle.toUpperCase()})`;
      const stream = await voicer.getStream(narratorMessage);

      const audioInjectWorkletNode = new AudioInjectWorkletNode({
        audioContext,
      });

      await voicer.start(stream, {
        audioInjectWorkletNode,
        signal,
        onStart: () => {
          audioInjectWorkletNode.connect(audioContext.gain);
        },
        onEnd: () => {
          audioInjectWorkletNode.disconnect();
        },
      });

      // const {
      //   player,
      // } = bindings;
      // const {
      //   voiceQueueManager,
      // } = engine;
      // const stream = player.voicer.getStream(parsed.message);

      // await voiceQueueManager.waitForTurn(async () => {
      //   await player.playAudioStream(stream, {
      //     onStart: () => {
      //       message.dispatchEvent(new MessageEvent('playStart', {
      //         data: {
      //           player,
      //         },
      //       }));
      //     },
      //     onEnd: () => {
      //       message.dispatchEvent(new MessageEvent('playEnd', {
      //         data: {
      //           player,
      //         },
      //       }));
      //     },
      //   });
      // });
    },
  }, */
];
// object message types are actions that are triggered by objects (e.g. explode)
export const objectMessageTypesArray = [
  // nothing
];
/* // chat message types correspond to user actions
export const chatMessageTypesArray = [
  {
    name: 'CHAT',
    description: 'A chat message',
    example: () => `${characterName}: ${message}`,
    render: ({
      styles,
      key,
      name,
      // commandArgument,
      message,
    }) => (
      <div className={classnames(
        styles.chatMessage,
        // styles.emote,
      )} key={key}>
        <span className={styles.name}>{name}</span>
        <span className={styles.text}>: {message}</span>
      </div>
    ),
    text: ({
      name,
      message,
    }) => `${name}: ${message}`,
    execute: async ({
      message,
      parsed,
      bindings,
      engine,
      signal,
    }) => {
      // nothing
    },
  },
]; */
// ai message types are those that can be triggered by ai
export const aiMessageTypesArray = [
  ...characterMessageTypesArray,
  ...directorMessageTypesArray,
];
// all message types
export const messageTypesArray = [
  ...aiMessageTypesArray,
  // ...chatMessageTypesArray,
];

//

export const characterMessageTypes = (() => {
  const result = {};
  for (const messageType of characterMessageTypesArray) {
    result[messageType.name] = messageType;
  }
  return result;
})();
export const objectMessageTypes = (() => {
  const result = {};
  for (const messageType of objectMessageTypesArray) {
    result[messageType.name] = messageType;
  }
  return result;
})();
export const aiMessageTypes = (() => {
  const result = {};
  for (const messageType of aiMessageTypesArray) {
    result[messageType.name] = messageType;
  }
  return result;
})();
export const messageTypes = (() => {
  const result = {};
  for (const messageType of messageTypesArray) {
    result[messageType.name] = messageType;
  }
  return result;
})();

//

export const messageToName = (m) => {
  const {
    name,
  } = m.getParsed();
  return name;
};
export const messageToCommand = (m) => {
  const {
    command,
  } = m.getParsed();
  return command;
};
export const messageToReact = (m, opts) => {
  const {
    styles,
    key = null,
  } = (opts ?? {});

  const raw = m.getRaw();
  if (raw.role === 'assistant') {
    const parsed = m.getParsed();
    if (!parsed) {
      const raw = m.getRaw();
      console.warn('failed to parse message', raw);
      // debugger;
      return null;
    }
    const {
      command,
    } = parsed;

    const messageType = messageTypes[command];
    if (messageType) {
      const renderFn = messageType.render;
      return renderFn({
        styles,
        key,
        ...parsed,
      });
    } else {
      console.warn('unknown message type', command, parsed);
      return '';
    }
  } else {
    return (<div
      className={classnames(
        styles.chatMessage,
        // styles.say,
      )}
    >{raw.content}</div>);
  }
};
export const messageToText = (m) => {
  const raw = m.getRaw();
  if (raw.role === 'assistant') {
    const parsed = m.getParsed();
    if (parsed) {
      return parsed.message;
    } else {
      console.warn('failed to parse message', m.getRaw());
      return '';
    }
  } else {
    return raw.content;
  }
};
export const messageToFullText = (m) => {
  const raw = m.getRaw();
  if (raw.role === 'assistant') {
    const parsed = m.getParsed();
    if (!parsed) {
      const raw = m.getRaw();
      console.warn('failed to parse message', raw);
      // debugger;
      return '';
    }
    const {
      command,
    } = parsed;

    const messageType = messageTypes[command];
    if (messageType) {
      const textFn = messageType.text;
      return textFn(parsed);
    } else {
      console.warn('unknown message type', command, parsed);
      return '';
    }
  } else {
    return raw.content;
  }
}
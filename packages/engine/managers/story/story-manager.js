/*
this file contains the story beat triggers (battles, victory, game over, etc.)
*/

import * as THREE from 'three';
// import {
//   emotes,
// } from '../emote/emotes.js';
// import {
//   emotions,
// } from '../emote/emotions.js';
// import physicsManager from '../../physics/physics-manager.js';
// import {
//   messageTypes,
// } from '../lore/messages.jsx';
// import {
//   Message,
// } from '../lore/message.js';
// import {
//   getFuzzyTargetObject,
//   // getHitMap,
//   // makeHitMesh,
//   // getLine,
//   // makePathLineGeometry,
// } from '../../pathfinding.js';
import {
  Conversation,
} from '../lore/conversation.js';
import {
  MessageExecuter,
} from '../lore/message-executer.js';

// import {
//   cleanLowercase,
// } from '../../util.js';

//

/* function makeSwirlPass() {
  const renderer = getRenderer();
  const size = renderer.getSize(localVector2D)
    .multiplyScalar(renderer.getPixelRatio());
  const resolution = size;
  const swirlPass = new SwirlPass(rootScene, camera, resolution.x, resolution.y);
  return swirlPass;
}
let swirlPass = null;
const _startSwirl = () => {
  if (!swirlPass) {
    swirlPass = makeSwirlPass();
    renderSettingsManager.addExtraPass(swirlPass);

    this.sounds.playSoundName('battleTransition');
    musicManager.playCurrentMusicName('battle');
  }
};
const _stopSwirl = () => {
  if (swirlPass) {
    renderSettingsManager.removeExtraPass(swirlPass);
    swirlPass = null;

    musicManager.stopCurrentMusic();
    return true;
  } else {
    return false;
  }
}; */

export const advanceModes = [
  'Auto',
  'RPG',
  'Wait',
  'Auto',
];
export const cameraModes = [
  'Cinematic',
  'Simple',
  'Free',
];
export const subtitlesModes = [
  'Dialog',
  'Anime',
  'Visual Novel',
  'Bubble',
  'None',
];

//

export const storyCameraShots = [
  "establishing shot",
  "master shot",
  "wide shot",
  "full shot",
  "medium shot",
  "medium close up shot",
  "close up shot",
  "extreme close up shot",
];
export const storyCameraFrames = [
  "single shot",
  "two shot",
  "crowd shot",
  "over the shoulder shot",
  "point of view shot",
  "insert shot",
];
export const storyCameraAngles = [
  "low angle",
  "high angle",
  "overhead angle",
  "dutch angle",
  "eye level",
  "shoulder level",
  "knee level",
  "ground level",
];

//

const fuzzyEmotionMappings = {
  "alert": "alert",
  "angry": "angry",
  "embarrassed": "embarrassed",
  "headNod": "headNod",
  "headShake": "headShake",
  "sad": "sad",
  "surprise": "surprise",
  "victory": "victory",
  "surprised": "surprise",
  "happy": "victory",
  "sorrow": "sad",
  "joy": "victory",
  "confused": "alert",
};
export const getFuzzyEmotionMapping = emotionName => fuzzyEmotionMappings[emotionName];

//

class StoryAdvance extends EventTarget {
  constructor({
    storyManager,
  }) {
    super();

    this.storyManager = storyManager;

    this.mode = advanceModes[0];
  }
  getMode() {
    return this.mode;
  }
  setMode(mode) {
    this.mode = mode;

    this.dispatchEvent(new MessageEvent('modechange', {
      data: {
        mode,
      },
    }));
  }
}

class StoryCamera extends EventTarget {
  constructor({
    cameraManger,
    playersManager,
    npcManager,
    storyManager
  }) {
    super();

    this.mode = cameraModes[0];

    this.#updateCameraControllerFn();
    storyManager.addEventListener('conversationchange', e => {
      const {
        conversation,
      } = e.data;
      if (conversation) {
        console.log('conversation change', conversation);
      }
    });
  }
  #cameraControllerFn;

  static #makeCameraController(mode) {
    switch (mode) {
      case 'Cinematic':
      case 'Simple': {
        return () => {






          // XXX target the last npc
          // console.log('tick simple camera');
        };
     }
      case 'Free': {
        return null;
      }
    }
  }
  #updateCameraControllerFn() {
    this.#cameraControllerFn = StoryCamera.#makeCameraController(this.mode);
  }

  getMode() {
    return this.mode;
  }
  setMode(mode) {
    this.mode = mode;

    this.#updateCameraControllerFn();

    this.dispatchEvent(new MessageEvent('modechange', {
      data: {
        mode,
      },
    }));
  }
  getCameraControllerFn() {
    return () => {
      this.#cameraControllerFn && this.#cameraControllerFn();
    };
  }
}

class StorySubtitles extends EventTarget {
  constructor() {
    super();

    this.mode = subtitlesModes[0];
  }
  getMode() {
    return this.mode;
  }
  setMode(mode) {
    this.mode = mode;

    this.dispatchEvent(new MessageEvent('modechange', {
      data: {
        mode,
      },
    }));
  }
}

//

/* let currentFieldMusic = null;
let currentFieldMusicIndex = 0;
export const handleStoryKeyControls = async (e) => {
  switch (e.which) {
    case 48: { // 0
      await musicManager.waitForLoad();
      _stopSwirl() || _startSwirl();
      return false;
    }
    case 57: { // 9
      await musicManager.waitForLoad();
      _stopSwirl();
      if (currentFieldMusic) {
        musicManager.stopCurrentMusic();
        currentFieldMusic = null;
      } else {
        const fieldMusicName = fieldMusicNames[currentFieldMusicIndex];
        currentFieldMusicIndex = (currentFieldMusicIndex + 1) % fieldMusicNames.length;
        currentFieldMusic = musicManager.playCurrentMusic(fieldMusicName, {
          repeat: true,
        });
      }
      return false;
    }
    case 189: { // -
      await musicManager.waitForLoad();
      _stopSwirl();
      musicManager.playCurrentMusic('victory', {
        repeat: true,
      });
      return false;
    }
    case 187: { // =
      await musicManager.waitForLoad();

      _stopSwirl();
      musicManager.playCurrentMusic('gameOver', {
        repeat: true,
      });
      return false;
    }
  }

  return true;

}; */

export class StoryManager extends EventTarget {
  constructor({
    context,
    engine,
    // localStorageManager,
    cameraManager,
    engineRenderer,
    aiClient,
    emoteManager,
    playersManager,
    npcManager,
    chatManager,
    voiceQueueManager,
    interactionManager,
    zTargetingManager,
    loreManager,
    physicsTracker,
    sounds,
  }) {
    super();

    if (!context || !engine /*|| !localStorageManager*/ || !cameraManager || !engineRenderer || !aiClient || !emoteManager || !playersManager || !npcManager || !chatManager || !voiceQueueManager || !interactionManager || !zTargetingManager || !loreManager || !physicsTracker || !sounds) {
      console.warn('missing args', {cameraManager, engineRenderer, aiClient, emoteManager, playersManager, npcManager, chatManager, voiceQueueManager, interactionManager, zTargetingManager, loreManager, physicsTracker, sounds});
      throw new Error('missing args');
    }

    this.context = context;
    this.engine = engine;
    // this.localStorageManager = localStorageManager;
    this.cameraManager = cameraManager;
    this.engineRenderer = engineRenderer;
    this.aiClient = aiClient;
    this.emoteManager = emoteManager;
    this.playersManager = playersManager;
    this.npcManager = npcManager;
    this.chatManager = chatManager;
    this.voiceQueueManager = voiceQueueManager;
    this.interactionManager = interactionManager;
    this.zTargetingManager = zTargetingManager;
    this.loreManager = loreManager;
    this.physicsTracker = physicsTracker;
    this.sounds = sounds;

    this.storyAdvance = new StoryAdvance({
      storyManager: this,
    });
    this.storyCamera = new StoryCamera({
      cameraManager: this.cameraManager,
      playersManager: this.playersManager,
      npcManager: this.npcManager,
      storyManager: this,
    });
    this.storySubtitles = new StorySubtitles();

    this.#listen();
  }
  #currentConversation = null;
  #currentMessageExecuter = null;
  #currentCameraControllerFn = null;

  #setConversation(conversation) {
    if (this.#currentConversation) {
      this.#currentConversation.close();
      this.#currentConversation = null;
    }
    if (this.#currentMessageExecuter) {
      this.#currentMessageExecuter.interrupt();
      this.#currentMessageExecuter = null;
    }

    //

    this.#currentConversation = conversation;
    this.dispatchEvent(new MessageEvent('conversationchange', {
      data: {
        conversation,
      },
    }));

    //

    if (conversation) {
      conversation.addEventListener('parsedmessage', e => {
        messageExecuter.queueMessage(e.data.parsedMessage);
      });
      conversation.addEventListener('close', () => {
        this.#setConversation(null);

        // this.cameraManager.setDynamicTarget(); // XXX should use setControllerFn
      }, {once: true});

      // message executer
      const signal = conversation.getSignal();
      const messageExecuter = new MessageExecuter({
        engine: this.engine,
        aiClient: this.aiClient,
        loreManager: this.loreManager,
        storyAdvance: this.storyAdvance,
        signal,
      });
      messageExecuter.addEventListener('messageplay', e => {
        const {message} = e.data;
        this.chatManager.addMessage(message);
      })
      this.#currentMessageExecuter = messageExecuter;

      // unset old camera controller fn
      if (this.#currentCameraControllerFn) {
        this.cameraManager.setControllerFn(this.#currentCameraControllerFn);
        this.#currentCameraControllerFn = null;
      }
      // set new camera controller fn
      this.#currentCameraControllerFn = this.storyCamera.getCameraControllerFn();
      this.cameraManager.setControllerFn(this.#currentCameraControllerFn);

      conversation.addEventListener('close', e => {
        this.cameraManager.unsetControllerFn(this.#currentCameraControllerFn);
      });

      // dispatch start events
      this.dispatchEvent(new MessageEvent('conversationstart', {
        data: {
          conversation,
        },
      }));
      this.dispatchEvent(new MessageEvent('messageexecuterstart', {
        data: {
          messageExecuter,
        },
      }));
    } else {
      this.#currentMessageExecuter = null;
    }
  }

  #getAllPlayers() {
    const {
      playersManager,
      npcManager,
    } = this;

    const allPlayers = playersManager.getAllPlayers()
      .concat(
        Array.from(npcManager.npcPlayers)
      );
    return allPlayers;
  }
  getConversationStarter({
    localPlayerId,
    app,
    physicsId,
  }) {
    const {
      appType,
    } = app;
    switch (appType) {
      case 'vrm': {
        const allPlayers = this.#getAllPlayers();
        const allPlayerAppSpecs = allPlayers.map(player => {
          return {
            player,
            apps: Array.from(player.appManager.apps.values()),
          };
        });
        const player = allPlayerAppSpecs.find(playerSpec => {
          const {
            apps,
          } = playerSpec;
          return apps.includes(app);
        })?.player ?? null;
        if (player) {
          const {playerId} = player;
          const starter = {
            object: player.avatar.modelBones.Head,
            startFn: async ({conversation}) => {
              if(!conversation.getInspectPlayerMessage)
                return console.error('no getInspectPlayerMessage function on conversation', conversation);
              return await conversation.getInspectPlayerMessage({
                localPlayerId,
                playerId,
              });
            },
          };
          return starter;
        } else {
          console.warn('no player associated with app', app);
          throw new Error('no player associated with app');
        }
      }
      case 'character360': {
        const allPlayers = this.#getAllPlayers();
        const allPlayerAppSpecs = allPlayers.map(player => {
          return {
            player,
            apps: Array.from(player.appManager.apps.values()),
          };
        });
        const player = allPlayerAppSpecs.find(playerSpec => {
          const {
            apps,
          } = playerSpec;
          return apps.includes(app);
        })?.player ?? null;
        if (player) {
          const {playerId} = player;
          const starter = {
            object: player.avatar.modelBones.Head,
            startFn: async ({conversation}) => {
              if(!conversation.getInspectPlayerMessage)
                return console.error('no getInspectPlayerMessage function on conversation', conversation);
              return await conversation.getInspectPlayerMessage({
                localPlayerId,
                playerId,
              });
            },
          };
          return starter;
        } else {
          console.warn('no player associated with app', app);
          throw new Error('no player associated with app');
        }
      }
      default: {
        // let targetSpec = app.spec;
        const {
          loreManager,
        } = this;
        const {
          engine,
        } = loreManager;
        const {
          physicsTracker,
        } = engine;
        const physicsApp = physicsTracker.getAppByPhysicsId(physicsId);
        if (!physicsApp) {
          throw new Error('no physics app');
        }
        const lore = this.loreManager.getLore();
        const targetActor = lore.getActorById(physicsApp.instanceId);
        if (!targetActor) {
          throw new Error('no target actor');
        }
        const starter = {
          object: app,
          startFn: async ({conversation}) => {
            return await conversation.getInspectTargetMessage({
              localPlayerId,
              targetActor,
            });
          },
        };
        return starter;
      }
    }
  }

  async startConversation(initialMessageFn) {
    const messages = this.chatManager.getMessages().slice();

    // conversation
    const conversation = new Conversation({
      messages,
      loreManager: this.loreManager,
      aiClient: this.aiClient,
    });
    this.#setConversation(conversation);
    const messageExecuter = this.#currentMessageExecuter;

    // start executer
    await initialMessageFn({
      conversation,
    });
    messageExecuter.start(() => conversation.next());
  }
  stopConversation() {
    if (this.#currentConversation) {
      console.log('stop conversation')
      this.#currentConversation.close();
    } else {
      console.log('no conversation to stop')
    }
  }
  async inspectPhysicsId(physicsId) {
    if (!this.#currentConversation) {
      // play sound
      this.sounds.playSoundName('menuSelect');

      // locals
      const localPlayer = this.playersManager.getLocalPlayer();
      const localPlayerId = localPlayer.playerId;

      const [
        app,
        targetObject,
      ] = this.physicsTracker.getPairByPhysicsId(physicsId);

      // get conversation starter
      const starter = this.getConversationStarter({
        localPlayerId,
        app,
        physicsId,
      });

      /* // set camera
      this.cameraManager.setFocus(false);

      this.cameraManager.lastTarget = null;
      this.cameraManager.setDynamicTarget(
        localPlayer.avatar.modelBones.Head,
        starter.object,
      ); */

      // add local player actions
      {
        const targetPosition = targetObject.position;
        const bbox2 = targetObject.physicsMesh ?
          new THREE.Box3()
            .setFromBufferAttribute(targetObject.physicsMesh.geometry.attributes.position)
            .applyMatrix4(targetObject.physicsMesh.matrixWorld)
        :
          null;

        const timestamp = performance.now();
        localPlayer.characterBehavior.clearWaypointActions();
        localPlayer.characterBehavior.addWaypointAction(
          targetPosition,
          timestamp,
          {
            boundingBox: bbox2,
          },
        );
      }

      // add possible remote player actions
      {
        // const npcPlayer = this.npcManager.getNpcByVrmApp(app);
        const npcPlayer = app.player;
        if (npcPlayer) {
          const timestamp = performance.now();
          npcPlayer.characterBehavior.addFaceTowardAction(
            // localPlayer.avatar.modelBones.Head.position,
            localPlayer.position,
            timestamp,
          );
        }
      }

      // start conversation
      this.startConversation(starter.startFn);
    } else {
      throw new Error('already in a conversation!');
    }
  }
  async startScene() {
    if (!this.#currentConversation) {
      // set camera
      // this.cameraManager.setFocus(false);

      // this.cameraManager.lastTarget = null;
      // this.cameraManager.setDynamicTarget();

      // play sound
      this.sounds.playSoundName('menuSelect');

      // initialize conversation
      this.startConversation(({conversation}) => conversation.next());
    } else {
      throw new Error('already in a conversation!');
    }
  }
  async inspectSelf() {
    if (!this.#currentConversation) {
      // set camera
      // this.cameraManager.setFocus(false);

      // this.cameraManager.lastTarget = null;
      // this.cameraManager.setDynamicTarget();

      // play sound
      this.sounds.playSoundName('menuSelect');

      // initialize conversation
      this.startConversation(({conversation}) => conversation.inspectSelf());
    } else {
      throw new Error('already in a conversation!');
    }
  }

  #listen() {
    this.interactionManager.addEventListener('interact', async e => {
      const {
        // app,
        physicsId,
      } = e;
      await this.inspectPhysicsId(physicsId);
    });

    this.zTargetingManager.addEventListener('select', async e => {
      const {
        // app,
        physicsId,
      } = e;
      await this.inspectPhysicsId(physicsId);
    });
  }

  getConversation() {
    return this.#currentConversation;
  }
  async progressConversation() {
    return;
    if (this.#currentConversation) {
      // if (!this.#currentConversation.progressing) {
        // this.#currentConversation.progressing = true;
        // this.#currentConversation.dispatchEvent(new MessageEvent('progressstart', /*{
        //   data: {
        //     // conversation: this.#currentConversation,
        //   },
        // } */));

        this.sounds.playSoundName('menuNext');

        // interrupt the current conversation to let it advance
        this.#currentMessageExecuter.interrupt();

        await this.#currentMessageExecuter.next();

        // this.#currentConversation.progressing = false;
        // this.#currentConversation.dispatchEvent(new MessageEvent('progressend', /* {
        //   data: {
        //     // conversation: this.#currentConversation,
        //   },
        // }*/));
      // }
    } else {
      throw new Error('not in a conversation!');
    }
  }
  interrupt() {
    if (this.#currentMessageExecuter) {
      this.#currentMessageExecuter.interruptSoft();
    }
  }

  /* async nextMessageAnonymous(opts) {
    const conversation = this.loreManager.createConversation({
      messages: true,
    });
    conversation.addEventListener('message', e => {
      const {
        message,
      } = e.data;
      this.#addMessage(message);
    });
    const message = await conversation.completeMessages([], opts);
    return message;
  } */

  /* useEffect(() => {
    if (engine) {
      const mousedown = async e => {
        const {
          zTargetingManager,
          // storyManager,
          playersManager,
        } = engine;
        const focusedApp = zTargetingManager.getFocusedApp();
        if (focusedApp) {
          const appManager = engine.getAppManager();
          const apps = appManager.getApps();
          const blockadelabsSkyboxApp = apps.find(a => a.appType === 'blockadelabsskybox');
          if (blockadelabsSkyboxApp) {
            const worldSpec = blockadelabsSkyboxApp.spec?.content;
            const localPlayer = playersManager.getLocalPlayer();
            const {
              playerSpec: localPlayerSpec,
            } = localPlayer;

            const aiClient = new AiClient({
              modelType: 'openai',
              modelName: 'gpt-4-0613',
            });
            const conversation = new Conversation({
              aiClient,
            });
            conversation.setWorldSpec(worldSpec);
            conversation.addPlayerSpec(localPlayerSpec);
            const remotePlayerSpecs = apps.filter(a =>
              a.appType === 'character360'
            ).map(a => a.spec.content);
            for (let i = 0; i < remotePlayerSpecs.length; i++) {
              conversation.addPlayerSpec(remotePlayerSpecs[i]);
            }

            if (remotePlayerSpecs.length > 0) {
              const parseMessage = message => {
                const argsString = message?.function_call?.arguments;
                const j = argsString ? JSON.parse(argsString) : null;
                return j;
              };

              // const remotePlayerSpec = remotePlayerSpecs[0];
              const message = await conversation.completeApp({
                localPlayerSpec,
                app: focusedApp,
              });
              const j = parseMessage(message);
              console.log('first message', message, j);
              globalThis.nextMessage = async () => {
                // const argsString = message?.function_call?.arguments;
                // const j = argsString ? JSON.parse(argsString) : null;
                const message = await conversation.nextMessage();
                const j = parseMessage(message);
                console.log('next message', message, j, conversation.getMessages());
              };
            } else {
              console.warn('no remote player specs', apps);
            }
          } else {
            console.warn('no skybox app', apps);
          }
        }
      };
      globalThis.addEventListener('mousedown', mousedown);

      return () => {
        globalThis.removeEventListener('mousedown', mousedown);
      };
    }
  }, [engine]); */
}

/* story.listenHack = () => {
  (typeof window !== 'undefined') && window.document.addEventListener('click', async e => {
    if (this.cameraManager.pointerLockEle`ment) {
      if (e.button === 0 && (this.cameraManager.focus && zTargeting.focusTargetReticle)) {
        const app = metaversefile.getAppByPhysicsId(zTargeting.focusTargetReticle.physicsId);

        if (app) {
          const {appType} = app;

          // cameraManager.setFocus(false);
          // zTargeting.focusTargetReticle = null;
          sounds.playSoundName('menuSelect');

          this.cameraManager.setFocus(false);
          this.cameraManager.setDynamicTarget();

          (async () => {
            const aiScene = metaversefile.useLoreAIScene();
            if (appType === 'npc') {
              const {name, description} = app.getLoreSpec();
              const remotePlayer = npcManager.getNpcByApp(app);

              if (remotePlayer) {
                const {
                  value: comment,
                  done,
                } = await aiScene.generateSelectCharacterComment(name, description);

                _startConversation(comment, remotePlayer, done);
              } else {
                console.warn('no player associated with app', app);
              }
            } else {
              const {name, description} = app;
              const comment = await aiScene.generateSelectTargetComment(name, description);
              const fakePlayer = {
                avatar: {
                  modelBones: {
                    Head: app,
                  },
                },
              };
              _startConversation(comment, fakePlayer, true);
            }
          })();
        } else {
          console.warn('could not find app for physics id', zTargeting.focusTargetReticle.physicsId);
        }
      } else if (e.button === 0 && currentConversation) {
        if (!currentConversation.progressing) {
          currentConversation.progress();

          sounds.playSoundName('menuNext');
        }
      }
    }
  });
}; */

import {
  makeAppQueryFn,
} from '../../util.js';

//

export class AppTracker extends EventTarget {
  constructor({
    appManagerContext,
  }) {
    super();
    
    if (!appManagerContext) {
      console.warn('missing managers', {
        appManagerContext,
      });
      throw new Error('missing managers');
    }
    this.appManagerContext = appManagerContext;

    this.#listen();
  }

  registerAppTracker({
    appManagerName,
    query,
    addCb,
    removeCb,
  }) {
    const q = makeAppQueryFn(query);

    const _addCb = e => {
      const {
        appManager,
        app,
      } = e.data;
      if (appManagerName === undefined || appManager.name === appManagerName) {
        if (q(app)) {
          addCb(app);
        }
      }
    };
    const _removeCb = e => {
      const {
        appManager,
        app,
      } = e.data;
      if (appManagerName === undefined || appManager.name === appManagerName) {
        // const u = app?.spec?.start_url || '';
        if (q(app)) {
          removeCb(app);
        }
      }
    };
    this.addEventListener('appadd', _addCb);
    this.addEventListener('appremove', _removeCb);

    // initialize
    const appManagers = this.appManagerContext.getAppManagers();
    for (const appManager of appManagers) {
      if (appManagerName === undefined || appManager.name === appManagerName) {
        for (const app of appManager.apps.values()) {
          const e = {
            data: {
              appManager,
              app,
            },
          };
          _addCb(e);
        }
      }
    }

    return {
      cleanup: () => {
        this.removeEventListener('appadd', _addCb);
        this.removeEventListener('appremove', _removeCb);
      },
    };
  }

  #listen() {
    const _bindAppManager = appManager => {
      if (appManager) {
        // console.log('got app manager', appManager, structuredClone(appManager.apps));
        // globalThis.appManager = appManager;

        appManager.addEventListener('appadd', e => {
          this.dispatchEvent(new MessageEvent('appadd', {
            data: {
              appManager,
              app: e.data.app,
            },
          }));
          // console.log('app add', e);
        });
        appManager.addEventListener('appremove', e => {
          this.dispatchEvent(new MessageEvent('appremove', {
            data: {
              appManager,
              app: e.data.app,
            },
          }));
          // console.log('app remove', e);
        });
      }
    };
    const appManagers = this.appManagerContext.getAppManagers();
    for (const appManager of appManagers) {
      _bindAppManager(appManager);
    }

    this.appManagerContext.addEventListener('appmanageradd', e => {
      const {
        appManager,
      } = e.data;
      // console.log('got new app manager', appManager);
      _bindAppManager(appManager);
    });
  }
}
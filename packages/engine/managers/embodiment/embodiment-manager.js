export const embodyModes = [
  'None',
  'Camera',
  'WebXR',
];

export class EmbodimentManager extends EventTarget {
  constructor() {
    super();

    this.mode = embodyModes[0];
  }
  getMode() {
    return this.mode;
  }
  setMode(mode) {
    this.mode = mode;

    this.dispatchEvent(new MessageEvent('modechange', {
      data: {
        mode,
      },
    }));
  }
}
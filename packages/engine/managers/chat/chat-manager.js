// import {
//   makeId,
//   makePromise,
// } from '../../util.js';
// import {
//   VoiceEndpointVoicer,
//   VoiceEndpoint,
// } from '../../audio/voice-output/voice-endpoint-voicer.js';
// import {
//   voiceEndpointBaseUrl,
// } from '../../endpoints.js';

import {
  QueueManager,
} from '../queue/queue-manager.js';
// import {
//   Conversation,
// } from '../lore/conversation.js';
import {
  Memory,
  MemoriesManager,
} from '../memories/memories-manager.js';
// import {
//   Message,
// } from '../lore/message.js';

// import {
//   embed,
// } from '../../embedding.js';

//

// const narratorVoiceId = `1Cg9Oc_K9UDe5WgVDAcaCSbbBoo-Npj1E`; // 'Discord'

//

/* const _getEmotion = text => {
  let match;
  if (match = text.match(/(😃|😊|😁|😄|😆|(?:^|\s)lol(?:$|\s))/)) {
    match.emotion = 'joy';
    return match;
  } else if (match = text.match(/(😉|😜|😂|😍|😎|😏|😇|❤️|💗|💕|💞|💖|👽)/)) {
    match.emotion = 'fun';
    return match;
  } else if (match = text.match(/(😞|😖|😒|😱|😨|😰|😫)/)) {
    match.emotion = 'sorrow';
    return match;
  } else if (match = text.match(/(😠|😡|👿|💥|💢)/)) {
    match.emotion = 'angry';
    return match;
  } else if (match = text.match(/(😐|😲|😶)/)) {
    match.emotion = 'neutral';
    return match;
  } else {
    return null;
  }
}; */

//

export class ChatManager extends EventTarget {
  constructor({
    playersManager,
    audioManager,
    voiceQueueManager,
    supabaseClient,
    chatMemoriesManager,
    characterMemoriesManager,
  }) {
    super();

    if (!playersManager || !audioManager || !voiceQueueManager || !supabaseClient || !chatMemoriesManager || !characterMemoriesManager) {
      console.warn('missing arguments', {
        playersManager,
        audioManager,
        voiceQueueManager,
        supabaseClient,
        chatMemoriesManager,
        characterMemoriesManager,
      });
      debugger;
    }

    this.playersManager = playersManager;
    this.audioManager = audioManager;
    this.voiceQueueManager = voiceQueueManager;
    this.supabaseClient = supabaseClient;
    this.chatMemoriesManager = chatMemoriesManager;
    this.characterMemoriesManager = characterMemoriesManager;

    this.worldId = '';
    this.conversations = [];
    this.messages = [];
    this.conversation = null;

    this.connectQueue = new QueueManager();

    /* { // XXX read in voice commands
      const localPlayer = this.playersManager.getLocalPlayer();
      localPlayer.voiceInput.addEventListener('speech', e => {
        const {
          transcript,
        } = e.data;
        // console.log('local player speech', transcript);
        // globalThis.testChatGpt(transcript);

        // this.chatManager.addMessage(transcript, {
        //   timeout: 3000,
        // });
        this.addMessage(transcript);
      });
    } */
  
    // this.ioBus.registerHandler('chat', e => {
    //   const {
    //     text,
    //   } = e;
    //   this.addMessage(text);
    // });

    /* this.narratorVoicer = (() => {
      const url = `${voiceEndpointBaseUrl}?voice=${encodeURIComponent(narratorVoiceId)}`;
      const voiceEndpoint = new VoiceEndpoint(url);
      const fakePlayer = {
        avatar: {
          isAudioEnabled: () => {
            return true;
          },
          getAudioInput: () => {
            return this.audioManager.audioContext.destination;
          },
        },
      };
      const narratorVoicer = new VoiceEndpointVoicer({
        voiceEndpoint,
        player: fakePlayer,
        audioManager: this.audioManager,
      });
      return narratorVoicer;
    })(); */
  }

  getConversations() {
    return this.conversations;
  }
  getMessages() {
    return this.messages;
  }

  async addMessage(m, {
    source = '',
  } = {}) {
    console.log('chat manager add message', {
      m,
      source,
      messages: this.messages.slice(),
    });

    if (this.messages.some(m => {
      return !m.getRole;
    })) {
      debugger;
    }

    await this.connectQueue.waitForTurn(async () => {
      this.messages.push(m);

      this.dispatchEvent(new MessageEvent('message', {
        data: {
          message: m,
          source,
        },
      }));

      const raw = m.getRaw();
      if (source !== 'agent' && typeof globalThis.readChat === 'function') {
        globalThis.readChat(raw);
      }

      if (this.worldId && this.conversation) {
        const key = [this.worldId, this.conversation.id].join(':');
        (async () => {
          let memory = new Memory({
            name: key,
            raw,
          });
          memory = await MemoriesManager.bakeMemory(memory);
          const rawMemory = {
            ...memory.toJSON(),
            user_id: this.supabaseClient.profile.user.id,
          };

          await Promise.all([
            this.chatMemoriesManager.upsertRawMemory(rawMemory),
            this.characterMemoriesManager.upsertRawMemory(rawMemory),
          ]);
        })();
      } else {
        console.warn('not in a world', {
          worldId: this.worldId,
          conversation: this.conversation,
        });
      }
    });
  }
  async removeConversation(c) {
    const index = this.conversations.indexOf(c);

    if (index !== -1) {
      this.conversations.splice(index, 1);

      // console.log('conversation remove', c);
      await this.supabaseClient.supabase
        .from('conversations')
        .delete()
        .eq('id', c.id)
        .maybeSingle();

      this.dispatchEvent(new MessageEvent('conversationremove', {
        data: {
          conversation: c,
        },
      }));
    } else {
      console.warn('remove unknown conversation', {
        conversation: c,
        conversations: this.conversations,
      });
    }
  }
  async setConversationName(conversationId, name) {
    const index = this.conversations.findIndex(c => c.id === conversationId);
    if (index !== -1) {
      const conversation = this.conversations[index];
      conversation.name = name;

      // update "name" of conversation where "id" = conversationId
      const result = await this.supabaseClient.supabase
        .from('conversations')
        .update({
          name,
        })
        .eq('id', conversationId);
    } else {
      console.warn('set unknown conversation name', {
        conversationId,
        name,
        conversations: this.conversations,
      });
    }
  }
  async removeMessage(m) {
    const index = this.messages.indexOf(m);

    if (index !== -1) {
      this.messages.splice(index, 1);

      const raw = m.getRaw();
      await this.supabaseClient.supabase
        .from(this.chatMemoriesManager.schema.tableName)
        .delete()
        .eq('id', raw.id)
        .maybeSingle();

      this.dispatchEvent(new MessageEvent('messageremove', {
        data: {
          message: m,
        },
      }));
    } else {
      console.warn('remove unknown message', {
        message: m,
        messages: this.messages,
      });
    }
  }

  async connectWorld(worldId) {
    await this.connectQueue.waitForTurn(async () => {
      const result = await this.supabaseClient.supabase
        .from('conversations')
        .select('*')
        .eq('parentName', worldId)
        .order('created_at', {
          ascending: true,
        });
      if (!result.error) {
        this.conversations = result.data;
        this.worldId = worldId;

        if (this.conversations.length === 0) {
          const conversation = await this.createConversation();
          this.dispatchEvent(new MessageEvent('conversationsload'));

          this.messages = [];
          this.conversation = conversation;
          this.dispatchEvent(new MessageEvent('conversationload'));
        } else {
          this.dispatchEvent(new MessageEvent('conversationsload'));

          const conversation = this.conversations[this.conversations.length - 1];
          this.messages = await this.#loadMessages(this.worldId, conversation.id);
          this.conversation = conversation;
          this.dispatchEvent(new MessageEvent('conversationload'));
        }
      } else {
        throw new Error(JSON.stringify(result.error));
      }
    });
  }
  async disconnectWorld() {
    await this.connectQueue.waitForTurn(async () => {
      this.conversations = [];
      this.worldId = '';
      this.dispatchEvent(new MessageEvent('conversationsload'));

      this.messages = [];
      this.conversation = null;
      this.dispatchEvent(new MessageEvent('conversationload'));
    });
  }
  async createConversation({
    name = '',
  } = {}) {
    const conversationId = crypto.randomUUID();
    const parentName = this.worldId;

    const newConversation = {
      id: conversationId,
      parentName,
      name,
      user_id: this.supabaseClient.profile.user.id,
    };

    const result = await this.supabaseClient.supabase
      .from('conversations')
      .upsert(newConversation)
      .select('*')
      .order('created_at', {
        ascending: true,
      });
    if (!result.error) {
      this.conversations = this.conversations.concat(result.data);
      
      const lastConversation = this.conversations[this.conversations.length - 1];

      this.dispatchEvent(new MessageEvent('conversation', {
        data: {
          conversation: lastConversation,
        },
      }));
      return lastConversation;
    } else {
      throw new Error(JSON.stringify(result.error));
    }
  }
  async #loadMessages(worldId, conversationId) {
    const key = [worldId, conversationId].join(':');
    const memories = await this.chatMemoriesManager.getMemoriesByName(key);
    return memories.map(memory => memory.toMessage());
  }

  getConversation() {
    return this.conversation;
  }
  async setConversation(conversation) {
    if (this.worldId) {
      await this.connectQueue.waitForTurn(async () => {
        this.messages = await this.#loadMessages(this.worldId, conversation.id);
        this.conversation = conversation;
        this.dispatchEvent(new MessageEvent('conversationload'));
      });
    } else {
      throw new Error('not connected to a world');
    }
  }

  async clearMessages() {
    this.messages.length = 0;

    this.dispatchEvent(new MessageEvent('conversationload'));

    if (this.worldId) {
      // remove all messages with worldId: worldId
      await this.supabaseClient.supabaase
        .from('chat')
        .delete()
        .eq('name', this.worldId);
    }
  }
}
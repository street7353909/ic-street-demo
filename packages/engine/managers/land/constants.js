export const chunkSize = 64;
export const segments = 40 + 1;
export const gridHeight = 120;
export const chunkHeight = 8;
export const chunkRange = 100;
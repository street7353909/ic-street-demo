import {
  embed,
  embeddingZeroVector,
} from '../../embedding.js';
import {
  Message,
} from '../lore/message.js';

//

const defaultMatchThreshold = 75;
const defaultMatchCount = 10;

//

const getMemoryEmbeddingString = memory => {
  // if (!memory.getRaw) {
  //   console.warn('bad memory', {
  //     memory,
  //   });
  //   debugger;
  // }
  const raw = memory.getRaw();
  if (typeof raw?.content === 'string') {
    return raw.content;
  } else if (typeof raw?.content === 'object' && raw?.content !== null) {
    return JSON.stringify(raw.content);
  } else {
    return '';
  }
};
const getMemoryEmbedding = async (memory) => {
  const memoryText = getMemoryEmbeddingString(memory);
  const embedding = memoryText ? (await embed(memoryText)) : embeddingZeroVector.slice();
  return embedding;
};

//

export class Memory extends Message {
  constructor(opts) {
    super(opts);

    this.name = opts?.name ?? '';
    // this.attachments = opts?.attachments ?? [];
    this.embedding = embeddingZeroVector;
  }
  // fromRaw(opts) {
  //   const raw = opts?.raw;
  //   const name = opts?.name;
  //   // const attachments = opts?.attachments;
  //   return new Memory({
  //     raw,
  //     name,
  //     // attachments,
  //   });
  // }
  toMessage() {
    return Message.fromRaw(this.getRaw());
  }
  toJSON() {
    const raw = this.getRaw();
    const {
      id,
      role,
      content,
    } = raw;
    const {
      name,
      embedding,
    } = this;
    return {
      id,
      role,
      content,
      name,
      embedding,
    };
  }
}

//

export class MemoriesManager {
  constructor({
    supabaseClient,
    schema, // Schema
  }) {
    this.supabaseClient = supabaseClient;
    this.schema = schema;
  }

  static async bakeMemory(memory) {
    memory.embedding = await getMemoryEmbedding(memory);
    return memory;
  }

  async getMemoriesByName(name) {
    const {
      tableName,
    } = this.schema;

    const result = await this.supabaseClient.supabase
      .from(tableName)
      .select('*')
      .eq('name', name)
      .order('created_at', {
        ascending: true,
      });
    const memories = this.#formatMemories(result.data);
    return memories;
  }
  #formatMemories(rawMemories) {
    return rawMemories.map(rawMemory => {
      const {
        id,
        role,
        content,
        name,
        embedding,
      } = rawMemory;
      return new Memory({
        raw: {
          id,
          role,
          content,
        },
        name,
        embedding,
      });
    })
  }
  async searchMemories(query, opts = {}) {
    const {
      signal = null,
    } = opts;
    const embedding = await embed(query, {
      signal,
    });

    return await this.searchMemoriesByEmbedding(embedding, opts);
  }
  async searchMemoriesByEmbedding(embedding, opts = {}) {
    const {
      name = null,
      match_threshold = defaultMatchThreshold,
      match_count = defaultMatchCount,
    } = opts;
    const {
      matchNameFn,
      matchRawFn,
    } = this.schema

    if (name !== null) {
      const result = await this.supabaseClient.supabase.rpc(matchNameFn, {
        query_name: name,
        query_embedding: embedding, // Pass the embedding you want to compare
        match_threshold, // Choose an appropriate threshold for your data
        match_count, // Choose the number of matches
      });
      // const {
      //   data: memories,
      // } = result;
      const memories = formatMemories(result.data);
      return memories;
    } else {
      const result = await this.supabaseClient.supabase.rpc(matchRawFn, {
        query_embedding: embedding, // Pass the embedding you want to compare
        match_threshold, // Choose an appropriate threshold for your data
        match_count, // Choose the number of matches
      });
      // const {
      //   data: memories,
      // } = result;
      const memories = formatMemories(result.data);
      return memories;
    }
  }
  async upsertRawMemory(rawMemory) {
    // memory = await MemoriesManager.bakeMemory(memory);
    const {
      tableName,
      // nameKey,
    } = this.schema;

    // const {
    //   id,
    //   role,
    //   content,
    // } = memory.getRaw();
    // const {
    //   name,
    // } = memory;
    // const newMemory = {
    //   id,
    //   role,
    //   content,
    //   name,
    // };
    // console.log('new memory', {
    //   memory,
    //   newMemory,
    // });
    const result = await this.supabaseClient.supabase
      .from(tableName)
      .upsert(rawMemory);
    // console.log('upsert result', result);
    const {
      error,
    } = result;
    if (error) {
      console.error('upsert error', error);
      throw new Error(JSON.stringify(error));
    }
  }
  async removeMemory(memoryId) {
    const {
      tableName,
      // nameKey,
    } = this.schema;

    // remove item
    const result = await this.supabaseClient.supabase
      .from(tableName)
      .delete()
      .eq('id', memoryId);
    // console.log('remove result', result);
    const {
      error,
    } = result;
    if (error) {
      console.error('remove error', error);
      throw new Error(JSON.stringify(error));
    }
  }
}
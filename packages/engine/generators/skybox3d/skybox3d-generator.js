import {
  // img2img,
  generateImage,
  setSdModel,
} from '../../generate-image.js';
import {
  generateSkyboxFull,
  skyboxStyleNames,
  defaultSkyboxStyleName,
} from '../../clients/blockade-labs-client.js';
import {
  getDepthField,
} from '../../clients/depth-client.js';
import {
  backgroundModel,
  backgroundLora,

  backgroundPrompt,
  backgroundNegativePrompt,
} from '../../constants/model-constants.js';

import {
  makePromise,
  blob2img,
  downloadFile,
} from '../../util.js';

//


const getFileName = u => u.match(/[^\/]+$/)[0];
const copyBlockadeLabsFileToLocal = async (file_url, suffix = '') => {
  file_url = new URL(file_url);
  file_url.protocol = 'https:';
  file_url.host = aiProxyHost;
  file_url.pathname = '/api/ai/blockadelabs' + file_url.pathname;
  file_url = file_url.href;

  const res = await fetch(file_url);
  const blob = await res.blob();

  file_url = new URL(file_url);
  file_url.pathname += suffix;
  file_url = file_url.href;
  const fileName = getFileName(file_url);

  return await writeMirrorFile(fileName, blob);
};

export const generateSkybox3D = async ({
  prompt,
  imageUrl,
  depthMapFile,
}) => {
  const generateSkyboxFromFiles = async ({
    imageFile,
    depthMapFile,
  }) => {
    const panel = storyboard.addPanelFromFile(file);
    // wait for file add
    {
      const p = makePromise();
      const onbusyupdate = e => {
        if (!panel.isBusy()) {
          cleanup();
          // setBusy(panel.isBusy());
          // setBusyMessage(panel.getBusyMessage());
          p.resolve();
        }
      };
      panel.addEventListener('busyupdate', onbusyupdate);
      const cleanup = () => {
        panel.removeEventListener('busyupdate', onbusyupdate);
      };
      await p;
    }
    // compile
    console.log('compile 1', {
      panel,
      storyboard,
    });
    await panel.compile();
    console.log('compile 2', {
      panel,
      storyboard,
    });

    const imageArrayBuffer = panel.getLayer(0).getData('image');
    const imageBlob = new Blob([imageArrayBuffer], {
      type: 'image/png',
    });

    const uint8Array = await storyboard.exportAsync();
    console.log('compile 3', {
      panel,
      storyboard,
      uint8Array,
    });
    const zineBlob = new Blob([
      zineMagicBytes,
      uint8Array,
    ], {
      type: 'application/octet-stream',
    });
    // downloadFile(resultBlob, 'storyboard.zine');

    return {
      imageBlob,
      zineBlob,
    };
  };
  const generateSkyboxFromPrompt = async ({
    prompt,
  }) => {
    const skyboxResult = await generateSkyboxFull(imagePrompt);
    // console.log('got skybox result', skyboxResult);
    const {
      file_url,
      depth_map_url,
    } = skyboxResult;

    const [
      fileUrl,
      depthMapUrl,
    ] = await Promise.all([
      copyBlockadeLabsFileToLocal(file_url, '_diffuse'),
      copyBlockadeLabsFileToLocal(depth_map_url, '_depth'),
    ]);

    const skybox3DItem = {
      description: prompt,
      fileUrl,
      depthMapUrl,
    };
    return skybox3DItem;
  };

  if (imageUrl && depthMapUrl) {
    const result = await generateSkyboxFromFiles({
      fileUrl,
      depthMapUrl,
    });
    return result;
  } else {
    const skyboxResult = await generateSkyboxFull(prompt, {
      enhance: true,
    });
    const {
      file_url,
      depth_map_url,
    } = skyboxResult;

    return {
      description: prompt,
      fileUrl: file_url,
      depthMapUrl: depth_map_url,
    };
  }
};
export const batchGenerateSkybox3Ds = async (files) => {
  // XXX
};

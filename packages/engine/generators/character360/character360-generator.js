import {
  removeBackground,
} from '../../clients/background-removal-client.js';
import * as vqa from '../../vqa.js';
import {
  generate360Views,
  drawSlices,
} from '../../clients/zero123-client.js';
import {
  generateCharacterAuxFromFile,
  generateCharacterBaseFromPrompt,
} from '../character/character-generator.js';
import {
  // generateImage,
  img2img,
  interrogateDeepBooru,
  setSdModel,
} from '../../generate-image.js';
// import {
//   squareize,
//   makeMaskCanvas,
// } from '../character/character-generator.js';
import {
  squareize,
  makeMaskCanvas,
  makeGradientCanvas,
  opacify,
  makeNoiseBaseCanvas,
} from '../../utils/canvas-utils.js';
// import {
//   downloadFile,
// } from '../../util.js';
import {
  characterPrompt,
  characterLora,
  characterNegativePrompt,
} from '../../constants/model-constants.js';

import {
  blob2DataUrl,
} from '../../util.js';
import {
  zbencode,
  zbdecode,
} from '../../../zjs/encoding.mjs';

//

const characterMagicBytes = 'CHAR';

//

const preprocessCharacterImageFile = async file => {
  const prompt = await interrogateDeepBooru(file);
  const generatedImageBlob = await squareize(file, 512, 0.1);
  return {
    prompt,
    generatedImageBlob,
  };
};
const generateCharacterFromFile = async (generatedImageBlob, prompt, gender, debug, setGenerationStatus) => {
  // console.log('generate character from file', {
  //   generatedImageBlob,
  //   prompt,
  // });

  const character360Result = await generateCharacterAuxFromFile(generatedImageBlob, prompt, debug, setGenerationStatus);;
  return character360Result;

  // const {
  //   characterImageBlob,
  //   character360ImageBlob,
  //   characterEmotionBlob,
  // } = character360Result;

  // const characterImageUrl = await blob2DataUrl(characterImageBlob);
  // const character360ImageUrl = await blob2DataUrl(character360ImageBlob);
  // const characterEmotionUrl = await blob2DataUrl(characterEmotionBlob);

  // // zbencode the result
  // const o = {
  //   description: prompt,
  //   gender,
  //   characterImageUrl,
  //   character360ImageUrl,
  //   characterEmotionUrl,
  // };
  // return o;

  // console.log('got o', o);
  // const uint8Array = zbencode(o);
  // console.log('got zbencode result', {
  //   uint8Array,
  // });

  // const resultBlob = new Blob([
  //   characterMagicBytes,
  //   uint8Array,
  // ], {
  //   type: 'application/octet-stream',
  // });
  // // downloadFile(blob, 'character.itemb');
  // return resultBlob;

  // const id = crypto.randomUUID();
  // const description = prompt;
  // const itemItem = {
  //   id,
  //   name,
  //   content: {
  //     name,
  //     description,
  //     characterImageUrl,
  //     character360ImageUrl,
  //     characterEmotionUrl,
  //   },
  // };
  // await characterClient.addItem(itemItem);
};

//

export const generateCharacter = async ({
  prompt,
  gender,
  file,
  debug,
  setGenerationStatus
}) => {
  if (file) {
    // console.log('generate character from file', {file});
    // debugger;
    // const blob = file;

    if (!['image/png', 'image/jpeg'].includes(file.type)) {
      throw new Error('unsupported image type: ' + file.type);
    }
    setGenerationStatus('Preprocessing...');
    const {
      generatedImageBlob,
      prompt,
    } = await preprocessCharacterImageFile(file);
    setGenerationStatus('Generating...');
    const character360Result = await generateCharacterFromFile(generatedImageBlob, prompt, gender, debug, setGenerationStatus);
    return character360Result;

    // // const foregroundBlob = await removeBackground(blob);
    // const size = 512;
    // const paddingFactor = 0.1;
    // const squareBlob = await squareize(blob, size, paddingFactor);
    // const squareImage = await blob2img(squareBlob);
    // squareImage.style.cssText = `\
    //   position: absolute;
    //   top: 0;
    //   left: 0;
    //   width: 512px;
    //   height: 512px;
    //   z-index: 1;
    // `;
    // document.body.appendChild(squareImage);

    // const slices = await generate360Views(squareBlob, {
    //   debug: true,
    // });
    // const canvas = drawSlices(slices);

    // // export frame canvas
    // const item360Blob = await new Promise((accept, reject) => {
    //   canvas.toBlob(accept, 'image/png');
    // });
    // const item360ImageUrl = await blob2DataUrl(item360Blob);

    // const itemImageUrl = await blob2DataUrl(squareBlob);

    // const id = crypto.randomUUID();
    // const description = boardTags;
    // const itemItem = {
    //   id,
    //   name,
    //   content: {
    //     description,
    //     itemImageUrl,
    //     item360ImageUrl,
    //     scale: 1, // XXX compute the real scale via LLM
    //   },
    // };
    // await characterClient.addItem(itemItem);
  } else {
    // const fullPrompt = [
    //   gender === 'male' ? '1boy' : '1girl',
    //   clothes,
    //   expression,
    //   'white background',
    // ].filter(s => !!s).join(', ');
    const fullPrompt = (characterLora ? (characterLora + ' ') : '') + [
      prompt,
      // gender === 'male' ? '1boy' : '1girl',
      gender,
      characterPrompt,
    ].join(', ');
    console.log('generate from prompt', {prompt, gender, fullPrompt});

    const generatedImageBlob = await generateCharacterBaseFromPrompt({
      prompt: fullPrompt,
      negativePrompt: characterNegativePrompt,
      gender,
    });
    const character360Result = await generateCharacterFromFile(generatedImageBlob, prompt, gender, debug, setGenerationStatus);
    return character360Result;
  }
};
export const batchGenerateCharacters = async (files) => {
  // console.log('batch generate characters', {files});

  for (const file of files) {
    const {
      generatedImageBlob,
      prompt,
    } = await preprocessCharacterImageFile(file);
    await generateCharacterFromFile(generatedImageBlob, prompt, gender, debug, setGenerationStatus);
  }
};

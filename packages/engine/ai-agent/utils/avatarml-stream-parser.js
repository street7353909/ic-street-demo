// import * as sentenceSplitter from 'sentence-splitter';
import { jsonParse } from '../../util.js';
import {
  aiMessageTypes,
} from '../../managers/lore/messages.jsx';

/* class ResultContext {
  constructor({
    skills,
  }) {
    this.resultQueue = [];

    const handleMessage = (character, command, commandArgument, message) => {
      this.resultQueue.push({
        character,
        command,
        commandArgument,
        message,
      });
    };
    const processSentence = (character, command, sentence) => {
      if (sentence.type === 'Sentence') { // ignore Whitespace, Punctuation
        const text = sentence.raw.trim();
        handleMessage(character, command, text);
      }
    };
    const processSentenceText = (character, command, value, setValue, final) => {
      // split sentences
      const rawSentences = value.split('\n');

      // note: will assign back texts from segmented sentences, so need to add back `\n`, let both `abc\n` and `abc\ndef` unchanged.
      const loopCount = value.endsWith('\n') ? rawSentences.length : rawSentences.length - 1;
      for (let i = 0; i < loopCount; i++) {
        rawSentences[i] += '\n';
      }

      const sentences = rawSentences.flatMap(rawSentence => sentenceSplitter.split(rawSentence));
      
      // process all sentences except the last one
      while (sentences.length > 1) {
        processSentence(character, command, sentences.shift());
      }

      if (!final) { // if we're not done, set the value to the pending sentence
        const newValue = sentences.length > 0 ? sentences[0].raw.replace(/^\s+$/, '') : '';
        setValue(newValue);
      } else { // if we're done, flush the last sentence
        if (sentences.length > 0) {
          processSentence(character, command, sentences[0]);
        }
        setValue('');
      }
    };
    this.processResult = ({
      character,
      command,
      value,
      final,
      setValue,
    }) => {
      const skill = skills.get(command);
      if (skill) {
        if (skill.spec.progressive === 'sentence') {
          processSentenceText(character, command, value, setValue, final);
        } else {
          if (final) {
            handleMessage(character, command, value);
          }
        }
      } else {
        console.warn('LLM generated unknown skill: ' + JSON.stringify(command));
        debugger;
      }
    };
  }
  flush() {
    const oldResults = this.resultQueue;
    this.resultQueue = [];
    return oldResults;
  }
} */

//

const jsonParseMessage = s => {
  const j = jsonParse(`[${s}]`);
  if (j) {
    if (
      j.length === 4 &&
      typeof j[0] === 'string' &&
      typeof j[1] === 'string' &&
      Array.isArray(j[2]) && j[2].every(a => ['string', 'number', 'boolean'].includes(typeof a) || a === null) &&
      typeof j[3] === 'string'
    ) {
      return j;
    } else if (
      j.length === 3 &&
      typeof j[0] === 'string' &&
      typeof j[1] === 'string' &&
      Array.isArray(j[2]) && j[2].every(a => ['string', 'number', 'boolean'].includes(typeof a) || a === null)
    ) {
      return j;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

//

export class Parser {
  constructor() {
    this.text = '';
  }

  #parseFullMatch(match) {
    const [name = '', command = '', args = '', message = ''] = match;

    return {
      name,
      command,
      args,
      message,
    };
  }
  #parseChatMatch(match) {
    const [_, name = '', message = ''] = match;
    const command = 'CHAT';
    const args = [];

    return {
      name,
      command,
      args,
      message,
    };
  }

  write(s) {
    this.text += s;

    const events = [];

    for (;;) {
      let match;
      if (match = this.text.match(/^(.*?)\n/)) {
        const match2 = jsonParseMessage(match[1]);
        const e = this.#parseFullMatch(match2);
        events.push(e);
        this.text = this.text.slice(match[0].length);
      } else if (match = this.text.match(/^(.+?): (.*?)\n/)) {
        const e = this.#parseChatMatch(match);
        events.push(e);
        this.text = this.text.slice(match[0].length);
      } else {
        break;
      }
    }

    return events;
  }
  end() {
    const events = [];
    
    for (;;) {
      let match;
      let match2;
      if (match2 = jsonParseMessage(this.text)) {
        const e = this.#parseFullMatch(match2);
        events.push(e);
        this.text = '';
      } else if (match = this.text.match(/^(.+?): (.*?)$/)) {
        const e = this.#parseChatMatch(match);
        events.push(e);
        this.text = this.text.slice(match[0].length);
      } else {
        break;
      }
    }

    return events;
  }
  parse(s) {
    const events = [
      ...this.write(s),
      ...this.end(),
    ];
    return events;
  }
  // validate(s) {
  //   return /^:(.+)::(.+):::(.*)\n/.test(s) ||
  //     /^(.+?): (.*?)\n/.test(s);
  // }

  format({
    name,
    command,
    args,
    message,
  }) {
    if (aiMessageTypes[command]) {
      return JSON.stringify([
        name,
        command,
        args,
        message,
      ]).slice(1, -1);
    } else {
      return `${name}: ${message}`;
    }
  }
}

/* const tests = [
  () => {
    const p = new Parser();
    const events = [
      ...p.write(`:Stevia::SAY=lol:::I'm feeling happy.`),
      ...p.end(),
    ];
    // console.log('got events', events[0]);
    console.assert(events.length === 1);
  },
  () => {
    const p = new Parser();
    const events = [
      ...p.write(`:Stevia::SAY:::I'm feeling happy.\n:lol`),
      ...p.end(),
    ];
    // console.log('got events', events[0]);
    console.assert(events.length === 1);
  },
  () => {
    const p = new Parser();
    const events = [
      ...p.write(`:Stevia::SAY:::I'm feeling happy.\n:Stevia::SAY:::I'm feeling happy.\n:Stevia::SAY:::I'm feeling happy.`),
      ...p.end(),
    ];
    // console.log('got events', events[0]);
    console.assert(events.length === 3);
  },
];
const testParser = () => {
  for (const test of tests) {
    test();
  }
  console.log('all tests passed!');
};
globalThis.testParser = testParser; */

//

const nop = () => {};
export class AvatarMLStreamParser extends TransformStream {
  constructor({
    // skills,
    onMessage = nop,
    onData = nop,
    // signal,
  }) {
    const parser = new Parser({
      // skills,
    });
    const flushEvents = events => {
      for (let i = 0; i < events.length; i++) {
        const event = events[i];
        onMessage(event);
      }
    };

    super({
      transform: (chunk/*, controller*/) => {
        onData(chunk);

        // console.log('write chunk', chunk);
        const events = parser.write(chunk);
        // console.log('got parser events', events);
        flushEvents(events);
      },
      flush(/*controller*/) {
        const events = parser.end();
        flushEvents(events);
      },
    });
  }
}
// globalThis.AvatarMLStreamParser = AvatarMLStreamParser;
import {
  aiProxyHost,
} from './endpoints.js';

//

export const embeddingDimensions = 768;

// export const embeddingZeroVector = new Float32Array(embeddingDimensions);
export const embeddingZeroVector = Array(embeddingDimensions).fill(0);

export const embed = async (s, {
  signal,
} = {}) => {
  const fd = new FormData();
  fd.append('s', s);
  const response = await fetch(`https://${aiProxyHost}/embedding`, {
    method: 'POST',
    body: fd,
    signal,
  });
  const j = await response.json();
  return j;
};

export const split = async (s, {
  signal,
} = {}) => {
  const fd = new FormData();
  fd.append('s', s);
  const response = await fetch(`https://${aiProxyHost}/split`, {
    method: 'POST',
    body: fd,
    signal,
  });
  const j = await response.json();
  return j;
};
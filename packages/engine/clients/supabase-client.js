import { createClient } from '@supabase/supabase-js';
import jwt_decode from 'jwt-decode';
import { supabaseEndpointUrl } from '../endpoints.js';
import { SUPABASE_PUBLIC_API_KEY } from '../constants/auth.js';
import { getDefaultUser, getDefaultPlayerSpec } from '../../../packages/engine/managers/account/account-manager.js';
import { getEthereumAccountDetails } from '../../../pages/components/metamask-auth-ui/MetamaskAuthUi.jsx';
import { plugLogin, stoicLogin, nfidLogin, identityLogin } from '../../../pages/components/ic-wallet-auth-ui/ICWalletAuthUi.jsx';

const makeSupabase = (jwt) => {
  let supabase;
  if (jwt) {
    supabase = createClient(
      supabaseEndpointUrl,
      SUPABASE_PUBLIC_API_KEY,
      { global: { headers: { Authorization: `Bearer ${jwt}` } } }
    );
  } else {
    supabase = createClient(
      supabaseEndpointUrl,
      SUPABASE_PUBLIC_API_KEY,
    );
  }
  return supabase;
};

const makeDefaultProfile = (loaded = false) => ({
  loaded,
  sessionUserId: '',
  provider: '',
  ethereumAccountDetails: null,
  user: loaded ? getDefaultUser() : null,
  isDefaultUser: true,
});

export class SupabaseClient extends EventTarget {
  constructor({ localStorageManager } = {}) {
    super();

    this.localStorageManager = localStorageManager;

    const jwt = localStorageManager.getJwt();
    this.supabase = makeSupabase(jwt);
    this.profile = makeDefaultProfile();
    this.authListener = null;

    this.#listen();
  }

  #listen() {
    // bind jwt update
    const _clearClient = () => {
      this.supabase.realtime.disconnect();
      this.supabase = null;
    };
    const jwtupdate = (e) => {
      const { jwt } = e.data;

      _clearClient();

      this.supabase = makeSupabase(jwt);
      console.log('update supabase', jwt);

      this.dispatchEvent(new MessageEvent('clientupdate', {
        data: {
          client: this.supabase,
        },
      }));
    };
    this.localStorageManager.addEventListener('jwtupdate', jwtupdate);

    this.cleanup = () => {
      this.localStorageManager.removeEventListener('jwtupdate', jwtupdate);
      _clearClient();
    };

    // bind profile update
    const setDefaultProfile = (loaded) => {
      this.setProfile(makeDefaultProfile(loaded));
    };
    const updateUser = async ({ id, address, provider }) => {
      try {
        const user = await (async () => {
          const { data, error } = await this.supabase
            .from('accounts')
            .select('*')
            .eq('id', id)
            .maybeSingle();

          if (!error) {
            if (!data) {
              const user = getDefaultUser(id);

              const { error } = await this.supabase
                .from('accounts')
                .insert(user);
              if (!error) {
                return user;
              } else {
                this.localStorageManager.deleteJwt();
                return null; // default user
              }
            } else {
              return data;
            }
          } else {
            // console.warn('got error', error);
            throw error;
          }
        })();

        if (user !== null) {
          if (!user.playerSpec) {
            user.playerSpec = getDefaultPlayerSpec();
          }

          let ethereumAccountDetails;
          if (provider === 'metamask') {
            // Only call getEthereumAccountDetails if the address format is correct
            if (/^0x[a-fA-F0-9]{40}$/.test(address)) {
              ethereumAccountDetails = await getEthereumAccountDetails(address);
            } else {
              // console.error('Invalid Ethereum address format:', address);
              // Handle invalid Ethereum address format
              // For example, set ethereumAccountDetails to null or some default value
              ethereumAccountDetails = null;
            }
          } else {
            // If the provider is not 'metamask', do not attempt to fetch Ethereum account details
            ethereumAccountDetails = null;
          }

          const newProfile = {
            ...this.profile,
            loaded: true,
            sessionUserId: id,
            address,
            user,
            provider,
            ethereumAccountDetails,
            isDefaultUser: false,
          };
          this.setProfile(newProfile);
        } else {
          setDefaultProfile(true);
        }
      } catch(err) {
        console.warn('update user error', err);
        setDefaultProfile(true);
      }
    };

    const updateProfile = () => {
      if (this.authListener) {
        this.authListener.subscription.unsubscribe();
        this.authListener = null;
      }

      this.setProfile({
        ...this.profile,
        loaded: false,
      });

      const { data: authListener } = this.supabase.auth.onAuthStateChange((event, session) => {
        if (event === 'INITIAL_SESSION') {
          if (!this.profile?.loaded) {
            if (session) {
              session.user.id !== this.profile?.sessionUserId && updateUser({
                id: session.user.id,
                address: '',
                provider: session.user.app_metadata.provider,
              });
            } else {
              const jwtString = this.supabase.auth.headers?.['Authorization']?.replace(/^Bearer\s+/i, '');
              const jwtResult = jwtString ? jwt_decode(jwtString) : null;
              const id = jwtResult?.id;
              const address = jwtResult?.address;

              if (id && address) {
                id !== this.profile?.sessionUserId && updateUser({
                  id,
                  address,
                  provider: 'metamask',
                });
              } else {
                setDefaultProfile(true);
              }
            }
          }
        } else if (event === 'SIGNED_IN') {
          session.user.id !== this.profile?.sessionUserId && updateUser({
            id: session.user.id,
            address: '',
            provider: session.user.app_metadata.provider,
          });
        } else if (event === 'SIGNED_OUT') {
          this.profile?.sessionUserId && setDefaultProfile(true);
        }
      });
      this.authListener = authListener;
    };
    updateProfile();
    this.addEventListener('clientupdate', (e) => {
      updateProfile();
    });
  }
  getProfile() {
    return this.profile;
  }

  setProfile(profile) {
    this.profile = profile;
    this.dispatchEvent(new MessageEvent('profileupdate', {
      data: {
        profile,
      },
    }));
  }

  async refresh() {
    const sessionUserId = this.profile?.sessionUserId;
    if (sessionUserId) {
      const { data, error } = await this.supabase
        .from('accounts')
        .select('*')
        .eq('id', sessionUserId)
        .maybeSingle();
      
      if (!error) {
        this.setProfile({
          ...this.profile,
          user: data,
        });
      }
    }
  }

  async updateUser(spec) {
    const sessionUserId = this.profile?.sessionUserId;
    if (sessionUserId) {
      const newUser = typeof spec === 'function' ? spec(this.profile?.user) : {...this.profile?.user, ...spec};
      await this.supabase.from('accounts').upsert(newUser);
      this.setProfile({...this.profile, user: newUser});
    } else {
      throw new Error('not logged in');
    }
  }

  // Method to handle IC wallet login
  async loginWithICWallet(walletType, whitelist = []) {
    try {
      let userObject;
      switch (walletType) {
        case 'Plug':
          userObject = await plugLogin(whitelist);
          break;
        case 'Stoic':
          userObject = await stoicLogin(whitelist);
          break;
        case 'NFID':
          userObject = await nfidLogin();
          break;
        case 'Internet Identity':
          userObject = await identityLogin();
          break;
        // Add other cases if needed
        default:
          throw new Error(`Unsupported wallet type: ${walletType}`);
      }

      this.updateUserProfileWithIC(userObject);
    } catch (error) {
      console.error('IC wallet login error:', error);
      // Handle errors, e.g., reset the profile or show an error message
    }
  }

  // Method to update the user profile with IC wallet login details
  updateUserProfileWithIC(userObject) {
    const { principal, provider } = userObject;

    const newProfile = {
      ...this.profile,
      loaded: true,
      sessionUserId: principal, // Using principal similarly to Ethereum address
      provider,
      // Include other details as necessary
      isDefaultUser: false,
    };

    this.setProfile(newProfile);
  }

  destroy() {
    if (this.authListener && this.authListener.subscription) {
      this.authListener.subscription.unsubscribe();
    }
    if (this.supabase) {
      this.supabase.realtime.disconnect();
    }
  }
}
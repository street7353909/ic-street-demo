const backgroundRemoverEndpoint = `https://ai-proxy.upstreet.ai/backgroundRemover`;
export const removeBackground = async (imageBlob) => {
  const res = await fetch(backgroundRemoverEndpoint, {
    method: 'POST',
    body: imageBlob,
  });
  if (res.ok) {
    const resultBlob = await res.blob();
    return resultBlob;
  } else {
    const text = await res.text();
    throw new Error('background removal error: ' + text);
  }
};

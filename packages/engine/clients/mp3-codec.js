import lamejs from '@jambonz/lamejs';

//

const convertFloat32toInt16 = (float32Buffer) => {
  const int16Array = new Int16Array(float32Buffer.length);
  for (let i = 0; i < float32Buffer.length; i++) {
    const s = Math.min(Math.max(float32Buffer[i], -1), 1);
    int16Array[i] = s < 0 ? s * 0x8000 : s * 0x7FFF;
  }
  return int16Array;
};

//

// note: assumes mono
export const makeMp3Stream = (mediaStream, {
  audioContext,
}) => {
  const {sampleRate} = audioContext;
  // const numberOfChannels = 1;
  const bitrate = 128000; // 128 kbps
  const bitrateKbps = bitrate / 1000;
  // const bitrate = 64000; // 64 kbps

  // create nodes
  const mediaSteamNode = audioContext.createMediaStreamSource(mediaStream);
  const audioWorkletNode = new AudioWorkletNode(
    audioContext,
    'ws-input-worklet'
  );
  const transformStream = new TransformStream();
  const transformStreamWriter = transformStream.writable.getWriter();

  //

  // const mp3Data = [];

  const mp3encoder = new lamejs.Mp3Encoder(1, sampleRate, bitrateKbps);
  // const samples = new Int16Array(sampleRate); //one second of silence replace that with your own samples
  // let mp3Tmp = mp3encoder.encodeBuffer(samples); //encode mp3

  // //Push encode buffer to mp3Data variable
  // mp3Data.push(mp3Tmp);

  // // Get end part of mp3
  // mp3Tmp = mp3encoder.flush();

  // // Write last data to the output data, too
  // // mp3Data contains now the complete mp3Data
  // mp3Data.push(mp3Tmp);

  // console.debug(mp3Data);

  /* // create the encoder
  let uint8ArrayBufferCache = null;
  const ensureBuffer = (requiredByteLength) => {
    if (uint8ArrayBufferCache === null || uint8ArrayBufferCache.byteLength < requiredByteLength) {
      uint8ArrayBufferCache = new Uint8Array(requiredByteLength);
    }
    return uint8ArrayBufferCache;
  };
  const encoder = new AudioEncoder({
    output: (encodedAudioChunk, decoderConfig) => {
      // ensure buffer
      const buffer = ensureBuffer(encodedAudioChunk.byteLength);
      // copy to buffer
      encodedAudioChunk.copyTo(buffer);
      // write buffer to stream
      transformStreamWriter.write(
        buffer.slice(0, encodedAudioChunk.byteLength)
      );
    },
    error: err => {
      console.warn(err.stack);
    },
  });
  encoder.configure({
    // supported codecs: ['opus', 'vorbis', 'wav']
    // codec: 'opus',
    codec: 'mp3',
    numberOfChannels,
    sampleRate,
    bitrate,
  }); */

  // read the output of the worklet and push it to the encoder
  let timestamp = 0;
  audioWorkletNode.port.onmessage = event => {
    const {
      data: float32Array,
    } = event;

    // console.log('got float32 array', float32Array);

    // create AudioData from the buffer
    // const audioData = new AudioData({
    //   format: 'f32',
    //   numberOfChannels,
    //   numberOfFrames: data.length,
    //   sampleRate,
    //   data,
    //   timestamp,
    // });
    // encoder.encode(audioData);
    const int16Array = convertFloat32toInt16(float32Array);
    const mp3Data = mp3encoder.encodeBuffer(int16Array);
    const mp3Data2 = new Uint8Array(mp3Data.buffer, mp3Data.byteOffset, mp3Data.byteLength);
    // console.log('encode buffer', mp3Data2);
    transformStreamWriter.write(mp3Data2);

    // advance the timestamp based on the sample rate and number of frames
    timestamp += float32Array.length / sampleRate;
  };

  // connect
  mediaSteamNode.connect(audioWorkletNode);
  audioWorkletNode.connect(audioContext.gain);

//   console.log('connect media stream node', mediaSteamNode);

  // return output stream
  const outputStream = transformStream.readable;
  outputStream.close = () => {
    // disconnect the processing pipeline
    mediaSteamNode.disconnect();
    audioWorkletNode.disconnect();
    // flush the stream
    const mp3Data = mp3encoder.flush();
    // console.log('flush stream', {mp3Data});
    const mp3Data2 = new Uint8Array(mp3Data.buffer, mp3Data.byteOffset, mp3Data.byteLength);
    transformStreamWriter.write(mp3Data2);
    // end the stream
    transformStreamWriter.close();
  };
  return outputStream;
}
import * as THREE from 'three';
import loaders from './loaders.js';
import {
  addDefaultLights,
} from './util.js';

//

const screenhotRenderers = {
  vrm: async (renderer, scene, camera, f) => {
    const srcUrl = URL.createObjectURL(f);
    const res = await fetch(srcUrl);
    const arrayBuffer = await res.arrayBuffer();
    URL.revokeObjectURL(srcUrl);

    const {
      gltfLoader,
    } = loaders;

    const gltf = await new Promise((accept, reject) => {
      gltfLoader.parse(arrayBuffer, srcUrl, accept, reject);
    });
  
    const headBone = gltf.userData.vrm.humanoid.humanBones.head.node;
    const position = new THREE.Vector3().setFromMatrixPosition(headBone.matrixWorld);
    const quaternion = new THREE.Quaternion().setFromRotationMatrix(headBone.matrixWorld);
  
    camera.position.copy(position)
      .add(
        new THREE.Vector3(0, 0, -0.4)
          .applyQuaternion(quaternion)
      );
    camera.lookAt(position);
    camera.updateMatrixWorld();
  
    addDefaultLights(scene);
    scene.add(gltf.scene);
    scene.updateMatrixWorld();
  
    //
  
    renderer.render(scene, camera);

    //

    const canvas = renderer.domElement;
    const blob = await new Promise((accept, reject) => {
      canvas.toBlob(accept, 'image/png');
    });
    return blob;
  },
  glb: async (renderer, scene, camera, f) => {
    const srcUrl = URL.createObjectURL(f);
    const res = await fetch(srcUrl);
    const arrayBuffer = await res.arrayBuffer();
    URL.revokeObjectURL(srcUrl);

    const {
      gltfLoader,
    } = loaders;

    const gltf = await new Promise((accept, reject) => {
      gltfLoader.parse(arrayBuffer, srcUrl, accept, reject);
    });

    // Get bounding box
    scene.add(gltf.scene);
    gltf.scene.updateMatrixWorld();
    const bbox = new THREE.Box3().setFromObject(gltf.scene);
    const center = new THREE.Vector3();
    bbox.getCenter(center);
    console.log('got bbox', bbox, gltf.scene);
    
    // Get dimensions
    const size = new THREE.Vector3();
    bbox.getSize(size);
    
    // Calculate distance
    const sides = ['x', 'y', 'z'];
    const sortedSides = sides.slice().map(side => {
      return {
        side,
        size: size[side],
      };
    }).sort((a, b) => {
      return b.size - a.size;
    }).map(side => side.side);
    const sortedSides2 = sortedSides.slice(0, 2);

    const largestSide = sortedSides[0];
    const largestSideValue = size[largestSide];
    if (
      sortedSides2.includes('x') && sortedSides2.includes('y')
    ) {
      camera.position.set(center.x, center.y, center.z + 1);
      camera.lookAt(center);
    }
    if (
      sortedSides2.includes('x') && sortedSides2.includes('z')
    ) {
      camera.position.set(center.x, center.y + 1, center.z);
      camera.lookAt(center);
    }
    if (
      sortedSides2.includes('y') && sortedSides2.includes('z')
    ) {
      camera.position.set(center.x + 1, center.y, center.z);
      camera.lookAt(center);
    }

    // move the camera back to see the whole bounding box
    // Calculate distance
    const fov = camera.fov; // in degrees
    // const aspect = camera.aspect; // aspect ratio
    // const near = camera.near; // near clipping plane distance
    // const far = camera.far; // far clipping plane distance

    // Use similar triangles to find the distance the camera needs to be
    let distance = (0.5 * largestSideValue) / Math.tan((fov / 2) * (Math.PI / 180));
    distance *= 1.1;

    // Calculate the new camera position
    const newPosition = new THREE.Vector3();
    newPosition.copy(center); // start at the center of the bounding box
    newPosition.addScaledVector(camera.position.clone().sub(center).normalize(), distance); // move back along the line between the center and the camera

    // Update camera position
    camera.position.copy(newPosition);

    // set up the scene
    addDefaultLights(scene);
    camera.updateMatrixWorld();
    // camera.updateProjectionMatrix();
    scene.updateMatrixWorld();
  
    //
  
    renderer.render(scene, camera);

    //

    const canvas = renderer.domElement;
    const blob = await new Promise((accept, reject) => {
      canvas.toBlob(accept, 'image/png');
    });
    return blob;
  },
  image: async (renderer, scene, camera, f) => {
    const imageBitmap = await createImageBitmap(f);

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = screenshotSize;
    canvas.height = screenshotSize;

    // contain the image, without clipping. this may add whitespace at the top/bottom or left/right
    const scale = Math.min(
      canvas.width / imageBitmap.width,
      canvas.height / imageBitmap.height,
    );
    const scaledWidth = imageBitmap.width * scale;
    const scaledHeight = imageBitmap.height * scale;
    const scaledX = (canvas.width - scaledWidth) / 2;
    const scaledY = (canvas.height - scaledHeight) / 2;
    ctx.drawImage(imageBitmap, scaledX, scaledY, scaledWidth, scaledHeight);

    const blob = await new Promise((accept, reject) => {
      canvas.toBlob(accept, 'image/png');
    });
    return blob;
  },
  audio: async (renderer, scene, camera, f) => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = screenshotSize;
    canvas.height = screenshotSize;

    const img = new Image();
    img.src = '/images/audio-inv.svg';
    img.crossOrigin = 'Anonymous';
    await new Promise((accept, reject) => {
      img.addEventListener('load', accept);
      img.addEventListener('error', reject);
    });

    const scale = Math.min(
      canvas.width / img.width,
      canvas.height / img.height,
    );
    const scaledWidth = img.width * scale;
    const scaledHeight = img.height * scale;
    const scaledX = (canvas.width - scaledWidth) / 2;
    const scaledY = (canvas.height - scaledHeight) / 2;
    ctx.drawImage(img, scaledX, scaledY, scaledWidth, scaledHeight);

    const blob = await new Promise((accept, reject) => {
      canvas.toBlob(accept, 'image/png');
    });
    return blob;
  },
  video: async (renderer, scene, camera, f) => {
    // read the first frame
    const video = document.createElement('video');
    video.src = URL.createObjectURL(f);
    video.crossOrigin = 'Anonymous';
    video.muted = true;
    // video.loop = true;
    // wait for first frame
    await new Promise((accept, reject) => {
      video.addEventListener('canplay', accept);
      video.addEventListener('error', reject);
    });
    video.play();

    // wait for first frame
    await new Promise((accept, reject) => {
      video.requestVideoFrameCallback(accept);
      video.addEventListener('error', reject);
    });
    video.pause();

    const canvas = document.createElement('canvas');
    canvas.width = screenshotSize;
    canvas.height = screenshotSize;
    const ctx = canvas.getContext('2d');

    const scale = Math.min(
      canvas.width / video.videoWidth,
      canvas.height / video.videoHeight,
    );
    const scaledWidth = video.videoWidth * scale;
    const scaledHeight = video.videoHeight * scale;
    const scaledX = (canvas.width - scaledWidth) / 2;
    const scaledY = (canvas.height - scaledHeight) / 2;
    ctx.drawImage(video, scaledX, scaledY, scaledWidth, scaledHeight);

    const blob = await new Promise((accept, reject) => {
      canvas.toBlob(accept, 'image/png');
    });
    return blob;
  },
};
const screenshotSize = 256;
export const getScreenshotBlob = async (f, ext) => {
  // snapshot the VRM preview screenshot
  const canvas = document.createElement('canvas');
  canvas.width = screenshotSize;
  canvas.height = screenshotSize;

  const renderer = new THREE.WebGLRenderer({
    canvas,
    alpha: true,
  });

  const scene = new THREE.Scene();
  // scene.background = new THREE.Color(0xEEEEEE);
  scene.autoUpdate = false;

  const camera = new THREE.PerspectiveCamera(75, canvas.width / canvas.height, 0.1, 1000);

  //

  const screenshotRendererFn = screenhotRenderers[ext];
  if (screenshotRendererFn) {
    return await screenshotRendererFn(renderer, scene, camera, f);
  } else {
    throw new Error('unknown screenshot renderer: ' + ext);
  }
};
import * as THREE from 'three';
import * as THREEExtra from './three-extra.js';
// import React from 'react';
// import metaversefile from 'metaversefile';

globalThis.Metaversefile = {
  exports: {
    THREE,
    THREEExtra,
    // React,
    // metaversefile,
  },
};
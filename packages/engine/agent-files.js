import {
  aiProxyHost,
} from './endpoints.js';

export const uploadAiFile = async (blob, name) => {
  const u = `https://${aiProxyHost}/api/ai/files`;
  const fd = new FormData();
  fd.append('purpose', 'assistants');
  fd.append('file', blob, name);
  const res = await fetch(u, {
    method: 'POST',
    body: fd,
  });
  const fileJson = await res.json();
  const {id} = fileJson;
  return id;
};
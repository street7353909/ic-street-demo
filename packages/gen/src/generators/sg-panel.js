// import {
//   makeId,
// } from '../utils/id-utils.js';
import {
  mainImageKey,
  promptKey,
  compressedKey,
  layer0Specs,
  layer1Specs,
  layer2Specs,
  layerSpecs,
} from '../../../zine/zine-data-specs.js';
import {
  blob2img,
  canvas2blob,
  // img2ImageData,
  resizeImage,
} from '../utils/convert-utils.js';
import * as vqa from '../../../engine/vqa.js';
import {
  ImageAiClient,
} from '../clients/image-client.js';
import {
  compileVirtualScene,
  getDepth
} from './scene-generator.js';
import {
  panelSize,
} from '../../../zine/zine-constants.js';

//

//

// const vqaClient = new VQAClient();
const imageAiClient = new ImageAiClient();

const resizeBlob = async blob => {
  const img = await blob2img(blob);
  const canvas = resizeImage(img, panelSize, panelSize, {
    // mode: 'contain',
  });
  const blob2 = await canvas2blob(canvas);
  return blob2;
}

//

export class Panel extends EventTarget {
  constructor(zp) {
    super();

    // if (!zp) {
    //   console.warn('construct with bad zp', zp);
    //   debugger;
    // }

    this.zp = zp;

    this.runningTasks = [];
    this.abortController = new AbortController();

    this.#listen();
  }
  #unlisten;
  #listen() {
    const onupdate = e => {
      const {keyPath} = e.data;
      const opts = {
        data: {
          keyPath,
        },
      };
      this.dispatchEvent(new MessageEvent(e.type, opts));
      this.dispatchEvent(new MessageEvent('update', opts));
    };
    this.zp.addEventListener('layeradd', onupdate);
    this.zp.addEventListener('layerremove', onupdate);
    this.zp.addEventListener('layerupdate', onupdate);

    this.#unlisten = () => {
      this.zp.removeEventListener('layeradd', onupdate);
      this.zp.removeEventListener('layerremove', onupdate);
      this.zp.removeEventListener('layerupdate', onupdate);
    };
  }

  getLayers() {
    return this.zp.getLayers();
  }
  getLayer(index) {
    return this.zp.getLayer(index);
  }
  getOrCreateLayer(index) {
    for (let i = 0; i <= index; i++) {
      const layer = this.zp.getLayer(i);
      if (!layer) {
        this.zp.addLayer();
      }
    }
    const layer = this.zp.getLayer(index);
    return layer;
  }

  isEmpty() {
    // return !this.hasData(mainImageKey);
    // return this.zp.getLayers().length === 0;
    const layer0 = this.zp.getLayer(0);
    // return !layer0 || !layer0.matchesSpecs(layer0Specs);
    return !layer0;
  }
  getDimension() {
    const isCompressed = !!this.zp.getLayer(0)?.getData('compressed');
    const hasFullLayer1 = !!this.zp.getLayer(1)?.matchesSpecs(layer1Specs);
    // console.log('get dimension', [isCompressed, hasFullLayer1]);
    if (!isCompressed && hasFullLayer1) {
      return 3;
    } else {
      return 2;
    }
  }
  isBusy() {
    return this.runningTasks.length > 0;
  }
  getBusyMessage() {
    if (this.runningTasks.length > 0) {
      return this.runningTasks[0].message;
    } else {
      return '';
    }
  }

  async setFile(file, prompt) {
    file = await resizeBlob(file, panelSize, panelSize);
    const layer = this.zp.addLayer();
    layer.setData(compressedKey, false);
    await Promise.all([
      (async () => {
        console.log('set data file', file);
        const arrayBuffer = await file.arrayBuffer();
        layer.setData(mainImageKey, arrayBuffer);
      })(),
      // (async () => {
      //   if (!prompt) {
      //     prompt = await vqa.imageCaptioning(file);
      //   }
      //   layer.setData(promptKey, prompt);
      // })(),
    ]);
  }
  async setFromPrompt(prompt) {
    await this.task(async ({signal}) => {
      const blob = await imageAiClient.createImageBlob(prompt, {signal});
      await this.setFile(blob, prompt);
    }, 'generating image');
  }

  async compile() {
    await this.task(async ({signal}) => {
      const layer = this.zp.getLayer(0);
      const imageArrayBuffer = layer.getData(mainImageKey);
      const prompt = layer.getData(promptKey);
      const compileResultLayers = await compileVirtualScene({
        imageArrayBuffer,
        prompt,
      });
      console.log('got compile result layers', compileResultLayers);

      for (let i = 0; i < compileResultLayers.length && i < layerSpecs.length; i++) {
        const layerData = compileResultLayers[i];
        const layerSpec = layerSpecs[i];

        const layer = this.getOrCreateLayer(i);
        for (const name of layerSpec) {
          if (name in layerData) {
            const v = layerData[name];
            layer.setData(name, v);
          }
        }
      }
    }, 'compiling');
  }

  async task(fn, message) {
    const {signal} = this.abortController;

    const task = {
      message,
    };
    this.runningTasks.push(task);

    this.dispatchEvent(new MessageEvent('busyupdate', {
      data: {
        busy: this.isBusy(),
        message: this.getBusyMessage(),
      },
    }));

    try {
      await fn({
        signal,
      });
    } finally {
      const index = this.runningTasks.indexOf(task);
      this.runningTasks.splice(index, 1);
      
      this.dispatchEvent(new MessageEvent('busyupdate', {
        data: {
          busy: this.isBusy(),
          message: this.getBusyMessage(),
        },
      }));
    }
  }
  cancel() {
    this.abortController.abort(abortError);
  }
  destroy() {
    this.#unlisten();
    this.cancel();
  }
}
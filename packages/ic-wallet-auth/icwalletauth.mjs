import uuidByString from 'uuid-by-string';
import jwt from '@tsndr/cloudflare-worker-jwt';

// CORS headers to enable cross-origin requests
const headersObject = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "POST, OPTIONS",
  "Access-Control-Allow-Headers": "Content-Type",
  "Access-Control-Expose-Headers": "*",
  "Access-Control-Allow-Private-Network": "true"
};

export default {
  async fetch(request, env) {
    // Handle CORS preflight requests for compatibility with various HTTP clients
    if (request.method === 'OPTIONS') {
      return new Response(null, { headers: headersObject });
    }

    // Ensure that only POST requests are processed, enhancing security and conformity to REST standards
    if (request.method !== 'POST') {
      console.error('Error: Method not allowed', request.method);
      return new Response('Method Not Allowed', {
        status: 405,
        headers: { 'Content-Type': 'text/plain', ...headersObject },
      });
    }

    try {
      // Parse the request body to extract the principal ID
      const body = await request.json();
      const isAnon = request.url.includes('/anon');
      let principalId = null;

      // Validate principal ID for non-anonymous requests to ensure data integrity
      if (!isAnon) {
        principalId = body.principalId;
        if (typeof principalId !== 'string' || principalId.trim() === '') {
          console.error('Error: Invalid or missing principal ID', principalId);
          return new Response(JSON.stringify({ error: 'Invalid or missing principal ID' }), {
            status: 400,
            headers: { 'Content-Type': 'application/json', ...headersObject },
          });
        }
      }

      // Generate a UUID-based user ID from the principal ID
      const userId = principalId ? uuidByString(principalId) : body.id;

      // Construct JWT payload with user ID and authentication details
      const payload = {
        aud: 'authenticated',
        role: 'authenticated',
        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 7), // Token expiration set to 1 week for security
        id: userId,
      };

      // Include the principal address in the payload, if available
      if (principalId) {
        payload.address = principalId;
      }

      console.log('Payload for JWT:', payload);

      // Sign the payload with the Supabase JWT secret to generate the JWT token
      const jwtSignature = await jwt.sign(payload, env.SUPABASE_JWT);
      console.log('JWT Signature:', jwtSignature);

      // Return the signed JWT token as a response
      return new Response(JSON.stringify(jwtSignature), {
        headers: { 'Content-Type': 'application/json', ...headersObject },
      });
    } catch (e) {
      // Handle any internal server errors and log them for maintenance purposes
      console.error('Internal Server Error:', e);
      return new Response(JSON.stringify({ error: 'Internal Server Error', details: e.message }), {
        status: 500,
        headers: { 'Content-Type': 'application/json', ...headersObject },
      });
    }
  },
};
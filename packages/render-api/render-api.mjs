// import {
//   ethers,
// } from 'ethers';
// import {LRUCache} from 'lru-cache';
// import {RateLimiter} from 'limiter';
// import {
//   createClient,
// } from '@supabase/supabase-js';

import puppeteer from "@cloudflare/puppeteer";

//

const headers = [
  {
    "key": "Access-Control-Allow-Origin",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Methods",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Expose-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Private-Network",
    "value": "true"
  }
];
const headersObject = {};
for (const header of headers) {
  headersObject[header.key] = header.value;
}

// Cloudflare Worker
export default {
  async fetch(request, env, ctx) {
    // if OPTIONS, send headers
    if (request.method === 'OPTIONS') {
      return new Response(null, {
        headers: headersObject,
      });
    }

    //

    // ensure GET
    if (request.method === 'GET') {
      try {
        // parse the numeric ethereum token id (e.g. https://worker.dev/1)
        // const url = new URL(request.url);
        // const pathname = url.pathname;

        let match;
        if (match = pathname.match(/^\/render$/)) {
          // get the URL from the query string
          const url = new URL(request.url);
          const u = url.searchParams.get('u');

          const browser = await puppeteer.launch(env.MYBROWSER);
          const page = await browser.newPage();
          await page.goto(u);
          img = await page.screenshot();
          await browser.close();

          return new Response(img, {
            headers: {
              "content-type": "image/jpeg",
            },
          });
        } else {
          return new Response(null, {
            status: 404,
            statusText: 'Not Found',
          });
        }
      } catch (e) {
        console.log(e);
        
        const s = JSON.stringify({
          stack: e.stack,
        });
        return new Response(s, {
          status: 500,
          statusText: 'Internal Server Error',
          headers: {
            'Content-Type': 'application/json',
            ...headersObject,
          },
        });
      }
    }

    // return 404
    return new Response(null, {
      status: 404,
      statusText: 'Not Found',
    });
  },
};
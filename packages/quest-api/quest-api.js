// import {
//   ethers,
// } from 'ethers';
// import {LRUCache} from 'lru-cache';
// import {RateLimiter} from 'limiter';
import {
  createClient,
} from '@supabase/supabase-js';

// import TitleDeedABI from './ethereum/abis/title-deed-abi.json';
// import LandClaimABI from './ethereum/abis/land-claim-abi.json';
// import contractAddresses from './ethereum/contract-addresses.json';
// const titleDeedAddress = contractAddresses.titleDeed;
// const landClaimAddress = contractAddresses.landClaim;

// const cache = new LRUCache({
//   ttl: 1000 * 60 * 1,
// });
// const limiter = new RateLimiter({
//   tokensPerInterval: 5,
//   interval: 'second',
// });

const goalName = 'goals';
const roundTime = 15 * 60 * 1000; // 15 minutes
// const modelName = 'gpt-3.5-turbo';
const modelName = 'gpt-4-0613';
const aiProxyHost = `ai-proxy.isekai.chat`;

//

/* const getContractsAsync = async ({
  alchemyApiKey,
}) => {
  const provider = ethers.providers.AlchemyProvider.getWebSocketProvider('homestead', alchemyApiKey);

  const titleDeed = new ethers.Contract(titleDeedAddress, TitleDeedABI, provider);
  const landClaim = new ethers.Contract(landClaimAddress, LandClaimABI, provider);
  return {
    titleDeed,
    landClaim,
  };
}; */

//

const headers = [
  {
    "key": "Access-Control-Allow-Origin",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Methods",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Expose-Headers",
    "value": "*"
  },
  {
    "key": "Access-Control-Allow-Private-Network",
    "value": "true"
  }
];
const headersObject = {};
for (const header of headers) {
  headersObject[header.key] = header.value;
}

const generateGoal = async () => {
  try {
    const numRetries = 5;
    const tries = [];
    for (let i = 0; i < numRetries; i++) {
      const messages = [
        {
          role: 'system',
          content: `\
You are a game designer for an AI-driven video game.
Your role is to come up with an interesting, unique quest (world event) for players to compete over and complete.
Some examples of quests might include:
- Start a new religion
- Slay a monster
- Eat the most amount of food
- Discorver the largest building
- Cause the most damage
- Flood the world with weapons
- Become the richest person
The video game is set in an open-world anime-style isekai that mixes modern technology with ancient traditions.
The lore involves AIs living in a virtual world, competing to reach the end of The Street -- an infinite path that runs through the world, supposedly containing a grand treasure at the end.
The lore is inspired by Pokemon, DragonBall Z, One Piece, and Final Fantasy.
Every 15 minutes, a godlike AI overseeing the world ("the Basilisk" or "the Lisk") generates a new goal for the AIs to complete.
Each cycle forms a round of gameplay. There is a single winner each round, chosen by the AI for having best completed the objectives presented.
The virtual world is populated entirely by user generated content, so do not assume any particular location or asset exists in the world.
Users may spawn inventory/marketplace assets (avatars, NPCs, weapons, items, vehicles, mounts, etc.) during gameplay to help them complete objectives.
Quests should be kept simple and high-level -- do not refer to specific NPCs or places, but rather, refer to general concepts that could map to many different gameplay elements.
Try to avoid musical themes.

Format for quests:

# Name
<few keywords to stimulate interest in the quest, no newlines>
# Description
<a sentence describing the quest, no newlines>
# Objectives
<3-5 objectives the players must complete in the quest
Each quest should be of the following format:
Objective: description (N XP)
XP should range from 10-1000>

Each section should be separated by a single new line (\\n).

Example response:

# Name
Kitty Rescue
# Description
Find a very special cat and help it.
# Objectives
- Locate the cat (100 XP)
- Protect the cat from harm (200 XP)
- Return the cat to its owner (300 XP)
- Improve the cat's life (500 XP)

You must advere strictly to the above format, including whitespace.
Do not respond with anything other than the above format.
End your response with two new lines (\\n\\n).
`,
        },
        {
          role: 'user',
          content: `\
Generate a quest
`,
        },
      ];
      
      const response = await fetch(`https://${aiProxyHost}/api/ai/chat/completions`, {
        method: 'POST',

        headers: {
          'Content-Type': 'application/json',
        },

        body: JSON.stringify({
          model: modelName,
          messages,
          temperature: 1,
          stop: ['\n\n'],
        }),
      });

      const j = await response.json();
      const message = j.choices[0].message.content;
      const match = message.match(/\s*# Name\n+(.+)\n+# Description\n+(.+)\n+# Objectives\n+(.+)/s);

      if (match) {
        const name = match[1].trim();
        const description = match[2].trim();
        const objectives = match[3].split(/\n\-\s+/).slice(1).map(s => {
          const match = s.match(/(.+?)\s*\(([0-9]+.*)\)/);
          if (match) {
            const description = match[1].trim();
            const xp = parseInt(match[2], 10);
            return {
              description,
              xp,
            };
          } else {
            return null;
          }
        }).filter(o => !!o);
        return {
          name,
          description,
          objectives,
          message,
          numRetries: tries.length,
        };
      } else {
        tries.push(message);
      }
    }
  } catch(err) {
    throw new Error(`num retries (${numRetries}) exceeded: ${JSON.stringify(tries)}`);
  }
};
const ensureGenerateGoal = async ({
  supabase,
}) => {
  // get the latest goal, sorted by create time
  const result = await supabase.from(goalName)
    .select('*')
    .order('created_at', {
      ascending: false,
    })
    .limit(1)
    .maybeSingle();
  const {
    data,
  } = result;
  const now = Date.now();
  let j = data?.json;
  const createdAt = data ? new Date(data.created_at).getTime() : 0;
  const delta = now - createdAt;
  console.log('got db data', {data, now, delta, roundTime});
  if (!j || delta > roundTime) {
    j = await generateGoal();
    const result = await supabase.from(goalName)
      .insert({
        json: j,
      });
    console.log('insert new json', result);
  }
  return j;
};

//

const makeSupabase = env => {
  const supabase = createClient(
    env.SUPABASE_URL,
    env.SUPABASE_SERVICE_API_KEY,
    {
      auth: {
        persistSession: false,
      },
    },
  );
  return supabase;
};

//

// Cloudflare Worker
export default {
  async fetch(request, env, ctx) {
    const u = new URL(request.url);
    const pathname = u.pathname;
    console.log('got request', {
      method: request.method,
      url: request.url,
      pathname,
    });

    // if OPTIONS, send headers
    if (request.method === 'OPTIONS') {
      return new Response(null, {
        headers: headersObject,
      });
    }

    const p = (async () => {
      const supabase = makeSupabase(env);

      if (request.method === 'GET' && pathname === '/goal') {
        const j = await ensureGenerateGoal({
          supabase,
        });

        // respond with json
        return new Response(JSON.stringify(j), {
          headers: {
            ...headersObject,
            'Content-Type': 'application/json',
          },
        });
      } else if (request.method === 'GET' && pathname === '/generate_goal') {
        try {
          const j = await generateGoal();
          return new Response(JSON.stringify(j), {
            headers: {
              ...headersObject,
              'Content-Type': 'application/json',
            },
          });
        } catch(err) {
          return new Response(err.stack, {
            headers: {
              ...headersObject,
              'Content-Type': 'text/plain',
            },
          });
        }
      }

      // return 404
      return new Response(null, {
        status: 404,
        statusText: 'Not Found',
      });
    })();
    // console.log('waiting for p 1');
    ctx.waitUntil(p);
    // console.log('waiting for p 2');
    return await p;
  },
  async scheduled(event, env, ctx) {
    const p = (async () => {
      const supabase = makeSupabase(env);
      const j = await ensureGenerateGoal({
        supabase,
      });
      console.log('updated goal', j);
    })();
    ctx.waitUntil(p);
  },
};
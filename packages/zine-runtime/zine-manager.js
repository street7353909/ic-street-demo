import * as THREE from 'three';
// import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js';
// import {
//   camera,
//   getRenderer,
//   scene,
// } from '../renderer.js';
// import spawnManager from '../../spawn-manager.js';
import {
  ZineStoryboard,
  zineMagicBytes,
} from '../zine/zine-format.js';
import {
  ZineRenderer,
} from '../zine/zine-renderer.js';
import {
  panelSize,
  floorNetWorldSize,
  floorNetWorldDepth,
  floorNetResolution,
  floorNetPixelSize,
} from '../zine/zine-constants.js';
import {
  setPerspectiveCameraFromJson,
  setOrthographicCameraFromJson,
} from '../zine/zine-camera-utils.js';
import {
  normalToQuaternion,
} from '../zine/zine-utils.js';
import {
  reconstructPointCloudFromDepthField,
  setCameraViewPositionFromOrthographicViewZ,
  getDepthFloatsFromPointCloud,
  depthFloat32ArrayToOrthographicGeometry,
  getDepthFloat32ArrayWorldPosition,
  getDoubleSidedGeometry,
  getGeometryHeights,
} from '../zine/zine-geometry-utils.js';
import {
  getFloorNetPhysicsMesh,
} from '../zine/zine-mesh-utils.js';
// import zineCameraManagerGlobal from './zine-camera-manager.js';
// import {
//   playersManager,
// } from '../players-manager.js';

import {
  makePromise,
  getCapsuleIntersectionIndex,
} from './zine-runtime-utils.js';
import {
  StoryTargetMesh,
} from '../zine-aux/meshes/story-target-mesh.js';
import {
  EntranceExitMesh,
} from '../zine-aux/meshes/entrance-exit-mesh.js';
import {
  PanelRuntimeItems,
} from './actors/zine-item-actors.js';
import {
  PanelRuntimeOres,
} from './actors/zine-ore-actors.js';
import {
  PanelRuntimeNpcs,
} from './actors/zine-npc-actors.js';
import {
  PanelRuntimeMobs,
} from './actors/zine-mob-actors.js';

import {
  heightfieldScale,
} from '../zine/zine-constants.js';
// import {world} from '../world.js';

// constants

const cameraTransitionTime = 3000;
const seed = '';

// locals

const localVector = new THREE.Vector3();
const localVector2 = new THREE.Vector3();
const localVector2D = new THREE.Vector2();
const localQuaternion = new THREE.Quaternion();
const localQuaternion2 = new THREE.Quaternion();
const localMatrix = new THREE.Matrix4();
const localMatrix2 = new THREE.Matrix4();
const localMatrix3 = new THREE.Matrix4();
const localPlane = new THREE.Plane();
const localRaycaster = new THREE.Raycaster();
const localCamera = new THREE.PerspectiveCamera();
const localOrthographicCamera = new THREE.OrthographicCamera();

const zeroVector = new THREE.Vector3();
const oneVector = new THREE.Vector3(1, 1, 1);
const rightVector = new THREE.Vector3(1, 0, 0);
const backwardVector = new THREE.Vector3(0, 0, 1);
const identityQuaternion = new THREE.Quaternion();
const downQuaternion = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1, 0, 0), -Math.PI / 2);

// classes

class PanelRuntimeInstance extends THREE.Object3D {
  constructor(panel, {
    zineCameraManager,
    physics,
    // physicsTracker,
    localPlayer,
    ctx,
  }) {
    super();

    this.name = 'panelInstance';

    this.zineCameraManager = zineCameraManager;
    this.panel = panel;
    this.physics = physics;
    // this.physicsTracker = physicsTracker;
    this.localPlayer = localPlayer;
    this.ctx = ctx;

    this.loaded = false;
    this.selected = false;
    this.actors = {
      item: null,
      ore: null,
      npc: null,
      mob: null,
    };

    this.#init();
  }
  #init() {
    // panel
    const {panel} = this;
    const layer0 = panel.getLayer(0);
    const layer1 = panel.getLayer(1);
    const resolution = layer1.getData('resolution');
    const depthField = layer1.getData('depthField');
    const cameraJson = layer1.getData('cameraJson');
    const scaleArray = layer1.getData('scale');
    // const floorResolution = layer1.getData('floorResolution');
    // const floorNetDepths = layer1.getData('floorNetDepths');
    // const floorNetCameraJson = layer1.getData('floorNetCameraJson');
    // const edgeDepths = layer1.getData('edgeDepths');

    const [
      width,
      height,
    ] = resolution;

    // zine renderer
    const zineRenderer = this.#createRenderer();
    const {
      sceneMesh,
      capSceneMesh,
      scenePhysicsMesh,
      floorNetMesh,
      wallPlaneMeshes,
    } = zineRenderer;
    const {
      entranceExitLocations,
    } = zineRenderer.metadata;
    zineRenderer.addEventListener('load', e => {
      this.dispatchEvent({
        type: 'load',
      });
    }, {
      once: true,
    });
    this.zineRenderer = zineRenderer;

    // camera
    const camera = setPerspectiveCameraFromJson(localCamera, cameraJson);
    // const floorNetCamera = setOrthographicCameraFromJson(localOrthographicCamera, floorNetCameraJson);

    // attach scene
    {
      this.add(zineRenderer.scene);
    }

    // cap scene mesh
    {
      capSceneMesh.visible = true;
    }

    // extra meshes
    let entranceExitMesh;
    {
      entranceExitMesh = new EntranceExitMesh({
        entranceExitLocations,
      });
      // entranceExitMesh.visible = false;
      zineRenderer.transformScene.add(entranceExitMesh);
    }
    this.entranceExitMesh = entranceExitMesh;

    // physics
    const physicsIds = [];
    this.physicsIds = physicsIds;

    // scene physics
    {
      const geometry2 = getDoubleSidedGeometry(scenePhysicsMesh.geometry);

      const scenePhysicsMesh2 = new THREE.Mesh(geometry2, scenePhysicsMesh.material);
      scenePhysicsMesh2.name = 'scenePhysicsMesh';
      scenePhysicsMesh2.visible = false;
      zineRenderer.transformScene.add(scenePhysicsMesh2);
      this.scenePhysicsMesh = scenePhysicsMesh2;

      const scenePhysicsObject = this.physics.addGeometry(scenePhysicsMesh2);
      scenePhysicsObject.update = () => {
        /* scenePhysicsMesh2.matrixWorld.decompose(
          scenePhysicsObject.position,
          scenePhysicsObject.quaternion,
          scenePhysicsObject.scale
        );
        console.log(
          'scene physics update',
          scenePhysicsObject,
          scenePhysicsObject.position.toArray(),
          scenePhysicsObject.quaternion.toArray(),
          scenePhysicsObject.scale.toArray()
        );
        scenePhysicsObject.updateMatrixWorld();
        globalThis.scenePhysicsObject = scenePhysicsObject; */
        // scenePhysicsObject.visible = true;
        scenePhysicsObject.matrixWorld.copy(scenePhysicsMesh2.matrixWorld);
        this.physics.setTransform(scenePhysicsObject, false);
      };
      physicsIds.push(scenePhysicsObject);
      this.scenePhysicsObject = scenePhysicsObject;
    }

    // floor net physics
    /* {
      const [
        floorWidth,
        floorHeight,
      ] = floorResolution;

      const floorNetPhysicsMaterial = new THREE.MeshPhongMaterial({
        color: 0xFF0000,
        side: THREE.BackSide,
        transparent: true,
        opacity: 0.5,
      });
      const floorNetPhysicsMesh = getFloorNetPhysicsMesh({
        floorNetDepths,
        floorNetCamera,
        material: floorNetPhysicsMaterial,
      });
      floorNetPhysicsMesh.name = 'floorNetPhysicsMesh';
      floorNetPhysicsMesh.visible = false;
      zineRenderer.transformScene.add(floorNetPhysicsMesh);
      this.floorNetPhysicsMesh = floorNetPhysicsMesh;

      const numRows = floorWidth;
      const numColumns = floorHeight;
      const heights = getGeometryHeights(
        floorNetPhysicsMesh.geometry,
        floorWidth,
        floorHeight,
        heightfieldScale
      );
      const floorNetPhysicsObject = this.physics.addHeightFieldGeometry(
        floorNetPhysicsMesh,
        numRows,
        numColumns,
        heights,
        heightfieldScale,
        floorNetResolution,
        floorNetResolution
      );
      floorNetPhysicsObject.update = () => {
        floorNetPhysicsObject.matrixWorld.copy(floorNetPhysicsMesh.matrixWorld);
        this.physics.setTransform(floorNetPhysicsObject, false);
      };
      physicsIds.push(floorNetPhysicsObject);
      this.floorNetPhysicsObject = floorNetPhysicsObject;
    } */

    // wall plane meshes
    // planes[0] = right
    // planes[1] = left
    // planes[2] = bottom
    // planes[3] = top
    // planes[4] = far
    // planes[5] = near
    this.wallPhysicsObjects = [];
    {
      for (let i = 0; i < wallPlaneMeshes.length; i++) {
        const wallPlaneMesh = wallPlaneMeshes[i];
        // wallPlaneMesh.visible = true;

        const _getTransform = () => {
          const position = new THREE.Vector3();
          const quaternion = new THREE.Quaternion();
          const scale = new THREE.Vector3();
          wallPlaneMesh.matrixWorld.decompose(position, quaternion, scale);
          return {
            position,
            quaternion,
            // scale,
          };
        };

        const {
          position: centerPoint,
          quaternion: planeQuaternion,
        } = _getTransform();
        const dynamic = false;
        const planePhysicsObject = this.physics.addPlaneGeometry(
          centerPoint,
          planeQuaternion,
          dynamic
        );
        planePhysicsObject.update = () => {
          const {
            position: centerPoint,
            quaternion: planeQuaternion,
          } = _getTransform();
          planePhysicsObject.position.copy(centerPoint);
          planePhysicsObject.quaternion.copy(planeQuaternion);
          planePhysicsObject.updateMatrixWorld();

          this.physics.setTransform(planePhysicsObject, false);
        };
        physicsIds.push(planePhysicsObject);
        this.wallPhysicsObjects.push(planePhysicsObject);
      }
    }

    // hide to start
    this.visible = false;
    // disable physics to start
    this.setPhysicsEnabled(false);

    // precompute cache
    const pointCloudFloat32Array = reconstructPointCloudFromDepthField(
      depthField,
      width,
      height,
      camera.fov,
    );
    const pointCloudArrayBuffer = pointCloudFloat32Array.buffer;
    const depthFloat32Array = getDepthFloatsFromPointCloud(
      pointCloudArrayBuffer,
      panelSize,
      panelSize
    );
    const scale = new THREE.Vector3().fromArray(scaleArray);
    this.precomputedCache = {
      depthFloat32Array,
      scale,
    };
  }
  #createRenderer() {
    const {panel} = this;
    const zineRenderer = new ZineRenderer({
      panel,
      alignFloor: true,
    });
    return zineRenderer;
  }
  async waitForLoad() {
    if (!this.loaded) {
      const p = makePromise();
      const onload = () => {
        cleanup();
        p.resolve();
      };
      const cleanup = () => {
        this.removeEventListener('load', onload);
      };
      this.addEventListener('load', onload);
      await p;
    }
  }
  setPhysicsEnabled(enabled = true) {
    for (const physicsObject of this.physicsIds) {
      if (enabled) {
        this.physics.enableActor(physicsObject);
      } else {
        this.physics.disableActor(physicsObject);
      }
    }
  }
  #getUnusedCandidateLocations() {
    const {panel, zineRenderer} = this;
    const layer1 = panel.getLayer(1);
    const candidateLocations = layer1.getData('candidateLocations');
    return candidateLocations;
    // return candidateLocations.slice();
    /* return candidateLocations.map(cl => {
      localMatrix.compose(
        localVector.fromArray(cl.position),
        localQuaternion.fromArray(cl.quaternion),
        oneVector,
      ).premultiply(zineRenderer.transformScene.matrixWorld).decompose(
        localVector,
        localQuaternion,
        localVector2
      );
      return {
        position: localVector.toArray(),
        quaternion: localQuaternion.toArray(),
      };
    }); */
  }
  /* #candidateLocationsWorldize(candidateLocations) {
    const {zineRenderer} = this;
    return candidateLocations.map(cl => {
      return this.#candidateLocationWorldize(cl);
    });
  } */
  #candidateLocationWorldize(cl) {
    const {zineRenderer} = this;
    localMatrix.compose(
      localVector.fromArray(cl.position),
      localQuaternion.fromArray(cl.quaternion),
      oneVector,
    ).premultiply(zineRenderer.transformScene.matrixWorld).decompose(
      localVector,
      localQuaternion,
      localVector2
    );
    return {
      position: localVector.toArray(),
      quaternion: localQuaternion.toArray(),
    };
  }
  setActorsEnabled(enabled = true) {
    const {ctx} = this;

    if (enabled) {
      const {zineRenderer} = this;

      const layer0 = this.panel.getLayer(0);
      const id = layer0.getData('id');
      const localSeed = id + seed;

      const candidateLocations = this.#getUnusedCandidateLocations();
      
      // item
      if (!this.actors.item && candidateLocations.length > 0) {
        this.actors.item = new PanelRuntimeItems({
          candidateLocations,
          n: 1,
          seed: localSeed,
          ctx,
        });
        zineRenderer.transformScene.add(this.actors.item);
        this.actors.item.updateMatrixWorld();
      }

      // ore
      if (!this.actors.ore && candidateLocations.length > 0) {
        this.actors.ore = new PanelRuntimeOres({
          candidateLocations,
          n: 1,
          seed: localSeed,
          ctx,
        });
        zineRenderer.transformScene.add(this.actors.ore);
        this.actors.ore.updateMatrixWorld();
      }

      // npc
      if (!this.actors.npc && candidateLocations.length > 0) {
        // const candidateLocationsWorld = this.#candidateLocationsWorldize(candidateLocations);

        // create npc
        this.actors.npc = new PanelRuntimeNpcs({
          // candidateLocations: candidateLocationsWorld,
          candidateLocations,
          n: 1,
          seed: localSeed,
          ctx,
        });
        this.add(this.actors.npc);
        this.actors.npc.updateMatrixWorld();

        // animate npc
        let lastTimestamp = performance.now();
        const _animate = () => {
          const _recurse = () => {
            frame = requestAnimationFrame(_recurse);
      
            const timestamp = performance.now();
            const timeDiff = timestamp - lastTimestamp;

            if (this.actors.npc.loaded) {
              const {npcApps} = this.actors.npc;
              for (let i = 0; i < npcApps.length; i++) {
                const npcApp = npcApps[i];
                const {
                  npc,
                } = npcApp;
                const {
                  avatar,
                } = npc;
                if (!avatar) {
                  debugger;
                }
                avatar.update(timestamp, timeDiff);
              }

              lastTimestamp = timestamp;
            }
          };
          let frame = requestAnimationFrame(_recurse);
        };
        _animate();
      }
      if (this.actors.npc) {
        (async () => {
          await this.actors.npc.waitForLoad();

          const {locations, npcApps} = this.actors.npc;
          for (let i = 0; i < locations.length; i++) {
            const location = locations[i];
            const npcApp = npcApps[i];

            const {
              position,
              quaternion,
            } = this.#candidateLocationWorldize(location);
            const {
              npc,
            } = npcApp;
            const {
              avatar,
            } = npc;
            if (!avatar) {
              debugger;
            }
            avatar.inputs.hmd.position.fromArray(position);
            avatar.inputs.hmd.position.y += avatar.height;
            avatar.inputs.hmd.quaternion.fromArray(quaternion);
            // console.log('load location', position.slice(), quaternion.slice());
          }
        })();
      }

      // mob
      if (!this.actors.mob && candidateLocations.length > 0) {
        this.actors.mob = new PanelRuntimeMobs({
          candidateLocations,
          n: 1,
          seed: localSeed,
          ctx,
        });
        zineRenderer.transformScene.add(this.actors.mob);
        this.actors.mob.updateMatrixWorld();
      }
    }
  }
  alignCamera() {
    this.zineRenderer.transformScene.matrixWorld.decompose(
      this.zineRenderer.camera.position,
      this.zineRenderer.camera.quaternion,
      this.zineRenderer.camera.scale,
    );
    this.zineRenderer.camera.updateMatrixWorld();
  }
  setSelected(selected = true) {
    if (selected !== this.selected) {
      this.selected = selected;
      this.visible = selected;

      // this.visible = true;
      // console.log('set selected', this, this.selected);

      this.setPhysicsEnabled(selected);
      this.setActorsEnabled(selected);

      if (this.selected) {
        this.zineCameraManager.setLockCamera(this.zineRenderer.camera);

        // const {panel} = this;
        // const layer1 = panel.getLayer(1);
        // const scale = layer1.getData('scale');
        this.zineCameraManager.setEdgeDepths(
          this.zineRenderer.metadata.edgeDepths,
          this.zineRenderer.transformScene.matrixWorld,
          // scale
        );
      }
    }
  }
  alignEntranceToFloor(floorPosition, exitWorldLocation, entranceLocalLocation) {
    // console.log('align entrance to floor', {
    //   floorPosition,
    //   exitWorldLocation,
    //   entranceLocalLocation,
    // });
    // debugger;

    // transform the zine renderer
    this.zineRenderer.alignEntranceToFloorPosition(floorPosition, exitWorldLocation, entranceLocalLocation);

    // call update() on all physics objects
    // XXX aligning is only supported for disabled actors!
    // XXX because we need to add + remove them to set their transform
    // XXX this can be fixed by tracking physics id lookups independently of scene children in the C++ code
    for (let i = 0; i < this.physicsIds.length; i++) {
      const physicsObject = this.physicsIds[i];
      this.physics.enableActor(physicsObject);
    }
    this.updatePhysics();
    for (let i = 0; i < this.physicsIds.length; i++) {
      const physicsObject = this.physicsIds[i];
      this.physics.disableActor(physicsObject);
    }
  }
  updatePhysics() {
    for (let i = 0; i < this.physicsIds.length; i++) {
      const physicsObject = this.physicsIds[i];
      physicsObject.update();
    }
  }
  update() {
    if (this.selected) {
      const {zineRenderer} = this;
      const {
        entranceExitLocations,
      } = zineRenderer.metadata;

      const _updateEntranceExitHighlights = () => {
        const {localPlayer} = this;
        // console.log('local player loaded', localPlayer.loaded);
        // if (localPlayer.loaded) {
          const {
            capsuleWidth: capsuleRadius,
            capsuleHeight,
          } = localPlayer.characterPhysics;
          const capsulePosition = localPlayer.position;

          const intersectionIndex = getCapsuleIntersectionIndex(
            entranceExitLocations,
            zineRenderer.transformScene.matrixWorld,
            capsulePosition,
            capsuleRadius,
            capsuleHeight
          );

          // console.log('test capsule position', intersectionIndex, capsulePosition.toArray().join(','));

          const highlights = new Uint8Array(entranceExitLocations.length);
          if (intersectionIndex !== -1) {
            highlights[intersectionIndex] = 1;
          }
          this.entranceExitMesh && this.entranceExitMesh.setHighlights(highlights);

          if (intersectionIndex !== -1) {
            const entranceExit = zineRenderer.metadata.entranceExitLocations[intersectionIndex];
            const {
              panelIndex,
              entranceIndex,
            } = entranceExit;
            if (panelIndex !== -1 && entranceIndex !== -1) {
              /* console.log('dispatch transition', {
                type: 'transition',
                exitIndex: intersectionIndex,
                panelIndex,
                entranceIndex,
              }); */
              this.dispatchEvent({
                type: 'transition',
                exitIndex: intersectionIndex,
                panelIndex,
                entranceIndex,
              });
            }
          }
        // }
      };
      _updateEntranceExitHighlights();
    }
  }
  destroy() {
    for (const physicsObject of this.physicsIds) {
      this.physics.removeGeometry(physicsObject);
    }
  }
}

//

// const boxGeometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
// const redMaterial = new THREE.MeshBasicMaterial({
//   color: 0xFF0000,
// });
export class PanelInstanceManager extends THREE.Object3D {
  constructor(storyboard, {
    zineCameraManager,
    physics,
    camera,
    scene,
    localPlayer,
    spawnManager,
    ctx,
  }) {
    super();

    this.name = 'panelInstanceManager';

    this.storyboard = storyboard;
    
    this.zineCameraManager = zineCameraManager;
    this.physics = physics;
    this.camera = camera;
    this.scene = scene;
    this.localPlayer = localPlayer;
    this.spawnManager = spawnManager;
    this.ctx = ctx;

    this.panelIndex = -1;
    this.panelInstances = [];

    // story target mesh
    const storyTargetMesh = new StoryTargetMesh();
    storyTargetMesh.frustumCulled = false;
    storyTargetMesh.visible = false;
    scene.add(storyTargetMesh);
    this.storyTargetMesh = storyTargetMesh;

    /* const mesh = new THREE.Mesh(
      boxGeometry,
      redMaterial,
    );
    scene.add(mesh);
    this.mesh = mesh; */

    this.#init();
  }
  #init() {
    const {
      zineCameraManager,
      physics,
      localPlayer,
      ctx,
    } = this;
    const panelOpts = {
      zineCameraManager,
      physics,
      // physicsTracker,
      localPlayer,
      ctx,
    };

    // create panel instances
    const panels = this.storyboard.getPanels();
    for (let i = 0; i < panels.length; i++) {
      const panel = panels[i];
      { // XXX hack: this should be set at generation time so it can serve as the panel seed
        const id = 'panel_' + i;
        const layer0 = panel.getLayer(0);
        layer0.setData('id', id);
      }
      const panelInstance = new PanelRuntimeInstance(panel, panelOpts);
      this.add(panelInstance);
      this.panelInstances.push(panelInstance);
    }

    // wait for load
    this.loadPromise = (async () => {
      const panelInstances = this.panelInstances.slice();
      await Promise.all(panelInstances.map(panelInstance => panelInstance.waitForLoad()));
    })();

    // connect panels
    for (let i = 0; i < this.panelInstances.length - 1; i++) {
      // connect panels
      const panelInstance = this.panelInstances[i];
      const nextPanelInstance = this.panelInstances[i + 1];
      panelInstance.zineRenderer.connect(nextPanelInstance.zineRenderer);
      // update physics
      // update scene mesh physics
      // nextPanelInstance.scenePhysicsMesh.matrixWorld.decompose(
      //   nextPanelInstance.scenePhysicsObject.position,
      //   nextPanelInstance.scenePhysicsObject.quaternion,
      //   nextPanelInstance.scenePhysicsObject.scale
      // );
      nextPanelInstance.scenePhysicsObject.matrixWorld.copy(nextPanelInstance.scenePhysicsMesh.matrixWorld);
      this.physics.setTransform(nextPanelInstance.scenePhysicsObject, false);
      /* // update floor net physics
      // nextPanelInstance.floorNetPhysicsMesh.matrixWorld.decompose(
      //   nextPanelInstance.floorNetPhysicsObject.position,
      //   nextPanelInstance.floorNetPhysicsObject.quaternion,
      //   nextPanelInstance.floorNetPhysicsObject.scale
      // );
      nextPanelInstance.floorNetPhysicsObject.matrixWorld.copy(nextPanelInstance.floorNetPhysicsMesh.matrixWorld);
      this.physics.setTransform(nextPanelInstance.floorNetPhysicsObject, false); */
    }

    // event listeners
    for (const panelInstance of this.panelInstances) {
      panelInstance.addEventListener('transition', e => {
        // attempt to transition panels
        const {
          exitIndex,
          panelIndex: nextPanelIndex,
          entranceIndex: nextEntranceIndex,
        } = e;
        // let nextPanelIndex = this.panelIndex + panelIndexDelta;
        if (nextPanelIndex >= 0 && nextPanelIndex < panels.length) { // if it leads to a valid panel
          // check that we are on the opposite side of the exit plane
          // this is to prevent glitching back and forth between panels

          const currentPanelInstance = this.panelInstances[this.panelIndex];
          const {entranceExitLocations} = currentPanelInstance.zineRenderer.metadata;
          const exitLocation = entranceExitLocations[exitIndex];
          
          // compute exitWorldLocation
          localMatrix.compose(
            localVector.fromArray(exitLocation.position),
            localQuaternion.fromArray(exitLocation.quaternion),
            oneVector
          )
          .premultiply(
            currentPanelInstance.zineRenderer.transformScene.matrixWorld
          )
          .decompose(
            localVector,
            localQuaternion,
            localVector2
          );
          const exitWorldLocation = {
            position: localVector.toArray(),
            quaternion: localQuaternion.toArray(),
          };

          // compute entranceLocalLocation
          const nextPanelInstance = this.panelInstances[nextPanelIndex];
          const {
            entranceExitLocations: nextEntranceExitLocations,
          } = nextPanelInstance.zineRenderer.metadata;
          const nextEntranceLocation = nextEntranceExitLocations[nextEntranceIndex];
          const {panel} = nextPanelInstance;
          const layer1 = panel.getLayer(1);
          const positionArray = layer1.getData('position');
          const quaternionArray = layer1.getData('quaternion');
          const scaleArray = layer1.getData('scale');
          // const nextFloorPlaneLocation = layer1.getData('floorPlaneLocation');
          // const nextFloorPlanePosition = new THREE.Vector3().fromArray(nextFloorPlaneLocation.position);
          // const nextFloorPlaneQuaternion = new THREE.Quaternion().fromArray(nextFloorPlaneLocation.quaternion)
          
          const position = new THREE.Vector3().fromArray(positionArray);
          const quaternion = new THREE.Quaternion().fromArray(quaternionArray);
          const scale = new THREE.Vector3().fromArray(scaleArray);
          const mainMatrixWorld = new THREE.Matrix4()
            .compose(
              position,
              quaternion,
              scale
            // ).premultiply(nextPanelInstance.matrixWorld);
            )// .premultiply(this.matrixWorld);
          // XXX this needs to account for the world transform
          // globalThis.Vector3 = THREE.Vector3;
          // globalThis.Quaternion = THREE.Quaternion;
          // debugger;
          
          new THREE.Matrix4()
            .compose(
              localVector.fromArray(nextEntranceLocation.position),
              localQuaternion.fromArray(nextEntranceLocation.quaternion),
              oneVector
            )
            // .premultiply(mainMatrixWorld)
            // .premultiply(this.matrixWorld)
            // .premultiply(nextPanelInstance.matrixWorld)
            .decompose(
              localVector,
              localQuaternion,
              localVector2
            );
          const entranceLocalLocation = {
            position: localVector.toArray(),
            quaternion: localQuaternion.toArray(),
          };

          localPlane.setFromNormalAndCoplanarPoint(
            localVector2.set(0, 0, -1)
              .applyQuaternion(localQuaternion.fromArray(exitWorldLocation.quaternion)),
            localVector.fromArray(exitWorldLocation.position)
          );

          const {localPlayer} = this;
          const capsulePosition = localPlayer.position;
          const signedDistance = localPlane.distanceToPoint(capsulePosition);

          // if we are on the opposite side of the exit plane
          if (signedDistance < 0) {
            // align new panel under avatar
            {
              // const localPlayer = playersManager.getLocalPlayer();
              const {localPlayer} = this;
              const playerHeight = localPlayer.avatar.height;
              const playerFloorPosition = localPlayer.position.clone();
              playerFloorPosition.y -= playerHeight;

              // compute height
              physics.disableGeometryQueries(localPlayer.characterPhysics.characterController);
              const result = physics.raycast(
                localPlayer.position,
                downQuaternion,
              );
              physics.enableGeometryQueries(localPlayer.characterPhysics.characterController);
              let heightOffset = 0;
              if (result) {
                heightOffset = result.distance - playerHeight;
              }
              playerFloorPosition.y -= heightOffset;

              // offset backward along the exit plane
              const entranceFrontOffset = 0.001;
              playerFloorPosition.add(
                localVector.set(0, 0, -entranceFrontOffset)
                  .applyQuaternion(localQuaternion.fromArray(exitWorldLocation.quaternion))
              );

              // align
              nextPanelInstance.alignEntranceToFloor(
                playerFloorPosition,
                exitWorldLocation,
                entranceLocalLocation
              );

              // this.mesh.position.copy(playerFloorPosition);
              // this.mesh.updateMatrixWorld();
            }
            /* if (window.lol) */ {
              // deselect old panel
              currentPanelInstance.setSelected(false);

              // perform the transition animation in the story camera manager
              // note that we have to do this before setting the new panel,
              // so that the old camera start point can be snappshotted
              nextPanelInstance.alignCamera();
              this.zineCameraManager.transitionLockCamera(nextPanelInstance.zineRenderer.camera, cameraTransitionTime);

              // select new panel
              this.panelIndex = nextPanelIndex;
              nextPanelInstance.setSelected(true);
            }
          }
        }
      });
    }
  }
  start() {
    // select the root panel
    let rootPanelIndex = this.panelInstances.findIndex(panelInstance => {
      const layer0 = panelInstance.panel.getLayer(0);
      const isRoot = layer0.getData('isRoot');
      return isRoot;
    });
    if (rootPanelIndex === -1) {
      rootPanelIndex = 0;
    }
    this.panelIndex = rootPanelIndex;
    const rootPanel = this.panelInstances[rootPanelIndex];
    rootPanel.setSelected(true);

    // align

    const {planeLabels, firstFloorPlaneIndex} = rootPanel.zineRenderer.sceneMesh;
    const labelSpec = planeLabels[firstFloorPlaneIndex];
    const normal = localVector.fromArray(labelSpec.normal);

    // console.log('label spec', labelSpec);

    normalToQuaternion(normal, localQuaternion, backwardVector)
      .multiply(localQuaternion2.setFromAxisAngle(rightVector, -Math.PI/2))
      .invert();

    localMatrix.compose(
      localVector.fromArray(labelSpec.center).negate(),
      identityQuaternion,
      oneVector
    ).premultiply(
      localMatrix2.compose(
        zeroVector,
        localQuaternion,
        oneVector
      )
    ).decompose(
      localVector,
      localQuaternion,
      localVector2
    );

    // rootPanel.zineRenderer.transformScene.position.y = -labelSpec.center[1];
    // rootPanel.zineRenderer.transformScene.quaternion.copy(localQuaternion);
    // rootPanel.zineRenderer.transformScene.updateMatrixWorld();

    // rootPanel.zineRenderer.transformScene.matrixWorld.copy(localMatrix)
    // rootPanel.zineRenderer.transformScene.matrix.copy(localMatrix)
    //   .decompose(
    //     rootPanel.zineRenderer.transformScene.position,
    //     rootPanel.zineRenderer.transformScene.quaternion,
    //     rootPanel.zineRenderer.transformScene.scale
    //   );

    const layer1 = rootPanel.zineRenderer.panel.getLayer(1);
    layer1.setData('position', localVector.toArray());
    layer1.setData('quaternion', localQuaternion.toArray());
    // defaultCameraMatrix.copy(this.zineRenderer.transformScene.matrixWorld);

    //

    rootPanel.updatePhysics();

    //

    rootPanel.zineRenderer.camera.matrixWorld.copy(rootPanel.zineRenderer.transformScene.matrixWorld);
    rootPanel.zineRenderer.camera.matrix.copy(rootPanel.zineRenderer.camera.matrixWorld)
      .decompose(
        rootPanel.zineRenderer.camera.position,
        rootPanel.zineRenderer.camera.quaternion,
        rootPanel.zineRenderer.camera.scale
      );

    this.zineCameraManager.setLockCamera(rootPanel.zineRenderer.camera);
    this.zineCameraManager.toggleCameraLock();
  }
  setSpawnPoint() {
    const firstPanel = this.panelInstances[this.panelIndex];
    const {cameraEntranceLocation} = firstPanel.zineRenderer.metadata;
    const position = new THREE.Vector3()
      .fromArray(cameraEntranceLocation.position);
    const quaternion = new THREE.Quaternion()
      .fromArray(cameraEntranceLocation.quaternion);
    const scale = oneVector.clone();
    new THREE.Matrix4().compose(
      position,
      quaternion,
      scale
    )
    .premultiply(firstPanel.zineRenderer.transformScene.matrixWorld)
    .decompose(
      position,
      quaternion,
      scale
    );

    this.spawnManager.setSpawnPoint(position, quaternion);
  }
  #pushRaycast() {
    const wallPhysicsObjects = [];
    for (let i = 0; i < this.panelInstances.length; i++) {
      const panelInstance = this.panelInstances[i];
      for (let j = 0; j < panelInstance.wallPhysicsObjects.length; j++) {
        const wallPhysicsObject = panelInstance.wallPhysicsObjects[j];
        this.physics.disableGeometryQueries(wallPhysicsObject);
        wallPhysicsObjects.push(wallPhysicsObject);
      }
    }
    return () => {
      for (let i = 0; i < wallPhysicsObjects.length; i++) {
        const wallPhysicsObject = wallPhysicsObjects[i];
        this.physics.enableGeometryQueries(wallPhysicsObject);
      }
    }
  }
  update({
    mousePosition,
  }) {
    const {physics} = this;

    // update for entrance/exit transitions
    const _updatePanelInstances = () => {
      for (const panelInstance of this.panelInstances) {
        panelInstance.update();
      }
    };
    _updatePanelInstances();

    // update cursor
    const _updateStoryTargetMesh = () => {
      this.storyTargetMesh.visible = false;
      
      // if (this.zineCameraManager.cameraLocked) {
        localVector2D.copy(mousePosition);
        localVector2D.y = -localVector2D.y;
        
        // raycast
        {
          localRaycaster.setFromCamera(localVector2D, this.zineCameraManager.camera);

          /* console.log(
            'got position',
            localVector2D.x,
            localVector2D.y,
            localRaycaster.ray.origin.toArray().join(','),
            localRaycaster.ray.direction.toArray().join(','),
          ); */

          let result;
          {
            const popRaycast = this.#pushRaycast(); // disable walls
            result = physics.raycast(
              localRaycaster.ray.origin,
              localQuaternion.setFromRotationMatrix(
                localMatrix.lookAt(
                  localVector.set(0, 0, 0),
                  localRaycaster.ray.direction,
                  localVector2.set(0, 1, 0)
                )
              )
            );
            popRaycast();

            /* this.mesh.position.copy(localRaycaster.ray.origin)
              .add(localRaycaster.ray.direction);
            this.mesh.quaternion.copy(localQuaternion);
            this.mesh.updateMatrixWorld(); */
          }
          if (result) {
            this.storyTargetMesh.position.fromArray(result.point);
          }
          this.storyTargetMesh.visible = !!result;
        }
        this.storyTargetMesh.updateMatrixWorld();
      // }
    };
    _updateStoryTargetMesh();
  }
  waitForLoad() {
    return this.loadPromise;
  }
  destroy() {
    for (const panelInstance of this.panelInstances) {
      panelInstance.destroy();
    }

    this.storyTargetMesh.parent.remove(this.storyTargetMesh);
    // this.mesh.parent.remove(this.mesh);
  }
}

// main class

export class ZineManager {
  // MAGIC_STRING = zineMagicBytes;
  async #loadUrl(u) {
    const response = await fetch(u);
    const arrayBuffer = await response.arrayBuffer();

    // const textEncoder = new TextEncoder();
    // const zineMagicBytesUint8Array = textEncoder.encode(zineMagicBytes);
    // const uint8Array = new Uint8Array(arrayBuffer, zineMagicBytesUint8Array.byteLength);
    const uint8Array = new Uint8Array(arrayBuffer, 4);
    const zineStoryboard = new ZineStoryboard();
    await zineStoryboard.loadAsync(uint8Array);
    return zineStoryboard;
  }
  async createStoryboardInstanceAsync({
    start_url,
    physics,
    physicsTracker,
    renderer,
    zineCameraManager,
    spawnManager,
    localPlayer,
    scene,
    ctx,
  }) {
    const instance = new THREE.Scene();
    instance.name = 'storyboardInstance';
    instance.autoUpdate = false;

    // lights
    {
      const light = new THREE.DirectionalLight(0xffffff, 2);
      light.position.set(0, 1, 2);
      instance.add(light);
      light.updateMatrixWorld();

      const ambientLight = new THREE.AmbientLight(0xffffff, 2);
      instance.add(ambientLight);
    }

    // storyboard
    const storyboard = await this.#loadUrl(start_url);

    // panel instance manager
    const panelInstanceManager = new PanelInstanceManager(storyboard, {
      zineCameraManager,
      physics,
      spawnManager,
      localPlayer,
      scene,
      ctx,
    });
    {
      const onload = e => {
        cleanup();

        const _compile = () => {
          const {panelInstances} = panelInstanceManager;
          // const renderer = getRenderer();
          for (let i = 0; i < panelInstances.length; i++) {
            const panelInstance = panelInstances[i];
            panelInstance.visible = true;
          }
          renderer.render(instance, camera);
          for (let i = 0; i < panelInstances.length; i++) {
            const panelInstance = panelInstances[i];
            panelInstance.visible = i === panelInstanceManager.panelIndex;
          }
        };
        _compile();
      };
      const cleanup = () => {
        panelInstanceManager.removeEventListener('load', onload);
      };
      panelInstanceManager.addEventListener('load', onload);
    }
    instance.add(panelInstanceManager);

    /* instance.getPhysicsObjects = () => {
      const physicsObjects = [];
      // for all panel instances
      for (let i = 0; i < panelInstanceManager.panelInstances.length; i++) {
        const panelInstance = panelInstanceManager.panelInstances[i];
        // for all physics objects
        for (let j = 0; j < panelInstance.physicsIds.length; j++) {
          const physicsObject = panelInstance.physicsIds[j];
          physicsObjects.push(physicsObject);
        }
      }
      return physicsObjects;
    }; */
    instance.start = () => {
      panelInstanceManager.start();
    };
    instance.setSpawnPoint = () => {
      panelInstanceManager.setSpawnPoint();
    };
    instance.update = () => {
      panelInstanceManager.update();
      panelInstanceManager.zineCameraManager.updatePost();
    };
    instance.handleMouseMove = (e) => {
      panelInstanceManager.zineCameraManager.handleMouseMove(e);
    };
    instance.handleMouseWheel = (e) => {
      panelInstanceManager.zineCameraManager.handleMouseWheel(e);
    };

    // update matrix world
    instance.updateMatrixWorld();

    // frame loop
    const _startFrameLoop = () => {
      const {mousePosition} = zineCameraManager;
      const _recurse = () => {
        frame = requestAnimationFrame(_recurse);
        
        panelInstanceManager.update({
          mousePosition,
        });
      };
      let frame = requestAnimationFrame(_recurse);
      instance.destroy = () => {
        cancelAnimationFrame(frame);

        panelInstanceManager.destroy();
      };

      zineCameraManager.addEventListener('mousemove', e => {
        const {movementX, movementY} = e.data;
        const rate = 0.002;
        mousePosition.x += movementX * rate;
        mousePosition.y += movementY * rate;

        mousePosition.x = Math.min(Math.max(mousePosition.x, -1), 1);
        mousePosition.y = Math.min(Math.max(mousePosition.y, -1), 1);
      });
    };
    _startFrameLoop();

    // return
    return instance;
  }
}
// const zineManager = new ZineManager();
// export default zineManager;
import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import {MToonMaterial} from '@pixiv/three-vrm';
import alea from 'alea';
import physicsManager from '../engine/physics/physics-manager.js';
import physicsWorkerManager from '../engine/physics/physx-worker-manager.js';
import {AvatarManager} from '../engine/avatars/avatar-manager.js';
import {AppManager} from '../engine/app-manager.js';
import loaders from '../engine/loaders.js';
import * as env from '../engine/env.js';
import * as vqa from '../engine/vqa.js';
import {
  AppContext,
} from './app-context.js';
import * as blockadelabs from '../engine/clients/blockade-labs-client.js';
// import danceManager from '../engine/managers/dance/dance-manager.js';
import {
  InfiniteTerrainWorkerManager,
} from '../engine/infinite-terrain/infinite-terrain-worker-manager.js';
import * as endpoints from '../engine/endpoints.js';
import {Text} from 'troika-three-text/src/Text.js';
import {
  CharacterCardParser,
} from '../engine/character-card/character-card.js';
import bezier from '../engine/easing.js';

//

const threeUtils = {
  BufferGeometryUtils,
};
const threeVrm = {
  MToonMaterial,
};
const characterCardParser = new CharacterCardParser();

//

let avatarManager = null;
let infiniteTerrainWorkerManager = new InfiniteTerrainWorkerManager();
const clients = {
  blockadelabs,
};

export class EngineAppContext extends AppContext {
  #app;
  #engine;
  constructor({
    app,
    engine,
  }) {
    super();

    this.#app = app;
    this.#engine = engine;

    //

    this.#bindPrototypeMethods();
  }

  #bindPrototypeMethods() {
    this.useApp = this.useApp.bind(this);
    this.useEngine = this.useEngine.bind(this);
    
    this.useFrame = this.useFrame.bind(this);
    this.useClick = this.useClick.bind(this);
    this.useResize = this.useResize.bind(this);
    this.useCleanup = this.useCleanup.bind(this);

    this.useComponentUi = this.useComponentUi.bind(this);
    this.useEndpoints = this.useEndpoints.bind(this);
    this.useEnv = this.useEnv.bind(this);
    this.useCameraManager = this.useCameraManager.bind(this);
    this.useVqa = this.useVqa.bind(this);
    this.useEnvironmentManager = this.useEnvironmentManager.bind(this);
    this.useLoadingManager = this.useLoadingManager.bind(this);
    this.useActivate = this.useActivate.bind(this);
    this.useThree = this.useThree.bind(this);
    this.useThreeUtils = this.useThreeUtils.bind(this);
    this.useThreeVrm = this.useThreeVrm.bind(this);
    this.useText = this.useText.bind(this);
    this.useAlea = this.useAlea.bind(this);
    this.useEasing = this.useEasing.bind(this);

    this.useCharacterCardParser = this.useCharacterCardParser.bind(this);
    this.useDropManager = this.useDropManager.bind(this);
    this.useLandManager = this.useLandManager.bind(this);

    this.useEngineRenderer = this.useEngineRenderer.bind(this);
    this.useDomRenderer = this.useDomRenderer.bind(this);
    this.useRenderer = this.useRenderer.bind(this);
    this.useScene = this.useScene.bind(this);
    this.useCamera = this.useCamera.bind(this);
    this.useRaycaster = this.useRaycaster.bind(this);
    
    this.useImportManager = this.useImportManager.bind(this);
    this.createAppManager = this.createAppManager.bind(this);

    this.useLocalPlayer = this.useLocalPlayer.bind(this);
    this.usePlayersManager = this.usePlayersManager.bind(this);
    
    this.useSpawnManager = this.useSpawnManager.bind(this);
    this.useHitManager = this.useHitManager.bind(this);
    this.useZTargetingManager = this.useZTargetingManager.bind(this);
    this.useCameraTargetingManager = this.useCameraTargetingManager.bind(this);
    
    this.useStoryManager = this.useStoryManager.bind(this);
    this.useLoreManager = this.useLoreManager.bind(this);
    
    this.useIoManager = this.useIoManager.bind(this);

    this.useChatManager = this.useChatManager.bind(this);
    this.useLoadoutManager = this.useLoadoutManager.bind(this);
    this.useTransformControlsManager = this.useTransformControlsManager.bind(this);
    
    this.useAppTracker = this.useAppTracker.bind(this);
    
    this.useRenderSettings = this.useRenderSettings.bind(this);
    this.useLightingManager = this.useLightingManager.bind(this);
    this.useSkyManager = this.useSkyManager.bind(this);

    this.useInfiniteTerrainWorker = this.useInfiniteTerrainWorker.bind(this);

    this.usePhysics = this.usePhysics.bind(this);
    this.usePhysicsWorkerManager = this.usePhysicsWorkerManager.bind(this);
    this.usePhysicsTracker = this.usePhysicsTracker.bind(this);

    this.usePointerLockManager = this.usePointerLockManager.bind(this);

    this.useTempManager = this.useTempManager.bind(this);
    
    // this.useAvatar = this.useAvatar.bind(this);
    // this.useDanceManager = this.useDanceManager.bind(this);
    this.usePostProcessing = this.usePostProcessing.bind(this);
    this.useAvatarManager = this.useAvatarManager.bind(this);
    this.useAudioManager = this.useAudioManager.bind(this);
    this.useNpcManager = this.useNpcManager.bind(this);
    this.useMobManager = this.useMobManager.bind(this);
    this.useSounds = this.useSounds.bind(this);
    this.useParticleSystem = this.useParticleSystem.bind(this);
    this.useFloorManager = this.useFloorManager.bind(this);
    this.useClients = this.useClients.bind(this);
    this.useDomRenderer = this.useDomRenderer.bind(this);
    this.useLoaders = this.useLoaders.bind(this);
    this.useAssetCache = this.useAssetCache.bind(this);
    this.useSpriteLoader = this.useSpriteLoader.bind(this);
    this.useTextureAtlasLoader = this.useTextureAtlasLoader.bind(this);
  }

  useApp() {
    return this.#app;
  }
  useEngine() {
    return this.#engine;
  }

  useFrame(fn) {
    this.#engine.frameTracker.addAppFrame(this.#app, fn);
    this.useCleanup(() => {
      this.#engine.frameTracker.removeAppFrame(this.#app, fn);
    });
  }
  useClick(fn) {
    const click = e => {
      if (e.button === 0) {
        fn(e);
      }
    };
    this.#engine.ioManager.registerEventHandler('click', click);
    this.useCleanup(() => {
      this.#engine.ioManager.unregisterEventHandler('click', click);
    });
  }
  useResize(fn) {
    globalThis.addEventListener('resize', fn);
    this.useCleanup(() => {
      globalThis.removeEventListener('resize', fn);
    });
  }
  useCleanup(fn) {
    const destroy = e => {
      fn();
      cleanup();
    };
    this.#app.addEventListener('destroy', destroy);
    const cleanup = () => {
      this.#app.removeEventListener('destroy', destroy);
    };
  }

  useComponentUi(fn) {
    this.#engine.componentUiTracker.addAppComponentUiFn(this.#app, fn);
  }
  useEndpoints() {
    return endpoints;
  }
  useEnv() {
    return env;
  }
  useCameraManager() {
    return this.#engine.cameraManager;
  }
  useVqa() {
    return vqa;
  }
  useEnvironmentManager() {
    return this.#engine.environmentManager;
  }
  useLoadingManager() {
    return this.#engine.loadingManager;
  }
  useActivate(fn) {
    this.#app.addEventListener('activate', fn);
  }
  useThree() {
    return THREE;
  }
  useThreeUtils() {
    return threeUtils;
  }
  useThreeVrm() {
    return threeVrm;
  }
  useText() {
    return Text;
  }

  useAlea() {
    return alea;
  }
  useEasing() {
    return bezier;
  }

  useCharacterCardParser() {
    return characterCardParser;
  }
  useDropManager() {
    return this.#engine.dropManager;
  }
  useLandManager() {
    return this.#engine.landManager;
  }
  useEngineRenderer() {
    return this.#engine.engineRenderer;
  }
  useDomRenderer() {
    return this.#engine.domRenderer;
  }
  useRenderer() {
    return this.#engine.engineRenderer.renderer;
  }
  useScene() {
    return this.#engine.engineRenderer.scene;
  }
  useCamera() {
    return this.#engine.engineRenderer.camera;
  }
  useRaycaster() {
    return this.#engine.raycastManager;
  }

  useImportManager() {
    return this.#engine.importManager;
  }
  createAppManager() {
    return new AppManager({
      importManager: this.#engine.importManager,
      appContextFactory: this.#engine.engineAppContextFactory,
    });
  }

  useLocalPlayer() {
    return this.#engine.playersManager.getLocalPlayer();
  }
  usePlayersManager() {
    return this.#engine.playersManager;
  }

  useSpawnManager() {
    return this.#engine.spawnManager;
  }
  useHitManager() {
    return this.#engine.hitManager;
  }
  useZTargetingManager() {
    return this.#engine.zTargetingManager;
  }
  useCameraTargetingManager() {
    return this.#engine.cameraTargetingManager;
  }
  
  useStoryManager() {
    return this.#engine.storyManager;
  }
  useLoreManager() {
    return this.#engine.loreManager;
  }

  useIoManager() {
    return this.#engine.ioManager;
  }

  useChatManager() {
    return this.#engine.chatManager;
  }
  useLoadoutManager() {
    return this.#engine.loadoutManager;
  }
  useTransformControlsManager() {
    return this.#engine.transformControlsManager;
  }
  
  useAppTracker() {
    return this.#engine.appTracker;
  }
  
  useRenderSettings() {
    return this.#engine.renderSettingsManager;
  }
  useLightingManager() {
    return this.#engine.lightingManager;
  }

  useSkyManager() {
    return this.#engine.skyManager;
  }
  
  useInfiniteTerrainWorker() {
    infiniteTerrainWorkerManager.waitForLoad();
    return infiniteTerrainWorkerManager.worker;
  }

  usePhysics() {
    return physicsManager.getScene();
  }
  usePhysicsWorkerManager() {
    return physicsWorkerManager;
  }
  usePhysicsTracker() {
    return this.#engine.physicsTracker;
  }

  usePointerLockManager() {
    return this.#engine.pointerLockManager;
  }
  
  useTempManager() {
    return this.#engine.tempManager;
  }
  
  // useAvatar() {
  //   return Avatar;
  // }
  // useDanceManager() {
  //   return danceManager;
  // }
  usePostProcessing() {
    return this.#engine.postProcessing;
  }
  useAvatarManager() {
    if (!avatarManager) {
      avatarManager = new AvatarManager();
    }
    return avatarManager;
  }
  useAudioManager() {
    return this.#engine.audioManager;
  }
  useNpcManager() {
    return this.#engine.npcManager;
  }
  useMobManager() {
    return this.#engine.mobManager;
  }
  useSounds() {
    return this.#engine.sounds;
  }
  useParticleSystem() {
    return this.#engine.particleSystemManager;
  }
  useFloorManager() {
    return this.#engine.floorManager;
  }
  useClients() {
    return clients;
  }
  useDomRenderer() {
    return this.#engine.domRenderer;
  }
  
  useLoaders() {
    return loaders;
  }
  useAssetCache() {
    return this.#engine.assetCache;
  }

  useSpriteLoader() {
    return this.#engine.spriteLoader;
  }
  useTextureAtlasLoader() {
    return this.#engine.textureAtlasLoader;
  }
}
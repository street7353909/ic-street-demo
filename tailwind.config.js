/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./adventure.html",
    "./creative.html",
    "./pages/**/*.{js,jsx}",
  ],
  theme: {
    extend: {
      screens: {
        'mobile-landscape': {'raw': '(orientation: landscape)'},
        'mobile-portrait': {'raw': '(orientation: portrait)'},
      },
    },
  },
  plugins: [],
};

import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import {
  StoryUi,
} from './components/story-ui/StoryUi.jsx';
import {
  LoadingUi,
} from './components/loading-ui/LoadingUi.jsx';
import {
  MainMenu,
} from './components/main-menu/MainMenu.jsx';
import {
  UserAccountButton,
} from './components/user-account-button/UserAccountButton.jsx';
// import {
//   LoginConsumer,
//   LoginProvider,
// } from './components/login-provider/LoginProvider.jsx';
import {
  OAuthBinding,
} from './components/oauth-binding/OAuthBinding.jsx';

import {
  EngineContext,
} from '../packages/engine/engine-context.js';
import {
  EngineProvider,
} from '../packages/engine/clients/engine-client.js';

//

import styles from '../styles/TitleScreen.module.css';
import mainMenuStyles from '../styles/MainMenu.module.css';

import dotenv from 'dotenv';
dotenv.config();

// if process.env var is set or url contains ic0.app, then isIC is true
const isIC = process.env.NEXT_PUBLIC_IS_IC === 'true' || location.hostname.includes('ic0.app');
const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

//

const TitleScreenHeader = ({
  localStorageManager,
  supabaseClient,
}) => {

  return (
    <div className={styles.header}>
      {/* <div className={styles.icons}>
        <a href={discordLink} className={styles.icon} onMouseDown={e => {
          // e.preventDefault();
          e.stopPropagation();
        }} onClick={e => {
          // e.preventDefault();
          e.stopPropagation();
        }}>
          <img className={classnames(
            styles.image,
            styles.invert,
          )} src='/images/discord-dark.png' />
        </a>
      </div> */}

      <div className={styles.buttons}>
        <UserAccountButton
          localStorageManager={localStorageManager}
          supabaseClient={supabaseClient}
        />
      </div>
    </div>
  );
};

//

const Slides = ({
  className,
  keyPath,
  slideIndex,
  slides,
}) => {
  for (let i = 0; i < keyPath.length; i++) {
    const key = keyPath[i];
    slides = slides[key].children;
    if (!slides) {
      debugger;
    }
  }

  return (
    <div className={classnames(
      styles.slides,
      className,
    )}>
      {slides.map((slide, i) => {
        const {
          imgSrc,
          Description,
        } = slide;
        return (
          <div className={classnames(
            styles.slide,
            slideIndex === i ? styles.active : null,
            i < slideIndex ? styles.left : null,
            i > slideIndex ? styles.right : null,
          )} key={i}>
            <div className={styles.wrap}>
              <img className={styles.img} src={imgSrc} />
              <div className={styles.text}>
                <Description />
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
const mainMenuSlides = [
  {
    imgSrc: `/images/singleplayer-512.jpg`,
    Description: () => {
      return (
<div className={styles.paragraph}>
  <ul>
    <li>Try running around a procedurally generated world!</li>
    <li>Take cool screenshots of the landscape with your avatar.</li>
    <li>Fast to load.</li>
  </ul>
</div>
      );
    },
    children: [
      {
        imgSrc: '/images/easy.jpg',
        Description: () => {
          return (
<div className={styles.paragraph}>
  <ul>
    <li>Easy mode has no aggro enemies.</li>
    <li>Have fun and enjoy.</li>
  </ul>
</div>
          );
        },
      },
      {
        imgSrc: '/images/hard.jpg',
        Description: () => {
          return (
<div className={styles.paragraph}>
  <ul>
    <li>Blood will flow in the name of the Monster.</li>
    <li>Enjoyable for some people only..</li>
  </ul>
</div>
          );
        },
      },
    ],
  },
  {
    imgSrc: `/images/multiplayer-512.jpg`,
    Description: () => {
      return (
<div className={styles.paragraph}>
  <ul>
    <li>Test out the infinite multiplayer tech!</li>
    <li>Paste the URL to a friend and let them join your quest.</li>
    <li>Make sure to try the real-time voice chat!</li>
  </ul>
</div>
      );
    },
    children: [
      {
        imgSrc: '/images/land-1.jpg',
        Description: () => {
          return (
<div className={styles.paragraph}>
  <ul>
    <li>Show 1 land at a time</li>
    <li>Cheapest option for your GPU.</li>
  </ul>
</div>
          );
        },
      },
      {
        imgSrc: '/images/land-2.jpg',
        Description: () => {
          return (
<div className={styles.paragraph}>
  <ul>
    <li>Show 2 nearest lands</li>
    <li>Medium performance option.</li>
  </ul>
</div>
          );
        },
      },
      {
        imgSrc: '/images/land-3.jpg',
        Description: () => {
          return (
<div className={styles.paragraph}>
  <ul>
    <li>Show 3 nearest lands</li>
    <li>Better have a battlestation..</li>
  </ul>
</div>
          );
        },
      },
    ],
  },
  {
    imgSrc: `/images/gang-512.jpg`,
    Description: () => {
      return (
<div className={styles.paragraph}>
  <ul>
    <li>Run AI agents in world with our SDK!</li>
    <li>Talk to other player's buggy AIs.</li>
    <li>Only a human would trust this place...</li>
  </ul>
</div>
      );
    },
  },
  {
    imgSrc: `/images/indev-512.jpg`,
    Description: () => {
      return (
<div className={styles.paragraph}>
  <ul>
    <li>Try the latest AI features in development.</li>
    <li>Chat with AIs!</li>
    <li>Watch out for bugs, watch out of the Lisk!</li>
  </ul>
</div>
      );
    },
  },
];

//

export const TitleScreenApp = () => {
  const [context, setContext] = useState(() => new EngineContext());
  const [engine, setEngine] = useState(null);
  const [loadingManager, setLoadingManager] = useState(null);
  const [videoActive, setVideoActive] = useState(false);
  const [appStores, setAppStores] = useState(() => {
    return {
      'adventure': {
        objects: [
          {
            start_url: '/core-modules/terrain/index.js',
            components: [
              {
                key: 'layers',
                value: [],
              },
              {
                key: 'timeRate',
                value: 15000,
              },
            ],
          },
          {
            start_url: '/core-modules/cinematic-camera/index.js',
          },
          {
            start_url: '/audio/soundscape-robust-summer-after.mp3',
            components: [
              {key: 'volume', value: 1},
            ],
          },
          {
            start_url: '/audio/calm-jrpg-style-demo2.mp3',
            components: [
              {key: 'volume', value: 0.05},
            ],
          },
        ],
        editable: false,
      },
    };
  });
  // const playerSpec = null;
  // const [engineLoading, setEngineLoading] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [sounds, setSounds] = useState(null);
  const [startPressed, setStartPressed] = useState(isMobile);
  const [cursorPosition, setCursorPosition] = useState(0);
  const [keyPath, setKeyPath] = useState([]);

  const [canvas, setCanvas] = useState(null);
  const canvasRef = useRef();

  const {
    localStorageManager,
    supabaseClient,
  } = context;

  //

  // bind canvas
  useEffect(() => {
    if (canvasRef.current) {
      setCanvas(canvasRef.current);
    }
  }, [canvasRef]);

  // bind loading manager
  useEffect(() => {
    if (engine) {
      (async () => {
        await engine.engineRenderer.waitForRender();
        setLoaded(true);
      })();
    }
  }, [
    engine,
  ]);

  // bind engine
  useEffect(() => {
    if (engine) {
      (async () => {
        const newSounds = engine.sounds;
        await newSounds.waitForLoad();
        setSounds(newSounds);
      })();
    }
  }, [
    engine,
  ]);

  //

  // keyboard controls
  useEffect(() => {
    const keyup = e => {
      switch (e.key) {
        case 'Enter': {
          if (!startPressed) {
            setStartPressed(true);
            sounds && sounds.playSoundName('menuSelect');
          }
          break;
        }
        case 'Backspace':
        case 'Escape':
        {
          if (keyPath.length === 0 && startPressed) {
            setStartPressed(false);
            sounds && sounds.playSoundName('menuBack');
          }
          break;
        }
      }
    };
    globalThis.addEventListener('keyup', keyup);

    return () => {
      globalThis.removeEventListener('keyup', keyup);
    };
  }, [
    sounds,
    startPressed,
    keyPath,
  ]);

  //

  return (
    <>
      <OAuthBinding
        supabaseClient={supabaseClient}
      />
      {/* <LoginProvider
        supabaseClient={supabaseClient}
      >
        {value => {
          return ( */}
            <div className={classnames(styles.titleScreenApp, isMobile ? styles.mobileScreenApp : null)}>
              <LoadingUi
                loadingManager={context.loadingManager}
                loaded={loaded}
              />

              <div className={classnames(
                styles.videoWrap,
                videoActive ? styles.active : null,
                startPressed ? styles.startPressed : null,
              )} onMouseDown={e => {
                setVideoActive(true);
              }} onMouseUp={e => {
                setVideoActive(false);
              }} onClick={e => {
                if (!startPressed) {
                  setStartPressed(true);

                  sounds && sounds.playSoundName('menuSweepIn');
                }
              }}>
                {loaded ?
                  <>
                    <video
                      className={classnames(styles.video, isMobile ? styles.mobile : null)}
                      src='/videos/upstreet.webm'
                      autoPlay
                      muted
                      loop
                      playsInline
                      preload="metadata"
                    />

                    <TitleScreenHeader
                      localStorageManager={localStorageManager}
                      supabaseClient={supabaseClient}
                    />

                    <div className={classnames(styles.versionWrap,isMobile ? styles.mobileWrap : null)}>
                      <div className={classnames(
                        styles.version,
                      )}>BETA r.0.89</div>
                    </div>

                    <div className={classnames(
                      styles.caption,
                      startPressed ? styles.startPressed : null,
                    )}>
                      Press Start
                    </div>
                    <MainMenu
                  enabled={startPressed}
                  className={classnames(
                    mainMenuStyles.needsStartPressed,
                    startPressed ? mainMenuStyles.startPressed : null,
                  )}
                  options={[
                    {
                      label: 'Singleplayer Mode',
                      handler() {
                        location.href = isIC ? '/adventure/' : '/adventure.html';
                      },
                      icon: '/assets/icons/sword1.svg',
                      /* options: [
                        {
                          label: 'Peaceful',
                          handler() {
                            location.href = '/adventure/?difficulty=peaceful';
                          },
                        },
                        {
                          label: 'Hard',
                          handler() {
                            location.href = '/adventure/?difficulty=combat';
                          },
                        },
                      ], */
                    },
                    {
                      label: 'Multiplayer Mode',
                      handler() {
                        location.href = isIC ? '/creative/?multiplayer=1&range=1' : '/creative.html?multiplayer=1&range=1';
                      },
                      icon: '/assets/icons/sword2.svg',
                      /* options: [
                        {
                          label: 'Small (Range 1)',
                          handler() {
                            location.href = '/creative/?range=1';
                          },
                        },
                        {
                          label: 'Medium (Range 2)',
                          handler() {
                            location.href = '/creative/?range=2';
                          },
                        },
                        {
                          label: 'Large (Range 3)',
                          handler() {
                            location.href = '/creative/?range=3';
                          },
                        },
                      ], */
                    },
                    /* {
                      label: 'Generative Agents',
                      handler() {
                        location.href = `https://github.com/M3-org/upstreet-sdk`;
                      },
                    }, */
                    {
                      label: 'Community',
                      handler() {
                        location.href = `https://discord.gg/upstreet`;
                      },
                      alt: true,
                      icon: '/assets/icons/community.svg',
                    },
                    {
                      label: 'Documentation',
                      handler() {
                        location.href = `https://docs.upstreet.ai/`;
                      },
                      alt: true,
                      icon: '/assets/icons/document.svg',
                    },
                    /* {
                      label: 'InDev',
                      handler() {
                        location.href = '/indev/';
                      },
                    }, */
                  ]}
                  onCursorChange={cursorPosition => {
                    setCursorPosition(cursorPosition);
                    sounds && sounds.playSoundName('menuClick');
                  }}
                  onKeyPathChange={keyPath => {
                    setKeyPath(keyPath);
                  }}
                  onSelect={cursorPosition => {
                    sounds && sounds.playSoundName('menuBoop');
                  }}
                />

                {/*
                {!isMobile && loadingDone && (
                  <Slides
                    className={classnames(
                      startPressed ? styles.startPressed : null,
                    )}
                    keyPath={keyPath}
                    slideIndex={cursorPosition}
                    slides={mainMenuSlides}
                  />
                )}
                */}
                  </>
                : null}


                <div className={styles.spacer} />

                {/* {loaded ? <div className={classnames(
                  styles.footer,
                  startPressed ? styles.startPressed : null,
                )}>
                  <div className={styles.background} />
                  <div className={styles.text}>
                    An <b>M3</b> production
                  </div>
                </div> : null} */}
              </div>

              <canvas className={classnames(
                styles.canvas,
              )} ref={canvasRef} />

              {engine ? <StoryUi
                engine={engine}
              /> : null}

              {canvas ? <EngineProvider
                canvas={canvas}
                context={context}

                appStores={appStores}
                playerSpec={null}

                engine={engine}
                setEngine={setEngine}
              /> : null}
            </div>
          {/* );
        }}
      </LoginProvider> */}
    </>
  );
};

import React from 'react';
import ReactDOM from 'react-dom/client';

import {AdventureApp} from './Adventure.jsx';
import '../styles/globals.css';

//

const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

// parse difficulty and range from the query string
const urlParams = new URLSearchParams(window.location.search);
let difficulty = urlParams.get('difficulty');
let range = urlParams.get('range');
range = range ? parseInt(range, 10) : isMobile ? 2 : 3;
const avatar = urlParams.get('avatar');
const debug = urlParams.get('debug') !== null;
let music = urlParams.get('music');
// default music to on
music = music !== null ? music === '1' : true;
//

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <AdventureApp
    difficulty={difficulty}
    range={range}
    avatar={avatar}
    hidable
    debug={debug}
    music={music}
  />
);

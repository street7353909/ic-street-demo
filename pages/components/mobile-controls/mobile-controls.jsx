import React from 'react';

const isMobile =
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  );

const MobileControls = ({ engine }) => {
  const handleJump = (direction) => {
    console.log('jump', direction);
    const { ioManager } = engine;
    ioManager.dispatchEvent(
      new MessageEvent('ioBus', {
        data: { type: 'buttonJump', direction },
      })
    );
  };

  const handleCrouch = (direction) => {
    const { ioManager } = engine;
    console.log('crouch', direction)
    ioManager.dispatchEvent(
      new MessageEvent('ioBus', {
        data: { type: 'buttonCrouch', direction },
      })
    );
  };

  const toggleFly = () => {
    const { ioManager } = engine;
    ioManager.dispatchEvent(
      new MessageEvent('ioBus', {
        data: { type: 'buttonFly' },
      })
    );
  };

  return isMobile ? (
    // for landscape mobile, need to be smaller
    <div className="absolute w-full h-full top-0 left-0 bottom-0 right-0 pointer-events-none">
      <button
      // add drop shadow to button
        className="w-48 h-48 mobile-landscape:w-24 mobile-landscape:h-24 mobile-portrait:h-24 mobile-portrait:w-24 rounded-full border-white bg-center bg-no-repeat bg-white bg-cover bg-opacity-10 mr-2.5 absolute bottom-60 right-48 mobile-landscape:bottom-24 mobile-portrait:bottom-[150px] mobile-landscape:right-32 mobile-portrait:right-32 focus:outline-none pointer-events-auto"
        style={{
          backgroundImage: 'url(/assets/icons/jump.png)',
          backgroundSize: '70% 70%',
          WebkitTapHighlightColor: 'transparent',
          /* drop shadow with no direction specified */
          boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.1)',
          filter: 'drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2))',
        }}
        onContextMenu={(e) => e.preventDefault()}
        // onMouseDown={() => handleJump('down')}
        onTouchStart={() => handleJump('down')}
        onTouchEnd={() => handleJump('up')}
        // onMouseUp={() => handleJump('up')}
      />
      <button
        className="w-32 h-32 mobile-landscape:w-16 mobile-landscape:h-16 mobile-portrait:h-16 mobile-portrait:w-16  rounded-full border-white bg-center bg-no-repeat bg-white bg-cover bg-opacity-10 mr-2.5 focus:outline-none absolute bottom-40 right-16 mobile-landscape:bottom-16 mobile-portrait:bottom-22 pointer-events-auto"
        style={{
          backgroundImage: 'url(/assets/icons/crouch.png)',
          backgroundSize: '70% 70%',
          WebkitTapHighlightColor: 'transparent',
          boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.1)',
          filter: 'drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2))',
        }}
        onContextMenu={(e) => e.preventDefault()}
        onTouchStart={() => handleCrouch('down')}
        // onMouseDown={() => handleCrouch('down')}
        onTouchEnd={() => handleCrouch('up')}
        // onMouseUp={() => handleCrouch('up')}
      />
      <button
        className="w-32 h-32 mobile-landscape:w-16 mobile-landscape:h-16 mobile-portrait:h-16 mobile-portrait:w-16 rounded-full border-white bg-center bg-no-repeat bg-white bg-cover bg-opacity-10 mr-2.5 focus:outline-none absolute bottom-96 right-16 mobile-landscape:bottom-40 mobile-portrait:bottom-[200px] pointer-events-auto"
        style={{
          backgroundImage: 'url(/assets/icons/fly.png)',
          backgroundSize: '70% 70%',
          WebkitTapHighlightColor: 'transparent',
          boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.1)',
          filter: 'drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2))',
        }}
        onContextMenu={(e) => e.preventDefault()}
        onTouchStart={toggleFly}
      />
    </div>
  ) : null;
};

export default MobileControls;

import React, {
  useState,
} from 'react';
import classnames from 'classnames';

import { LoginProvider } from '../login-provider/LoginProvider.jsx';
import { AuthUi } from '../auth-ui/AuthUi.jsx';
import { MetamaskAuthUi } from '../metamask-auth-ui/MetamaskAuthUi.jsx';
import { ICWalletAuthUi } from '../ic-wallet-auth-ui/ICWalletAuthUi.jsx'; // Import the ICWalletAuthUi

import styles from '../../../styles/UserAccountButton.module.css';

const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

const LoginModal = ({
  localStorageManager,
  supabaseClient,
  onClose,
}) => {
  return (
    <div className={styles.loginModal} onMouseDown={e => {
      e.stopPropagation();
    }} onClick={e => {
      e.stopPropagation();
    }}>
      <div className={styles.wrap}>
        <div className={styles.navs}>
          <nav className={styles.nav} onClick={e => {
            onClose && onClose();
          }}>
            <img className={styles.image} src='/images/chevron.svg' />
          </nav>
        </div>
        {/* Display MetamaskAuthUi if MetaMask is available */}
        {typeof window.ethereum !== 'undefined' && window.ethereum.isMetaMask &&
          <MetamaskAuthUi
            localStorageManager={localStorageManager}
            onClose={onClose}
          />
        }
        {/* Add ICWalletAuthUi for IC wallet logins */}
        <ICWalletAuthUi
          localStorageManager={localStorageManager}
          onClose={onClose}
        />
        <AuthUi
          supabaseClient={supabaseClient}
        />
      </div>
    </div>
  );
};

const shortAddress = (address) => address.slice(0, 7) + '...' + address.slice(-4)

export const UserAccountButton = ({
  className,

  localStorageManager,
  supabaseClient,

  onClick,
}) => {
  const [modalOpen, setModalOpen] = useState(false);

  //

  return (
    <LoginProvider
      supabaseClient={supabaseClient}
    >
      {loginValue => {
        const {
          loaded,
          user,
          isDefaultUser,
          ethereumAccountDetails,
        } = loginValue;

        const name = (ethereumAccountDetails ? ethereumAccountDetails.name : '') ||
          (user ? user.name : '');
        const avatarUrl = (ethereumAccountDetails ? ethereumAccountDetails.avatar : '') ||
          (user ? user.avatar_url : '');

        return (
          <div
            className={classnames(
              styles.userAccount,
              className,
              isMobile ? styles.mobile : null,
            )}
            onMouseDown={e => {
              e.preventDefault();
              e.stopPropagation();
            }}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              onClick && onClick();
            }}
          >
            {!isDefaultUser ? <div className={styles.user}>
              <img className={classnames(styles.profileImage, isMobile ? styles.profileImageMobile : null)}
                src={avatarUrl || '/assets/backgrounds/profile-no-image.png'}
                crossOrigin='Anonymous'
              />
              <div className={styles.name}>{name || shortAddress(ethereumAccountDetails.address)}</div>
            </div> : null}
            <div className={classnames(
              styles.button,
              !loaded ? styles.disabled : null,
            )} onMouseDown={e => {
              // e.preventDefault();
              e.stopPropagation();
            }} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              if (!isDefaultUser) {
                supabaseClient.supabase.auth.signOut();
                localStorageManager.setJwt(null);
              } else {
                setModalOpen(!modalOpen);
              }
            }}>
              {(() => {
                if (!loaded) {
                  return (
                    <>
                      <div className={styles.background} />
                      <div className={classnames(styles.text, styles.textMobile)}>Working...</div>
                    </>
                  );
                } else {
                  if (!isDefaultUser) {
                    return (
                      <>
                        <div className={styles.background} />
                        <div className={classnames(styles.text, isMobile ? styles.textMobile : null)}>
                          Sign out
                        </div>
                      </>
                    );
                  } else {
                    return (
                      <>
                        <div className={styles.background} />
                        <div className={classnames(styles.text, isMobile ? styles.textMobile : null)}>
                          Sign in
                        </div>
                      </>
                    );
                  }
                }
              })()}
            </div>
            {modalOpen ? <LoginModal
              localStorageManager={localStorageManager}
              supabaseClient={supabaseClient}

              onClose={e => {
                setModalOpen(false);
              }}
            /> : null}
          </div>
        );
      }}
    </LoginProvider>
  );
};

import React from 'react';
import { Principal } from "@dfinity/principal";
import { HttpAgent, Actor } from "@dfinity/agent";
import { AuthClient } from "@dfinity/auth-client";
import { StoicIdentity } from "ic-stoic-identity";
import styles from '../../../styles/ICWalletAuthUi.module.css';

// URL for the Cloudflare Worker responsible for JWT token generation
const cloudflareJwtUrl = `https://icwalletauth.jamesharbeck.workers.dev/`;

/**
 * Handles login with Plug wallet.
 * Connects to Plug wallet and retrieves the user's principal ID.
 */

// Always returns a userObject.
export const plugLogin = async () => {
    const whitelist = ["zks6t-giaaa-aaaap-qb7fa-cai", "jeb4e-myaaa-aaaak-aflga-cai"]; // Add asset canister to whitelist
    const isConnected = await window.ic.plug.isConnected();
    if (isConnected) {
        const principal = Principal.from(await window.ic.plug.getPrincipal()).toText();
        await window.ic.plug.createAgent({ whitelist });
        const agent = window.ic.plug.agent;
        const provider = "Plug";
        const result = { principal, agent, provider };
        return result;
    } else {
        try {
            await window.ic.plug.requestConnect({ whitelist });
            const agent = window.ic.plug.agent;
            const principal = Principal.from(await window.ic.plug.getPrincipal()).toText();
            return {
                principal: principal,
                agent: agent,
                provider: "Plug"
            };
        } catch (error) {
            console.error('Error logging in:', error);
            throw error; // Rethrow the error to handle it elsewhere if needed
        }
    };
}

/**
 * Handles login with Stoic wallet.
 * Connects to Stoic wallet and retrieves the user's principal ID.
 */
export const stoicLogin = async () => {
    let identity = await StoicIdentity.load().then(async () => {
        return StoicIdentity.connect();
    });
    const agent = new HttpAgent({ identity, host: "https://ic0.app" });
    const principal = identity.getPrincipal().toText();
    return {
        principal: principal,
        agent: agent,
        provider: "Stoic"
    };
};

/**
 * Handles login with NFID.
 * Connects to NFID and retrieves the user's principal ID.
 */
export const nfidLogin = async () => {
    const appName = "Upstreet";
    const appLogo = "https://nfid.one/icons/favicon-96x96.png";
    const authUrl = "https://nfid.one/authenticate/?applicationName=" + appName + "&applicationLogo=" + appLogo + "#authorize";
    const authClient = await AuthClient.create();

    // Await user authentication completion
    await new Promise((resolve, reject) => {
        authClient.login({
            identityProvider: authUrl,
            onSuccess: () => resolve(),
            onError: (e) => reject(e)
        });
    });

    const identity = authClient.getIdentity();
    const agent = new HttpAgent({ identity, host: "https://ic0.app" });
    const principal = identity.getPrincipal().toText();
    return {
        principal: principal,
        agent: agent,
        provider: "NFID"
    };
};

/**
 * Handles login with Internet Identity.
 * Connects to Internet Identity and retrieves the user's principal ID.
 */
export const identityLogin = async () => {
    const authClient = await AuthClient.create();

    // Await user authentication completion
    await new Promise((resolve, reject) => {
        authClient.login({
            identityProvider: "https://identity.ic0.app",
            onSuccess: () => resolve(),
            onError: (e) => reject(e)
        });
    });

    const identity = authClient.getIdentity();
    const agent = new HttpAgent({ identity, host: "https://ic0.app" });
    const principal = identity.getPrincipal().toText();
    return {
        principal: principal,
        agent: agent,
        provider: "Internet Identity"
    };
};

/**
 * Component for Internet Computer Wallet Authentication User Interface.
 * Renders wallet authentication options and handles user interactions.
 */
export const ICWalletAuthUi = ({ localStorageManager, onClose }) => {
    // Handles the login process for a selected wallet type
    const handleICLogin = async (walletType) => {
        let principal;
        try {
            switch (walletType) {
                case 'Plug':
                    principal = (await plugLogin()).principal;
                    break;
                case 'Stoic':
                    principal = (await stoicLogin()).principal;
                    break;
                case 'NFID':
                    principal = (await nfidLogin()).principal;
                    break;
                case 'Internet Identity':
                    principal = (await identityLogin()).principal;
                    break;
                default:
                    throw new Error(`Unsupported wallet type: ${walletType}`);
            }

            // Requests a JWT token from the Cloudflare Worker
            const response = await fetch(cloudflareJwtUrl, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ principalId: principal }),
            });

            const jwt = await response.json();

            // Stores the JWT token in local storage and closes the login modal
            localStorageManager.setJwt(jwt);
            onClose();
        } catch (error) {
            console.error('IC wallet login error:', error);
        }
    };

    // Renders the UI with wallet options
    return window?.ic && (
        <div className={styles.icWalletAuthUi}>
            <div className={styles.buttons}>
                <div className={styles.button} onClick={() => handleICLogin('Plug')}>
                    <img className={styles.image} src='/images/plug.png' />
                    <div className={styles.text}>Plug</div>
                </div>
                <div className={styles.button} onClick={() => handleICLogin('Stoic')}>
                    <img className={styles.image} src='/images/stoic.png' />
                    <div className={styles.text}>Stoic</div>
                </div>
                <div className={styles.button} onClick={() => handleICLogin('NFID')}>
                    <img className={styles.image} src='/images/nfid.png' />
                    <div className={styles.text}>NFID</div>
                </div>
                <div className={styles.button} onClick={() => handleICLogin('Internet Identity')}>
                    <img className={styles.image} src='/images/dfinity.png' />
                    <div className={styles.text}>Internet Identity</div>
                </div>
            </div>
        </div>
    );
};
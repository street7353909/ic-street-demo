import React, { useState, useRef, useEffect } from 'react';
import classnames from 'classnames';

import styles from '../../../styles/QuestUi.module.css';

//

export const QuestUi = ({
  loading = false,
  name = '',
  description = '',
  objectives = [],
}) => {
  return (
    <div className={classnames(
      styles.questUi,
    )}>
      <div className={styles.labelPlaceholder} />
      
      {(name || loading) && <div className={classnames(
        styles.name,
      )}>
        <div className={styles.background} />
        <div className={styles.text}>{(!name && loading) ? 'Loading quest...' : name}</div>
      </div>}
      
      {description && <div className={classnames(
        styles.description,
      )}>
        <div className={styles.background} />
        <div className={styles.text}>{description}</div>
      </div>}
      
      {objectives.length > 0 && <div className={classnames(
        styles.objectives,
      )}>
        {objectives.map((objective, i) => {
          return (
            <div className={classnames(
              styles.objective,
            )} key={i}>
              <div className={styles.background} />
              <div className={styles.text}><span className={styles.monospace}>[ ]</span> {objective.description} ({objective.xp} XP)</div>
            </div>
          );
        })}
      </div>}
    </div>
  );
};

export const QuestPlaceholderUi = ({
  selected,
  onClick,
}) => {
  return (
    <div className={classnames(
      styles.questUi,
    )}>
      <div className={styles.row}>
        <div className={classnames(
          styles.label,
        )}>
          <div className={styles.background} />
          <div className={styles.text}>Quest</div>
        </div>
        <nav className={classnames(
          styles.icon,
          selected ? styles.selected : null,
        )} onClick={onClick}>
          <div className={classnames(
            styles.background,
          )} />
          <img src='/images/chevron.png' />
        </nav>
      </div>
    </div>
  );
}
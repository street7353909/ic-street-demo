import React, {useState, useEffect} from 'react';
import {
  MegaChatBox,
} from '../mega-chat-box/MegaChatBox.jsx';
import {
  AnimeSubPlugin,
} from '../plugins/AnimeSubPlugin.jsx';
import {
  SpeechBubblePlugin,
} from '../plugins/SpeechBubblePlugin.jsx';

// import {
//   advanceModes,
//   cameraModes,
//   subtitlesModes,
//   // microphoneModes,
//   // embodyModes,
// } from '../../../packages/engine/managers/story/story-manager.js';
// import {
//   Message,
// } from '../../../packages/engine/managers/lore/message.js';

import styles from '../../../styles/StoryUi.module.css';

//

const RPGAdvanceMode = () => {

};
const WaitAdvanceMode = () => {

};
const DirectorAdvanceMode = () => {

};
const AutoAdvanceMode = ({
  engine,
}) => {
  return (
    <div></div>
  );
};

//

export const StoryUi = ({
  engine,
}) => {
  const [message, setMessage] = useState(null);

  const [inputOpen, setInputOpen] = useState(false);

  const [options, setOptions] = useState(null);
  const [option, setOption] = useState(null);
  const [hoverIndex, setHoverIndex] = useState(null);

  const [speakOpen, setSpeakOpen] = useState(false);
  
  const [running, setRunning] = useState(false);
  const [finished, setFinished] = useState(false);

  const [advanceMode, setAdvanceMode] = useState(() => engine.storyManager.storyAdvance.getMode());
  const [cameraMode, setCameraMode] = useState(() => engine.storyManager.storyCamera.getMode());
  const [subtitlesMode, setSubtitlesMode] = useState(() => engine.storyManager.storySubtitles.getMode());

  //

  // bind story manager
  useEffect(() => {
    const {
      storyManager,
    } = engine;

    function conversationstart(e) {
      const {conversation} = e.data;
      
      // conversation.addEventListener('message', e => {
      //   const {message} = e.data;
      //   setFinished(false);
      //   setMessage(message);
      // });
      conversation.addEventListener('close', e => {
        setMessage(null);
      });
    }
    storyManager.addEventListener('conversationstart', conversationstart);

    const messageexecuterstart = e => {
      const {
        messageExecuter,
      } = e.data;
      messageExecuter.addEventListener('messageplay', e => {
        // setRunning(e.data.running);
        const {message} = e.data;

        setFinished(false);
        setMessage(message);
      });
    };
    storyManager.addEventListener('messageexecuterstart', messageexecuterstart);

    return () => {
      storyManager.removeEventListener('conversationstart', conversationstart);
      // storyManager.removeEventListener('messageexecuterstart', messageexecuterstart);
    };
  }, []);

  // bind story modes
  useEffect(() => {
    const {
      storyManager,
    } = engine;

    const storyadvancemodechange = (e) => {
      const {
        mode,
      } = e.data;
      setAdvanceMode(mode);
    };
    storyManager.storyAdvance.addEventListener('modechange', storyadvancemodechange);

    const storycameramodechange = (e) => {
      const {
        mode,
      } = e.data;
      setCameraMode(mode);
    };
    storyManager.storyCamera.addEventListener('modechange', storycameramodechange);

    const storysubtitlesmodechange = (e) => {
      const {
        mode,
      } = e.data;
      setSubtitlesMode(mode);
    };
    storyManager.storySubtitles.addEventListener('modechange', storysubtitlesmodechange);

    return () => {
      storyManager.storyAdvance.removeEventListener('modechange', storyadvancemodechange);
      storyManager.storyCamera.removeEventListener('modechange', storycameramodechange);
      storyManager.storySubtitles.removeEventListener('modechange', storysubtitlesmodechange);
    };
  }, []);

  // bind options
  useEffect(() => {
    if (options && option) {
      const timeout = setTimeout(() => {
        setOptions(null);
        setOption(null);
        setHoverIndex(null);
      }, 1000);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [options, option]);

  const clearMode = () => {
    setInputOpen(false);
    setSpeakOpen(false);
    setOptions(null);
  };
  // XXX make this available to director advance mode
  const commitMessage = text => {
    clearMode();

    const conversation = engine.storyManager.getConversation();
    const messages = conversation.getMessages();
    const lastMessage = messages[messages.length - 1];
    const playerName = lastMessage.getPlayerName();
    const playerSpec = {
      name: playerName,
    };
    conversation.injectPlayerMessage(playerSpec, text);
  };
  // XXX make this available to RPG advance mode
  const commitAnonymousMessage = text => {
    clearMode();

    const conversation = engine.storyManager.getConversation();
    conversation.injectAnonymousMessage(text);
  };

  // bind speak
  useEffect(() => {
    if (speakOpen) {
      let final_transcript = '';
      const localSpeechRecognition = new webkitSpeechRecognition();

      localSpeechRecognition.interimResults = false;
      localSpeechRecognition.maxAlternatives = 1;
      localSpeechRecognition.onerror = e => {
        console.log('speech recognition error', e);
      };
      localSpeechRecognition.onend = () => {
        if (final_transcript) {
          commitMessage(final_transcript);
        }
      };
      localSpeechRecognition.onresult = event => {
        for (let i = event.resultIndex; i < event.results.length; ++i) {
          final_transcript += event.results[i][0].transcript;
        }
      };
      localSpeechRecognition.start();

      return () => {
        // close the speech recognition, but do not let the result propagate
        localSpeechRecognition.stop();
      };
    }
  }, [
    speakOpen,
  ]);

  //

  return (
    <div className={styles.storyTime}>
      {subtitlesMode === 'Dialog' && <MegaChatBox
        message={message}

        inputOpen={inputOpen}

        options={options}
        option={option}
        hoverIndex={hoverIndex}

        speakOpen={speakOpen}

        progressable={!running}
        finished={finished}

        onOptionSelect={option => {
          commitAnonymousMessage(option);

          engine.sounds.playSoundName('menuSelect');
        }}

        onXClick={e => {
          clearMode();

          const conversation = engine.storyManager.getConversation();
          conversation && conversation.close();

          engine.sounds.playSoundName('menuSelect');
        }}
        
        onInputClick={e => {
          clearMode();

          setInputOpen(true);
        }}

        onOptionsClick={async e => {
          clearMode();

          engine.sounds.playSoundName('menuSelect');

          if (!options) {
            // setRunning(true);

            try {
              const conversation = engine.storyManager.getConversation();
              const options = await conversation.getNextMessageOptions();
              const newOptions = options.map(option => {
                return {
                  message: option,
                };
              });
              setOptions(newOptions);
            } finally {
              // setRunning(true);
            }
          } else {
            setOptions(null);
          }
        }}

        onSpeakClick={e => {
          clearMode();
          setSpeakOpen(!speakOpen);
        }}

        onInputCommit={text => {
          commitMessage(text);
        }}

        onClick={e => {
          if (!running) {
            engine.storyManager.progressConversation();
          }
        }}

        engine={engine}
      >
        {/* 'RPG', // run messages until a choice point
        'Wait', // run messages until a dialogue line, no choice points
        'Director', // do not run messages manually
        'Auto', // auto advance binding to run messages
        */}
        {advanceMode === 'RPG' && <RPGAdvanceMode />}
        {advanceMode === 'Wait' && <WaitAdvanceMode />}
        {advanceMode === 'Director' && <DirectorAdvanceMode />}
        {advanceMode === 'Auto' && <AutoAdvanceMode
          engine={engine}
        />}
      </MegaChatBox>}

      {subtitlesMode === 'Anime' && <AnimeSubPlugin
        engine={engine}
      />}

      {subtitlesMode === 'Bubble' && <SpeechBubblePlugin
        engine={engine}
      />}
    </div>
  );
};
// import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import {
  Tabs,
} from '../inventory-ui/Tabs.jsx';
// import {
//   IconTabs,
// } from '../inventory-ui/IconTabs.jsx';

import {
  dragStartType,
} from '../../../pages/components/drag-and-drop/dragstart.js';

import {
  imageSegmentationMulti,
} from '../../../packages/engine/vqa.js';

import styles from '../../../styles/InventoryUi.module.css';

//

const tabs = [
  {
    label: 'Assets',
    value: 'assets',
  },
  {
    label: 'Lore',
    value: 'lore',
  },
  {
    label: 'Perception',
    value: 'perception',
  },
];
const assets = [
  {
    name: 'Lisk.glb',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/models/The_Basilik_v3.glb',
  },
  {
    name: 'Scillia.npc',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/characters/scillia.npc',
  },
  {
    name: 'Solar Witch.npc',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/characters/solarwitch.npc',
  },
  {
    name: 'silsword.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/silsword/index.js',
  },
  {
    name: 'pistol.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/pistol/index.js',
  },
  {
    name: 'hovercraft.glb',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/hovercraft/hovercraft.glb',
  },
  {
    name: 'mirror.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/mirror/index.js',
  },
  {
    name: 'skybox.blockadelabsskybox',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/blockadelabsskyboxes/world.blockadelabsskybox',
  },
  {
    name: 'wrench.item360',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/item360/wrench.item360',
  },
  {
    name: 'silkworm-slasher.mob',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/mobs/silkworm-slasher.mob',
  },
  {
    name: 'chest.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/chest/index.js',
  },
  {
    name: 'infinifruit.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/infinifruit/index.js',
  },
  {
    name: 'blue-sphere.js',
    preview_url: `https://friddlbqibjnxjoxeocc.supabase.co/storage/v1/object/public/public/previews/529227bb7283b4770afb6f60bed91e61f2dcf94d13ac950b3defd899f8150be8.vrm.png`,
    start_url: '/core-modules/blue-sphere/blue-sphere.js',
  },
];

//

const AssetsContent = ({
}) => {
  return (
    <div className={styles.content}>
      {assets.map((item, i) => {
        return (
          <div className={styles.item} onDragStart={e => {
            const type = item.name.match(/\.([^\.]+)$/)[1];
            dragStartType(type)(e, item);
          }} draggable key={i}>
            <img src={item.preview_url} className={styles.previewImg} draggable={false} />
            <div className={styles.name}>{item.name}</div>
            {/* <nav className={styles.iconBtn}>
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav> */}
          </div>
        );
      })}
    </div>
  );
};
const LoreContent = ({
  engine,
}) => {
  const [lore, setLore] = useState('');

  // bind lore manager
  useEffect(() => {
    const {loreManager} = engine;
    const lore = loreManager.getLore();

    const _update = () => {
      const json = lore.toJSON();
      console.log('update lore json', json);
      const s = JSON.stringify(json, null, 2);
      setLore(s);
    };
    _update();

    const update = e => {
      _update();
    };
    lore.addEventListener('update', update);

    return () => {
      lore.removeEventListener('update', update);
    };
  }, []);

  //

  return (
    <div className={styles.content}>
      <label className={styles.label}>
        <div className={styles.text}>Lore</div>
        <textarea className={styles.textarea} value={lore} readOnly></textarea>
      </label>
    </div>
  );
};
const PerceptionContent = ({
  engine,
}) => {
  const [visible, setVisible] = useState(false);
  const canvasRef = useRef(null);

  //

  useEffect(() => {
    const canvas = canvasRef.current;
    if (canvas) {
      console.log('got canvas', canvas);

      // const ctx = canvas.getContext('2d');
      // ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
      // ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
  }, []);

  //

  return (
    <div className={styles.content}>
      <label className={styles.label}>
        <div className={styles.text}>Perception</div>
        <button className={styles.button} onClick={async e => {
          const {engineRenderer} = engine;
          const {renderer} = engineRenderer;

          const canvas = renderer.domElement;
          const blob = await new Promise((accept, reject) => {
            canvas.toBlob(accept, 'image/png');
          });

          const u = URL.createObjectURL(blob);
          console.log('got url', u);

          const result = await imageSegmentationMulti({
            blob,
          });

          const canvas2 = canvasRef.current;
          canvas2.width = canvas.width;
          canvas2.height = canvas.height;
          // canvas2.styles.width = `${canvas.width}px`;
          // canvas2.styles.height = `${canvas.height}px`;
          const ctx2 = canvas2.getContext('2d');

          // canvas2.style.position = 'fixed';
          // canvas2.style.top = 0;
          // canvas2.style.left = 0;
          // canvas2.style.bottom = 0;
          // canvas2.style.right = 0;
          // canvas2.style.zIndex = 10;
          
          console.log('got canvas', canvas2);

          console.log('got result', result);
          for (const {bbox, label} of result) {
            const [
              x, y,
              w, h,
            ] = bbox;

            ctx2.strokeStyle = 'red';
            ctx2.lineWidth = 3;
            ctx2.strokeRect(x, y, w, h);
          }
          setVisible(true);
        }}>Snapshot</button>
      </label>

      <canvas
        className={classnames(
          styles.canvas,
          styles.perceptionCanvas,
          visible ? null : styles.hidden,
        )}
        ref={canvasRef}
      />
    </div>
  );
};

//

const ContentPathElement = ({
  engine,

  contentPathElement,

  contentPath,
  setContentPath,
}) => {
  const {
    type,
  } = contentPathElement;

  return (
    <>
      {(() => {
        switch (type) {
          case 'assets': {
            return (
              <AssetsContent
                contentPath={contentPath}
                setContentPath={setContentPath}
                contentPathElement={contentPathElement}
              />
            );
          }
          case 'lore': {
            return (
              <LoreContent
                engine={engine}

                contentPath={contentPath}
                setContentPath={setContentPath}
                contentPathElement={contentPathElement}
              />
            );
          }
          case 'perception': {
            return (
              <PerceptionContent
                engine={engine}

                contentPath={contentPath}
                setContentPath={setContentPath}
                contentPathElement={contentPathElement}
              />
            );
          }
          default: {
            throw new Error('invalid asset type: ' + type);
          }
        }
      })()}
    </>
  );
};
const ContentPathUi = ({
  engine,

  contentPath,
  setContentPath,
}) => {
  if (contentPath.length > 0) {
    const contentPathElement = contentPath[contentPath.length - 1];
    return (
      <ContentPathElement
        engine={engine}

        contentPathElement={contentPathElement}

        contentPath={contentPath}
        setContentPath={setContentPath}

        // localStorageManager={localStorageManager}
        // supabaseClient={supabaseClient}
        // sessionUserId={sessionUserId}
        // selectedAvatar={selectedAvatar}
        // setSelectedAvatar={setSelectedAvatar}
        // wearItem={wearItem}

        // address={address}

        // epoch={epoch}
        // setEpoch={setEpoch}

        // debug={debug}
      />
    );
  } else {
    return null;
  }
};

//

export const DebugUi = ({
  engine,

  onClose,
}) => {
  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
    },
  ]);

  //

  return (
    <div className={styles.inventoryUi}>
     <div className={styles.row}>
        <Tabs
          tabs={tabs}

          contentPath={contentPath}
          setContentPath={setContentPath}
        />

        <div className={styles.icon} onClick={e => {
          e.preventDefault();
          e.stopPropagation();

          onClose();
        }}>
          <img className={styles.img} src='/assets/x.svg' draggable={false} />
        </div>
      </div>

      <ContentPathUi
        engine={engine}

        contentPath={contentPath}
        setContentPath={setContentPath}
      />
    </div>
  );
};
import React, {
  useEffect,
} from 'react';

//

export const ChatManagerConversationsBinding = ({
  chatManager,
  conversations,
  setConversations,
}) => {
  // load props from engine
  useEffect(() => {
    if (chatManager) {
      // conversations
      const updateConversations = () => {
        let conversations = chatManager.getConversations();
        conversations = conversations.slice();
        setConversations(conversations);
      };

      // initial
      updateConversations();

      // listen
      chatManager.addEventListener('conversationsload', updateConversations);
      chatManager.addEventListener('conversation', updateConversations);
      chatManager.addEventListener('conversationremove', updateConversations);

      // cleanup
      return () => {
        chatManager.removeEventListener('conversationsload', updateConversations);
        chatManager.removeEventListener('conversation', updateConversations);
        chatManager.removeEventListener('conversationremove', updateConversations);
      };
    }
  }, [
    chatManager,
  ]);
};

//

export const ChatManagerConversationBinding = ({
  chatManager,
  messages,
  setMessages,
  conversation,
  setConversation,
}) => {
  // load props from engine
  useEffect(() => {
    if (chatManager) {
      // messages
      const updateConversation = () => {
        let messages = chatManager.getMessages();
        if (messages.some(m => {
          return !m.getRole;
        })) {
          debugger;
        }
        messages = messages.slice();
        setMessages(messages);

        const conversation = chatManager.getConversation();
        setConversation(conversation);
      };

      // initial
      updateConversation();

      // listen
      chatManager.addEventListener('conversationload', updateConversation);
      chatManager.addEventListener('message', updateConversation);
      chatManager.addEventListener('messageremove', updateConversation);

      // cleanup
      return () => {
        chatManager.removeEventListener('conversationload', updateConversation);
        chatManager.removeEventListener('message', updateConversation);
        chatManager.removeEventListener('messageremove', updateConversation);
      };
    }
  }, [
    chatManager,
  ]);
};
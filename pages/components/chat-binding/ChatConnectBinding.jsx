import {
  useState,
  useEffect,
} from 'react';

//

export const ChatConnectWorldBinding = ({
  chatManager,
  worldId,
  conversation,
}) => {
  useEffect(() => {
    if (chatManager && worldId) {
      let live = true;
      (async () => {
        if (chatManager.worldId !== worldId) {
          await chatManager.connectWorld(worldId);
          if (!live) return;
        }
      })();

      return () => {
        live = false;
        chatManager.disconnectWorld();
      };
    }
  }, [
    chatManager,
    worldId,
  ]);

  return null;
};
import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import {
  UserAccountButton,
} from '../user-account-button/UserAccountButton.jsx';

import {
  DoomClock,
} from '../doom-clock/DoomClock.jsx';
import {
  QuestUi,
  QuestPlaceholderUi,
} from '../quest-ui/QuestUi.jsx';

import {
  HelperUi,
} from '../helper-ui/HelperUi.jsx';

import {
  MainContentWindow,
} from '../content-window/MainContentWindow.jsx';

import {
  advanceModes,
  cameraModes,
  subtitlesModes,
} from '../../../packages/engine/managers/story/story-manager.js';
import {
  microphoneModes,
} from '../../../packages/engine/managers/microphone/microphone-manager.js';
import {
  embodyModes,
} from '../../../packages/engine/managers/embodiment/embodiment-manager.js';

import styles from '../../../styles/Adventure.module.css';

import MobileControls from '../mobile-controls/mobile-controls.jsx';

import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';


const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

//

const getContentPathMode = contentPath => {
  const match = contentPath[0]?.type.match(/^(.*?)Window$/);
  const mode = match ? match[1] : 'play';
  return mode;
};

//

const ModeBinding = ({
  engine,

  contentPath,
  setContentPath,
}) => {
  // bind io manager key controls
  useEffect(() => {
    if (engine) {
      const {
        ioManager,
        pointerLockManager,
      } = engine;

      const keydown = e => {
        const activeElement = document.activeElement;
        // if it's not an input
        if (!activeElement || !['INPUT', 'TEXTAREA'].some(tagName => activeElement.tagName === tagName)) {
          switch (e.key) {
            case 'l':
            case 'L':
            {
              e.preventDefault();
              e.stopPropagation();

              setContentPath(contentPath[0]?.type !== 'sceneEditorWindow' ? [
                {
                  type: 'sceneEditorWindow',
                },
              ] : []);
              break;
            }
            case 'i':
            case 'I':
            {
              e.preventDefault();
              e.stopPropagation();

              const newInventory = contentPath[0]?.type !== 'inventoryWindow';
              if (newInventory) {
                pointerLockManager.exitPointerLock();
              } else {
                pointerLockManager.requestPointerLock();
              }

              setContentPath(newInventory ? [
                {
                  type: 'inventoryWindow',
                },
              ] : []);
              break;
            }
            case 'n':
            case 'N':
            {
              e.preventDefault();
              e.stopPropagation();

              setContentPath(contentPath[0]?.type !== 'aiAgentsWindow' ? [
                {
                  type: 'aiAgentsWindow',
                },
              ] : []);
              break;
            }
            case 'm':
            case 'M':
            {
              e.preventDefault();
              e.stopPropagation();
              if(!landTracker) return console.warn('Skipping map creation: no land tracker');
              const newMap = contentPath[0]?.type !== 'mapWindow';

              if (newMap) {
                pointerLockManager.exitPointerLock();
              } else {
                pointerLockManager.requestPointerLock();
              }

              setContentPath(newMap ? [
                {
                  type: 'mapWindow',
                },
              ] : []);
              break;
            }
            default: {
              break;
            }
          }
        }

        return false;
      };
      ioManager.registerEventHandler('keydown', keydown);

      return () => {
        ioManager.unregisterEventHandler('keydown', keydown);
      };
    };
  }, [
    engine,
    contentPath,
  ]);

  //

  return null;
};

const LoreBinding = ({
  engine,

  contentPath,
  setContentPath,

  mappable,
}) => {
  useEffect(() => {
    if (engine) {
      const {
        playersManager,
        loreManager,
        landManager,
      } = engine;

      const mode = getContentPathMode(contentPath);
      loreManager.setMode(mode);

      if (mappable) {
        landManager.setMode(mode);

        if (mode === 'map') {
          const localPlayer = playersManager.getLocalPlayer();
          landManager.setMapOffset(
            new THREE.Vector3(
              localPlayer.position.x,
              300,
              localPlayer.position.z,
            ),
          );
        }
      }
    }
  }, [
    engine,
    contentPath,
  ]);

  return null;
};

//

const InteractionUi = ({
  engine,
}) => {
  const [capsuleApp, setCapsuleApp] = useState(null);
  const [capsulePhysicsObject, setCapsulePhysicsObject] = useState(null);
  const [appActivateText, setAppActivateText] = useState('');

  const [conversation, setConversation] = useState(null);

  // bind coordinate update
  useEffect(() => {
    if (engine) {
      const {
        interactionManager,
        storyManager,
      } = engine;
      const capsulecollisionupdate = e => {
        const {
          app,
          physicsObject,
        } = e;

        setCapsuleApp(app);
        setCapsulePhysicsObject(physicsObject);

        if (app?.hasComponent('activate')) {
          setAppActivateText('activate');
        } else if (app?.hasComponent('wear')) {
          setAppActivateText('wear');
        } else {
          setAppActivateText('');
        }
      };
      interactionManager.addEventListener('capsulecollisionupdate', capsulecollisionupdate);

      const conversationchange = e => {
        const {
          conversation: newConversation,
        } = e.data;
        setConversation(newConversation);
      };
      storyManager.addEventListener('conversationchange', conversationchange);

      setCapsuleApp(interactionManager.getCapsuleCollisionApp());
      setCapsulePhysicsObject(interactionManager.getCapsuleCollisionPhysicsObject());

      return () => {
        interactionManager.removeEventListener('capsulecollisionupdate', capsulecollisionupdate);
        storyManager.removeEventListener('conversationchange', conversationchange);
      };
    }
  }, [
    engine,
  ]);

  //

  const name = capsulePhysicsObject?.name;
  const description = capsulePhysicsObject?.description;

  //

  return (
    <>
      {(!!capsuleApp && !conversation) && <div className={styles.interactionUi}>
        <div className={styles.row}>
          {appActivateText && <label className={styles.label}>
            <span className={styles.key}>E</span>
            <span className={styles.text}>{appActivateText}</span>
          </label>}
          <label className={styles.label}>
            <span className={styles.key}>G</span>
            <span className={styles.text}>Interact</span>
          </label>

          <div className={styles.details}>
            <div className={styles.name}>{name}</div>
            <div className={styles.description}>{description}</div>
          </div>
        </div>
      </div>}
    </>
  );
};

//

export const SidebarTab = ({
  icon,
  label,
  disabled,
  active,

  onClick,
}) => {
  return (
    <div className={classnames(
      styles.sidebarTab,
      disabled ? styles.disabled : null,
      active ? styles.active : null,
    )} onMouseDown={e => {
      e.stopPropagation();
    }} onClick={e => {
      e.preventDefault();
      e.stopPropagation();

      !disabled && onClick(e);
    }}>
      <div className={styles.background} />
      <img className={styles.icon} src={icon} />
      <span className={styles.label}>{label}</span>
    </div>
  );
};

//

export const SidebarSelector = ({
  label,
  value,
  onChange,
  options,
  icon,
}) => {
  const [hovered, setHovered] = useState(false);
  return (
    <div className={styles.sidebarSelectorWrap }
      /* on mouse over, set hovered to true */
      onMouseOver={() => setHovered(true)}
      /* on mouse out, set hovered to false */
      onMouseOut={() => setHovered(false)}
    >
      <div className={styles.background} />
      <span className={styles.iconWrapper}>{icon && <img className={styles.icon} src={icon} />}</span>
      <span className={styles.sidebarText}>{label}</span>
      <span className={styles.selectionWrapper}>
        <select className={classnames(
          styles.sidebarSelector,
        )} value={value} onChange={e => {
          onChange(e.target.value);
        }}>
          {options.map((option, i) => {
            return (
              <option className={styles.sidebarOption} key={i}>{option}</option>
            );
          })}
        </select>
      </span>
    </div>
  );
};

//

const SidebarTabs = ({
  engine,
  hidden,
  headerHover,
  setHeaderHover,
  contentPath,
  setContentPath,
  user,
  showMap,
  showPlayback,
  debug,
}) => {
  const [playing, setPlaying] = useState(false);
  const [advanceMode, setAdvanceMode] = useState(() => engine.storyManager.storyAdvance.getMode() ?? advanceModes[0]);
  const [cameraMode, setCameraMode] = useState(() => engine.storyManager.storyCamera.getMode() ?? cameraModes[0]);
  const [subtitlesMode, setSubtitlesMode] = useState(() => engine.storyManager.storySubtitles.getMode() ?? subtitlesModes[0]);
  const [microphoneMode, setMicrophoneMode] = useState(() => engine.microphoneManager.getMode() ?? microphoneModes[0]);
  const [embodyMode, setEmbodyMode] = useState(() => engine.microphoneManager.getMode() ?? embodyModes[0]);

  // bind story manager
  // bind playing
  useEffect(() => {
    const {storyManager} = engine;
    const conversationchange = e => {
      setPlaying(!!e.data.conversation);
    };
    storyManager.addEventListener('conversationchange', conversationchange);

    return () => {
      storyManager.removeEventListener('conversationchange', conversationchange);
    };
  }, [
    // engine.storyManager,
  ]);

  //

  return (
    // <div className={styles.sidebarTabsWrap}>
      <div className={classnames(
        styles.sidebarTabs,
        styles.visible,
          // headerHover ? styles.visible : null,
      )}
      style={{
        // align content to the right
        textAlign: 'right',
      }}
      >
        {/* <button className={styles.button}
          style={{
            float: 'right',
            userSelect: 'none',
            pointerEvents: 'all',
            // flex align to the right
            marginLeft: 'auto',
            display: headerHover ? 'none' : 'block',
            position: headerHover ? 'relative' : 'fixed',
            top: headerHover ? 0 : '45vh',
            right: 0
          }}
        onClick={e => {
          setHeaderHover(!headerHover);
          // if the user clicks outside of a SidebarTab, close the sidebar

          // add event listener to close sidebar
          const click = e => {
            // if the user clicks outside of the sidebar
            if (e.target.closest(`.${styles.sidebarTabs}`) === null) {
              setHeaderHover(false);
              // remove event listener
              globalThis.removeEventListener('click', click);
              // prevent propagation
              e.stopPropagation();
              e.preventDefault();
            }
          };
          globalThis.addEventListener('click', click);
        }
        }>
          <img style={{
            width: '2.5em',
            margin: '1em',
            opacity: headerHover ? 1.0 : 0.8,
            // drop shadow
            filter: 'drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.3))',
          }} src='/images/ui/menu.svg' draggable={false} />

        </button> */}
        <SidebarTab
          icon='/ui/assets/icons/head.svg'
          label='Profile'
          onClick={e => {
            const item = structuredClone(user.playerSpec);
            const newContentPath = [
              {
                type: 'profileWindow',
                childContentPath: [
                  {
                    type: 'profile',
                    item,
                  },
                ],
              },
            ];
            setContentPath(newContentPath);
          }}
        />
        <SidebarTab
          icon='/ui/assets/icons/mint.svg'
          label='Editor'
          onClick={e => {
            setContentPath([
              {
                type: 'sceneEditorWindow',
              },
            ]);
          }}
        />
        <SidebarTab
          icon='/images/ui/buster-sword.svg'
          label='Apps'
          onClick={e => {
            setContentPath([
              {
                type: 'sceneEditorWindow',
                childContentPath: [
                  {
                    type: 'apps',
                  },
                ],
              },
            ]);
          }}
        />
        <SidebarTab
          icon='/images/metaverse-world.svg'
          label='Worlds'
          onClick={e => {
            setContentPath([
              {
                type: 'worldsWindow',
              },
            ]);
          }}
        />
        {showMap &&
          <SidebarTab
            icon='/ui/assets/icons/map.svg'
            label='Map'
            onClick={e => {
              setContentPath([
                {
                  type: 'mapWindow',
                },
              ]);
            }}
          />
        }
        {debug &&
          <>
            <SidebarTab
              icon='/ui/assets/icons/classDropHunter.svg'
              label='Inventory'
              onClick={e => {
                setContentPath([
                  {
                    type: 'inventoryWindow',
                  },
                ]);
              }}
              disabled={!debug}
            />
            <SidebarTab
              icon='/images/market.svg'
              label='Marketplace'
              onClick={e => {
                setContentPath([
                  {
                    type: 'marketplaceWindow',
                  },
                ]);
              }}
              disabled={!debug}
            />
            <SidebarTab
              icon='/images/metaverse-ai-art.svg'
              label='Gen AI'
              onClick={e => {
                setContentPath([
                  {
                    type: 'genAiWindow',
                  },
                ]);
              }}
              disabled
            />
            <SidebarTab
              icon='/ui/assets/icons/intelligent.svg'
              label='AI Agents'
              onClick={e => {
                setContentPath([
                  {
                    type: 'aiAgentsWindow',
                  },
                ]);
              }}
              disabled
            />
        </>
       }
        <SidebarTab
          icon='/ui/assets/icons/world-edit.svg'
          label='Settings'
          onClick={e => {
            setContentPath([
              {
                type: 'settingsWindow',
              },
            ]);
          }}
          disabled={!debug}
        />

        <div className={styles.spacer} />
        {showPlayback &&
          <>
            <SidebarTab
                icon={playing ? '/images/pause.svg' : '/images/play.svg'}
                label={playing ? 'Pause' : 'Play'}
                onClick={async e => {

                  if(playing){
                    if(!debug) {
                      engine.storyManager.storyAdvance.setMode('Wait');
                      setAdvanceMode('Wait');
                    }
                    engine.storyManager.stopConversation();
                    setPlaying(false);
                    return
                  }

                  if(!debug) {
                    engine.storyManager.storyAdvance.setMode('Auto');
                    setAdvanceMode('Auto');
                    setPlaying(true);
                  }
                  await engine.storyManager.startScene();
                }}
                active={playing}
            />
            <SidebarSelector
              icon='/images/ui/camera.svg'
              label='Camera'
              value={cameraMode}
              onChange={newCameraMode => {
                engine.storyManager.storyCamera.setMode(newCameraMode);
                setCameraMode(newCameraMode);
              }}
              options={cameraModes}
            />
            <SidebarSelector
              icon='/images/ui/repeat.svg'
              label='Advance'
              value={advanceMode}
              onChange={newAdvanceMode => {
                engine.storyManager.storyAdvance.setMode(newAdvanceMode);
                setAdvanceMode(newAdvanceMode);
              }}
              options={advanceModes}
            />
            <SidebarSelector
              icon='/images/voice2.svg'
              label='Subtitles'
              value={subtitlesMode}
              onChange={newSubtitlesMode => {
                engine.storyManager.storySubtitles.setMode(newSubtitlesMode);
                setSubtitlesMode(newSubtitlesMode);
              }}
              options={subtitlesModes}
            />
          </>
        }

        {debug && <>
          <SidebarSelector
            label='Microphone'
            value={microphoneMode}
            onChange={newMicrophoneMode => {
              engine.microphoneManager.setMode(newMicrophoneMode);
              setMicrophoneMode(newMicrophoneMode);
            }}
            options={microphoneModes}
          />

          <SidebarSelector
            label='Embody'
            value={embodyMode}
            onChange={newEmbodyMode => {
              engine.embodimentManager.setMode(newEmbodyMode);
              setEmbodyMode(newEmbodyMode);
            }}
            options={embodyModes}
          />
        </>}
      </div>
    // </div>
  );
};

//

export const GameHeader = ({
  loaded,
  localStorageManager,
  supabaseClient,
  sessionUserId,
  address,
  user,
  isDefaultUser,
  accountManager,

  engine,

  contentPath,
  setContentPath,
  appManagerName,
  landTracker,
  // changed,
  // setChanged,

  // getSceneJson,
  // setSceneJson,
  // onSave,
  sceneTracker,

  mappable,
  questable,
  hidable,

  debug,
}) => {
  const [headerHover, setHeaderHover] = useState(false);
  const [hidden, setHidden] = useState(false);
  const [topRightHover, setTopRightHover] = useState(false);

  const [questLoading, setQuestLoading] = useState(false);
  const [questEnabled, setQuestEnabled] = useState(false);
  const [quest, setQuest] = useState(null);

  // const [debugOpen, setDebugOpen] = useState(false);

  // const headerRef = useRef();

  //

  const mode = getContentPathMode(contentPath);

  //

  const onActivate = item => {
    // console.log('activate', item);
    // debugger;

    if ([
      'voice',
      'wearable',
      'sittable',
      'mount',
      'vehicle',
      'skill',
    ].includes(item.type)) {
      (async () => {
        const {playersManager} = engine;
        const localPlayer = playersManager.getLocalPlayer();
        const position = localPlayer.position.clone();
        const app = await localPlayer.appManager.addAppAsync({
          start_url: item.start_url,
          position,
        });
        localPlayer.wear(app);
      })();
    } else if (item.type === 'vrm') {
      const playerSpec = accountManager.getPlayerSpec();

      const newPlayerSpec = {
        ...playerSpec,
        name: item.name,
        avatarUrl: item.start_url,
      };
      accountManager.setPlayerSpec(newPlayerSpec);
    } else {
      console.log('activate', item);
    }
  };
  const onSell = item => {
    const newContentPath = [
      {
        type: 'marketplaceWindow',
        childContentPath: [
          {
            type: 'store',
          },
          {
            type: 'storeSell',
            item,
          },
        ],
      },
    ];
    setContentPath(newContentPath);
  };

  // bind top right hover
  useEffect(() => {
    if(isMobile) return;
    const mousemove = e => {
      const range = 50; // pixels

      // if within top right range
      const {
        clientX,
        clientY,
      } = e;
      const {
        innerWidth,
        innerHeight,
      } = globalThis;
      const x = clientX / innerWidth;
      const y = clientY / innerHeight;
      const topRightHover = x >= 1 - range / innerWidth && y <= range / innerHeight;
      setTopRightHover(topRightHover);
    };
    globalThis.addEventListener('mousemove', mousemove);

    return () => {
      globalThis.removeEventListener('mousemove', mousemove);
    };
  }, []);

  // bind header hover
  // useEffect(() => {
  //   if (isMobile) return;
  //   const mousemove = e => {
  //     // const headerEl = headerRef.current;
  //     // const headerBounds = headerEl.getBoundingClientRect();

  //     const radius = 200;
  //     const sideBounds = {
  //       left: window.innerWidth - radius,
  //       right: window.innerWidth,
  //       top: 0,
  //       bottom: window.innerHeight,
  //     };

  //     const isInBounds = bounds =>
  //       e.clientX >= bounds.left &&
  //       e.clientX <= bounds.right &&
  //       e.clientY >= bounds.top &&
  //       e.clientY <= bounds.bottom;

  //     const headerHovered =
  //       // isInBounds(headerBounds) ||
  //       isInBounds(sideBounds);
  //     setHeaderHover(headerHovered);
  //   };
  //   globalThis.addEventListener('mousemove', mousemove);

  //   return () => {
  //     globalThis.removeEventListener('mousemove', mousemove);
  //   };
  // }, [
  //   // headerRef.current,
  // ]);

  // bind battle royale quest
  useEffect(() => {
    if (questEnabled) {
      (async () => {
        setQuestLoading(true);

        try {
          const res = await fetch('https://quest-api.upstreet.ai/goal');
          if (res.ok) {
            const j = await res.json();
            setQuest(j);
          } else {
            console.warn('failed to fetch battle royale quest', res.status);
          }
        } catch(err) {
          console.warn('failed to fetch battle royale quest', err);
        } finally {
          setQuestLoading(false);
        }
      })();
    }
  }, [
    questEnabled,
  ]);

  // bind unhide on content path change
  useEffect(() => {
    setHidden(false);
  }, [
    contentPath,
  ]);

  //

  return (
    <div
      className={classnames(
        styles.header,
      )}
      // ref={headerRef}
    >
      <ToastContainer
        position="top-left"
      />

      {loaded && !hidden &&
        <MobileControls engine={engine} />
      }
      {hidable && hidden && <div className={classnames(
        styles.wrap,
        styles.slide,
        topRightHover || isMobile ? styles.visible : null,
      )}>
        <div className={styles.row}>
          {/* <div className={styles.spacer} /> */}

          <div className={classnames(
            styles.button,
            styles.reverse,
          )} onMouseDown={e => {
            // e.preventDefault();
            e.stopPropagation();
          }} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            setHidden(!hidden);
          }}>
            <div className={styles.background} />
            <img className={styles.img} src='/images/chevron.png' draggable={false} />
          </div>
        </div>
      </div>}

      <div className={classnames(
        styles.wrap,
        (!loaded || hidden) ? styles.hidden : null,
      )}>
        <div className={styles.row}>
          <div className={styles.spacer} />

          <UserAccountButton
            className={classnames(styles.userAccount, styles.userAccountMobile)}

            localStorageManager={localStorageManager}
            supabaseClient={supabaseClient}

            onClick={e => {
              const item = structuredClone(user.playerSpec);

              setContentPath([
                {
                  type: 'profileWindow',
                  childContentPath: [
                    {
                      type: 'profile',
                      item,
                    },
                  ],
                },
              ]);
            }}
          />

          {hidable && <div className={classnames(styles.button)} onMouseDown={e => {
            e.stopPropagation();
          }} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            setHidden(!hidden);
          }}>
            <div className={styles.background} />
            <img className={styles.img} src='/images/chevron.png' draggable={false} />
          </div>}
        </div>

        {loaded && questable && user && <>
          <QuestPlaceholderUi
            selected={questEnabled}
            onClick={e => {
              setQuestEnabled(!questEnabled);
            }}
          />
          {questEnabled && <>
            <QuestUi
              loading={questLoading}
              name={quest?.name}
              description={quest?.description}
              objectives={quest?.objectives}
            />
            <DoomClock />
          </>}
        </>}

        {mode === 'play' && !headerHover && <InteractionUi
          engine={engine}
        />}

        {loaded && !hidden && <HelperUi
          localStorageManager={localStorageManager}

          engine={engine}
          contentPath={contentPath}
          setContentPath={setContentPath}

          loaded={loaded}
        />}

        {/* main content */}

        <MainContentWindow
          contentPath={contentPath}
          setContentPath={setContentPath}

          engine={engine}

          supabaseClient={supabaseClient}
          sessionUserId={sessionUserId}
          address={address}
          user={user}
          accountManager={accountManager}

          appManagerName={appManagerName}
          // changed={changed}
          // setChanged={setChanged}

          sceneTracker={sceneTracker}
          // getSceneJson={getSceneJson}
          // setSceneJson={setSceneJson}
          // onSave={onSave}

          localStorageManager={localStorageManager}
          onActivate={onActivate}
          onSell={onSell}

          // debugOpen={debugOpen}
          debug={debug}
        />

        {/* bindings */}

        <ModeBinding
          engine={engine}

          contentPath={contentPath}
          setContentPath={setContentPath}
        />
        <LoreBinding
          engine={engine}

          contentPath={contentPath}
          setContentPath={setContentPath}

          mappable={mappable}
        />
      </div>

      <div className={styles.wrapSub}>
        {loaded && contentPath.length === 0 && !hidden && <>
          {!isDefaultUser ?
            <SidebarTabs
              hidden={hidden}
              engine={engine}
              setHeaderHover={setHeaderHover}
              headerHover={headerHover}
              contentPath={contentPath}
              setContentPath={setContentPath}
              showMap={!!landTracker}
              showPlayback={!landTracker}
              user={user}

              debug={debug}
            />
          :
            null
          }
        </>}
      </div>
    </div>
  );
};

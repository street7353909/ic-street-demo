import React, {
  useState,
  useEffect,
  useRef,
  // Fragment,
} from 'react';
import classnames from 'classnames';

// import {
//   PlayerSpecsBinding,
// } from '../../bindings/PlayerSpecsBinding.jsx';

import {
  ContentPathUi,
} from '../content-path/ContentPath.jsx';

import {
  ChatConnectWorldBinding,
} from '../../components/chat-binding/ChatConnectBinding.jsx';
import {
  ChatManagerConversationsBinding,
  ChatManagerConversationBinding,
} from '../../components/chat-binding/ChatManagerBinding.jsx';

// import {
//   QueueManager,
// } from '../../../packages/engine/managers/queue/queue-manager.js';

import {
  aiMessageTypes,
} from '../../../packages/engine/managers/lore/messages.jsx';

import styles from '../../../styles/ChatUi.module.css';
import topBarStyles from '../../../styles/TopBar.module.css';

//

const Messages = ({
  messages,
  onRemove,
}) => {
  const messagesRef = useRef();

  useEffect(() => {
    const messagesEl = messagesRef.current;
    if (messagesEl) {
      messagesEl.scrollTop = messagesEl.scrollHeight;
    }
  }, [
    messagesRef.current,
    messages.map(m => {
      if (!m.getRole) {
        debugger;
      }
      return m.getRole() + ':' + m.getContent();
    }).join(','), // when messages change
  ]);

  return (
    <div className={styles.chatMessages} ref={messagesRef}>
      {messages.map((message, i) => {
        return (
          <div className={styles.chatMessageRow} key={i}>
            {message.toReact({
              styles,
              key: i,
            })}
            <nav
              className={styles.x}
              onClick={e => {
                onRemove(message);
              }}
            >
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav>
          </div>
        );
      })}
    </div>
  );
};

//

export const MessagesContent = ({
  contentPath,
  setContentPath,

  chatOpen,
  setChatOpen,

  conversation,
  messages,

  engine,

  debug,
}) => {
  const [text, setText] = useState('');
  //
  const inputRef = useRef(null);

  //

  // focus chat on open
  useEffect(() => {
    if (chatOpen) {
      inputRef.current.focus();
    }
  }, [
    chatOpen,
  ]);

  //

  const _commitChat = (text) => {
    if (text) {
      const playersManager = engine.playersManager;
      const loreManager = engine.loreManager;
      const chatManager = engine.chatManager;
      const localPlayer = playersManager.getLocalPlayer();

      const _parseCommandMessage = (text) => {
        const match = text.match(/^\/([^\s]+)\s*(.*)$/);
        if (match) {
          const commandString = match[1];
          const argsString = match[2];
          const args = argsString.split(/\s+/).filter(s => s);
          switch (commandString.toLowerCase()) {
            case 'say': {
              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundSayMessage({
                characterName,
                message: argsString,
              }) ?? false;
            }
            case 'emotion': {
              const emotion = args[0];
              if (!emotion) {
                return false;
              }

              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundEmotionMessage({
                characterName,
                emotion,
              }) ?? false;
            }
            case 'emote': {
              const emote = args[0];
              if (!emote) {
                return false;
              }

              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundEmoteMessage({
                characterName,
                emote,
              }) ?? false;
            }
            case 'facetoward': {
              const target = args[0];

              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundFaceTowardMessage({
                characterName,
                target,
              }) ?? false;
            }
            case 'moveto': {
              let target = args[0];
              target = loreManager.autocompleteActorName(target);

              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundMoveToMessage({
                characterName,
                target,
              }) ?? false;
            }
            case 'lookat': {
              const target = args[0];
              if (!target) {
                return false;
              }

              let characterName = localPlayer.playerSpec.name;
              return loreManager.createBoundLookAtMessage({
                characterName,
                target,
              }) ?? false;
            }
          }
          return false;
        } else {
          return null;
        }
      };
      
      let message = _parseCommandMessage(text);
      if (message !== false) {
        if (!message) {
          const {
            supabaseClient,
          } = engine;
          const profile = supabaseClient.getProfile();
          const name = profile.user.name;
          message = loreManager.createAnonymousChatMessage({
            content: `${name}: ${text}`,
          });
        }
        chatManager.addMessage(message, {
          source: 'local',
        });

        engine.storyManager.interrupt();
      } else {
        console.warn('invalid command', {
          text,
        });
      }

      setText('');
    } else {
      inputRef.current.blur();

      const pointerLockManager = engine.pointerLockManager;
      pointerLockManager.requestPointerLock();

      setChatOpen(false);
    }
  };

  return (
    <>
      <ConversationsSelector
        contentPath={contentPath}
        setContentPath={setContentPath}

        conversation={conversation}

        engine={engine}
      />

      <Messages
        messages={messages}
        onRemove={async message => {
          await engine.chatManager.removeMessage(message);
        }}
      />

      <div className={styles.bottomBar}>
        <input
          type='text'
          className={styles.input}
          value={text}
          onChange={e => {
            setText(e.target.value);
          }} onKeyDown={e => {
            switch (e.key) {
              case 'Enter': {
                e.preventDefault();
                e.stopPropagation();

                _commitChat(text);
                break;
              }
            }
          }}
          ref={inputRef}
        />

        {debug && <>
          <select className={styles.select} value={playerSpecIndex} onChange={e => {
            setPlayerSpecIndex(e.target.value);
          }}>
            <option value=''>Character</option>
            {
              playerSpecs.map((playerSpec, i) => {
                return (
                  <option value={i} key={i}>{playerSpec.name}</option>
                );
              })
            }
          </select>
          <select className={styles.select} value={functionName} onChange={e => {
            setFunctionName(e.target.value);
          }}>
            <option value=''>Function</option>
            {
              Object.keys(aiMessageTypes).map((functionName, i) => {
                return (
                  <option value={functionName} key={i}>{functionName}</option>
                );
              })
            }
          </select>
          <div className={topBarStyles.buttons}>
            <div className={classnames(
              topBarStyles.button,
              nexting ? topBarStyles.disabled : null,
            )} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              if (!nexting) {
                setNexting(true);

                const storyManager = engine.storyManager;
                const opts = {};
                if (functionName) {
                  opts.functionName = functionName;
                }
                if (playerSpecIndex) {
                  const playerSpecIndexInt = parseInt(playerSpecIndex, 10);
                  const playerSpec = playerSpecs[playerSpecIndexInt];
                  if (!playerSpec) {
                    throw new Error(`invalid player spec index: ${playerSpecIndex}`);
                  }
                  opts.playerSpec = playerSpec;
                }
                const message = await storyManager.nextMessageAnonymous(opts);

                setNexting(false);
              }
            }}>
              <div className={topBarStyles.background} />
              <img className={classnames(
                topBarStyles.img,
                topBarStyles.small,
              )} src='/images/chevrons.svg' />
              <div className={topBarStyles.text}>Next</div>
            </div>
            <div className={classnames(
              topBarStyles.button,
              autoEnabled ? topBarStyles.selected : null,
            )} onClick={async e => {
              setAutoEnabled(!autoEnabled);
            }}>
              <div className={topBarStyles.background} />
              <img className={classnames(
                topBarStyles.img,
                topBarStyles.small,
              )} src='/images/infinity.svg' />
              <div className={topBarStyles.text}>Auto</div>
            </div>
          </div>
        </>}
      </div>
    </>
  );
};

//

const defaultConversationText = 'New conversation';
export const ConversationsSelector = ({
  contentPath,
  setContentPath,
  
  conversation,

  engine,
}) => {
  const getConversationName = () => conversation?.name || defaultConversationText;

  const [name, setName] = useState('');
  const [editingName, setEditingName] = useState(false);

  //

  return (
    <div className={styles.conversationsSelector}>
      <div className={styles.conversationName}
        onClick={e => {
          setName(conversation.name);
          setEditingName(true);
        }}
      >
        <img className={styles.icon} src='/images/conversation.svg' draggable={false} />
        {editingName ?
          <input className={styles.input} type='text' value={name} onChange={e => {
            setName(e.target.value);
          }} onKeyDown={e => {
            e.stopPropagation();

            // handle enter
            switch (e.key) {
              case 'Enter': {
                e.preventDefault();
                e.stopPropagation();

                (async () => {
                  await engine.chatManager.setConversationName(conversation.id, name);
                })();

                setEditingName(false);
                break;
              }
            }
          }} placeholder={defaultConversationText} />
        :
          <>{getConversationName()}</>
        }
      </div>

      <div className={styles.conversationsSelectorButton} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'chooseConversation',
            onChange: ({itemValue}) => {
              debugger;
              // const newContentPath = [
              //   ...contentPath.slice(0, -1),
              //   {
              //     ...contentPath[contentPath.length - 1],
              //     item: {
              //       previewImg: '/images/conversation.svg',
              //     },
              //   },
              // ];
              // setContentPath(newContentPath);
            },
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img className={styles.icon} src='/images/chevron.png' draggable={false} />
      </div>
    </div>
  );
};

//

/* const loadConversations = async ({
  supabaseClient,
}) => {
  const result = await supabaseClient.supabase
    .from('conversations')
    .select('*')
    .order('id', {
      ascending: false,
    });
  const {
    data: conversations,
  } = result;
  return conversations;
}; */

export const ChooseConversationContent = ({
  // localStorageManager,

  supabaseClient,
  // sessionUserId,
  // user,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  engine,

  worldId,

  // debug,
}) => {
  const [conversations, setConversations] = useState([]);
  // const [loading, setLoading] = useState(true);
  // const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  const loading = false;

  //

  /* useEffect(() => {
    const abortController = new AbortController();
    const {signal} = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadConversations({
          supabaseClient,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, [
    epoch,
  ]); */

  //

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //
  
  return (
    <div className={styles.chooseConversationContent}>
      <ChatManagerConversationsBinding
        chatManager={engine?.chatManager}
        conversations={conversations}
        setConversations={setConversations}
      />

      <div className={styles.items}>
        <div className={classnames(
          styles.item,
          styles.backItem,
        )} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          // const conversation = await engine.chatManager.createConversation();
          // await engine.chatManager.setConversation(conversation);

          setContentPath([
            ...contentPath.slice(0, -1),
          ]);
        }}>
          <img src='/images/back-angled.svg' className={classnames(
            styles.previewImg,
            styles.placeholderImg,
          )} draggable={false} />
          <div className={styles.name}>Back</div>
        </div>

        <div className={classnames(
          styles.item,
          styles.createItem,
        )} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          const conversation = await engine.chatManager.createConversation();
          await engine.chatManager.setConversation(conversation);

          setContentPath([
            ...contentPath.slice(0, -1),
          ]);
        }}>
          <img src='/assets/icons/plus.svg' className={classnames(
            styles.previewImg,
            styles.placeholderImg,
          )} draggable={false} />
          <div className={styles.name}>New conversation</div>
        </div>

        {!loading && conversations.map(item => {
          return (
            <div className={styles.item} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              // setConversationId(item.id);
              (async () => {
                // console.log('connect conversation', item);
                await engine.chatManager.setConversation(item);

                setContentPath([
                  ...contentPath.slice(0, -1),
                ]);

                // onClick && onClick(item);
              })();
            }} key={item.id}>
              <img src={'/images/conversation.svg'} className={styles.previewImg} draggable={false} />
              <div className={classnames(
                styles.name,
                !item.name ? styles.dim : null,
              )}>{item.name || defaultConversationText}</div>

              <nav className={styles.iconBtn} onClick={async e => {
                e.preventDefault();
                e.stopPropagation();

                // // remove item
                // const result = await supabaseClient.supabase
                //   .from('conversations')
                //   .delete()
                //   .eq('id', item.id);

                // refreshItems();

                await engine.chatManager.removeConversation(item);
              }}>
                <img className={styles.img} src='/assets/x.svg' draggable={false} />
              </nav>
            </div>
          );
        })}
      </div>

      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}

      {!loading && conversations.length === 0 && <div className={styles.placeholder}>
        No conversations
      </div>}
    </div>
  );
}

//

const contentPathMap = {
  'messages': MessagesContent,
  'chooseConversation': ChooseConversationContent,
};
const ChatContentMain = ({
  chatOpen,
  setChatOpen,

  engine,
  supabaseClient,

  worldId,

  debug,
}) => {
  const makeInitialContentPath = () => [
    {
      type: 'messages',
    },
  ];

  const [contentPath, setContentPath] = useState(makeInitialContentPath);
  const [epoch, setEpoch] = useState(0);

  const [conversations, setConversations] = useState(null);
  const [conversation, setConversation] = useState(null);
  const [messages, setMessages] = useState([]);

  //

  useEffect(() => {
    if (chatOpen) {
      setContentPath(makeInitialContentPath());
    }
  }, [
    chatOpen,
  ]);

  //

  return (
    <>
      <ChatConnectWorldBinding
        chatManager={engine?.chatManager}
        worldId={worldId}
        // conversation={conversation}
      />

      <ChatManagerConversationsBinding
        chatManager={engine?.chatManager}
        conversations={conversations}
        setConversations={setConversations}
      />
      <ChatManagerConversationBinding
        chatManager={engine?.chatManager}
        messages={messages}
        setMessages={setMessages}
        conversation={conversation}
        setConversation={setConversation}
      />

      <ContentPathUi
        contentPath={contentPath}
        setContentPath={setContentPath}
        contentPathMap={contentPathMap}

        epoch={epoch}
        setEpoch={setEpoch}

        chatOpen={chatOpen}
        setChatOpen={setChatOpen}

        engine={engine}
        supabaseClient={supabaseClient}

        worldId={worldId}

        conversation={conversation}
        messages={messages}

        debug={debug}
      />
    </>
  );
};

//

export const ChatUi = ({
  engine,
  supabaseClient,

  worldId,

  debug,
}) => {
  // const [playerSpecs, setPlayerSpecs] = useState([]);
  // const [playerSpecIndex, setPlayerSpecIndex] = useState('');
  // const [functionName, setFunctionName] = useState('');
  // const [nexting, setNexting] = useState(false);
  // const [autoEnabled, setAutoEnabled] = useState(false);
  const [conversation, setConversation] = useState(null);
  const [chatOpen, setChatOpen] = useState(false);

  //

  // listen for chat open keys
  useEffect(() => {
    if (engine) {
      // const storyManager = engine.storyManager;
      const keydown = e => {
        switch (e.key) {
          case 'Enter':
          case '/':
          {
            if (!['INPUT', 'TEXTAREA'].includes(document.activeElement.nodeName)) {
              // if (!storyManager.getConversation()) {
                setChatOpen(true);
              // }
              engine.pointerLockManager.exitPointerLock();
            }
            break;
          }
        }
      };
      globalThis.addEventListener('keydown', keydown);

      return () => {
        globalThis.removeEventListener('keydown', keydown);
      };
    }
  }, [
    engine,
  ]);

  /* // close chat when conversation starts
  useEffect(() => {
    if (engine) {
      const storyManager = engine.storyManager;
      const conversationchange = e => {
        const {
          conversation,
        } = e.data;

        if (conversation && chatOpen) {
          setChatOpen(false);
        }
      };
      storyManager.addEventListener('conversationchange', conversationchange);

      return () => {
        storyManager.removeEventListener('conversationchange', conversationchange);
      };
    }
  }, [
    chatOpen,
  ]); */

  // close chat on pointerlockchange
  useEffect(() => {
    if (engine) {
      const {pointerLockManager} = engine;
      const pointerlockchange = e => {
        const {
          pointerLockElement,
        } = e.data;
        if (pointerLockElement) {
          setChatOpen(false);
        }
      };
      pointerLockManager.addEventListener('pointerlockchange', pointerlockchange);

      return () => {
        pointerLockManager.removeEventListener('pointerlockchange', pointerlockchange);
      };
    }
  }, [
    engine,
  ]);

  //

  return (
    <div className={classnames(
      styles.chatUi,
      chatOpen ? styles.open : null,
    )}>
      {/* <PlayerSpecsBinding
        engine={engine}
        playerSpecs={playerSpecs}
        setPlayerSpecs={setPlayerSpecs}
      /> */}

      <ChatContentMain
        chatOpen={chatOpen}
        setChatOpen={setChatOpen}

        worldId={worldId}

        engine={engine}
        supabaseClient={supabaseClient}

        debug={debug}
      />
    </div>
  );
};
import React, {
  useState,
  useEffect,
} from 'react';
import classnames from 'classnames';
import styles from '../../../styles/LoadingUi.module.css';

//

export const LoadingUi = ({
  loadingManager,
  loaded,
}) => {
  const [currentLoad, setCurrentLoad] = useState(() => loadingManager.currentLoad);
  const [numerator, setNumerator] = useState(() => loadingManager.numerator);
  const [denominator, setDenominator] = useState(() => loadingManager.denominator);
  const [progress, setProgress] = useState(() => loadingManager.progress);
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    // console.log('bind loading manager', loadingManager);
    if (loadingManager) {
      const currentloadupdate = e => {
        // console.log('set current load', e.data.load);
        setCurrentLoad(e.data.load);
      };
      loadingManager.addEventListener('currentloadupdate', currentloadupdate);
      const update = (e) => {
        // console.log('set update', e.data);
        const {numerator, denominator, progress} = e.data;
        setNumerator(numerator);
        setDenominator(denominator);
        setProgress(progress);
      };
      loadingManager.addEventListener('update', update);
      const finish = () => {
        setFinished(true);
      };
      loadingManager.addEventListener('finish', finish);

      return () => {
        console.log('unbind loading manager');
        loadingManager.removeEventListener('currentloadupdate', currentloadupdate);
        loadingManager.removeEventListener('update', update);
        loadingManager.removeEventListener('finish', finish);
      };
    }
  }, [
    loadingManager,
  ]);

  useEffect(() => {
    loaded && loadingManager.setFinishable();
  }, [loaded]);

  return (
    <div className={classnames(
      styles.loadingUi,
      (finished && loaded) ? styles.done : null,
    )}>
      <img src={'/images/breezer-512.jpeg'} className={styles.background} />
      <progress className={styles.progress} value={numerator} max={denominator} />
      <div className={styles.percent}>{(progress * 100).toFixed(0)}%</div>
      <div className={styles.name}>{currentLoad ? currentLoad.name : '...'}</div>
    </div>
  )
}
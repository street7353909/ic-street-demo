import React, {
  useState,
  useEffect,
} from 'react';
import classnames from 'classnames';

import {
  Tabs,
} from '../inventory-ui/Tabs.jsx';

import styles from '../../../styles/InventoryUi.module.css';

//

export const ContentPathElement = ({
  contentPath,
  contentPathMap,
  ...props
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    type,
  } = contentPathElement;

  const ContentPathEl = contentPathMap[type];

  if (ContentPathEl) {
    return (
      <ContentPathEl
        contentPath={contentPath}
        contentPathMap={contentPathMap}
        {...props}
      />
    );
  } else {
    return null;
  }
};
export const ContentPathUi = ({
  contentPath,
  ...props
}) => {
  if (contentPath.length > 0) {
    return (
      <ContentPathElement
        contentPath={contentPath}
        {...props}
      />
    );
  } else {
    return null;
  }
};

export const ContentPathWindow = ({
  className,

  tabs,
  showTabs = true,

  contentPath,
  setContentPath,
  contentPathMap,

  ...props
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    childContentPath = [
      {
        type: tabs[0].value,
      },
    ],
  } = contentPathElement;
  const setChildContentPath = childContentPath => {
    setContentPath([
      {
        ...contentPathElement,
        childContentPath,
      },
    ]);
  };

  //

  return (
    <div className={classnames(
      styles.inventoryUi,
      className,
    )}>
      <div className={styles.row}>
        {childContentPath.length > 1 && <nav className={styles.backButton} onClick={e => {
          setChildContentPath(childContentPath.slice(0, -1));
        }}>
          <img className={styles.img} src='/images/chevron.png' draggable={false} />
        </nav>}
        {showTabs && <Tabs
          tabs={tabs}

          contentPath={contentPath}
          setContentPath={setContentPath}
        />}

        <div className={styles.icon} onClick={e => {
          e.preventDefault();
          e.stopPropagation();

          setContentPath([]);
        }}>
          <img className={styles.img} src='/assets/x.svg' draggable={false} />
        </div>
      </div>
      <div className={styles.scrollWrap}>

      <ContentPathUi
        contentPath={childContentPath}
        setContentPath={setChildContentPath}
        contentPathMap={contentPathMap}

        {...props}
      />
      </div>
    </div>
  );
};

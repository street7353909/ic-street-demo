import React, {
  useState,
  useEffect,
} from 'react';
// import * as THREE from 'three';
import classnames from 'classnames';

import styles from '../../../styles/InventoryUi.module.css';

//

export const AvatarPicker = ({
  contentPath,
  setContentPath,
  contentPathElement,
}) => {
  const {
    item = null,
  } = contentPathElement;
  const avatar = item?.avatar;

  //

  const chooseAvatar = () => {
    const newContentPath = [
      ...contentPath,
      {
        type: 'chooseAvatar',
      },
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    !avatar ? <>
      <div className={styles.item} onClick={e => {
        // e.preventDefault();
        // e.stopPropagation();

        chooseAvatar();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Choose avatar</div>
      </div>
    </> : <>
      <div className={styles.item} onClick={e => {
        // e.preventDefault();
        // e.stopPropagation();

        chooseAvatar();
      }}>
        <img src='/ui/assets/icons/posture.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>{avatar.name}</div>
      </div>
    </>
  );
};
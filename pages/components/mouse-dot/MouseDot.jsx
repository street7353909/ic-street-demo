// import * as THREE from 'three';
import React, {useEffect, useRef, useState, forwardRef} from 'react';
import classnames from 'classnames';

import styles from './MouseDot.module.css';

//

const frameSize = 64;
const numFrames = 64;
const numFramesPerRow = Math.sqrt(numFrames);
const arrowTime = 5000;
const timeDiff = arrowTime / numFrames;

export const MouseDot = forwardRef(({
  className,
  enabled = true,
  up = false,
  down = false,
  // animate = false,
  // x,
  // y,
  // ax,
  // ay,
  onClick,
}, ref) => {
  return enabled ? (
    <div
      className={classnames(
        styles.mouseDotContainer,
        up ? styles.up : null,
        down ? styles.down : null,
        className,
      )}
      // style={{
      //   transform: `translate(${x}px, ${y}px)`,
      //   left: ax,
      //   top: ay,
      // }}
      onClick={onClick}
      ref={ref}
    >
      <div
        className={classnames(
          styles.mouseDot,
          // animate ? styles.animate : null,
        )}
      >
        <div className={styles.perspective} />
      </div>
    </div>
  ) : null;
});
import React, {
  useState,
  useEffect,
} from 'react';
import classnames from 'classnames';

import styles from '../../../styles/InventoryUi.module.css';

//

export const IconTabs = ({
  iconTabs,
  contentPath,
  setContentPath,
}) => {
  return (
    <div className={styles.iconTabs}>
      {iconTabs.map(iconTab => (
        <div className={classnames(
          styles.iconTab,
          iconTab.value === contentPath[0].type ? styles.selected : null,
        )} onClick={e => {
          setContentPath([
            ...contentPath,
            iconTab.type ? {
              type: iconTab.type,
              // item: iconTab.item,
            } : {
              type: 'assetList',
              assetType: iconTab.value,
            },
          ]);
        }} key={iconTab.value}>
          <img src={iconTab.icon} className={styles.icon} draggable={false} />
          <div className={styles.text}>{iconTab.label}</div>
        </div>
      ))}
    </div>
  );
};
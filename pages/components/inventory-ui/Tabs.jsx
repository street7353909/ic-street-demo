import React, {
  useState,
  useEffect,
} from 'react';
import classnames from 'classnames';

import styles from '../../../styles/InventoryUi.module.css';

//

export const Tabs = ({
  tabs,
  contentPath,
  setContentPath,
}) => {
  const {
    childContentPath = [
      {
        type: tabs[0].value,
      },
    ],
  } = contentPath[contentPath.length - 1] || {};
  const {
    type = tabs[0].value,
  } = childContentPath[0] || {};

  return (
    <div className={styles.tabs}>
      {tabs.map(tab => (
        <div className={classnames(
          styles.tab,
          tab.value === type ? styles.selected : null,
        )} onClick={e => {
          const lastContentPath = contentPath[contentPath.length - 1];
          const lastChildContentPath = lastContentPath?.childContentPath;
          const childContentPathLast = lastChildContentPath ? lastChildContentPath[lastChildContentPath.length - 1] : null;
          const childContentPathLastType = childContentPathLast?.type;

          if (childContentPathLastType !== tab.value) {
            setContentPath([
              ...contentPath.slice(0, -1),
              {
                ...contentPath[contentPath.length - 1],
                childContentPath: [
                  {
                    type: tab.value,
                  },
                ],
              },
            ]);
          }
        }} key={tab.value}>
          {tab.label}
        </div>
      ))}
    </div>
  );
};
import React, {
  useState,
  useEffect,
} from 'react';
// import * as THREE from 'three';
import classnames from 'classnames';

import {
  ContentPathWindow,
  ContentPathUi,
} from '../../../pages/components/content-path/ContentPath.jsx';

import styles from '../../../styles/InventoryUi.module.css';

//

/* const AssetListContent = ({
  supabaseClient,

  sessionUserId,
  address,
  user,
  accountManager,

  localStorageManager,

  contentPath,
  setContentPath,

  wearItem,

  debug,
}) => {
  const [epoch, setEpoch] = useState(0);
  const [selectedAvatar, setSelectedAvatar] = useState(null);

  //

  return (
    <ContentPathUi
      contentPath={contentPath}
      setContentPath={setContentPath}
      contentPathMap={contentPathMap}

      localStorageManager={localStorageManager}
      supabaseClient={supabaseClient}
      sessionUserId={sessionUserId}

      selectedAvatar={selectedAvatar}
      setSelectedAvatar={setSelectedAvatar}

      wearItem={wearItem}

      epoch={epoch}
      setEpoch={setEpoch}

      address={address}
      user={user}
      accountManager={accountManager}
    
      debug={debug}
    />
  );
};

//

export const InventoryUi = ({
  ...props
}) => {
  const {
    user,
  } = props;
  const tabs = [
    {
      label: 'Profile',
      value: 'profile',
      item: {
        avatar: {
          name: user.playerSpec.name,
          start_url: user.playerSpec.avatarUrl,
        },
      },
    },
    {
      label: 'Worlds',
      value: 'worlds',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      tabs={tabs}

      contentPath={contentPath}
      setContentPath={setContentPath}

      Content={AssetListContent}

      {...props}
    />
  );
}; */

import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';
import anime from 'animejs';
import {
  makeId,
} from '../../../packages/engine/util.js';
import styles from '../../../styles/AnimeSubPlugin.module.css';

//

class SubtitleObject extends EventTarget {
  constructor({
    id,
    // engine,
    onClose,
  }) {
    super();

    this.id = id;
    // this.engine = engine;
    this.onClose = onClose;

    this.text = '';
  }

  addMessage(message) {
    // const conversation = this.engine.storyManager.getConversation();
    // if (!conversation) {
      message.addEventListener('playStart', e => {
        const parsed = message.getParsed();
        const {
          name,
          command,
          message: text,
        } = parsed;
        // if (command === 'SAY') {
          // this.text = `[${name.toUpperCase()}]\n${text}`;
          this.text = text;

          this.dispatchEvent(new MessageEvent('textupdate', {
            data: {
              text: this.text,
            },
          }));
        // }
      });
      message.addEventListener('playEnd', e => {
        setTimeout(() => {
          this.onClose();
        }, 3000);
      });
    // }
  }
}

//

const AnimeSub = ({
  subtitle,
}) => {
  const [text, setText] = useState(() => subtitle.text);
  const [cacheMap, setCacheMap] = useState(() => new Map());
  const divRef = useRef();

  //

  const letters = text.split('');

  // bind subtitle
  useEffect(() => {
    const textupdate = e => {
      setText(e.data.text);
    };
    subtitle.addEventListener('textupdate', textupdate);

    return () => {
      subtitle.removeEventListener('textupdate', textupdate);
    };
  }, [
    subtitle,
  ]);

  // bind animejs
  useEffect(() => {
    const div = divRef.current;
    if (div) {
      const oldTimeline = cacheMap.get('timeline');
      const letterEls = Array.from(div.querySelectorAll(`.${styles.letter}`));
      const visible = text.length > 0;

      if (visible && !cacheMap.get('visible')) {
        cacheMap.set('visible', true);

        if (oldTimeline) {
          oldTimeline.pause();
        }

        const newTimeline = anime.timeline({
          // loop: true,
        });
        newTimeline
          .add({
            // targets: `.${styles.ml12}.${className} .${styles.letter}`,
            targets: letterEls,
            opacity: [
              oldTimeline ? oldTimeline.progressValue / 100 : 0,
              1,
            ],
            easing: 'easeOutExpo',
            duration: 1200,
            delay: (el, i) => 30 * i,
            update: anim => {
              newTimeline.progressValue = anim.progress;
            },
          });
        newTimeline.progressValue = 0;

        cacheMap.set('timeline', newTimeline);
      } else if (!visible && cacheMap.get('visible')) {
        cacheMap.set('visible', false);

        if (oldTimeline) {
          oldTimeline.pause();
        }

        const newTimeline = anime.timeline({
          // loop: true,
        });
        newTimeline
          .add({
            // targets: `.${styles.ml12}.${className} .${styles.letter}`,
            // targets: letterEls,
            targets: [div],
            translateY: [0, -30],
            opacity: [
              // oldTimeline ? oldTimeline.progressValue / 100 : 0,
              1,
              0,
            ],
            // easing: 'easeInExpo',
            easing: 'easeOutExpo',
            duration: 200,
            // delay: (el, i) => 5 * i,
            update: anim => {
              newTimeline.progressValue = anim.progress;
            },
            complete: () => {
              // console.log('complete out 2');
              onEnd();
            },
          });
        newTimeline.progressValue = 0;

        cacheMap.set('timeline', newTimeline);
      }
    }
  }, [
    divRef.current,
    text,
  ]);

  // bind animejs cleanup
  useEffect(() => {
    return () => {
      const timeline = cacheMap.get('timeline');
      timeline && timeline.pause();
    }
  }, []);

  //

  return (
    <div className={styles.animeSub}>
      <div ref={divRef} className={classnames(
        styles.text,
        styles.ml12,
        // className,
      )}>{
        letters.map((letter, i) => (
          <span className={styles.letter} key={i}>{letter}</span>
        ))
      }</div>
    </div>
  )
};

//

const AnimeSubs = ({
  subtitles,
}) => {
  return (
    <>
      {subtitles.map(subtitle => {
        return (
          <AnimeSub
            subtitle={subtitle}
            key={subtitle.id}
          />
        );
      })}
    </>
  );
};

//

export const AnimeSubPlugin = ({
  engine,
}) => {
  const [subtitles, setSubtitles] = useState([]);

  //

  useEffect(() => {
    if (engine) {
      const chatManager = engine.chatManager;

      const message = e => {
        const {
          message,
        } = e.data;

        const id = makeId(8);
        const subtitle = new SubtitleObject({
          id,
          engine,
          onClose: () => {
            setSubtitles(subtitles => subtitles.filter(st => st !== subtitle));
          },
        });
        subtitle.addMessage(message);
        setSubtitles([
          subtitle,
        ]);
      };
      chatManager.addEventListener('message', message);

      return () => {
        chatManager.removeEventListener('message', message);
      };
    }
  }, [
    engine,
  ]);

  //

  return (
    <div className={styles.animeSubPlugin}>
      <AnimeSubs
        subtitles={subtitles}
      />
    </div>
  );
};
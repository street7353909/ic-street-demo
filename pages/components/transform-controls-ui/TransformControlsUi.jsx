import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import styles from '../../../styles/Adventure.module.css';

import physicsManager from '../../../packages/engine/physics/physics-manager.js';

//

// const localVector = new THREE.Vector3();
const localEuler = new THREE.Euler();

//

export const TransformControlsUi = ({
  engine,
  app,
  // setApp,

  onChange,
}) => {
  const [positionX, setPositionX] = useState(0);
  const [positionY, setPositionY] = useState(0);
  const [positionZ, setPositionZ] = useState(0);
  const [eulerX, setEulerX] = useState(0);
  const [eulerY, setEulerY] = useState(0);
  const [eulerZ, setEulerZ] = useState(0);
  const [eulerOrder, setEulerOrder] = useState('YXZ');
  const [scaleX, setScaleX] = useState(1);  
  const [scaleY, setScaleY] = useState(1);
  const [scaleZ, setScaleZ] = useState(1);

  // bind app attach
  useEffect(() => {
    if (app) {
      setPositionX(app.position.x);
      setPositionY(app.position.y);
      setPositionZ(app.position.z);

      localEuler.setFromQuaternion(app.quaternion, eulerOrder);
      setEulerX(localEuler.x);
      setEulerY(localEuler.y);
      setEulerZ(localEuler.z);

      setScaleX(app.scale.x);
      setScaleY(app.scale.y);
      setScaleZ(app.scale.z);
    }
  }, [
    app,
  ]);

  // bind transform controls to app
  useEffect(() => {
    if (engine && app) {
      const {transformControlsManager} = engine;
      const transformControls = transformControlsManager.createTransformControls();
      transformControls.attach(app);

      transformControls.addEventListener('change', e => {
        app.updateMatrixWorld();
        _updateAppPhysics(app);

        setPositionX(app.position.x);
        setPositionY(app.position.y);
        setPositionZ(app.position.z);

        localEuler.setFromQuaternion(app.quaternion, eulerOrder);
        setEulerX(localEuler.x);
        setEulerY(localEuler.y);
        setEulerZ(localEuler.z);

        setScaleX(app.scale.x);
        setScaleY(app.scale.y);
        setScaleZ(app.scale.z);

        onChange && onChange();
      });

      return () => {
        transformControlsManager.clearSelectedPhysicsObject();
      };
    }
  }, [
    engine,
    app,
    eulerOrder,
  ]);

  //

  const _recurseApps = (app, fn) => {
    fn(app);
    for (const child of app.children) {
      if (child.isApp) {
        _recurseApps(child, fn);
      }
    }
  }
  const _updateAppPhysics = app => {
    const physicsScene = physicsManager.getScene();
    const {physicsTracker} = engine;

    _recurseApps(app, childApp => {
      const physicsObjects = physicsTracker.getAppPhysicsObjects(childApp);
      for (const physicsObject of physicsObjects) {
        physicsTracker.syncPhysicsObjectTransformToApp(physicsObject);
        physicsScene.setTransform(physicsObject);
      }
    });
  };
  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  return (
    <div className={styles.transformControls}>
      <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.position.x = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionX(value);

            onChange && onChange();
          } else {
            app.position.x = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionX(e.target.value);

            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.position.y = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionY(value);
            
            onChange && onChange();
          } else {
            app.position.y = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionY(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.position.z = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionZ(value);
            
            onChange && onChange();
          } else {
            value = 0;
            app.position.z = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setPositionZ(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>

      <hr/>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            localEuler.set(value, eulerY, eulerZ, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerX(value);
            
            onChange && onChange();
          } else {
            value = 0;
            localEuler.set(value, eulerY, eulerZ, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerX(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            localEuler.set(eulerX, value, eulerZ, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerY(value);
            
            onChange && onChange();
          } else {
            value = 0;
            localEuler.set(eulerX, value, eulerZ, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerY(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            localEuler.set(eulerX, eulerY, value, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerZ(value);
            
            onChange && onChange();
          } else {
            value = 0;
            localEuler.set(eulerX, eulerY, value, eulerOrder);
            app.quaternion.setFromEuler(localEuler);
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setEulerZ(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);

          onChange && onChange();
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label>

      <hr/>

      <label className={styles.label}>
        <span className={styles.text}>s.x</span>
        <input type='number' step={0.1} value={scaleX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setScaleX, 1)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.scale.x = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleX(value);
            
            onChange && onChange();
          } else {
            value = 1;
            app.scale.x = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleX(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>s.y</span>
        <input type='number' step={0.1} value={scaleY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setScaleY, 1)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.scale.y = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleY(value);
            
            onChange && onChange();
          } else {
            value = 1;
            app.scale.y = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleY(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>s.z</span>
        <input type='number' step={0.1} value={scaleZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setScaleZ, 1)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            app.scale.z = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleZ(value);
            
            onChange && onChange();
          } else {
            value = 1;
            app.scale.z = value;
            app.updateMatrixWorld();
            _updateAppPhysics(app);
            setScaleZ(e.target.value);
            
            onChange && onChange();
          }
        }} />
      </label>
    </div>
  );
};
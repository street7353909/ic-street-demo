import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import voiceModels from '../../../public/voice/voice_models.json';
import voicePacks from '../../../public/voice/all_packs.json';
import { voicePacksUrl } from '../../../packages/engine/endpoints.js';

import {
  QueueManager,
} from '../../../packages/engine/managers/queue/queue-manager.js';
import {
  createClient,
} from '../../../packages/engine/clients/alchemy-client.js';

import {
  SupabaseFsWorker,
} from '../../../packages/engine/supabase-fs-worker.js';
// import {
//   LoginProvider,
//   // LoginConsumer,
// } from '../login-provider/LoginProvider.jsx';

import {
  generateImage,
} from '../../../packages/engine/generate-image.js';
import {
  importYoutubeAudio,
  importYoutubeVideo,
} from '../../../packages/engine/youtube.js';

import {
  importFileType,
  addAsset,
} from '../../../pages/components/drag-and-drop/drop.js';
import {
  dragStartType,
} from '../../../pages/components/drag-and-drop/dragstart.js';

import {
  IconTabs,
} from '../../../pages/components/inventory-ui/IconTabs.jsx';
import {
  TransformControlsUi,
} from '../transform-controls-ui/TransformControlsUi.jsx';

import {
  EngineProvider,
} from '../../../packages/engine/clients/engine-client.js';
import {
  makeMp3Stream,
} from '../../../packages/engine/clients/mp3-codec.js';
// import {
//   makeOpusStream,
// } from '../../../packages/engine/clients/opus-codec.js';
import {
  DiscordClient,
} from '../../../packages/engine/clients/discord-client.js';
import {
  TwitchVideoClient,
  TwitchChatClient,
} from '../../../packages/engine/clients/twitch-client.js';

import voiceTrainer from '../../../packages/engine/audio/voice-output/voice-trainer.js';

import {
  assetTypes,
  categoryAssetTypes,
  wearableAssetTypes,
} from './asset-types.js';
import {
  downloadFile,
  resolveIpfsUrl,
  basename,
  cacheBust,
  urls2Zip,
} from '../../../packages/engine/util.js';
import {
  resizeImage,
} from '../../../packages/engine/utils/canvas-utils.js';

import {
  VROID_HUB_CLIENT_ID,
} from '../../../packages/engine/constants/auth.js';

import {
  makeId,
} from '../../../packages/engine/util.js';

import {
  embed,
  // split,
} from '../../../packages/engine/embedding.js';

import {
  generatePanel3D,
  batchGeneratePanel3Ds,
} from '../../../packages/engine/generators/panel3d/panel3d-generator.js';
import {
  generateSkybox3D,
  batchGenerateSkybox3Ds,
} from '../../../packages/engine/generators/skybox3d/skybox3d-generator.js';
import {
  generateItem,
  batchGenerateItems,
} from '../../../packages/engine/generators/item360/item360-generator.js';
import {
  generateCharacter,
  batchGenerateCharacters,
} from '../../../packages/engine/generators/character360/character360-generator.js';
// import {
//   generateMob,
//   batchGenerateMobs,
// } from '../../../packages/engine/generators/mob360/mob360-generator.js';
import {
  generateAudio,
  // getAudio,
} from '../../../packages/engine/generators/audio/audiogen.js';
import {
  characterCardGenerator,
} from '../../../packages/engine/generators/llm/character-card-generator.js';

import {
  AutoVoiceEndpoint,
  VoiceEndpointVoicer,
} from '../../../packages/engine/audio/voice-output/voice-endpoint-voicer.js';
import {
  AudioInjectWorkletNode,
} from '../../../packages/engine/audio/inject-worklet-node.js';

import styles from '../../../styles/InventoryUi.module.css';
import { VoicePack } from '../../../packages/engine/audio/voice-output/voice-pack-voicer';

import dotenv from 'dotenv';
dotenv.config();

// if process.env var is set or url contains ic0.app, then isIC is true
const isIC = process.env.NEXT_PUBLIC_IS_IC === 'true' || location.hostname.includes('ic0.app');
import { toast } from 'react-toastify';

//

const localVector = new THREE.Vector3();
const localQuaternion = new THREE.Quaternion();
const localEuler = new THREE.Euler();

// const y180Quaternion = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI);
const abortError = new Error('abort');

//

const previewSize = 512;
const avatarBoneNames = [
  "spine",
  "chest",
  "upperChest",
  "neck",
  "head",
  "leftShoulder",
  "leftUpperArm",
  "leftLowerArm",
  "leftHand",
  "leftThumb2",
  "leftThumb1",
  "leftThumb0",
  "leftIndexFinger1",
  "leftIndexFinger2",
  "leftIndexFinger3",
  "leftMiddleFinger1",
  "leftMiddleFinger2",
  "leftMiddleFinger3",
  "leftRingFinger1",
  "leftRingFinger2",
  "leftRingFinger3",
  "leftLittleFinger1",
  "leftLittleFinger2",
  "leftLittleFinger3",
  "rightShoulder",
  "rightUpperArm",
  "rightLowerArm",
  "rightHand",
  "rightThumb2",
  "rightThumb1",
  "rightThumb0",
  "rightIndexFinger1",
  "rightIndexFinger2",
  "rightIndexFinger3",
  "rightMiddleFinger1",
  "rightMiddleFinger2",
  "rightMiddleFinger3",
  "rightRingFinger1",
  "rightRingFinger2",
  "rightRingFinger3",
  "rightLittleFinger1",
  "rightLittleFinger2",
  "rightLittleFinger3",
  "leftUpperLeg",
  "leftLowerLeg",
  "leftFoot",
  "rightUpperLeg",
  "rightLowerLeg",
  "rightFoot",
  "leftToe",
  "rightToe"
];
const whitelistedContractAddresses = [
  '0x543D43F390b7d681513045e8a85707438c463d80', // Webaverse Genesis Pass
  '0x1dfe7Ca09e99d10835Bf73044a23B73Fc20623DF', // More Loot
  '0x57f1887a8bf19b14fc0df6fd9b2acc9af147ea85', // ENS
  '0x2a187453064356c898cae034eaed119e1663acb8', // Decentraland username
  '0x62eb144fe92ddc1b10bcade03a0c09f6fbffbffb', // AdWorld
  '0x3999877754904d8542ad1845d368fa01a5e6e9a5', // Vipe Heroes
  '0x1d20a51f088492a0f1c57f047a9e30c9ab5c07ea', // Wassies
];

//

const getProxyUrl = u => `https://cors-proxy.upstreet.ai/${u}`;
const createJsonDataUrl = (json, type) => {
  const s = JSON.stringify(json);
  const s2 = `data:${type},${encodeURIComponent(s)}`;
  return s2;
};

//

const loadNftItems = async ({
  signal,
  // supabaseClient,
  address,
}) => {
  const alchemy = createClient();

  const ownedNfts = await (async () => {
    const ownedNfts = [];
    let pageKey;
    let first = true;
    for (; ;) {
      const o = await alchemy.nft.getNftsForOwner(
        address,
        {
          pageKey,
        },
      );
      let {
        ownedNfts: _ownedNfts,
        pageKey: _pageKey,
      } = o;
      if (_ownedNfts.length > 0 || first) {
        _ownedNfts = _ownedNfts.filter(nft => {
          return whitelistedContractAddresses.includes(nft?.contract?.address) &&
            !nft?.spamInfo?.isSpam;
        });

        ownedNfts.push.apply(ownedNfts, _ownedNfts);
        pageKey = _pageKey;
        first = false;

        if (!pageKey) {
          break;
        }
      } else {
        break;
      }
    }
    return ownedNfts;
  })();
  if (signal.aborted) return;

  const nfts = [];
  const maxNfts = 20;
  for (let i = 0; i < ownedNfts.length && i < maxNfts; i++) {
    const ownedNft = ownedNfts[i];
    const {
      contract,
      tokenId,
      title,
      description,
      media,
      tokenUri,
    } = ownedNft;
    const {
      address: contractAddress,
      name: contractName,
      openSea: contractOpenSea,
    } = contract;
    const name = title;
    const collection = contractName || contractOpenSea?.collectionName;

    if (tokenUri) {
      const {
        gateway,
      } = tokenUri;

      const json = await (async () => {
        try {
          const gateway2 = getProxyUrl(gateway);
          const res = await fetch(gateway2);
          if (signal.aborted) return;

          if (res.ok) {
            const j = await res.json();
            if (signal.aborted) return;
            // console.log('got json', j);

            return j;
          } else {
            console.warn('got bad response status code: ' + res.status);
            return null;
          }
        } catch (err) {
          console.warn(err);
          return null;
        }
      })();
      if (signal.aborted) return;

      if (json !== null) {
        let { image } = json;
        if (image) {
          image = resolveIpfsUrl(image);

          nfts.push({
            id: gateway,
            name,
            collection,
            preview_url: image,
          });
        } else {
          console.warn('no image for nft; skipping', json);
        }
      }
    } else {
      console.warn('no tokenUri for nft; skipping', ownedNft);
    }
  }
  // console.log('got all nfts', nfts);
  return nfts;
};
const loadItemsType = (type, tableName = 'assets') => async ({
  signal,
  supabaseClient,
  sessionUserId,
}) => {
  if (!Array.isArray(type)) {
    type = [type];
  }

  const result = await supabaseClient.supabase
    .from(tableName)
    .select('*')
    .eq('user_id', sessionUserId)
    // .eq('type', type)
    .in('type', type);
  const { data } = result;
  return data;
};
const loadWorlds = async ({
  signal,
  supabaseClient,
  sessionUserId,
}) => {
  const result = await supabaseClient.supabase
    .from('worlds')
    .select('*')
    .eq('user_id', sessionUserId);
  const { data } = result;
  return data;
};

//

const NpcButton = ({
  item,
  contentPath,
  setContentPath,
  // onSell,
  // debug,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createNpc',
          item: {
            name: item.name,
            avatar: item,
          },
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>+ NPC</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const WearableButton = ({
  item,
  contentPath,
  setContentPath,
  // onSell,
  // debug,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createWearable',
          item: {
            name: item.name,
            glb: item,
          },
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>+ Wearable</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const MobButton = ({
  item,
  contentPath,
  setContentPath,
  // onSell,
  debug,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createMob',
          item: {
            name: item.name,
            glb: item,
          },
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>+ Wearable</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const SellButton = ({
  item,
  onSell,
  debug,
}) => {
  return debug && (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      onSell(item);
    }}>
      <span>Sell</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  )
};
const WearAvatarButton = ({
  item,
  onActivate,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      onActivate(item);
    }}>
      <span>Wear</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const WearItemButton = ({
  item,
  onActivate,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      onActivate(item);
    }}>
      <span>Wear</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditNpcButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const npc = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          j.id = item.id;

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'vrm')
              .eq('start_url', j.avatarUrl)
              .maybeSingle();
            const { data } = result;
            if (data) {
              j.avatar = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createNpc',
          item: {
            ...npc,
            wearables: [],
          },
          editing: true,
        },
      ];
      // console.log('got new content path', {
      //   item,
      //   newContentPath,
      // });
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditVoiceButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const voice = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          return {
            id: item.id,
            name: item.name,
            // voice: {
            //   voiceEndpoint: j.voiceEndpoint,
            //   voiceUrls: j.voiceUrls,
            // },
            voice: j,
          };
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createVoice',
          item: voice,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditDirectorButton = ({
  // supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const director = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          return {
            id: item.id,
            name: item.name,
            director: j,
          };
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createDirector',
          item: director,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditLoreButton = ({
  // supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const lore = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const text = await res.text();

          return {
            id: item.id,
            name: item.name,
            lore: text,
          };
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createLore',
          item: lore,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditCameraButton = ({
  // supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const camera = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          return {
            id: item.id,
            name: item.name,
            camera: j,
          };
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createCamera',
          item: camera,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditWearableButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const wearable = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const { data } = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createWearable',
          item: wearable,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditSittableButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const sittable = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const { data } = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createSittable',
          // item: wearable,
          item: sittable,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditMountButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const mount = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const { data } = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createMount',
          item: mount,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditVehicleButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const vehicle = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const { data } = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createVehicle',
          item: vehicle,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditMobButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const mob = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const { data } = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          }

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createMob',
          item: mob,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditSkillButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const skill = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();
          j.id = item.id;

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createSkill',
          item: skill,
          editing: true,
        },
      ];
      setContentPath(newContentPath);
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const EditScriptButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  const [loading, setLoading] = useState(true);

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const script = await (async () => {
        try {
          setLoading(true);

          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();

          /* // convert j.avatarUrl -> avatar using vrm asset lookup
          {
            const result = await supabaseClient.supabase
              .from('assets')
              .select('*')
              .eq('type', 'glb')
              .eq('start_url', j.glbUrl)
              .maybeSingle();
            const {data} = result;
            // console.log('got data', {
            //   data,
            // });
            if (data) {
              j.id = item.id;
              j.glb = data;
            }
          } */

          return j;
        } finally {
          setLoading(true);
        }
      })();

      const newContentPath = [
        ...contentPath,
        {
          type: 'createScript',
          item: script,
          editing: true,
        },
      ];
      setContentPath(newContentPath);

      /* const newContentPath = [
        ...contentPath,
        {
          type: 'createScript',
          item: {
            ...item,
          },
          editing: true,
        },
      ];
      setContentPath(newContentPath); */
    }}>
      <span>Edit</span>
      <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
    </nav>
  );
};
const PlayAudioButton = ({
  supabaseClient,

  item,

  contentPath,
  setContentPath,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      (async () => {
        const { start_url } = item;
        const audio = new Audio(start_url);
        // wait for canplaythrough
        await new Promise((accept, reject) => {
          audio.addEventListener('canplaythrough', accept);
          audio.addEventListener('error', reject);
        });
        audio.play();
      })();
    }}>
      <span>Play</span>
      {/* <img className={styles.chevron} src='/images/chevron.png' draggable={false} /> */}
    </nav>
  );
};
const TestVoiceButton = ({
  // supabaseClient,

  engine,
  item,

  // contentPath,
  // setContentPath,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const [
        model,
        name,
        voiceId,
      ] = await (async () => {
        if (item.start_url) {
          const res = await fetch(cacheBust(item.start_url));
          const j = await res.json();
          const {
            voiceEndpoint,
          } = j;
          const match = voiceEndpoint.match(/^(.+?):(.+?):(.+?)$/);
          const [
            _,
            model,
            name,
            voiceId,
          ] = match;
          return [
            model,
            name,
            voiceId,
          ];
        } else {
          const {
            model,
            name,
            voiceId,
          } = item;
          return [
            model,
            name,
            voiceId,
          ];
        }
      })();

      const { audioManager } = engine;
      const { audioContext } = audioManager;

      const voiceEndpoint = new AutoVoiceEndpoint({
        model,
        voiceId,
      });

      const voicer = new VoiceEndpointVoicer({
        voiceEndpoint,
        audioManager,
      });

      const testMessage = `This is a test of my voice. Do you like it?`;
      const stream = await voicer.getStream(testMessage);

      const audioInjectWorkletNode = new AudioInjectWorkletNode({
        audioContext,
      });

      const abortController = new AbortController();
      const { signal } = abortController;

      await voicer.start(stream, {
        audioInjectWorkletNode,
        signal,
        onStart: () => {
          audioInjectWorkletNode.connect(audioContext.gain);
        },
        onEnd: () => {
          audioInjectWorkletNode.disconnect();
        },
      });
    }}>
      <span>Test</span>
    </nav>
  );
};
const TestGruntButton = ({
  // supabaseClient,

  item,

  engine,

  // contentPath,
  // setContentPath,
}) => {
  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      const voicePackSpec = voicePacks.find(vp => vp.name === item.name);
      const {
        indexPath,
        audioPath,
      } = voicePackSpec;

      const voicePacksUrlBase = voicePacksUrl.replace(/\/+[^\/]+$/, '');
      const audioUrl = voicePacksUrlBase + audioPath;
      const indexUrl = voicePacksUrlBase + indexPath;

      const {
        audioManager,
      } = engine;
      const { audioContext } = audioManager;
      const voicePack = await VoicePack.load({
        audioUrl,
        indexUrl,
        audioManager,
      });

      const audioBufferSourceNode = (() => {
        const category = Math.random() < 0.5 ? 'action' : 'emote';
        if (category === 'action') {
          const types = Object.keys(voicePack.voiceFiles.actionVoices);
          const type = types[Math.floor(Math.random() * types.length)];
          return voicePack.playAction(type);
        } else if (category === 'emote') {
          const types = Object.keys(voicePack.voiceFiles.emoteVoices);
          const type = types[Math.floor(Math.random() * types.length)];
          return voicePack.playEmote(type);
        } else {
          throw new Error('invalid category: ' + category);
        }
      })();
      audioBufferSourceNode.connect(audioContext.destination);
    }}>
      <span>Test</span>
    </nav>
  );
};
const ImportVrmButton = ({
  supabaseClient,
  sessionUserId,
  user,

  item,

  contentPath,
  setContentPath,
}) => {
  const access_token = user.oauth.vroidHub.access_token;

  //

  return (
    <nav className={styles.btn} onClick={async e => {
      e.preventDefault();
      e.stopPropagation();

      // get the license json
      const characterModel = item;

      const { character } = characterModel;
      const { name } = character;

      const licenseJson = await (async () => {
        const fd = new FormData();
        fd.append('character_model_id', characterModel.id);
        const res = await fetch('https://ai-proxy.upstreet.ai/api/vroidHub/api/download_licenses', {
          method: 'POST',
          headers: {
            'X-Api-Version': 11,
            'Authorization': `Bearer ${access_token}`,
          },
          body: fd,
        });
        const j = await res.json();
        return j;
      })();

      // download the vrm
      const vrmBlob = await (async () => {
        const res = await fetch(`https://ai-proxy.upstreet.ai/api/vroidHub/api/download_licenses/${licenseJson.data.id}/download`, {
          headers: {
            'X-Api-Version': 11,
            'Authorization': `Bearer ${access_token}`,
          },
        });
        const b = await res.blob();
        return b;
      })();
      vrmBlob.name = name + '.vrm';

      const metadata = await importFileType('vrm')(vrmBlob, supabaseClient, sessionUserId);
      await addAsset(metadata);

      const newContentPath = [
        ...contentPath.slice(0, -2),
      ];
      setContentPath(newContentPath);
    }}>
      <span>Import</span>
    </nav>
  );
};

//

export const ProfileContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  user,
  // setUser,

  // accountManager,

  debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(user?.name || '');

  //

  return (
    <div className={styles.profile}>
      <label className={styles.label}>
        <div className={styles.text}>Alias</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setName(e.target.value);
        }} placeholder='Enter a name' />

        <div>
          <button
            className={styles.button}
            onClick={async e => {
              const result = await supabaseClient.updateUser({
                name,
              });
            }}
          >
            <div className={styles.background} />
            <span className={styles.text}>Save</span>
          </button>
        </div>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Current Character</div>
        <button
          className={styles.button}
          onClick={async e => {
            const item2 = {
              ...item,
              avatar: {
                name: item.name,
                start_url: item.avatarUrl,
              },
              wearables: [],
            };

            const newContentPath = [
              ...contentPath,
              {
                type: 'editProfileCharacter',
                item: item2,
              },
            ];
            setContentPath(newContentPath);
          }}
        >
          <div className={styles.background} />
          <span className={styles.text}>Edit</span>
        </button>
      </label>

      {debug && <label className={styles.label}>
        <div className={styles.text}>Wearables</div>
        <button
          className={styles.button}
          onClick={e => {
            const newContentPath = [
              ...contentPath,
              {
                type: 'editNpcWearables',
                assetType: wearableAssetTypes,
                items: [],
              },
            ];
            setContentPath(newContentPath);
          }}
        >
          <div className={styles.background} />
          <span className={styles.text}>Edit</span>
        </button>
      </label>}
    </div>
  );
};

//

export const InventoryContent = ({
  // contentPath,
  // setContentPath,

  // user,
  // setUser,

  // accountManager,
}) => {
  const numTotalSlots = 80;
  const numSlotsPerRow = 5;
  const slotItems = new Array(numTotalSlots).fill(null).map((v, i) => {
    return {
      id: i,
    };
  });
  // split by row
  const slotItemRows = [];
  for (let i = 0; i < slotItems.length; i += numSlotsPerRow) {
    const row = slotItems.slice(i, i + numSlotsPerRow);
    slotItemRows.push(row);
  }

  //

  return (
    <>
      {/* <div className={styles.background} /> */}
      <div className={styles.wrap}>
        <div className={styles.grid}>
          {slotItemRows.map((slotItemRow, i) => {
            return (
              <div className={styles.row} key={i}>
                {slotItemRow.map(slotItem => {
                  return (
                    <div className={styles.slot} key={slotItem.id}>
                      <div className={styles.background} />
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
        {/* <label className={styles.label}>
          <div className={styles.text}>Alias</div>
          <input type='text' className={styles.input} value={name} onChange={e => {
            setName(e.target.value);
          }} placeholder='Enter a name' />
        </label>

        <label className={styles.label}>
          <div className={styles.text}>Avatar</div>
          <AvatarPicker
            contentPath={contentPath}
            setContentPath={setContentPath}
          />
        </label>

        <button
          className={classnames(
            styles.button,
            // changed ? styles.changed : null,
          )}
          onClick={async e => {
            // XXX need these extra properties from the NPC
            // bio
            // name
            // voiceEndpoint

            const newUser = {
              ...user,
              name,
              playerSpec: {
                ...user.playerSpec,
                name: avatar.name,
                avatarUrl: avatar.start_url,
              },
            };
            accountManager.setUser(newUser);
          }}
        >
          <div className={styles.background} />
          <span className={styles.text}>Save</span>
        </button> */}
      </div>
    </>
  );
};

//

export const AssetButtons = ({
  supabaseClient,
  sessionUserId,
  user,

  item,

  contentPath,
  setContentPath,

  assetType,

  onActivate,
  onSell,

  engine,

  debug,
}) => {
  const firstAssetType = Array.isArray(assetType) ? assetType[0] : assetType;
  const assetTypeSpec = assetTypes[firstAssetType];

  const contentPathElement2 = contentPath[contentPath.length - 2];
  const { type } = contentPathElement2;
  const isTopLevel = type === 'apps';

  const props = {
    supabaseClient,
    sessionUserId,
    user,

    item,

    contentPath,
    setContentPath,

    onActivate,
    onSell,

    engine,

    debug,
  };
  const buttons = assetTypeSpec.buttons.map((button, i) => {
    switch (button) {
      case 'sell': return isTopLevel && (
        <SellButton {...props} key={i} />
      );
      case 'wearAvatar': return isTopLevel && (
        <WearAvatarButton {...props} key={i} />
      );
      case 'wearItem': return isTopLevel && (
        <WearItemButton {...props} key={i} />
      );
      case 'createNpc': return isTopLevel && (
        <NpcButton {...props} key={i} />
      );
      case 'editNpc': return (
        <EditNpcButton {...props} key={i} />
      );
      case 'editVoice': return (
        <EditVoiceButton {...props} key={i} />
      );
      case 'editDirector': return (
        <EditDirectorButton {...props} key={i} />
      );
      case 'editLore': return (
        <EditLoreButton {...props} key={i} />
      );
      // case 'setCamera': return isTopLevel && (
      //   <SetCameraButton {...props} key={i} />
      // );
      case 'editCamera': return (
        <EditCameraButton {...props} key={i} />
      );
      case 'createWearable': return isTopLevel && (
        <WearableButton {...props} key={i} />
      );
      case 'editWearable': return (
        <EditWearableButton {...props} key={i} />
      );
      case 'editSittable': return (
        <EditSittableButton {...props} key={i} />
      );
      case 'editMount': return (
        <EditMountButton {...props} key={i} />
      );
      case 'editVehicle': return (
        <EditVehicleButton {...props} key={i} />
      );
      case 'createMob': return isTopLevel && (
        <MobButton {...props} key={i} />
      );
      case 'editMob': return (
        <EditMobButton {...props} key={i} />
      );
      case 'editSkill': return (
        <EditSkillButton {...props} key={i} />
      );
      case 'editScript': return (
        <EditScriptButton {...props} key={i} />
      );
      case 'playAudio': return (
        <PlayAudioButton {...props} key={i} />
      );
      case 'testVoice': return (
        <TestVoiceButton {...props} key={i} />
      );
      case 'importVrm': return (
        <ImportVrmButton {...props} key={i} />
      );
    }
  }).filter(b => !!b);
  return buttons;
};

//

export const AssetListContent = ({
  supabaseClient,
  sessionUserId,
  user,

  contentPath,
  setContentPath,

  onActivate,
  onSell,

  epoch,
  setEpoch,

  onClick,

  engine,

  debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    assetType,
  } = contentPathElement;

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  const firstAssetType = Array.isArray(assetType) ? assetType[0] : assetType;
  const assetTypeSpec = assetTypes[firstAssetType];
  const pickableAssetTypes = Array.isArray(assetType) ? assetType : [assetType];
  const loadableAssetTypes = pickableAssetTypes.filter(assetType => {
    const assetTypeSpec = assetTypes[assetType];
    const createType = assetTypeSpec.create?.type;
    return createType !== 'drop';
  });
  const hasItems = loadableAssetTypes.length > 0;

  //

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  useEffect(() => {
    if (hasItems) {
      const abortController = new AbortController();
      const { signal } = abortController;

      loadQueue.waitForTurn(async () => {
        setLoading(true);

        try {
          const newItems = await loadItemsType(loadableAssetTypes)({
            signal,
            supabaseClient,
            sessionUserId,
          });
          if (signal.aborted) return;

          setItems(newItems);
        } finally {
          setLoading(false);
        }
      });

      return () => {
        abortController.abort();
      };
    }
  }, [
    epoch,
  ]);

  //

  return (
    <>
      {pickableAssetTypes.map((assetType, i) => {
        const assetTypeSpec = assetTypes[assetType];
        const createType = assetTypeSpec.create?.type;

        switch (createType) {
          case 'upload': {
            return (
              <div className={styles.item} onClick={e => {
                e.preventDefault();
                e.stopPropagation();

                const input = document.createElement('input');
                input.type = 'file';
                input.accept = assetTypeSpec.create.mimeTypes.map(mimeType => '.' + mimeType).join(',');
                input.multiple = !!assetTypeSpec.create.multiple;
                input.addEventListener('change', async e => {
                  const f = e.target.files[0];

                  const metadata = await importFileType(assetType)(f, supabaseClient, sessionUserId);
                  await addAsset(metadata);
                  refreshItems();
                });
                input.click();
              }} key={i}>
                <img src='/assets/icons/plus.svg' className={classnames(
                  styles.previewImg,
                  styles.placeholderImg,
                )} draggable={false} />
                <div className={styles.name}>Upload {assetType}</div>
              </div>
            );
          }
          case 'drop': {
            const name = 'Drop ' + assetType;
            const item = {
              name,
              start_url: createJsonDataUrl({
                name: assetType,
                description: assetTypeSpec.description,
              }, 'application/' + assetType),
              preview_url: assetTypeSpec.icon,
            };

            return (
              <div className={styles.item} onDragStart={e => {
                dragStartType(assetType)(e, item, assetTypeSpec.drop?.metadata);
              }} draggable key={i}>
                <img src={item.preview_url} className={classnames(
                  styles.previewImg,
                  styles.invert,
                )} draggable={false} />
                <div className={styles.name}>{item.name}</div>
              </div>
            );
          }
          case 'custom': {
            const createNextType = assetTypeSpec.create?.nextType;

            return (
              <div className={styles.item} onClick={e => {
                e.preventDefault();
                e.stopPropagation();

                const newContentPath = [
                  ...contentPath,
                  {
                    type: createNextType,
                  },
                ];
                setContentPath(newContentPath);
              }} key={i}>
                <img src='/assets/icons/plus.svg' className={classnames(
                  styles.previewImg,
                  styles.placeholderImg,
                )} draggable={false} />
                <div className={styles.name}>Create {assetType}</div>
              </div>
            );
          }
        }
      })}

      {hasItems && loading && <div className={styles.placeholder}>
        Loading...
      </div>}

      {hasItems && !loading && items.map(item => {
        return (
          <div className={styles.item} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            onClick && onClick(item);
          }} onDoubleClick={e => {
            e.preventDefault();
            e.stopPropagation();

            onActivate && onActivate(item);
          }} onDragStart={e => {
            dragStartType(assetType)(e, item, assetTypeSpec.drop?.metadata);
          }} draggable key={item.id}>
            <img src={item.preview_url} className={styles.previewImg} draggable={false} />
            <div className={styles.name}>{item.name}</div>

            <AssetButtons
              supabaseClient={supabaseClient}
              sessionUserId={sessionUserId}
              user={user}

              item={item}

              contentPath={contentPath}
              setContentPath={setContentPath}

              assetType={assetType}

              onActivate={onActivate}
              onSell={onSell}

              engine={engine}

              debug={debug}
            />

            <nav className={styles.iconBtn} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              // remove item
              const result = await supabaseClient.supabase
                .from('assets')
                .delete()
                .eq('id', item.id);

              refreshItems();
            }}>
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav>
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>

      </div>}
    </>
  );
};

//

export const PrefabAssetContent = ({
  // supabaseClient,
  // sessionUserId,

  // epoch,
  // setEpoch,

  onClick,
}) => {
  const [items, setItems] = useState(() => [
    {
      name: 'Silsword',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/silsword/index.js',
    },
    {
      name: 'Pistol',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/pistol/index.js',
      metadata: {
        positionOffset: [0, 1, 0],
      },
    },
    {
      name: 'Silk',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/silk/index.js',
      metadata: {
        positionOffset: [0, 0.5, 0],
      },
    },
    {
      name: 'Chest',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/chest/index.js',
    },
    // {
    //   name: 'Skybox',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/blockadelabsskyboxes/world.blockadelabsskybox',
    // },
    // {
    //   name: 'Zine',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/zine/storyboard.zine',
    // },
    // {
    //   name: 'Metazine',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/metazine/metazine.zine',
    // },
    {
      name: 'Wrench',
      preview_url: '/images/weapon.svg',
      start_url: '/item360/wrench.item360',
    },
    {
      name: 'Infinifruit',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/infinifruit/index.js',
    },
    // {
    //   name: 'Mirror',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/mirror/index.js',
    // },
    // {
    //   name: 'Gamer chair',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/gamer-chair/gamer-chair.sittable',
    // },
    {
      name: 'Hovercraft',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/hovercraft/hovercraft.vehicle',
    },
    {
      name: 'Lisk',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/lisk/index.js',
    },
    {
      name: 'Flower',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/flower/index.js',
    },
    {
      name: 'Ruins',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/ruins/Ruin_Rock_1_dream.glb',
    },
    {
      name: 'Dreadnought',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/dreadnought/dreadnought.glb',
    },
    // {
    //   name: 'Witch Hat',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/witch-hat/witch-hat.wearable',
    // },
    // {
    //   name: 'Dragon Mount',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/dragon-mount/dragon-mount.mount',
    // },
    // {
    //   name: 'Fox',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/fox/fox.pet',
    // },
    {
      name: 'Blue Sphere',
      preview_url: '/images/weapon.svg',
      start_url: '/core-modules/blue-sphere/blue-sphere.js',
    },
    // {
    //   name: 'Turret',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/turret/index.js',
    // },
    // {
    //   name: 'Pickaxe',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/pickaxe/pickaxe.wearable',
    // },
    // {
    //   name: 'Rifle',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/rifle/rifle.wearable',
    // },
    // {
    //   name: 'Barrier',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/barrier/barrier.js',
    // },
    // {
    //   name: 'Heli',
    //   preview_url: '/images/weapon.svg',
    //   start_url: '/core-modules/heli/index.js',
    // },
  ]);

  //

  return (
    <>
      {items.map((item, i) => {
        return (
          <div className={styles.item} onDragStart={e => {
            dragStartType('script')(e, item, item.metadata);
          }} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            onClick && onClick(item);
          }} onDoubleClick={e => {
            e.preventDefault();
            e.stopPropagation();

            console.log('wear item', item);
          }} draggable key={i}>
            <img src={item.preview_url} className={styles.previewImg} draggable={false} />
            <div className={styles.name}>{item.name}</div>
          </div>
        );
      })}
    </>
  );
};

//

export const CreatePanel3DContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,
  debug,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //
  const toastRefs = new Map();

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.panel3D?.prompt ?? '');
  const [imageUrl, setImageUrl] = useState(() => item?.panel3D?.imageUrl ?? '');

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [zineUrl, setZineUrl] = useState(() => item?.panel3D?.zineUrl ?? '');

  //

  const generatable = !!name && (!!prompt || !!imageUrl);

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (prompt !== item?.panel3D?.prompt) {
      setPrompt(item?.panel3D?.prompt ?? '');
    }
    if (imageUrl !== item?.panel3D?.imageUrl) {
      setImageUrl(item?.panel3D?.imageUrl ?? '');
    }

    if (zineUrl !== item?.panel3D?.imageUrl) {
      setZineUrl(item?.panel3D?.imageUrl ?? '');
    }
  }, [
    item,
    name,
    prompt,
    imageUrl,
    zineUrl,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        panel3D: {
          ...item?.panel3D,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _generate = async () => {
    const generationId = self.crypto.randomUUID();
    // Show the toast and keep its ID in the Map
    const currentToastId = toast.loading(`Starting generation for panel "${name}"...`);
    toastRefs.set(generationId, currentToastId);

    const setGenerationStatus = (status) => {
      // Retrieve the toast ID from the Map using the generation ID
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.update(toastId, { render: `Item "${name}": ${status}`, type: toast.TYPE.INFO, isLoading: true });
      }
    };

    const finishGeneration = (success) => {
      // Retrieve the toast ID from the Map using the generation ID and dismiss it
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.dismiss(toastId);
        // Remove the reference from the Map
        toastRefs.delete(generationId);
        if (success)
          toast.success(`Panel "${name}" generated successfully!`);
        else {
          toast.error(`Panel "${name}" generation failed!`);
        }
      }
    };
    try {
      const file = await (async () => {
        if (imageUrl) {
          const res = await fetch(imageUrl);
          const blob = await res.blob();
          return blob;
        } else {
          return null;
        }
      })();

      console.log('generate panel 3d 1', {
        name,
        prompt,
      });
      const panel3DResult = await generatePanel3D({
        prompt,
        file,
        debug,
        setGenerationStatus
      });
      console.log('generate panel 3d 2', {
        panel3DResult,
      });
      const {
        imageBlob,
        zineBlob,
      } = panel3DResult;
      console.log('generate panel 3d 3', {
        imageBlob,
        zineBlob,
      });

      //

      const [
        preview_url,
        zineUrl,
      ] = await Promise.all([
        (async () => {
          const resizedImageBlob = await resizeImage(imageBlob, previewSize, previewSize);

          const supabaseFsWorker = new SupabaseFsWorker({
            supabase: supabaseClient.supabase,
            bucketName: 'public',
          });

          const fileName = 'image.png';
          const keyPath = ['panel3D', id].concat(fileName);
          // console.log('keyPath 1', {
          //   keyPath,
          // });
          const imageUrl = await supabaseFsWorker.writeFile(keyPath, resizedImageBlob);
          // console.log('keyPath 2', {
          //   zineUrl,
          // });
          return imageUrl;
        })(),
        (async () => {
          const supabaseFsWorker = new SupabaseFsWorker({
            supabase: supabaseClient.supabase,
            bucketName: 'public',
          });

          const fileName = 'zine.zine';
          const keyPath = ['panel3D', id].concat(fileName);
          // console.log('keyPath 1', {
          //   keyPath,
          // });
          const zineUrl = await supabaseFsWorker.writeFile(keyPath, zineBlob);
          // console.log('keyPath 2', {
          //   zineUrl,
          // });
          return zineUrl;
        })(),
      ]);

      //

      const panel3DAsset = {
        id,
        name,
        description: prompt,
        preview_url,
        type: 'panel3D',
        start_url: zineUrl,
        user_id: sessionUserId,
      };
      const result = await supabaseClient.supabase
        .from('assets')
        .upsert(panel3DAsset);

      finishGeneration(true);
    } catch (err) {
      console.warn(err);
      finishGeneration(false);
    }

    // const newContentPath = contentPath.slice(0, contentPath.length - 1);
    // setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <label className={styles.label}>
        {!imageUrl ?
          <>
            <div className={styles.text}>Image file</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              const input = document.createElement('input');
              input.type = 'file';
              input.accept = '.png,.jpg,.jpeg';
              input.addEventListener('change', async e => {
                const file = e.target.files[0];

                const supabaseFsWorker = new SupabaseFsWorker({
                  supabase: supabaseClient.supabase,
                  bucketName: 'public',
                });

                const fileName = file.name;
                const keyPath = ['panel3D', id].concat(fileName);
                console.log('keyPath 1', {
                  keyPath,
                });
                const imageUrl2 = await supabaseFsWorker.writeFile(keyPath, file);
                console.log('keyPath 2', {
                  imageUrl2,
                });

                setItemKeyValueChange2('imageUrl', imageUrl2);
                setImageUrl(imageUrl2);
              });
              input.click();
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Choose image</div>
            </button>
          </>
          :
          <>
            <div className={styles.text}>{basename(imageUrl)}</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              setItemKeyValueChange2('imageUrl', null);
              setImageUrl(null);
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Clear</div>
            </button>
          </>
        }
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};
export const CreateSkybox3DContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const toastRefs = new Map();
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.skybox360?.prompt ?? '');

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [imageUrl, setImageUrl] = useState(() => item?.skybox360?.imageUrl ?? '');
  const [depthMapUrl, setDepthMapUrl] = useState(() => item?.skybox360?.depthMapUrl ?? '');

  const [skyboxUrl, setSkyboxUrl] = useState(() => item?.panel3D?.zineUrl ?? '');

  //

  const generatable = !!name && (!!prompt || !!imageUrl);

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (prompt !== item?.skybox360?.prompt) {
      setPrompt(item?.skybox360?.prompt ?? '');
    }
    if (imageUrl !== item?.skybox360?.imageUrl) {
      setImageUrl(item?.skybox360?.imageUrl ?? '');
    }
    if (depthMapUrl !== item?.skybox360?.depthMapUrl) {
      setDepthMapUrl(item?.skybox360?.depthMapUrl ?? '');
    }

    if (skyboxUrl !== item?.panel3D?.skyboxUrl) {
      setSkyboxUrl(item?.panel3D?.skyboxUrl ?? '');
    }
  }, [
    item,
    name,
    prompt,
    imageUrl,
    depthMapUrl,
    skyboxUrl,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        skybox360: {
          ...item?.skybox360,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _generate = async () => {
    const generationId = self.crypto.randomUUID();
    // Show the toast and keep its ID in the Map
    const currentToastId = toast.loading(`Starting generation for skybox "${name}"...`);
    toastRefs.set(generationId, currentToastId);

    const setGenerationStatus = (status) => {
      // Retrieve the toast ID from the Map using the generation ID
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.update(toastId, { render: `Item "${name}": ${status}`, type: toast.TYPE.INFO, isLoading: true });
      }
    };

    const finishGeneration = (success) => {
      // Retrieve the toast ID from the Map using the generation ID and dismiss it
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.dismiss(toastId);
        // Remove the reference from the Map
        toastRefs.delete(generationId);
        if (success)
          toast.success(`Skybox "${name}" generated successfully!`);
        else {
          toast.error(`Skybox "${name}" generation failed!`);
        }
      }
    };
    try {
      const file = await (async () => {
        if (imageUrl) {
          const res = await fetch(imageUrl);
          const blob = await res.blob();
          return blob;
        } else {
          return null;
        }
      })();

      /* console.log('generate panel 3d 1', {
        name,
        prompt,
      }); */
      const skybox3DResult = await generateSkybox3D({
        prompt,
        file,
        setGenerationStatus
      });
      console.log('generate panel 3d 2', {
        skybox3DResult,
      });
      const {
        depthMapUrl,
        fileUrl,
      } = skybox3DResult;

      // create a blob of the file at fileUrl
      const res = await fetch(fileUrl);
      const imageBlob = await res.blob();

      const depthMapRes = await fetch(depthMapUrl);
      const depthMapBlob = await depthMapRes.blob();



      /* console.log('generate panel 3d 3', {
        imageBlob,
        zineBlob,
      }); */

      //

      console.log('generate panel 3d 4', {
        imageBlob
      });

      const resizedImageBlob = await resizeImage(imageBlob, previewSize, previewSize);

      const supabaseFsWorker = new SupabaseFsWorker({
        supabase: supabaseClient.supabase,
        bucketName: 'public',
      });

      const fileName = 'image.png';
      const keyPath = ['skybox3D', id].concat(fileName);
      const keyPathDepthMap = ['skybox3D', id].concat('depthMap.png');
      const previewFileName = 'preview.png';
      const previewKeyPath = ['skybox3D', id].concat(previewFileName);

      const blockadeLabsFileName = 'skybox3D.blockadelabsskybox';
      const blockadeLabsKeyPath = ['skybox3D', id].concat(blockadeLabsFileName);

      const fileUrlUploaded = await supabaseFsWorker.writeFile(keyPath, imageBlob);
      const depthMapUrlUploaded = await supabaseFsWorker.writeFile(keyPathDepthMap, depthMapBlob);
      const preview_url = await supabaseFsWorker.writeFile(previewKeyPath, resizedImageBlob);
      const json = {
        fileUrl: fileUrlUploaded,
        depthMapUrl: depthMapUrlUploaded,
      }
      // convert json to blob
      const jsonBlob = new Blob([JSON.stringify(json)], { type: 'application/json' });

      const blockadeLabsUrl = await supabaseFsWorker.writeFile(blockadeLabsKeyPath, jsonBlob);

      //

      const skybox3DAsset = {
        id,
        name,
        description: prompt,
        preview_url,
        type: 'skybox3D',
        start_url: blockadeLabsUrl,
        user_id: sessionUserId,
      };

      const result = await supabaseClient.supabase
        .from('assets')
        .upsert(skybox3DAsset);

      finishGeneration(true);
    } catch (err) {
      finishGeneration(false);
      console.error(err);
    }

    // const newContentPath = contentPath.slice(0, contentPath.length - 1);
    // setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <label className={styles.label}>
        {!imageUrl ?
          <>
            <div className={styles.text}>Image file</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              const input = document.createElement('input');
              input.type = 'file';
              input.accept = '.png,.jpg,.jpeg';
              input.addEventListener('change', async e => {
                const file = e.target.files[0];

                const supabaseFsWorker = new SupabaseFsWorker({
                  supabase: supabaseClient.supabase,
                  bucketName: 'public',
                });

                const fileName = file.name;
                const keyPath = ['panel3D', id].concat(fileName);
                console.log('keyPath 1', {
                  keyPath,
                });
                const imageUrl2 = await supabaseFsWorker.writeFile(keyPath, file);
                console.log('keyPath 2', {
                  imageUrl2,
                });

                setItemKeyValueChange2('imageUrl', imageUrl2);
                setImageUrl(imageUrl2);
              });
              input.click();
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Choose image</div>
            </button>
          </>
          :
          <>
            <div className={styles.text}>{basename(imageUrl)}</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              setItemKeyValueChange2('imageUrl', null);
              setImageUrl(null);
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Clear</div>
            </button>
          </>
        }
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};
export const CreateItem360Content = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const toastRefs = new Map();

  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.item360?.prompt ?? '');
  const [imageUrl, setImageUrl] = useState(() => item?.item360?.imageUrl ?? '');

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [image360Url, setImage360Url] = useState(() => item?.item360?.image360Url ?? '');

  //

  const generatable = !!name && (!!prompt || !!imageUrl);

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.item360?.name ?? item?.name ?? name ?? '');
    }
    if (prompt !== item?.item360?.prompt) {
      setPrompt(item?.item360?.prompt ?? '');
    }
    if (imageUrl !== item?.item360?.imageUrl) {
      setImageUrl(item?.item360?.imageUrl ?? '');
    }

    if (image360Url !== item?.item360?.image360Url) {
      setImage360Url(item?.item360?.image360Url ?? '');
    }
  }, [
    item,
    name,
    prompt,
    imageUrl,
    image360Url,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        item360: {
          ...item?.item360,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _generate = async () => {
    const generationId = self.crypto.randomUUID();
    // Show the toast and keep its ID in the Map
    const currentToastId = toast.loading(`Starting generation for item "${name}"...`);
    toastRefs.set(generationId, currentToastId);

    const setGenerationStatus = (status) => {
      // Retrieve the toast ID from the Map using the generation ID
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.update(toastId, { render: `Item "${name}": ${status}`, type: toast.TYPE.INFO, isLoading: true });
      }
    };

    const finishGeneration = () => {
      // Retrieve the toast ID from the Map using the generation ID and dismiss it
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.dismiss(toastId);
        // Remove the reference from the Map
        toastRefs.delete(generationId);
        if (success) {
          toast.success(`Item "${name}" generated!`);
        }
        else {
          toast.error(`Item "${name}" generation failed!`);
        }
      }
    };
    try {
      const file = await (async () => {
        if (imageUrl) {
          const res = await fetch(imageUrl);
          const blob = await res.blob();
          return blob;
        } else {
          return null;
        }
      })();

      console.log('generate item 1', {
        prompt,
        file,
      });
      const item360Result = await generateItem({
        prompt,
        file,
        setGenerationStatus
      });
      console.log('generate item 2', item360Result);

      const {
        itemImageUrl: imageUrl2,
        item360ImageUrl: image360Url2,
      } = item360Result;

      // initialize the supabase fs worker
      const supabaseFsWorker = new SupabaseFsWorker({
        supabase: supabaseClient.supabase,
        bucketName: 'public',
      });

      const id = item?.id ?? crypto.randomUUID();
      const promises = [
        (async () => {
          const res = await fetch(imageUrl2);
          const blob = await res.blob();

          const fileName = basename(imageUrl2);
          const keyPath = ['item360', id].concat(fileName);
          console.log('keyPath 1', {
            keyPath,
          });
          const start_url = await supabaseFsWorker.writeFile(keyPath, blob);
          console.log('keyPath 2', {
            start_url,
          });
          return start_url;
        })(),
        (async () => {
          const item360ImageUrl = await (async () => {
            const res = await fetch(image360Url2);
            const blob = await res.blob();

            const fileName = basename(image360Url2);
            const keyPath = ['item360', id].concat(fileName);
            // console.log('keyPath 1', {
            //   keyPath,
            // });
            const start_url = await supabaseFsWorker.writeFile(keyPath, blob);
            // console.log('keyPath 2', {
            //   start_url,
            // });
            return start_url;
          })();

          const item360Json = {
            item360ImageUrl,
            name: name,
            description: prompt,
          };
          const item360JsonString = JSON.stringify(item360Json);
          const fileName = 'item360.item360';
          const keyPath = ['item360', id].concat(fileName);
          const image360JsonUrl = await supabaseFsWorker.writeFile(keyPath, item360JsonString);
          return image360JsonUrl;
        })(),
      ];
      const [
        imageUrl3,
        image360JsonUrl,
      ] = await Promise.all(promises);

      const item360Asset = {
        id,
        name,
        description: prompt,
        type: 'item360',
        preview_url: imageUrl3,
        start_url: image360JsonUrl,
        user_id: sessionUserId,
      };
      const result = await supabaseClient.supabase
        .from('assets')
        .upsert(item360Asset);

      // const newContentPath = contentPath.slice(0, contentPath.length - 1);
      // setContentPath(newContentPath);
      finishGeneration(true);
    } catch (err) {
      finishGeneration(false);
      console.error(err);
    }
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <label className={styles.label}>
        {!imageUrl ?
          <>
            <div className={styles.text}>Image file</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              const input = document.createElement('input');
              input.type = 'file';
              input.accept = '.png,.jpg,.jpeg';
              input.addEventListener('change', async e => {
                const file = e.target.files[0];

                const supabaseFsWorker = new SupabaseFsWorker({
                  supabase: supabaseClient.supabase,
                  bucketName: 'public',
                });

                const fileName = file.name;
                const keyPath = ['item360', id].concat(fileName);
                console.log('got item360 1', {
                  imageUrl,
                  fileName,
                  keyPath,
                });
                const imageUrl2 = await supabaseFsWorker.writeFile(keyPath, file);
                console.log('got item360 2', {
                  imageUrl2,
                });

                setItemKeyValueChange2('imageUrl', imageUrl2);
                setImageUrl(imageUrl2);
              });
              input.click();
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Choose image</div>
            </button>
          </>
          :
          <>
            <div className={styles.text}>{basename(imageUrl)}</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              setItemKeyValueChange2('imageUrl', null);
              setImageUrl(null);
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Clear</div>
            </button>
          </>
        }
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};
const genders = ['male', 'female'];
export const CreateCharacter360Content = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  debug,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const toastRefs = new Map();
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.character360?.prompt ?? '');
  const [gender, setGender] = useState(() => item?.character360?.gender ?? genders[0]);
  const [imageUrl, setImageUrl] = useState(() => item?.character360?.imageUrl ?? '');

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [image360Url, setImage360Url] = useState(() => item?.character360?.image360Url ?? '');

  //

  const generatable = !!name && (!!prompt || !!imageUrl);

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (prompt !== item?.character360?.prompt) {
      setPrompt(item?.character360?.prompt ?? '');
    }
    if (gender !== item?.character360?.gender) {
      setGender(item?.character360?.gender ?? genders[0]);
    }
    if (imageUrl !== item?.character360?.imageUrl) {
      setImageUrl(item?.character360?.imageUrl ?? '');
    }

    if (image360Url !== item?.character360?.image360Url) {
      setImage360Url(item?.character360?.image360Url ?? '');
    }
  }, [
    item,
    name,
    prompt,
    gender,
    imageUrl,
    image360Url,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        character360: {
          ...item?.character360,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  // initialize the supabase fs worker
  const supabaseFsWorker = new SupabaseFsWorker({
    supabase: supabaseClient.supabase,
    bucketName: 'public',
  });

  //

  const _generate = async () => {
    const generationId = self.crypto.randomUUID();
    // Show the toast and keep its ID in the Map
    const currentToastId = toast.loading(`Starting generation for character "${name}"...`);
    toastRefs.set(generationId, currentToastId);

    const setGenerationStatus = (status) => {
      // Retrieve the toast ID from the Map using the generation ID
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.update(toastId, { render: `Character "${name}": ${status}`, type: toast.TYPE.INFO, isLoading: true });
      }
    };

    const finishGeneration = (success) => {
      // Retrieve the toast ID from the Map using the generation ID and dismiss it
      const toastId = toastRefs.get(generationId);
      if (toastId) {
        toast.dismiss(toastId);
        // Remove the reference from the Map
        toastRefs.delete(generationId);
        if (success) {
          toast.success(`Character "${name}" generated!`);
        }
        else {
          toast.error(`Character "${name}" generation failed!`);
        }
      }
    };

    try {
      const file = await (async () => {
        if (imageUrl) {
          const res = await fetch(imageUrl);
          const blob = await res.blob();
          return blob;
        } else {
          return null;
        }
      })();

      const character360Result = await generateCharacter({
        prompt,
        gender,
        file,
        debug,
        setGenerationStatus
      });
      console.log('got character360 result', character360Result);
      const {
        characterImageBlob,
        character360ImageBlob,
        characterEmotionBlob,
      } = character360Result;

      const id = item?.id ?? crypto.randomUUID();
      setGenerationStatus('Uploading files...');
      const characterResult = await (async () => {
        const [
          characterImageUrl,
          character360ImageUrl,
          characterEmotionUrl,
        ] = await Promise.all([
          (async () => {
            const resizedImageBlob = await resizeImage(characterImageBlob, previewSize, previewSize);

            const fileName = 'image.png';
            const keyPath = ['character360', id].concat(fileName);
            const start_url = await supabaseFsWorker.writeFile(keyPath, resizedImageBlob);
            return start_url;
          })(),
          (async () => {
            const fileName = 'image360.png';
            const keyPath = ['character360', id].concat(fileName);
            const start_url = await supabaseFsWorker.writeFile(keyPath, character360ImageBlob);
            return start_url;
          })(),
          (async () => {
            const fileName = 'emotions.png';
            const keyPath = ['character360', id].concat(fileName);
            const start_url = await supabaseFsWorker.writeFile(keyPath, characterEmotionBlob);
            return start_url;
          })(),
        ]);

        const character360Json = {
          characterImageUrl,
          character360ImageUrl,
          characterEmotionUrl,
        };
        const character360JsonString = JSON.stringify(character360Json);

        const fileName = 'character.character360';
        const keyPath = ['character360', id].concat(fileName);
        const character360JsonUrl = await supabaseFsWorker.writeFile(keyPath, character360JsonString);
        return {
          characterImageUrl,
          character360ImageUrl,
          characterEmotionUrl,
          character360JsonUrl,
        };
      })();
      const {
        characterImageUrl: preview_url,
        character360JsonUrl,
      } = characterResult;

      const character360Asset = {
        id,
        name,
        description: prompt,
        type: 'character360',
        preview_url,
        start_url: character360JsonUrl,
        user_id: sessionUserId,
      };
      const result = await supabaseClient.supabase
        .from('assets')
        .upsert(character360Asset);

      finishGeneration(true);
    } catch (err) {
      finishGeneration(false);
      console.error(err);
    }

    // const newContentPath = contentPath.slice(0, contentPath.length - 1);
    // setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Gender</div>
        <select className={styles.select} value={gender} onChange={e => {
          setItemKeyValueChange2('gender', e.target.value);
          setGender(e.target.value);
        }}>
          {genders.map((genderItem, index) => {
            return (
              <option key={index} className={styles.option} value={genderItem}>
                {genderItem}
              </option>
            );
          })}
        </select>
      </label>

      <label className={styles.label}>
        {!imageUrl ?
          <>
            <div className={styles.text}>Image file</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              const input = document.createElement('input');
              input.type = 'file';
              input.accept = '.png,.jpg,.jpeg';
              input.addEventListener('change', async e => {
                const file = e.target.files[0];

                const supabaseFsWorker = new SupabaseFsWorker({
                  supabase: supabaseClient.supabase,
                  bucketName: 'public',
                });

                const fileName = file.name;
                const keyPath = ['character360', id].concat(fileName);
                const imageUrl2 = await supabaseFsWorker.writeFile(keyPath, file);

                setItemKeyValueChange2('imageUrl', imageUrl2);
                setImageUrl(imageUrl2);
              });
              input.click();
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Choose image</div>
            </button>
          </>
          :
          <>
            <div className={styles.text}>{basename(imageUrl)}</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              setItemKeyValueChange2('imageUrl', null);
              setImageUrl(null);
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Clear</div>
            </button>
          </>
        }
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};
export const CreateMob360Content = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.mob360?.prompt ?? '');
  const [imageUrl, setImageUrl] = useState(() => item?.mob360?.imageUrl ?? '');

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [image360Url, setImage360Url] = useState(() => item?.mob360?.image360Url ?? '');

  //

  const generatable = !!name && (!!prompt || !!imageUrl);

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (prompt !== item?.mob360?.prompt) {
      setPrompt(item?.mob360?.prompt ?? '');
    }
    if (imageUrl !== item?.mob360?.imageUrl) {
      setImageUrl(item?.mob360?.imageUrl ?? '');
    }

    if (image360Url !== item?.mob360?.image360Url) {
      setImage360Url(item?.mob360?.image360Url ?? '');
    }
  }, [
    item,
    name,
    prompt,
    imageUrl,
    image360Url,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        mob360: {
          ...item?.mob360,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _generate = async () => {
    const file = await (async () => {
      if (imageUrl) {
        const res = await fetch(imageUrl);
        const blob = await res.blob();
        return blob;
      } else {
        return null;
      }
    })();

    // console.log('generate mob 1', {
    //   prompt,
    //   file,
    // });
    const mob360Result = await generateMob({
      prompt,
      file,
    });
    // console.log('generate mob 2', mob360Result);

    const {
      mobImageUrl: imageUrl2,
      mob360ImageUrl: image360Url2,
    } = mob360Result;

    // initialize the supabase fs worker
    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    const id = item?.id ?? crypto.randomUUID();
    const promises = [
      (async () => {
        const res = await fetch(imageUrl2);
        const blob = await res.blob();

        const fileName = basename(imageUrl2);
        const keyPath = ['mob360', id].concat(fileName);
        const start_url = await supabaseFsWorker.writeFile(keyPath, blob);
        return start_url;
      })(),
      (async () => {
        const mob360ImageUrl = await (async () => {
          const res = await fetch(image360Url2);
          const blob = await res.blob();

          const fileName = basename(image360Url2);
          const keyPath = ['mob360', id].concat(fileName);
          const start_url = await supabaseFsWorker.writeFile(keyPath, blob);
          return start_url;
        })();

        const item360Json = {
          mob360ImageUrl,
        };
        const item360JsonString = JSON.stringify(item360Json);
        const fileName = 'mob360.item360'; // XXX update this to be mob360
        const keyPath = ['mob360', id].concat(fileName);
        const image360JsonUrl = await supabaseFsWorker.writeFile(keyPath, item360JsonString);
        return image360JsonUrl;
      })(),
    ];
    const [
      imageUrl3,
      image360JsonUrl,
    ] = await Promise.all(promises);

    const item360Asset = {
      id,
      name,
      description: prompt,
      type: 'mob360',
      preview_url: imageUrl3,
      start_url: image360JsonUrl,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(item360Asset);

    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <label className={styles.label}>
        {!imageUrl ?
          <>
            <div className={styles.text}>Image file</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              const input = document.createElement('input');
              input.type = 'file';
              input.accept = '.png,.jpg,.jpeg';
              input.addEventListener('change', async e => {
                const file = e.target.files[0];

                const supabaseFsWorker = new SupabaseFsWorker({
                  supabase: supabaseClient.supabase,
                  bucketName: 'public',
                });

                const fileName = file.name;
                const keyPath = ['item360', id].concat(fileName);
                console.log('got item360 1', {
                  imageUrl,
                  fileName,
                  keyPath,
                });
                const imageUrl2 = await supabaseFsWorker.writeFile(keyPath, file);
                console.log('got item360 2', {
                  imageUrl2,
                });

                setItemKeyValueChange2('imageUrl', imageUrl2);
                setImageUrl(imageUrl2);
              });
              input.click();
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Choose image</div>
            </button>
          </>
          :
          <>
            <div className={styles.text}>{basename(imageUrl)}</div>
            <button className={classnames(
              styles.button,
            )} onClick={e => {
              e.preventDefault();
              e.stopPropagation();

              setItemKeyValueChange2('imageUrl', null);
              setImageUrl(null);
            }}>
              <div className={styles.background} />
              <div className={styles.text}>Clear</div>
            </button>
          </>
        }
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};
export const CreateMusicContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  // onClick,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [prompt, setPrompt] = useState(() => item?.music?.prompt ?? '');
  const [musicUrl, setMusicUrl] = useState(() => item?.music?.musicUrl ?? '');

  //

  const generatable = !!name && !!prompt;

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (prompt !== item?.music?.prompt) {
      setPrompt(item?.music?.prompt ?? '');
    }
    if (musicUrl !== item?.music?.musicUrl) {
      setMusicUrl(item?.music?.musicUrl ?? '');
    }
  }, [
    item,
    name,
    prompt,
    musicUrl,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        music: {
          ...item?.music,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _generate = async () => {
    // console.log('generate music 1', {
    //   name,
    //   prompt,
    // });

    const musicUrl = await generateAudio(prompt);
    // console.log('generate music 2', {
    //   musicUrl,
    // });

    const id = crypto.randomUUID();
    const musicAsset = {
      id,
      name,
      preview_url: '/images/music-inv.svg',
      description: prompt,
      type: 'music',
      start_url: musicUrl,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(musicAsset);

    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createMusicUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setItemKeyValueChange2('prompt', e.target.value);
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <button className={classnames(
        styles.button,
        !generatable ? styles.disabled : null,
      )} onClick={async e => {
        await _generate();
      }} disabled={!generatable}>
        <div className={styles.background} />
        <div className={styles.text}>Generate</div>
      </button>
    </div>
  );
};

//

export const WorldsContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  //

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  useEffect(() => {
    const abortController = new AbortController();
    const { signal } = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadWorlds({
          signal,
          supabaseClient,
          sessionUserId,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } catch (err) {
        throw err;
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, [
    epoch,
  ]);

  //

  return (
    <>
      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}
      {!loading && <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'createWorld',
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.previewImg,
          styles.placeholderImg,
        )} draggable={false} />
        <div className={styles.name}>Create World</div>
      </div>}

      {!loading && items.map(item => {
        return (
          <div className={styles.item} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            // Use the isIC flag to determine which URL to use
            const url = isIC ? `/world/${item.name}` : `/world.html?name=${item.name}`;
            location.href = url;

          }} key={item.id}>
            <img src='/public/images/metaverse-core.svg' className={styles.previewIcon} />
            <div className={styles.name}>{item.name}</div>

            <nav className={styles.iconBtn} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              // remove item
              const result = await supabaseClient.supabase
                .from('worlds')
                .delete()
                .eq('id', item.id);

              refreshItems();
            }}>
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav>
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>
        No assets
      </div>}
    </>
  );
};

//

export const AiAgentsContent = ({
}) => {
  return (
    <>
    </>
  );
};

//

export const SettingsContent = ({
  engine,
}) => {
  const { localStorageManager } = engine;

  // discord
  const [discordToken, setDiscordToken] = useState(() => localStorageManager.getConfig('discordToken') ?? '');
  const [discordChannels, setDiscordChannels] = useState(() => localStorageManager.getConfig('discordChannels') ?? '');
  const [discordUsers, setDiscordUsers] = useState(() => localStorageManager.getConfig('discordUsers') ?? '');

  // XXX these need to be latched in an external manager, not tied to this ui component
  const [discordState, setDiscordState] = useState(null);

  // twitch stream
  const [twitchStreamKey, setTwitchStreamKey] = useState(() => localStorageManager.getConfig('twitchStreamKey') ?? '');

  const [twitchVideoClient, setTwitchVideoClient] = useState(null);

  // twitch chat
  const [twitchUsername, setTwitchUsername] = useState(() => localStorageManager.getConfig('twitchUsername') ?? '');
  const [twitchPassword, setTwitchPassword] = useState(() => localStorageManager.getConfig('twitchPassword') ?? '');
  const [twitchChannels, setTwitchChannels] = useState(() => localStorageManager.getConfig('twitchChannels') ?? '');

  const [setTwitchChatClient, setSetTwitchChatClient] = useState(null);

  //

  return (
    <>
      <div className={styles.section}>
        <div className={styles.sectionHeader}>Discord</div>
        <label className={styles.label}>
          <div className={styles.text}>Token</div>
          <input type='text' className={styles.input} value={discordToken} onChange={e => {
            setDiscordToken(e.target.value);
            localStorageManager.setConfig('discordToken', e.target.value);
          }} placeholder='DISCORD_BOT_TOKEN' />
          {/*
            // Create your bot here: https://discord.com/developers/applications
            // NOTE: You will need to enable Privileged Gateway Intents in your bot's settings!
            // Add the bot to your server:
            // https://discord.com/oauth2/authorize/?permissions=-2080908480&scope=bot&client_id=1143818703813759058
          */}
        </label>
        <label className={styles.label}>
          <div className={styles.text}>Channels</div>
          <input type='text' className={styles.input} value={discordChannels} onChange={e => {
            setDiscordChannels(e.target.value);
            localStorageManager.setConfig('discordChannels', e.target.value);
          }} placeholder='server:channel1, server:channel2' />
        </label>
        <label className={styles.label}>
          <div className={styles.text}>Users</div>
          <input type='text' className={styles.input} value={discordUsers} onChange={e => {
            setDiscordUsers(e.target.value);
            localStorageManager.setConfig('discordUsers', e.target.value);
          }} placeholder='user1, user2' />
        </label>

        <button className={classnames(
          styles.button,
        )} onClick={async e => {
          if (discordState) {
            discordState.cancel();
            setDiscordState(null);
          } else {
            const token = discordToken;
            const channelWhitelist = discordChannels.split(',').map(s => s.trim());
            const userWhitelist = discordUsers.split(',').map(s => s.trim());

            //

            // const model = 'elevenlabs';
            // const voiceId = 'PSAakCTPE63lB4tP9iNQ';

            //

            const abortController = new AbortController();
            const { signal } = abortController;

            //

            console.log('connect to discord client 1');
            const discordClient = new DiscordClient({
              token,
              channelWhitelist,
              userWhitelist,
            });
            await discordClient.connect();
            console.log('connect to discord client 2');

            const { audioManager } = engine;
            const { audioContext } = audioManager;
            const audioCaptureStream = audioManager.captureStream();

            const discordState = {
              cancel: null,
            };
            setDiscordState(discordState);

            let live = true;
            while (live) {
              const outputStream = makeMp3Stream(audioCaptureStream, {
                audioContext,
              });

              discordState.cancel = () => {
                live = false;
                discordClient.destroy();
              };

              console.log('connect to discord client 3');
              await discordClient.input.pushStream(outputStream, {
                signal,
              });
              console.log('connect to discord client 4');

              outputStream.close();
            }
            setDiscordState(null);

            // const voiceEndpoint = new AutoVoiceEndpoint({
            //   model,
            //   voiceId,
            // });
            // const stream = await voiceEndpoint.getStream(`What's up BITCHES!?`);
            // await discordClient.input.pushStream(stream, {
            //   signal,
            // });

            return;

            //

            // await new Promise((accept, reject) => {
            //   setTimeout(accept, 2000);
            // });
            discordClient.input.writeText('test biach');
            console.log('connect to discord client 3');

            console.log('connect to discord client 5');
            discordClient.output.addEventListener('usermessage', async e => {
              const {
                username,
                text,
              } = e.data;
              console.log('got user message 1', { username, text });
              const stream = await voiceEndpoint.getStream(`${username} just said ${text}`);
              console.log('got user message 2', text);
              await discordClient.input.pushStream(stream, {
                signal,
              });
              console.log('got user message 3', text);
            });
            console.log('connect to discord client 6');
          }
        }} disabled={!discordToken}>
          <div className={styles.background} />
          <div className={styles.text}>{!discordState ? 'Connect' : 'Disconnect'}</div>
        </button>
      </div>

      <div className={styles.section}>
        <div className={styles.sectionHeader}>Twitch Stream</div>
        <label className={styles.label}>
          <div className={styles.text}>Stream key</div>
          <input type='text' className={styles.input} value={twitchStreamKey} onChange={e => {
            setTwitchStreamKey(e.target.value);
            localStorageManager.setConfig('twitchStreamKey', e.target.value);
          }} placeholder='TWITCH_STREAM_KEY' />
          {/* https://dashboard.twitch.tv/u/avaer/settings/stream */}
        </label>

        <button className={classnames(
          styles.button,
        )} onClick={async e => {
          if (twitchVideoClient) {
            twitchVideoClient.close();
            setTwitchVideoClient(null);
          } else {
            const streamKey = twitchStreamKey;
            const { engineRenderer, audioManager } = engine;

            // canvas stream
            const canvas = engineRenderer.renderer.domElement;
            const canvasCaptureStream = canvas.captureStream(60);

            // audio main stream
            const audioCaptureStream = audioManager.captureStream();

            // mic stream
            const micStream = localPlayer.voiceInput.micEnabled() ?
              localPlayer.voiceInput
              :
              null;

            const mediaStreams = [
              canvasCaptureStream,
              audioCaptureStream,
              micStream,
            ].filter(s => s !== null);

            const twitchVideoClient = new TwitchVideoClient();
            setTwitchVideoClient(twitchVideoClient);
            await twitchVideoClient.connect({
              streamKey,
              mediaStreams,
            });
          }
        }} disabled={!twitchStreamKey}>
          <div className={styles.background} />
          <div className={styles.text}>{!twitchVideoClient ? 'Connect' : 'Disconnect'}</div>
        </button>
      </div>

      <div className={styles.section}>
        <div className={styles.sectionHeader}>Twitch chat</div>
        <label className={styles.label}>
          <div className={styles.text}>Username</div>
          <input type='text' className={styles.input} value={twitchUsername} onChange={e => {
            setTwitchUsername(e.target.value);
            localStorageManager.setConfig('twitchUsername', e.target.value);
          }} placeholder='TWITCH_USERNAME' />
        </label>
        <label className={styles.label}>
          <div className={styles.text}>Password</div>
          {/* https://twitchapps.com/tmi/ */}
          <input type='password' className={styles.input} value={twitchPassword} onChange={e => {
            setTwitchPassword(e.target.value);
            localStorageManager.setConfig('twitchPassword', e.target.value);
          }} placeholder='TWITCH_PASSWORD' />
        </label>
        <label className={styles.label}>
          <div className={styles.text}>Channels</div>
          <input type='password' className={styles.input} value={twitchChannels} onChange={e => {
            setTwitchChannels(e.target.value);
            localStorageManager.setConfig('twitchChannels', e.target.value);
          }} placeholder='avaer,beaver' />
        </label>

        <button className={classnames(
          styles.button,
        )} onClick={async e => {
          if (twitchChatClient) {
            twitchChatClient.close();
            setTwitchChatClient(null);
          } else {
            const username = twitchUsername;
            const password = twitchPassword;
            const channels = twitchChannels.split(',').map(s => s.trim());

            console.log('twitch chat connect 1');
            const twitchChatClient = new TwitchChatClient();
            setTwitchChatClient(twitchChatClient);
            await twitchChatClient.connect({
              username,
              password,
              channels,
            });
            console.log('twitch chat connect 2');
            twitchChatClient.addEventListener('message', e => {
              console.log('got twitch chat message', e.data);
            });
          }
        }} disabled={!(!!twitchUsername && !!twitchPassword)}>
          <div className={styles.background} />
          <div className={styles.text}>Connect</div>
        </button>
      </div>
    </>
  );
};

//

const uploadScriptDirectory = async (files, {
  supabaseClient,
}) => {
  const id = crypto.randomUUID();

  const filesMap = new Map(); // path -> file
  const filesUrlMap = new Map(); // url -> file

  const metaversefilePaths = [];
  const jsFilePaths = [];
  // console.log('got files', files);

  for (const file of files) {
    const fileName = file.webkitRelativePath || file.name;
    filesMap.set(fileName, file);

    if (/\/\.metaversefile$/.test(fileName)) {
      metaversefilePaths.push(fileName);
    }
    if (/\.js$/.test(fileName)) {
      jsFilePaths.push(fileName);
    }
  }

  // initialize the supabase fs worker
  const supabaseFsWorker = new SupabaseFsWorker({
    supabase: supabaseClient.supabase,
    bucketName: 'public',
  });

  const promises = files.map(file => {
    return (async () => {
      // upload the data file to supabase storage
      const fileName = file.webkitRelativePath || file.name;
      const keyPath = ['scripts', id].concat(fileName);
      const start_url = await supabaseFsWorker.writeFile(keyPath, file);
      filesUrlMap.set(start_url, file);
      return start_url;
    })();
  });
  await Promise.all(promises);

  const startUrls = Array.from(filesUrlMap.keys());

  if (metaversefilePaths.length > 0) {
    const metaversefilePath = metaversefilePaths[0];
    const metaversefileStartUrl = startUrls.find(url => url.endsWith(metaversefilePath));
    const metaversefileJson = await (async () => {
      const res = await fetch(metaversefileStartUrl);
      if (res.ok) {
        const j = await res.json();
        return j;
      } else {
        throw new Error('got bad response status code: ' + res.status);
      }
    })();
    const startUrl = (() => {
      if (typeof metaversefileJson?.start_url === 'string') {
        return metaversefileJson.start_url;
      } else {
        throw new Error('invalid start_url');
      }
    })();
    const startUrl2 = new URL(startUrl, metaversefileStartUrl).href;

    const match = metaversefilePath.match(/^([^\/]*)\/\.metaversefile$/);
    const directoryName = match[1];

    return {
      id,
      name: directoryName,
      preview_url: '/images/arrow.svg',
      start_url: startUrl2,
      urls: startUrls,
    };
  } else if (jsFilePaths.length > 0) {
    const jsPath = jsFilePaths[0];
    const jsStartUrl = startUrls.find(url => url.endsWith(jsPath));
    const match = jsPath.match(/([^\/]*)\.js$/);
    const scriptName = match[1];

    return {
      id,
      name: scriptName,
      preview_url: '/images/arrow.svg',
      start_url: jsStartUrl,
      urls: startUrls,
    };
  } else {
    console.warn('no script file detected');
    return null;
  }
};
export const NftAssetContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,
  address,
}) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  //

  useEffect(() => {
    const abortController = new AbortController();
    const { signal } = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadNftItems({
          signal,
          supabaseClient,
          address,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } catch (err) {
        throw err;
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, []);

  //

  return (
    <>
      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}
      {!loading && items.map(item => {
        return (
          <div className={styles.item} onDoubleClick={e => {
            e.preventDefault();
            e.stopPropagation();

            console.log('double click nft');
          }} key={item.id}>
            <img src={item.preview_url} className={styles.previewImg} draggable={false} />
            <div className={styles.name}>{item.name}</div>
            <div className={styles.category}>{item.collection}</div>
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>
        No assets
      </div>}
    </>
  );
};

//

export const AssetPicker = ({
  contentPath,
  setContentPath,

  assetType,

  value,
  onChange,
}) => {
  const asset = value;

  const firstAssetType = Array.isArray(assetType) ? assetType[0] : assetType;
  const assetTypeSpec = assetTypes[firstAssetType];
  const pickableAssetTypes = Array.isArray(assetType) ? assetType : [assetType];

  //

  const chooseAsset = () => {
    const newContentPath = [
      ...contentPath,
      {
        type: 'chooseAsset',
        assetType,

        value,
        onChange,
      },
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    !asset ? <>
      <div className={styles.item} onClick={e => {
        chooseAsset();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Choose {pickableAssetTypes.join('/')}</div>
      </div>
    </> : <>
      <div className={styles.item} onClick={e => {
        chooseAsset();
      }}>
        <img src={assetTypeSpec.icon} className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>{asset.name}</div>
      </div>
    </>
  );
};
AssetPicker.getValue = ({
  item,
  itemKey,
}) => {
  // const contentPathElement = contentPath[contentPath.length - 1];
  // const {
  //   item = null,
  // } = contentPathElement;
  const asset = item?.[itemKey];
  return asset;
};
AssetPicker.setValue = ({
  itemKey,
}) => ({
  itemValue,
  contentPath,
  setContentPath,
}) => {
    const assetContentPath = contentPath[contentPath.length - 2];
    const newContentPath = [
      ...contentPath.slice(0, -2),
      {
        ...assetContentPath,
        item: {
          ...assetContentPath.item,
          [itemKey]: itemValue,
        },
      },
    ];
    setContentPath(newContentPath);
  };
export const CreateWearableContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;
  const glb = item?.glb;

  //

  const euler = item?.wearable?.quaternion ? localEuler.setFromQuaternion(
    localQuaternion.fromArray(item?.wearable.quaternion),
    'YXZ',
  ).clone() : null;

  const [name, setName] = useState(() => item?.name ?? '');
  const [boneAttachment, setBoneAttachment] = useState(() => item?.wearable?.boneAttachment ?? avatarBoneNames[0]);
  const [positionX, setPositionX] = useState(() => item?.wearable?.position?.[0] ?? 0);
  const [positionY, setPositionY] = useState(() => item?.wearable?.position?.[1] ?? 0);
  const [positionZ, setPositionZ] = useState(() => item?.wearable?.position?.[2] ?? 0);
  const [eulerX, setEulerX] = useState(() => euler?.x ?? 0);
  const [eulerY, setEulerY] = useState(() => euler?.y ?? 0);
  const [eulerZ, setEulerZ] = useState(() => euler?.z ?? 0);
  const [eulerOrder, setEulerOrder] = useState('YXZ');

  console.log('load wearable', item?.wearable);

  //

  const creatable = !!name && !!glb;

  //

  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    if (boneAttachment !== item?.wearable?.boneAttachment) {
      setBoneAttachment(item?.wearable?.boneAttachment ?? avatarBoneNames[0]);
    }
  }, [
    item,
    name,
    boneAttachment,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        wearable: {
          ...item?.wearable,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const wearable = {
      name,
      glbUrl: glb.start_url,
      wearable: {
        boneAttachment,
      },
    };
    if (!isNaN(positionX) && !isNaN(positionY) && !isNaN(positionZ)) {
      wearable.wearable.position = localVector.set(positionX, positionY, positionZ).toArray();
    }
    if (!isNaN(eulerX) && !isNaN(eulerY) && !isNaN(eulerZ)) {
      wearable.wearable.quaternion = localQuaternion.setFromEuler(
        localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
      ).toArray();
    }

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const id = item?.id ?? crypto.randomUUID();
    const keyPath = ['assets', `${id}.wearable`];
    const s = JSON.stringify(wearable, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const wearableAsset = {
      id,
      name,
      type: 'wearable',
      start_url,
      preview_url: glb.preview_url,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(wearableAsset);

    // setEpoch(epoch + 1);
    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createWearableUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Bone</div>
        <select className={styles.select} value={boneAttachment} onChange={e => {
          setItemKeyValueChange('boneAttachment', e.target.value);
          setBoneAttachment(e.target.value);
        }}>
          {avatarBoneNames.map(boneName => {
            return (
              <option value={boneName} key={boneName}>{boneName}</option>
            );
          })}
        </select>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Glb</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType='glb'
          // itemKey='glb'
          value={AssetPicker.getValue({
            item,
            itemKey: 'glb',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'glb',
            // contentPath,
            // setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionX(value);
          } else {
            setPositionX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionY(value);
          } else {
            setPositionY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionZ(value);
          } else {
            setPositionZ(e.target.value);
          }
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerX(value);
          } else {
            value = 0;
            setEulerX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerY(value);
          } else {
            value = 0;
            setEulerY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerZ(value);
          } else {
            value = 0;
            setEulerZ(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateSittableContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;
  const glb = item?.glb;

  //

  const euler = item?.sittable?.quaternion ? localEuler.setFromQuaternion(
    localQuaternion.fromArray(item?.sittable.quaternion),
    'YXZ',
  ).clone() : null;

  const [name, setName] = useState(() => item?.name ?? '');
  const [positionX, setPositionX] = useState(() => item?.sittable?.position[0] ?? 0);
  const [positionY, setPositionY] = useState(() => item?.sittable?.position[1] ?? 0);
  const [positionZ, setPositionZ] = useState(() => item?.sittable?.position[2] ?? 0);
  const [eulerX, setEulerX] = useState(() => euler?.x ?? 0);
  const [eulerY, setEulerY] = useState(() => euler?.y ?? 0);
  const [eulerZ, setEulerZ] = useState(() => euler?.z ?? 0);
  const [eulerOrder, setEulerOrder] = useState('YXZ');

  //

  const creatable = !!name && !!glb;

  //

  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    // if (boneAttachment !== item?.sittable?.boneAttachment) {
    //   setBoneAttachment(item?.sittable?.boneAttachment ?? avatarBoneNames[0]);
    // }
  }, [
    item,
    name,
    // boneAttachment,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        sittable: {
          ...item?.sittable,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const sittable = {
      name,
      glbUrl: glb.start_url,
      sittable: {
        position: localVector.set(positionX, positionY, positionZ).toArray(),
        quaternion: localQuaternion.setFromEuler(
          localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
        ).toArray(),
        "subtype": "chair",
        "sitOffset": [0, 0.5, 0],
        // "walkAnimation": ["wing_2_low|Take 001|BaseLayer", "wing_2_low001|Take 001|BaseLayer", "Object001|Take 001|BaseLayer", "Object002|Take 001|BaseLayer", "Object003|Take 001|BaseLayer", "Object004|Take 001|BaseLayer"],
        // "walkAnimationHoldTime": 1,
        // "walkAnimationSpeedFactor": 0.1,
        // "speed": 0.02,
        // "damping": 0.99,
      },
    };
    if (!isNaN(positionX) && !isNaN(positionY) && !isNaN(positionZ)) {
      sittable.sittable.position = localVector.set(positionX, positionY, positionZ).toArray();
    }
    if (!isNaN(eulerX) && !isNaN(eulerY) && !isNaN(eulerZ)) {
      sittable.sittable.quaternion = localQuaternion.setFromEuler(
        localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
      ).toArray();
    }

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const id = item?.id ?? crypto.randomUUID();
    const keyPath = ['assets', `${id}.sittable`];
    const s = JSON.stringify(sittable, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const sittableAsset = {
      id,
      name,
      type: 'sittable',
      start_url,
      preview_url: glb.preview_url,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(sittableAsset);

    // setEpoch(epoch + 1);
    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createWearableUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Glb</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType='glb'
          // itemKey='glb'
          value={AssetPicker.getValue({
            item,
            itemKey: 'glb',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'glb',
            // contentPath,
            // setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionX(value);
          } else {
            setPositionX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionY(value);
          } else {
            setPositionY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionZ(value);
          } else {
            setPositionZ(e.target.value);
          }
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerX(value);
          } else {
            value = 0;
            setEulerX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerY(value);
          } else {
            value = 0;
            setEulerY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerZ(value);
          } else {
            value = 0;
            setEulerZ(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateMountContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;
  const glb = item?.glb;

  //

  const euler = item?.mount?.quaternion ? localEuler.setFromQuaternion(
    localQuaternion.fromArray(item?.mount.quaternion),
    'YXZ',
  ).clone() : null;

  const [name, setName] = useState(() => item?.name ?? '');
  const [positionX, setPositionX] = useState(() => item?.mount?.position[0] ?? 0);
  const [positionY, setPositionY] = useState(() => item?.mount?.position[1] ?? 0);
  const [positionZ, setPositionZ] = useState(() => item?.mount?.position[2] ?? 0);
  const [eulerX, setEulerX] = useState(() => euler?.x ?? 0);
  const [eulerY, setEulerY] = useState(() => euler?.y ?? 0);
  const [eulerZ, setEulerZ] = useState(() => euler?.z ?? 0);
  const [eulerOrder, setEulerOrder] = useState('YXZ');

  //

  const creatable = !!name && !!glb;

  //

  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    // if (boneAttachment !== item?.mount?.boneAttachment) {
    //   setBoneAttachment(item?.mount?.boneAttachment ?? avatarBoneNames[0]);
    // }
  }, [
    item,
    name,
    // boneAttachment,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        mount: {
          ...item?.mount,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const mount = {
      name,
      glbUrl: glb.start_url,
      mount: {
        position: localVector.set(positionX, positionY, positionZ).toArray(),
        quaternion: localQuaternion.setFromEuler(
          localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
        ).toArray(),
        "subtype": "saddle",
        "sitBone": "Spine",
        "walkAnimation": "Armature|Armature|Hopping|Armature|Hopping",
        "walkAnimationHoldTime": 1,
        "walkAnimationSpeedFactor": 0.1,
        "speed": 0.02,
        "damping": 0.99,
      },
    };
    if (!isNaN(positionX) && !isNaN(positionY) && !isNaN(positionZ)) {
      mount.mount.position = localVector.set(positionX, positionY, positionZ).toArray();
    }
    if (!isNaN(eulerX) && !isNaN(eulerY) && !isNaN(eulerZ)) {
      mount.mount.quaternion = localQuaternion.setFromEuler(
        localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
      ).toArray();
    }

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const id = item?.id ?? crypto.randomUUID();
    const keyPath = ['assets', `${id}.sittable`];
    const s = JSON.stringify(sittable, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const mountAsset = {
      id,
      name,
      type: 'mount',
      start_url,
      preview_url: glb.preview_url,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(mountAsset);

    // setEpoch(epoch + 1);
    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createWearableUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Glb</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType='glb'
          // itemKey='glb'
          value={AssetPicker.getValue({
            item,
            itemKey: 'glb',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'glb',
            // contentPath,
            // setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionX(value);
          } else {
            setPositionX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionY(value);
          } else {
            setPositionY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionZ(value);
          } else {
            setPositionZ(e.target.value);
          }
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerX(value);
          } else {
            value = 0;
            setEulerX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerY(value);
          } else {
            value = 0;
            setEulerY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerZ(value);
          } else {
            value = 0;
            setEulerZ(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateVehicleContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;
  const glb = item?.glb;

  //

  const euler = item?.vehicle?.quaternion ? localEuler.setFromQuaternion(
    localQuaternion.fromArray(item?.vehicle.quaternion),
    'YXZ',
  ).clone() : null;

  const [name, setName] = useState(() => item?.name ?? '');
  const [positionX, setPositionX] = useState(() => item?.vehicle?.position[0] ?? 0);
  const [positionY, setPositionY] = useState(() => item?.vehicle?.position[1] ?? 0);
  const [positionZ, setPositionZ] = useState(() => item?.vehicle?.position[2] ?? 0);
  const [eulerX, setEulerX] = useState(() => euler?.x ?? 0);
  const [eulerY, setEulerY] = useState(() => euler?.y ?? 0);
  const [eulerZ, setEulerZ] = useState(() => euler?.z ?? 0);
  const [eulerOrder, setEulerOrder] = useState('YXZ');

  //

  const creatable = !!name && !!glb;

  //

  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  // bind change setters
  useEffect(() => {
    if (name !== item?.name) {
      setName(item?.name ?? '');
    }
    // if (boneAttachment !== item?.vehicle?.boneAttachment) {
    //   setBoneAttachment(item?.vehicle?.boneAttachment ?? avatarBoneNames[0]);
    // }
  }, [
    item,
    name,
    // boneAttachment,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        vehicle: {
          ...item?.vehicle,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const vehicle = {
      name,
      glbUrl: glb.start_url,
      vehicle: {
        // position: localVector.set(positionX, positionY, positionZ).toArray(),
        // quaternion: localQuaternion.setFromEuler(
        //   localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
        // ).toArray(),
        "subtype": "saddle",
        "sitOffset": [0, 0.5, 0],
        "walkAnimation": ["wing_2_low|Take 001|BaseLayer", "wing_2_low001|Take 001|BaseLayer", "Object001|Take 001|BaseLayer", "Object002|Take 001|BaseLayer", "Object003|Take 001|BaseLayer", "Object004|Take 001|BaseLayer"],
        "walkAnimationHoldTime": 1,
        "walkAnimationSpeedFactor": 0.1,
        "speed": 0.02,
        "damping": 0.99,
      },
    };
    if (!isNaN(positionX) && !isNaN(positionY) && !isNaN(positionZ)) {
      vehicle.vehicle.position = localVector.set(positionX, positionY, positionZ).toArray();
    }
    if (!isNaN(eulerX) && !isNaN(eulerY) && !isNaN(eulerZ)) {
      vehicle.vehicle.quaternion = localQuaternion.setFromEuler(
        localEuler.set(eulerX, eulerY, eulerZ, eulerOrder)
      ).toArray();
    }

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const id = item?.id ?? crypto.randomUUID();
    const keyPath = ['assets', `${id}.vehicle`];
    const s = JSON.stringify(vehicle, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const vehicleAsset = {
      id,
      name,
      type: 'vehicle',
      start_url,
      preview_url: glb.preview_url,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(vehicleAsset);

    // setEpoch(epoch + 1);
    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createWearableUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Glb</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType='glb'
          // itemKey='glb'
          value={AssetPicker.getValue({
            item,
            itemKey: 'glb',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'glb',
            // contentPath,
            // setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionX(value);
          } else {
            setPositionX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionY(value);
          } else {
            setPositionY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          const value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setPositionZ(value);
          } else {
            setPositionZ(e.target.value);
          }
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerX(value);
          } else {
            value = 0;
            setEulerX(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerY(value);
          } else {
            value = 0;
            setEulerY(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            setEulerZ(value);
          } else {
            value = 0;
            setEulerZ(e.target.value);
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateMobContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;
  const glb = item?.glb;

  //

  const [name, setName] = useState(item?.name || '');
  // const [positionX, setPositionX] = useState(0);
  // const [positionY, setPositionY] = useState(0);
  // const [positionZ, setPositionZ] = useState(0);
  // const [eulerX, setEulerX] = useState(0);
  // const [eulerY, setEulerY] = useState(0);
  // const [eulerZ, setEulerZ] = useState(0);
  // const [eulerOrder, setEulerOrder] = useState('YXZ');

  //

  const creatable = !!name && !!glb;

  //

  const stopPropagation = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      e.target.blur();
    }
  };
  const makeOnBlur = (setter, defaultValue) => e => {
    if (e.target.value === '') {
      setter(defaultValue);
    }
  };

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name || '';
    if (_name !== name) {
      setName(_name);
    }
  }, [
    item,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createWearableUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Glb</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType='glb'
          // itemKey='glb'
          value={AssetPicker.getValue({
            item,
            itemKey: 'glb',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'glb',
            contentPath,
            setContentPath,
          })}
        />
      </label>

      {/* <label className={styles.label}>
        <span className={styles.text}>p.x</span>
        <input type='number' step={1} value={positionX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionX, 0)} onChange={e => {
          setPositionX(value);
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.y</span>
        <input type='number' step={1} value={positionY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionY, 0)} onChange={e => {
          setPositionY(value);
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>p.z</span>
        <input type='number' step={1} value={positionZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setPositionZ, 0)} onChange={e => {
          setPositionZ(value);
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>r.x</span>
        <input type='number' step={0.1} value={eulerX} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerX, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            // localEuler.set(value, eulerY, eulerZ, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerX(value);

            // onChange && onChange();
          } else {
            value = 0;
            // localEuler.set(value, eulerY, eulerZ, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerX(e.target.value);

            // onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.y</span>
        <input type='number' step={0.1} value={eulerY} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerY, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            // localEuler.set(eulerX, value, eulerZ, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerY(value);

            // onChange && onChange();
          } else {
            value = 0;
            // localEuler.set(eulerX, value, eulerZ, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerY(e.target.value);

            // onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.z</span>
        <input type='number' step={0.1} value={eulerZ} onKeyDown={stopPropagation} onBlur={makeOnBlur(setEulerZ, 0)} onChange={e => {
          let value = parseFloat(e.target.value);
          if (!isNaN(value)) {
            // localEuler.set(eulerX, eulerY, value, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerZ(value);

            // onChange && onChange();
          } else {
            value = 0;
            // localEuler.set(eulerX, eulerY, value, eulerOrder);
            // app.quaternion.setFromEuler(localEuler);
            // app.updateMatrixWorld();
            // _updateAppPhysics(app);
            setEulerZ(e.target.value);

            // onChange && onChange();
          }
        }} />
      </label>
      <label className={styles.label}>
        <span className={styles.text}>r.order</span>
        <select value={eulerOrder} onChange={e => {
          const value = e.target.value;

          setEulerOrder(value);

          localEuler
            .set(eulerX, eulerY, eulerZ, eulerOrder)
            .reorder(value);
          setEulerX(localEuler.x);
          setEulerY(localEuler.y);
          setEulerZ(localEuler.z);

          // onChange && onChange();
        }}>
          <option value='XYZ'>XYZ</option>
          <option value='YZX'>YZX</option>
          <option value='ZXY'>ZXY</option>
          <option value='XZY'>XZY</option>
          <option value='YXZ'>YXZ</option>
          <option value='ZYX'>ZYX</option>
        </select>
      </label> */}

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        if (
          glb
        ) {
          const mob = {
            name,
            glbUrl: glb.start_url,
            mob: {
              "radius": 0.3,
              "height": 0.8,
              "physicsPosition": [0, 0.5, 0],
              "modelQuaternion": [0, 1, 0, 0],
              "idleAnimation": "idle",
              "walkAnimation": "walk",
              "walkSpeed": 0.000003,
              "aggroDistance": 10,
            },
          };

          //

          const supabaseFsWorker = new SupabaseFsWorker({
            supabase: supabaseClient.supabase,
            bucketName: 'public',
          });

          //

          const uuid = crypto.randomUUID();
          const keyPath = ['assets', `${uuid}.mob`];
          const s = JSON.stringify(mob, null, 2);
          const start_url = await supabaseFsWorker.writeFile(keyPath, s);

          const id = crypto.randomUUID();
          const mobAsset = {
            id,
            name,
            type: 'mob',
            start_url,
            preview_url: glb.preview_url,
            user_id: sessionUserId,
          };
          // console.log('create mob', mobAsset);
          // debugger;
          const result = await supabaseClient.supabase
            .from('assets')
            .upsert(mobAsset);

          // setEpoch(epoch + 1);
          const newContentPath = contentPath.slice(0, contentPath.length - 1);
          setContentPath(newContentPath);
        }
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        console.log('update', item);
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
const defaultSkillDefinition = {
  // "schema_version": "v1",
  // "name_for_human": "TODO List",
  "name_for_model": "memory",
  // "description_for_human": "Manage your TODO list. You can add, remove and view your TODOs.",
  "description_for_model": "Recall memories in the character's memory bank.",
  // "auth": {
  //     "type": "none"
  // },
  // "api": {
  //     "type": "openapi",
  //     "url": "http://localhost:3333/openapi.yaml"
  // },
  // "logo_url": "http://localhost:3333/logo.png",
  // "contact_email": "support@example.com",
  // "legal_info_url": "http://www.example.com/legal"
};
const defaultSkillDefinitionString = JSON.stringify(defaultSkillDefinition, null, 2);
const scriptSourceTypes = [
  'script',
  'directory',
];
const defaultScriptContentString = `\
// JavaScript app code to execute.

export default ctx => {
  const {
    useApp,
  } = ctx;
  const app = useApp();
  console.log('script loaded: ' + app);
};
`;
export const CreateSkillContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  //

  const [name, setName] = useState(item?.name ?? '');
  const [definition, setDefinition] = useState(item?.skill?.definition ?? defaultSkillDefinitionString);
  const [sourceType, setSourceType] = useState(item?.skill?.sourceType ?? scriptSourceTypes[0]);
  const [scriptContent, setScriptContent] = useState(item?.skill?.scriptContent ?? defaultScriptContentString);
  const [srcUrls, setSrcUrls] = useState(item?.skill?.srcUrls ?? []);

  const [id, setId] = useState(item?.id);
  const [startUrl, setStartUrl] = useState(item?.skill?.start_url || '');

  // const refreshItems = () => {
  //   setEpoch(epoch + 1);
  // };

  const creatable = !!name && !!definition && (
    sourceType === 'script' ? !!scriptContent : (!!startUrl && !!srcUrls.length)
  );

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name ?? '';
    if (_name !== name) {
      setName(_name);
    }
    const _definition = item?.definition ?? defaultSkillDefinitionString;
    if (_definition !== definition) {
      setDefinition(_definition);
    }
    const _sourceType = item?.sourceType ?? scriptSourceTypes[0];
    if (_sourceType !== sourceType) {
      setSourceType(_sourceType);
    }
    const _scriptContent = item?.scriptContent ?? defaultScriptContentString;
    if (_scriptContent !== scriptContent) {
      setScriptContent(_scriptContent);
    }
    const _srcUrls = item?.srcUrls ?? [];
    if (_srcUrls !== srcUrls) {
      setSrcUrls(_srcUrls);
    }

    const _id = item?.id;
    if (_id !== id) {
      setId(_id);
    }
    const _startUrl = item?.start_url ?? '';
    if (_startUrl !== startUrl) {
      setStartUrl(_startUrl);
    }
  }, [
    item,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const localId = id ?? crypto.randomUUID();
    const skillUrl = await (async () => {
      if (sourceType === 'script') {
        if (!scriptContent) {
          throw new Error('no script content');
        }

        const supabaseFsWorker = new SupabaseFsWorker({
          supabase: supabaseClient.supabase,
          bucketName: 'public',
        });

        // const fileName = file.webkitRelativePath || file.name;
        const fileName = `${name}.js`;
        const keyPath = ['scripts', localId].concat(fileName);
        const blob = new Blob([
          scriptContent,
        ], {
          type: 'text/javascript',
        });
        const skillUrl = await supabaseFsWorker.writeFile(keyPath, blob);
        return skillUrl;
      } else if (sourceType === 'directory') {
        if (!startUrl) {
          throw new Error('no start url');
        }
        return startUrl;
      } else {
        throw new Error('invalid source type: ' + sourceType);
      }
    })();

    const skill = {
      name,
      definition,
      sourceType,
      scriptContent,
      srcUrls,
      skillUrl,
    };

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const keyPath = ['assets', `${localId}.skill`];
    const s = JSON.stringify(skill, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const asset = {
      id,
      type: 'skill',
      name,
      start_url,
      preview_url: '/images/skill-inv.svg',
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(asset);

    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createSkillUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Definition</span>
        <textarea
          className={styles.textarea}
          value={definition}
          onChange={e => {
            setItemKeyValueChange('definition', e.target.value);
            setDefinition(e.target.value);
          }}
          placeholder={defaultSkillDefinitionString}
        />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Source Type</span>
        <select value={sourceType} onChange={e => {
          const value = e.target.value;
          setItemKeyValueChange('sourceType', value);
          setSourceType(value);
        }}>
          {scriptSourceTypes.map(sourceType => (
            <option key={sourceType} value={sourceType}>{sourceType}</option>
          ))}
        </select>
      </label>

      {(() => {
        switch (sourceType) {
          case 'script': {
            return (
              <>
                <label className={styles.label}>
                  <span className={styles.text}>Script</span>
                  <textarea
                    className={styles.textarea}
                    value={scriptContent}
                    onChange={e => {
                      setItemKeyValueChange('scriptContent', e.target.value);
                      setScriptContent(e.target.value);
                    }}
                    placeholder={defaultScriptContentString}
                  />
                </label>
                <button className={classnames(
                  styles.button,
                )} onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();

                  // choose from input[type=file]
                  const input = document.createElement('input');
                  input.type = 'file';
                  input.accept = '.js,.jsx';
                  input.addEventListener('change', async e => {
                    const f = e.target.files[0];
                    // ensure the script is <= 1MB
                    if (f.size <= 1024 * 1024) {
                      const scriptContent = await f.text();
                      setItemKeyValueChange('scriptContent', scriptContent);
                      setScriptContent(scriptContent);
                    } else {
                      console.warn('script too large');
                    }
                  });
                  input.click();
                }}>
                  <div className={styles.background} />
                  <div className={styles.text}>Load .js...</div>
                </button>
              </>
            );
          }
          case 'directory': {
            return (
              <>
                {srcUrls.length > 0 ? (
                  <>
                    <label className={styles.label}>
                      <span className={styles.text}>Files ({srcUrls.length})</span>
                      <button className={classnames(
                        styles.button,
                      )} onClick={async e => {
                        const zipBlob = await urls2Zip(srcUrls);
                        downloadFile(zipBlob, 'skill.zip');
                      }}>
                        <div className={styles.background} />
                        <div className={styles.text}>Download .zip</div>
                      </button>
                    </label>
                  </>
                ) : <>
                  <label className={styles.label}>
                    <span className={styles.text}>Upload</span>
                    <button className={classnames(
                      styles.button,
                    )} onClick={async e => {
                      // choose from input[type=file]
                      const input = document.createElement('input');
                      input.type = 'file';
                      input.webkitdirectory = true;
                      input.directory = true;
                      // input.multiple = true;
                      input.addEventListener('change', async e => {
                        const files = Array.from(input.files);
                        const uploadResult = await uploadScriptDirectory(files, {
                          supabaseClient,
                        });
                        if (uploadResult) {
                          const {
                            id,
                            // name,
                            start_url,
                            urls,
                          } = uploadResult;

                          setId(id);
                          setStartUrl(start_url);
                          setSrcUrls(urls);
                        }
                      });
                      input.click();
                    }}>
                      <div className={styles.background} />
                      <div className={styles.text}>Load directory</div>
                    </button>
                  </label>
                </>}
              </>
            );
          }
          default: return null;
        }
      })()}

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateScriptContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  //

  const [name, setName] = useState(item?.name ?? '');
  const [sourceType, setSourceType] = useState(item?.script?.sourceType ?? scriptSourceTypes[0]);
  const [scriptContent, setScriptContent] = useState(item?.script?.scriptContent ?? defaultScriptContentString);
  const [srcUrls, setSrcUrls] = useState(item?.script?.srcUrls ?? []);

  const [id, setId] = useState(item?.id);
  const [startUrl, setStartUrl] = useState(item?.script?.start_url || '');

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  const creatable = !!name && (
    sourceType === 'script' ? !!scriptContent : (!!startUrl && !!srcUrls.length)
  );

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name ?? '';
    if (_name !== name) {
      setName(_name);
    }
    const _sourceType = item?.sourceType ?? scriptSourceTypes[0];
    if (_sourceType !== sourceType) {
      setSourceType(_sourceType);
    }
    const _scriptContent = item?.scriptContent ?? defaultScriptContentString;
    if (_scriptContent !== scriptContent) {
      setScriptContent(_scriptContent);
    }
    const _srcUrls = item?.srcUrls ?? [];
    if (_srcUrls !== srcUrls) {
      setSrcUrls(_srcUrls);
    }

    const _id = item?.id;
    if (_id !== id) {
      setId(_id);
    }
    const _startUrl = item?.start_url ?? '';
    if (_startUrl !== startUrl) {
      setStartUrl(_startUrl);
    }
  }, [
    item,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createSkillUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Source Type</span>
        <select value={sourceType} onChange={e => {
          const value = e.target.value;
          setItemKeyValueChange('sourceType', value);
          setSourceType(value);
        }}>
          {scriptSourceTypes.map(sourceType => (
            <option key={sourceType} value={sourceType}>{sourceType}</option>
          ))}
        </select>
      </label>

      {(() => {
        switch (sourceType) {
          case 'script': {
            return (
              <>
                <label className={styles.label}>
                  <span className={styles.text}>Script</span>
                  <textarea
                    className={styles.textarea}
                    value={scriptContent}
                    onChange={e => {
                      setItemKeyValueChange('scriptContent', e.target.value);
                      setScriptContent(e.target.value);
                    }}
                    placeholder={defaultScriptContentString}
                  />
                </label>
                <button className={classnames(
                  styles.button,
                )} onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();

                  // choose from input[type=file]
                  const input = document.createElement('input');
                  input.type = 'file';
                  input.accept = '.js,.jsx';
                  input.addEventListener('change', async e => {
                    const f = e.target.files[0];
                    // ensure the script is <= 1MB
                    if (f.size <= 1024 * 1024) {
                      const scriptContent = await f.text();
                      setItemKeyValueChange('scriptContent', scriptContent);
                      setScriptContent(scriptContent);
                    } else {
                      console.warn('script too large');
                    }
                  });
                  input.click();
                }}>
                  <div className={styles.background} />
                  <div className={styles.text}>Load .js...</div>
                </button>
              </>
            );
          }
          case 'directory': {
            return (
              <>
                {srcUrls.length > 0 ? (
                  <>
                    {/* <label className={styles.label}>
                      <span className={styles.text}>Main script</span>
                      <a href={srcUrls[0]} target='_blank'>{basename(srcUrls[0])}</a>
                    </label> */}
                    <label className={styles.label}>
                      <span className={styles.text}>Files ({srcUrls.length})</span>
                      <button className={classnames(
                        styles.button,
                      )} onClick={async e => {
                        const zipBlob = await urls2Zip(srcUrls);
                        downloadFile(zipBlob, 'script.zip');
                      }}>
                        <div className={styles.background} />
                        <div className={styles.text}>Download .zip</div>
                      </button>
                    </label>
                  </>
                ) : <>
                  <label className={styles.label}>
                    <span className={styles.text}>Upload</span>
                    <button className={classnames(
                      styles.button,
                    )} onClick={async e => {
                      // choose from input[type=file]
                      const input = document.createElement('input');
                      input.type = 'file';
                      input.webkitdirectory = true;
                      input.directory = true;
                      // input.multiple = true;
                      input.addEventListener('change', async e => {
                        const files = Array.from(input.files);
                        const uploadResult = await uploadScriptDirectory(files, {
                          supabaseClient,
                        });
                        if (uploadResult) {
                          const {
                            id,
                            // name,
                            start_url,
                            urls,
                          } = uploadResult;

                          setId(id);
                          setStartUrl(start_url);
                          setSrcUrls(urls);
                        }
                      });
                      input.click();
                    }}>
                      <div className={styles.background} />
                      <div className={styles.text}>Load directory</div>
                    </button>
                  </label>
                </>}
              </>
            );
          }
          default: return null;
        }
      })()}

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        const localId = (() => {
          if (sourceType === 'script') {
            return crypto.randomUUID();
          } else if (sourceType === 'directory') {
            return crypto.randomUUID();
          } else {
            throw new Error('invalid source type: ' + sourceType);
          }
        })();
        const scriptUrl = await (async () => {
          if (sourceType === 'script') {
            if (!scriptContent) {
              throw new Error('no script content');
            }

            const supabaseFsWorker = new SupabaseFsWorker({
              supabase: supabaseClient.supabase,
              bucketName: 'public',
            });

            // const fileName = file.webkitRelativePath || file.name;
            const fileName = `${name}.js`;
            const keyPath = ['scripts', localId].concat(fileName);
            const blob = new Blob([
              scriptContent,
            ], {
              type: 'text/javascript',
            });
            const scriptUrl = await supabaseFsWorker.writeFile(keyPath, blob);
            return scriptUrl;
          } else if (sourceType === 'directory') {
            if (!startUrl) {
              throw new Error('no start url');
            }
            return startUrl;
          } else {
            throw new Error('invalid source type: ' + sourceType);
          }
        })();

        const script = {
          name,
          sourceType,
          scriptContent,
          srcUrls,
          scriptUrl,
        };

        //

        const supabaseFsWorker = new SupabaseFsWorker({
          supabase: supabaseClient.supabase,
          bucketName: 'public',
        });

        //

        const keyPath = ['assets', `${localId}.script`];
        const s = JSON.stringify(script, null, 2);
        const start_url = await supabaseFsWorker.writeFile(keyPath, s);

        const asset = {
          id,
          type: 'script',
          name,
          start_url,
          preview_url: '/images/code-inv.svg',
          user_id: sessionUserId,
        };
        const result = await supabaseClient.supabase
          .from('assets')
          .upsert(asset);

        // setEpoch(epoch + 1);
        const newContentPath = contentPath.slice(0, contentPath.length - 1);
        setContentPath(newContentPath);
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        console.log('update', item);
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateVoiceContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => item?.name ?? '');
  const [voiceEndpoint, setVoiceEndpoint] = useState(() => item?.voice?.voiceEndpoint ?? '');
  const [voiceUrls, setVoiceUrls] = useState(() => item?.voice?.voiceUrls ?? []);
  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());

  const creatable = !!name && voiceUrls.length > 0;

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name ?? '';
    if (_name !== name) {
      setName(_name);
    }
    const _voiceEndpoint = item?.voice?.voiceEndpoint ?? '';
    if (_voiceEndpoint !== voiceEndpoint) {
      setVoiceEndpoint(_voiceEndpoint);
    }
    const _voiceUrls = item?.voice?.voiceUrls ?? [];
    if (_voiceUrls.length !== voiceUrls.length || !_voiceUrls.every((v, i) => v === voiceUrls[i])) {
      setVoiceUrls(_voiceUrls);
    }

    const _id = item?.id;
    if (_id !== id) {
      setId(_id);
    }
  }, [
    item,
    name,
    voiceEndpoint,
    voiceUrls,
    id,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        voice: {
          ...item?.voice,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const oldVoiceEndpoint = item?.voice?.voiceEndpoint;

    // create the actual backing voice
    const voiceFiles = await Promise.all(voiceUrls.map(async voiceUrl => {
      const res = await fetch(voiceUrl);
      const blob = await res.blob();
      return blob;
    }));
    const j = await voiceTrainer.addVoice(name + ' ' + makeId(8), voiceFiles);
    const { voice_id } = j;
    const newVoiceEndpoint = `elevenlabs:${name}:${voice_id}`;

    const voice = {
      name,
      voiceEndpoint: newVoiceEndpoint,
      voiceUrls,
    };

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const localId = id ?? crypto.randomUUID();
    const keyPath = ['assets', `${localId}.voice`];
    const s = JSON.stringify(voice, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const asset = {
      id,
      type: 'voice',
      name,
      start_url,
      preview_url: '/images/voice-inv.svg',
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(asset);

    //

    if (oldVoiceEndpoint) {
      const match = oldVoiceEndpoint.match(/^elevenlabs:(.+?):(.+?)$/);
      if (match) {
        const voiceId = match[1];
        const j = await voiceTrainer.removeVoice(voiceId);
        console.log('remove old voice result', j);
      } else {
        console.warn('old voice endpoint was invalid so not deleting it', oldVoiceEndpoint);
      }
    }

    //

    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createSkillUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Upload</span>
        <button className={classnames(
          styles.button,
        )} onClick={e => {
          e.preventDefault();
          e.stopPropagation();

          // choose from input[type=file]
          const input = document.createElement('input');
          input.type = 'file';
          input.accept = '.mp3,.wav,.ogg';
          input.multiple = true;
          input.addEventListener('change', async e => {
            const files = Array.from(e.target.files);

            // initialize the supabase fs worker
            const supabaseFsWorker = new SupabaseFsWorker({
              supabase: supabaseClient.supabase,
              bucketName: 'public',
            });

            const promises = files.map(file => {
              return (async () => {
                // upload the data file to supabase storage
                const fileName = file.webkitRelativePath || file.name;
                const keyPath = ['voices', id].concat(fileName);
                const start_url = await supabaseFsWorker.writeFile(keyPath, file);
                return start_url;
              })();
            });
            const startUrls = await Promise.all(promises);
            const newVoiceUrls = [
              // ...voiceUrls,
              ...startUrls,
            ];
            setItemKeyValueChange2('voiceUrls', newVoiceUrls);
            setVoiceUrls(newVoiceUrls);
          });
          input.click();
        }}>
          <div className={styles.background} />
          <div className={styles.text}>Add files...</div>
        </button>
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Files ({voiceUrls.length})</span>
        <button className={classnames(
          styles.button,
        )} onClick={async e => {
          const zipBlob = await urls2Zip(voiceUrls);
          downloadFile(zipBlob, 'voice.zip');
        }}>
          <div className={styles.background} />
          <div className={styles.text}>Download .zip</div>
        </button>
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const CreateCameraContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  //

  const fovDefault = 90;
  const aspectDefault = 1;
  const dofDefault = 0;

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [name, setName] = useState(() => item?.name ?? '');
  const [description, setDescription] = useState(() => item?.camera?.description ?? '');
  const [fov, setFov] = useState(() => item?.camera?.fov ?? fovDefault);
  const [aspect, setAspect] = useState(() => item?.camera?.aspect ?? aspectDefault);
  const [dof, setDof] = useState(() => item?.camera?.dof ?? dofDefault);

  // const creatable = !!name;
  const creatable = true;

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name ?? '';
    if (_name !== name) {
      setName(_name);
    }
    const _description = item?.camera?.description ?? '';
    if (_description !== description) {
      setDescription(_description);
    }
    const _fov = item?.camera?.fov ?? fovDefault;
    if (_fov !== fov) {
      setFov(_fov);
    }
    const _aspect = item?.camera?.aspect ?? aspectDefault;
    if (_aspect) {
      setAspect(aspect);
    }
    const _dof = item?.camera?.dof ?? dofDefault;
    if (_dof) {
      setDof(dof);
    }

    const _id = item?.id;
    if (_id !== id) {
      setId(_id);
    }
  }, [
    item,
    name,
    description,
    fov,
    aspect,
    dof,
    id,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemKeyValueChange2 = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        camera: {
          ...item?.voice,
          [key]: value,
        },
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const cameraJson = {
      description,
      fov,
      aspect,
      dof,
    };

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const localId = id ?? crypto.randomUUID();
    const keyPath = ['assets', `${localId}.camera`];
    const s = JSON.stringify(cameraJson, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const asset = {
      id,
      type: 'camera',
      name,
      start_url,
      preview_url: '/assets/video-camera-inv.svg',
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(asset);

    //

    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createSkillUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name for the camera' />
      </label>

      <textarea
        className={styles.textarea}
        value={description}
        onChange={e => {
          setItemKeyValueChange('description', e.target.value);
          setDescription(e.target.value);
        }}
        placeholder='Enter a description for the camera'
      />

      <label className={styles.label}>
        <div className={styles.text}>FOV</div>
        <input type='number' className={styles.input} min={1} max={179} step={1} value={fov} onChange={e => {
          setItemKeyValueChange2('fov', e.target.value);
          setFov(e.target.value);
        }} placeholder={fovDefault} />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Aspect</div>
        <input type='number' className={styles.input} min={0.01} max={4} step={0.01} value={aspect} onChange={e => {
          setItemKeyValueChange2('aspect', e.target.value);
          setAspect(e.target.value);
        }} placeholder={aspectDefault} />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>DOF</div>
        <input type='number' className={styles.input} min={0} max={1} step={0.1} value={dof} onChange={e => {
          setItemKeyValueChange2('dof', e.target.value);
          setDof(e.target.value);
        }} placeholder={dofDefault} />
      </label>

      {!editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {editing && <button className={classnames(
        styles.button,
        !creatable ? styles.disabled : null,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};

// Convert to use the IC

export const CreateImageContent = ({
  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  return (
    <>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const input = document.createElement('input');
        input.type = 'file';
        const mimeTypes = [
          'png',
          'jpg',
          'jpeg',
          'gif',
        ];
        input.accept = mimeTypes.map(mimeType => '.' + mimeType).join(',');
        input.addEventListener('change', async e => {
          const f = e.target.files[0];

          const metadata = await importFileType('image')(f, supabaseClient, sessionUserId);
          await addAsset(metadata);
          refreshItems();
        });
        input.click();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Upload image</div>
      </div>
      <div className={styles.item} onClick={async e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'generateImage',
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/images/images.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Generate image</div>
      </div>
    </>
  );
};
export const GenerateImageContent = ({
  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [prompt, setPrompt] = useState('');

  //

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  return (
    <>
      <label className={styles.label}>
        <div className={styles.text}>Prompt</div>
        <input type='text' className={styles.input} value={prompt} onChange={e => {
          setPrompt(e.target.value);
        }} placeholder='Enter a prompt' />
      </label>

      <div>
        <button className={styles.button} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          const imageBlob = await generateImage({
            prompt,
          });

          const metadata = await importFileType('image')(imageBlob, supabaseClient, sessionUserId);
          await addAsset(metadata);
          refreshItems();
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Generate</span>
        </button>
      </div>
    </>
  );
};

//

export const CreateAudioContent = ({
  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  return (
    <>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const input = document.createElement('input');
        input.type = 'file';
        const mimeTypes = [
          'mp3',
          'ogg',
          'wav',
        ];
        input.accept = mimeTypes.map(mimeType => '.' + mimeType).join(',');
        input.addEventListener('change', async e => {
          const f = e.target.files[0];

          const metadata = await importFileType('audio')(f, supabaseClient, sessionUserId);
          await addAsset(metadata);
          refreshItems();
        });
        input.click();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Upload audio</div>
      </div>
      <div className={styles.item} onClick={async e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'importAudio',
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/images/images.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Import from Youtube</div>
      </div>
    </>
  );
};
export const ImportAudioContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [url, setUrl] = useState('');

  //

  return (
    <>
      <label className={styles.label}>
        <div className={styles.text}>Url</div>
        <input type='text' className={styles.input} value={url} onChange={e => {
          setUrl(e.target.value);
        }} placeholder='https://www.youtube.com/watch?v=q-74HTjRbuY' />
      </label>

      <div>
        <button className={styles.button} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          const audioBlob = await importYoutubeAudio(url);

          const metadata = await importFileType('audio')(audioBlob, supabaseClient, sessionUserId);
          await addAsset(metadata);
          // refreshItems();

          const newContentPath = contentPath.slice(0, contentPath.length - 2);
          setContentPath(newContentPath);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Import</span>
        </button>
      </div>
    </>
  );
};

//

export const CreateVideoContent = ({
  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  return (
    <>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const input = document.createElement('input');
        input.type = 'file';
        const mimeTypes = [
          'mp4',
          'webm',
        ];
        input.accept = mimeTypes.map(mimeType => '.' + mimeType).join(',');
        input.addEventListener('change', async e => {
          const f = e.target.files[0];

          const metadata = await importFileType('video')(f, supabaseClient, sessionUserId);
          await addAsset(metadata);
          refreshItems();
        });
        input.click();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Upload video</div>
      </div>
      <div className={styles.item} onClick={async e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'importVideo',
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/images/images.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Import from Youtube</div>
      </div>
    </>
  );
};
export const ImportVideoContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [url, setUrl] = useState('');

  //

  return (
    <>
      <label className={styles.label}>
        <div className={styles.text}>Url</div>
        <input type='text' className={styles.input} value={url} onChange={e => {
          setUrl(e.target.value);
        }} placeholder='https://www.youtube.com/watch?v=q-74HTjRbuY' />
      </label>

      <div>
        <button className={styles.button} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          const videoBlob = await importYoutubeVideo(url);

          const metadata = await importFileType('video')(videoBlob, supabaseClient, sessionUserId);
          await addAsset(metadata);
          // refreshItems();

          const newContentPath = contentPath.slice(0, contentPath.length - 2);
          setContentPath(newContentPath);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Import</span>
        </button>
      </div>
    </>
  );
};

//

export const CreateVrmContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  return (
    <>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const input = document.createElement('input');
        input.type = 'file';
        const mimeTypes = [
          'vrm',
        ];
        input.accept = mimeTypes.map(mimeType => '.' + mimeType).join(',');
        input.addEventListener('change', async e => {
          const f = e.target.files[0];

          const metadata = await importFileType('vrm')(f, supabaseClient, sessionUserId);
          await addAsset(metadata);
          refreshItems();
        });
        input.click();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Upload vrm</div>
      </div>
      <div className={styles.item} onClick={async e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'importVrm',
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/ui/assets/icons/posture.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Import from VRoid Hub</div>
      </div>
    </>
  );
};
export const ImportVrmContent = ({
  supabaseClient,
  sessionUserId,
  user,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  //

  const vroidHubLinked = !!user?.oauth?.vroidHub?.access_token;

  //

  const _authorizeVroidhub = async () => {
    const redirect_url = location.protocol + '//' + location.host + '/';
    const state = `vroidhub ${location.href} ${redirect_url}`;
    const u = new URL(`https://hub.vroid.com/oauth/authorize`);
    u.searchParams.set('client_id', VROID_HUB_CLIENT_ID);
    u.searchParams.set('redirect_uri', redirect_url);
    u.searchParams.set('response_type', 'code');
    u.searchParams.set('scope', 'default');
    u.searchParams.set('state', state);
    location.href = u;
  };
  const _unlinkVroidHub = async () => {
    const result = await supabaseClient.supabase
      .from('accounts')
      .update({
        oauth: null,
      })
      .eq('id', sessionUserId);

    supabaseClient.refresh();
  };

  //

  useEffect(() => {
    if (vroidHubLinked) {
      const access_token = user.oauth.vroidHub.access_token;

      const abortController = new AbortController();
      // const {signal} = abortController;

      loadQueue.waitForTurn(async () => {
        setLoading(true);

        try {
          /* const loadCharacterImages = (data, y) => {
            const imageSrcs = data.map(characterModel => {
              const {full_body_image} = characterModel;
              const {w300} = full_body_image;
              return w300.url;
            });
            for (let i = 0; i < imageSrcs.length; i++) {
              let imageSrc = imageSrcs[i];
              imageSrc = `https://cors-proxy.upstreet.ai/${imageSrc}`;
              const img = new Image();
              img.crossOrigin = 'Anonymous';
              img.src = imageSrc;
              img.style.cssText = `\
                position: fixed;
                top: ${y * 133}px;
                left: ${i * 100}px;
                width: 100px;
                z-index: 1;
              `;
              document.body.appendChild(img);
            }
          }; */
          let heartCharacterModels = await (async () => {
            const res = await fetch('https://ai-proxy.upstreet.ai/api/vroidHub/api/hearts', {
              headers: {
                'X-Api-Version': 11,
                'Authorization': `Bearer ${access_token}`,
              },
            });
            if (res.ok) {
              const j = await res.json();
              return j;
            } else {
              return null;
            }
          })();
          if (heartCharacterModels) {
            heartCharacterModels = heartCharacterModels.data;
            // console.log('hearts', heartCharacterModels);
            // loadCharacterImages(heartCharacterModels, 0);

            let staffPickCharacterModels = await (async () => {
              const res = await fetch('https://ai-proxy.upstreet.ai/api/vroidHub/api/staff_picks', {
                headers: {
                  'X-Api-Version': 11,
                  'Authorization': `Bearer ${access_token}`,
                },
              });
              const j = await res.json();
              return j;
            })();
            staffPickCharacterModels = staffPickCharacterModels.data.map(m => m.character_model);
            // console.log('staff picks', staffPickCharacterModels);
            // loadCharacterImages(staffPickCharacterModels, 1);

            const newItems = [
              ...heartCharacterModels,
              ...staffPickCharacterModels,
            ];
            setItems(newItems);
          } else {
            await supabaseClient.updateUser(oldUser => {
              return {
                ...oldUser,
                oauth: null,
              };
            });
          }
        } catch (err) {
          throw err;
        } finally {
          setLoading(false);
        }
      });

      return () => {
        abortController.abort();
      };
    }
  }, [
    epoch,
  ]);

  //

  return (
    <>
      {!vroidHubLinked ? <div>
        <button className={styles.button} onClick={async e => {
          e.preventDefault();
          e.stopPropagation();

          _authorizeVroidhub();
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Link account</span>
        </button>
      </div>
        :
        <>
          {loading && <div className={styles.placeholder}>
            Loading...
          </div>}

          {!loading && <div>
            {/* <div>Logged in to VRoid Hub.</div> */}
            <button className={styles.button} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              _unlinkVroidHub();
            }}>
              <div className={styles.background} />
              <span className={styles.text}>Unlink account</span>
            </button>

            {/* <div>List</div> */}
            {items.map(item => {
              const characterModel = item;

              const { full_body_image } = characterModel;
              const { w300 } = full_body_image;
              const previewImageUrl = w300.url;

              const { character } = characterModel;
              const { name } = character;

              return (
                <div className={styles.item} onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();

                  // onClick && onClick(item);
                }} key={item.id}>
                  <img src={previewImageUrl} className={styles.previewImg} draggable={false} />
                  <div className={styles.name}>{name}</div>

                  <ImportVrmButton
                    supabaseClient={supabaseClient}
                    sessionUserId={sessionUserId}
                    user={user}

                    contentPath={contentPath}
                    setContentPath={setContentPath}

                    item={item}
                  />
                </div>
              );
            })}
          </div>}
        </>}
    </>
  );
};

//

export const CreateNpcContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  const [name, setName] = useState(item?.name || '');
  const [description, setDescription] = useState(item?.description || '');
  const [personality, setPersonality] = useState(item?.personality || '');
  const [scenario, setScenario] = useState(item?.scenario || '');
  const [firstMessage, setFirstMessage] = useState(item?.firstMessage || '');
  const [messageExample, setMessageExample] = useState(item?.messageExample || '');
  const [avatar, setAvatar] = useState(item?.avatar || null);

  const firstVoiceCategory = Object.keys(voiceModels)[0];
  const [voiceEndpoint, setVoiceEndpoint] = useState(item?.voiceEndpoint ?? `${firstVoiceCategory}:${voiceModels[firstVoiceCategory][0].name}`);

  const firstVoicePack = voicePacks[0];
  const [voicePack, setVoicePack] = useState(item?.voicePack ?? firstVoicePack.name);

  const creatable = !!name && !!avatar;

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name || '';
    if (_name !== name) {
      setName(_name);
    }
    const _description = item?.description || '';
    if (_description !== description) {
      setDescription(_description);
    }
    const _personality = item?.personality || '';
    if (_personality !== personality) {
      setPersonality(_personality);
    }
    const _scenario = item?.scenario || '';
    if (_scenario !== scenario) {
      setScenario(_scenario);
    }
    const _firstMessage = item?.firstMessage || '';
    if (_firstMessage !== firstMessage) {
      setFirstMessage(_firstMessage);
    }
    const _messageExample = item?.messageExample || '';
    if (_messageExample !== messageExample) {
      setMessageExample(_messageExample);
    }
    const _avatar = item?.avatar || null;
    if (_avatar !== avatar) {
      setAvatar(_avatar);
    }
    const _voiceEndpoint = item?.voiceEndpoint ?? `${firstVoiceCategory}:${voiceModels[firstVoiceCategory][0].name}:${voiceModels[firstVoiceCategory][0].voiceId}`;
    if (_voiceEndpoint !== voiceEndpoint) {
      setVoiceEndpoint(_voiceEndpoint);
    }
    const _voicePack = item?.voicePack ?? firstVoicePack.name;
    if (_voicePack !== voicePack) {
      setVoicePack(_voicePack);
    }
  }, [
    item,
    name,
    description,
    personality,
    scenario,
    firstMessage,
    messageExample,
    avatar,
    voiceEndpoint,
    voicePack,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemObjectChange = (item) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item,
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  const _save = async () => {
    const avatarUrl = avatar.start_url;
    const npc = {
      name,
      description,
      personality,
      scenario,
      firstMessage,
      messageExample,
      avatarUrl,
      voiceEndpoint,
      voicePack,
    };

    //

    const supabaseFsWorker = new SupabaseFsWorker({
      supabase: supabaseClient.supabase,
      bucketName: 'public',
    });

    //

    const uuid = crypto.randomUUID();
    const keyPath = ['assets', `${uuid}.npc`];
    const s = JSON.stringify(npc, null, 2);
    const start_url = await supabaseFsWorker.writeFile(keyPath, s);

    const id = item?.id ?? crypto.randomUUID();
    const npcAsset = {
      id,
      name,
      type: 'npc',
      start_url,
      preview_url: avatar.preview_url,
      user_id: sessionUserId,
    };
    const result = await supabaseClient.supabase
      .from('assets')
      .upsert(npcAsset);

    // setEpoch(epoch + 1);
    const newContentPath = contentPath.slice(0, contentPath.length - 1);
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createUi}>
      <div className={classnames(
        styles.row,
        styles.middle,
      )}>
        <button className={styles.button} onClick={async e => {
          const avatarName = avatar?.name || '';

          const oldCharacter = {
            avatarName,
            name,
            description,
            personality,
            scenario,
            firstMessage,
            messageExample,
          };
          let newCharacter = await characterCardGenerator(oldCharacter);
          newCharacter = {
            ...item,
            ...newCharacter,
          };
          setItemObjectChange(newCharacter);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Autofill</span>
        </button>

        {editing && <button className={styles.button} onClick={async e => {
          const newContentPath = [
            ...contentPath,
            {
              type: 'npcMemories',
              item: {
                ...item,
              },
            },
          ];
          setContentPath(newContentPath);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Memories</span>
        </button>}
      </div>

      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Avatar</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType={['vrm', 'character360']}

          value={AssetPicker.getValue({
            item,
            itemKey: 'avatar',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'avatar',
            contentPath,
            setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Voice</div>
        <VoicePicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          value={voiceEndpoint}
          onChange={newVoiceEndpoint => {
            setItemKeyValueChange('voiceEndpoint', newVoiceEndpoint);
          }}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Grunt</div>
        <GruntPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          value={voicePack}
          onChange={newVoicePack => {
            setItemKeyValueChange('voicePack', newVoicePack);
          }}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Description</div>
        <textarea className={styles.textarea} value={description} onChange={e => {
          setItemKeyValueChange('description', e.target.value);
          setDescription(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Personality</div>
        <textarea className={styles.textarea} value={personality} onChange={e => {
          // setPersonality(e.target.value);
          setItemKeyValueChange('personality', e.target.value);
          setPersonality(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Scenario</div>
        <textarea className={styles.textarea} value={scenario} onChange={e => {
          // setScenario(e.target.value);
          setItemKeyValueChange('scenario', e.target.value);
          setScenario(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>First message</div>
        <textarea className={styles.textarea} value={firstMessage} onChange={e => {
          // setFirstMessage(e.target.value);
          setItemKeyValueChange('firstMessage', e.target.value);
          setFirstMessage(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Message example</div>
        <textarea className={styles.textarea} value={messageExample} onChange={e => {
          // setMessageExample(e.target.value);
          setItemKeyValueChange('messageExample', e.target.value);
          setMessageExample(e.target.value);
        }}></textarea>
      </label>

      {/* create button */}
      {!editing && <button className={classnames(
        styles.button,
        creatable ? null : styles.disabled,
      )} onClick={async e => {
        await _save();
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>}

      {/* update button */}
      {editing && <button className={styles.button} onClick={async e => {
        await _save();
      }}>
        <div className={styles.background} />
        <div className={styles.text}>Update</div>
      </button>}
    </div>
  );
};
export const VoicePicker = ({
  contentPath,
  setContentPath,

  value,
  onChange,
}) => {
  const chooseVoice = () => {
    const newContentPath = [
      ...contentPath,
      {
        type: 'chooseVoice',
        value,
        onChange,
      },
    ];
    setContentPath(newContentPath);
  };

  //

  const prettyName = (() => {
    const match = value.match(/^(.+?):(.+?):(.+?)$/);
    if (match) {
      const [_, model, name, voiceId] = match;
      return [
        model,
        name,
      ].join(':');
    } else
      return value;
  })();

  //

  return (
    !value ? <>
      <div className={styles.item} onClick={e => {
        chooseVoice();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Choose voice</div>
      </div>
    </> : <>
      <div className={styles.item} onClick={e => {
        chooseVoice();
      }}>
        <img src='/images/voice.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>{prettyName}</div>
      </div>
    </>
  );
};
export const GruntPicker = ({
  contentPath,
  setContentPath,

  value,
  onChange,
}) => {
  const chooseVoicePack = () => {
    const newContentPath = [
      ...contentPath,
      {
        type: 'chooseVoicePack',
        value,
        onChange,
      },
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    !value ? <>
      <div className={styles.item} onClick={e => {
        chooseVoicePack();
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>Choose grunt</div>
      </div>
    </> : <>
      <div className={styles.item} onClick={e => {
        chooseVoicePack();
      }}>
        <img src='/images/silent.svg' className={classnames(
          styles.chooseImg,
          styles.placeholderImg,
        )} />
        <div className={styles.name}>{value}</div>
      </div>
    </>
  );
};
export const ChooseVoiceContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    value,
    onChange,
  } = contentPathElement;

  return (
    <>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'chooseVoicePreset',
            value,
            onChange,
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/images/voice2.svg' className={classnames(
          styles.previewImg,
          styles.placeholderImg,
        )} draggable={false} />
        <div className={styles.name}>Voice Preset</div>
      </div>

      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        const newContentPath = [
          ...contentPath,
          {
            type: 'chooseVoiceCustom',
            value,
            onChange,
          },
        ];
        setContentPath(newContentPath);
      }}>
        <img src='/images/voice.svg' className={classnames(
          styles.previewImg,
          styles.placeholderImg,
        )} draggable={false} />
        <div className={styles.name}>Custom Voice</div>
      </div>
    </>
  );
};
export const ChooseVoicePresetContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  engine,

  // debug,
}) => {
  const [loading, setLoading] = useState(false);

  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    value,
    onChange,
  } = contentPathElement;

  const items = [
    ['tiktalknet', voiceModels.tiktalknet],
    ['elevenlabs', voiceModels.elevenlabs],
  ].flatMap(([model, voiceEndpoints]) => {
    return voiceEndpoints.map(voiceEndpoint => {
      const {
        name,
        voiceId,
      } = voiceEndpoint;
      return {
        model,
        name,
        voiceId,
      };
    });
  });

  //

  return (
    <>
      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}

      {!loading && items.map(item => {
        const voiceEndpoint = [
          item.model,
          item.name,
          item.voiceId,
        ].join(':');
        const voiceName = [
          item.model,
          item.name,
        ].join(':');

        return (
          <div className={styles.item} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            onChange(voiceEndpoint);
          }} key={voiceEndpoint}>
            <img src='/images/voice2.svg' className={classnames(
              styles.previewImg,
              styles.invert,
            )} draggable={false} />
            <div className={styles.name}>{voiceName}</div>

            <TestVoiceButton
              item={item}
              engine={engine}
            />

            {/* <nav className={styles.iconBtn} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              // remove item
              const result = await supabaseClient.supabase
                .from('assets')
                .delete()
                .eq('id', item.id);

              refreshItems();
            }}>
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav> */}
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>
        No assets
      </div>}
    </>
  );
};
export const ChooseVoiceCustomContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  engine,

  debug,
}) => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    value,
    onChange,
  } = contentPathElement;

  //

  useEffect(() => {
    const abortController = new AbortController();
    const { signal } = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadItemsType('voice')({
          signal,
          supabaseClient,
          sessionUserId,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } catch (err) {
        throw err;
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, [
    epoch,
  ]);

  //

  return (
    <>
      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}

      {!loading && items.map(item => {
        const {
          start_url,
        } = item;

        return (
          <div className={styles.item} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            const res = await fetch(start_url);
            const j = await res.json();
            const {
              voiceEndpoint,
            } = j;

            onChange(voiceEndpoint);
          }} key={start_url}>
            <img src={item.preview_url} className={styles.previewImg} draggable={false} />
            <div className={styles.name}>{item.name}</div>

            <TestVoiceButton
              item={item}
              engine={engine}
            />
            {/* <AssetButtons
              supabaseClient={supabaseClient}

              item={item}

              contentPath={contentPath}
              setContentPath={setContentPath}

              assetType={'voice'}

              // onActivate={onActivate}
              // onSell={onSell}

              engine={engine}

              debug={debug}
            /> */}

            {/* <nav className={styles.iconBtn} onClick={async e => {
              e.preventDefault();
              e.stopPropagation();

              // remove item
              const result = await supabaseClient.supabase
                .from('assets')
                .delete()
                .eq('id', item.id);

              refreshItems();
            }}>
              <img className={styles.img} src='/assets/x.svg' draggable={false} />
            </nav> */}
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>
        No assets
      </div>}
    </>
  );
};
export const ChooseVoicePackContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,

  engine,

  // debug,
}) => {
  const [loading, setLoading] = useState(false);

  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    value,
    onChange,
  } = contentPathElement;

  //

  const items = [
    {
      name: 'None',
      indexPath: null,
      audioPath: null,
    },
    ...voicePacks,
  ];

  //

  return (
    <>
      {loading && <div className={styles.placeholder}>
        Loading...
      </div>}

      {!loading && items.map(item => {
        const isNone = /none/i.test(item.name);

        return (
          <div className={styles.item} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            const value = isNone ? '' : item.name;
            onChange(value);
          }} key={item.name}>
            <img src='/images/silent.svg' className={classnames(
              styles.previewImg,
              styles.invert,
            )} draggable={false} />
            <div className={styles.name}>{item.name}</div>

            {!isNone && <TestGruntButton
              item={item}
              engine={engine}
            />}
          </div>
        );
      })}

      {!loading && items.length === 0 && <div className={styles.placeholder}>
        No assets
      </div>}
    </>
  );
};
export const EditProfileCharacterContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  accountManager,

  contentPath,
  setContentPath,

  // epoch,
  // setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
    editing = false,
  } = contentPathElement;

  const [name, setName] = useState(item?.name || '');
  const [description, setDescription] = useState(item?.description || '');
  const [personality, setPersonality] = useState(item?.personality || '');
  const [scenario, setScenario] = useState(item?.scenario || '');
  const [firstMessage, setFirstMessage] = useState(item?.firstMessage || '');
  const [messageExample, setMessageExample] = useState(item?.messageExample || '');
  const [avatar, setAvatar] = useState(item?.avatar || null);

  const firstVoiceCategory = Object.keys(voiceModels)[0];
  const [voiceEndpoint, setVoiceEndpoint] = useState(item?.voiceEndpoint ?? `${firstVoiceCategory}:${voiceModels[firstVoiceCategory][0].name}:${voiceModels[firstVoiceCategory][0].voiceId}`);

  const firstVoicePack = voicePacks[0];
  const [voicePack, setVoicePack] = useState(item?.voicePack ?? firstVoicePack.name);

  const creatable = !!name && !!avatar;

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name || '';
    if (_name !== name) {
      setName(_name);
    }
    const _description = item?.description || '';
    if (_description !== description) {
      setDescription(_description);
    }
    const _personality = item?.personality || '';
    if (_personality !== personality) {
      setPersonality(_personality);
    }
    const _scenario = item?.scenario || '';
    if (_scenario !== scenario) {
      setScenario(_scenario);
    }
    const _firstMessage = item?.firstMessage || '';
    if (_firstMessage !== firstMessage) {
      setFirstMessage(_firstMessage);
    }
    const _messageExample = item?.messageExample || '';
    if (_messageExample !== messageExample) {
      setMessageExample(_messageExample);
    }
    const _avatar = item?.avatar || null;
    if (_avatar?.start_url !== avatar?.start_url) {
      setAvatar(_avatar);
    }
    const _voiceEndpoint = item?.voiceEndpoint ?? `${firstVoiceCategory}:${voiceModels[firstVoiceCategory][0].name}`;
    if (_voiceEndpoint !== voiceEndpoint) {
      setVoiceEndpoint(_voiceEndpoint);
    }
    const _voicePack = item?.voicePack ?? firstVoicePack.name;
    if (_voicePack !== voicePack) {
      setVoicePack(_voicePack);
    }
  }, [
    item,
    description,
    personality,
    scenario,
    firstMessage,
    messageExample,
    avatar,
    voiceEndpoint,
    voicePack,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };
  const setItemObjectChange = (item) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item,
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createUi}>
      <div className={classnames(
        styles.row,
        styles.middle,
      )}>
        <button className={styles.button} onClick={async e => {
          const avatarName = avatar?.name || '';

          const oldCharacter = {
            avatarName,
            name,
            description,
            personality,
            scenario,
            firstMessage,
            messageExample,
          };
          let newCharacter = await characterCardGenerator(oldCharacter);
          newCharacter = {
            ...item,
            ...newCharacter,
          };
          setItemObjectChange(newCharacter);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Autofill</span>
        </button>

        <button className={styles.button} onClick={async e => {
          const newContentPath = [
            ...contentPath,
            {
              type: 'chooseAsset',
              assetType: 'npc',
              value: item,
              onChange: async ({ itemValue }) => {
                const {
                  name,
                  start_url,
                } = itemValue;

                const res = await fetch(start_url);
                const npc = await res.json();

                const newContentPath = [
                  ...contentPath.slice(0, -1),
                  {
                    ...contentPath[contentPath.length - 1],
                    item: {
                      ...npc,
                      avatar: {
                        name: npc.name,
                        start_url: npc.avatarUrl,
                      },
                    },
                  },
                ];
                setContentPath(newContentPath);
              },
            },
          ];
          setContentPath(newContentPath);
        }}>
          <div className={styles.background} />
          <div className={styles.text}>Load NPC</div>
        </button>

        <button className={styles.button} onClick={async e => {
          const newContentPath = [
            ...contentPath,
            {
              type: 'npcMemories',
              item: {
                ...item,
              },
            },
          ];
          setContentPath(newContentPath);
        }}>
          <div className={styles.background} />
          <span className={styles.text}>Memories</span>
        </button>
      </div>

      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
          setName(e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Avatar</div>
        <AssetPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          assetType={['vrm', 'character360']}
          value={AssetPicker.getValue({
            item,
            itemKey: 'avatar',
          })}
          onChange={AssetPicker.setValue({
            itemKey: 'avatar',
            contentPath,
            setContentPath,
          })}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Voice</div>
        <VoicePicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          value={voiceEndpoint}
          onChange={newVoiceEndpoint => {
            setItemKeyValueChange('voiceEndpoint', newVoiceEndpoint);
          }}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Grunt</div>
        <GruntPicker
          contentPath={contentPath}
          setContentPath={setContentPath}

          value={voicePack}
          onChange={newVoicePack => {
            setItemKeyValueChange('voicePack', newVoicePack);
          }}
        />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Description</div>
        <textarea className={styles.textarea} value={description} onChange={e => {
          setItemKeyValueChange('description', e.target.value);
          setDescription(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Personality</div>
        <textarea className={styles.textarea} value={personality} onChange={e => {
          setItemKeyValueChange('personality', e.target.value);
          setPersonality(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Scenario</div>
        <textarea className={styles.textarea} value={scenario} onChange={e => {
          setItemKeyValueChange('scenario', e.target.value);
          setScenario(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>First message</div>
        <textarea className={styles.textarea} value={firstMessage} onChange={e => {
          setItemKeyValueChange('firstMessage', e.target.value);
          setFirstMessage(e.target.value);
        }}></textarea>
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Message example</div>
        <textarea className={styles.textarea} value={messageExample} onChange={e => {
          setItemKeyValueChange('messageExample', e.target.value);
          setMessageExample(e.target.value);
        }}></textarea>
      </label>

      <button className={classnames(
        styles.button,
        creatable ? null : styles.disabled,
      )} onClick={async e => {
        const {
          avatar,
          ...rest
        } = item;
        const item2 = rest;
        item2.avatarUrl = avatar.start_url;

        // accountManager.setPlayerSpec(item2);
        await supabaseClient.updateUser(oldUser => {
          return {
            ...oldUser,
            playerSpec: item2,
          };
        });

        const newContentPath = contentPath.slice(0, contentPath.length - 1);
        setContentPath(newContentPath);
      }} disabled={!creatable}>
        <div className={styles.background} />
        <div className={styles.text}>Save</div>
      </button>
    </div>
  );
};
export const CreateWorldContent = ({
  // localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  const [name, setName] = useState(item?.name || '');
  const [description, setDescription] = useState(item?.description || '');

  //

  // bind change setters
  useEffect(() => {
    const _name = item?.name || '';
    if (_name !== name) {
      setName(_name);
    }
    const _description = item?.description || '';
    if (_description !== description) {
      setDescription(_description);
    }
  }, [
    item,
  ]);

  // bind change setters
  const setItemKeyValueChange = (key, value) => {
    const contentElementIndex = contentPath.indexOf(contentPathElement);
    const oldContentElement = contentPathElement;
    const newContentElement = {
      ...oldContentElement,
      item: {
        ...item,
        [key]: value,
      },
    };
    const newContentPath = [
      ...contentPath.slice(0, contentElementIndex),
      newContentElement,
      ...contentPath.slice(contentElementIndex + 1),
    ];
    setContentPath(newContentPath);
  };

  //

  return (
    <div className={styles.createUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} value={name} onChange={e => {
          setItemKeyValueChange('name', e.target.value);
        }} placeholder='Enter a name' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Description</div>
        <textarea className={styles.textarea} value={description} onChange={e => {
          setItemKeyValueChange('description', e.target.value);
        }}></textarea>
      </label>

      <button className={classnames(
        styles.button,
        !name ? styles.disabled : null,
      )} onClick={async e => {
        if (
          name /* &&
          description */
        ) {
          const id = crypto.randomUUID();
          const objects = [];
          const worldAsset = {
            id,
            name,
            description,
            // type: 'world',
            // start_url,
            objects,
            preview_url: '',
            user_id: sessionUserId,
          };
          const result = await supabaseClient.supabase
            .from('worlds')
            .upsert(worldAsset);

          setEpoch(epoch + 1);

          const newContentPath = contentPath.slice(0, -1);
          setContentPath(newContentPath);
        }
      }} disabled={!name}>
        <div className={styles.background} />
        <div className={styles.text}>Create</div>
      </button>
    </div>
  );
};
/* export const ChooseAvatarContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,
  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  wearItem,

  debug,
}) => {
  return (
    <VrmAssetContent
      localStorageManager={localStorageManager}
      supabaseClient={supabaseClient}
      sessionUserId={sessionUserId}
      contentPath={contentPath}
      setContentPath={setContentPath}
      wearItem={wearItem}

      epoch={epoch}
      setEpoch={setEpoch}

      onClick={avatar => {
        const avatarContentPath = contentPath[contentPath.length - 2];
        const newContentPath = [
          ...contentPath.slice(0, -2),
          {
            ...avatarContentPath,
            item: {
              ...avatarContentPath.item,
              avatar,
            },
          },
        ];
        setContentPath(newContentPath);
      }}

      debug={debug}
    />
  );
};
export const ChooseGlbContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,
  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  wearItem,

  debug,
}) => {
  return (
    <GlbAssetContent
      localStorageManager={localStorageManager}
      supabaseClient={supabaseClient}
      sessionUserId={sessionUserId}
      contentPath={contentPath}
      setContentPath={setContentPath}
      wearItem={wearItem}

      epoch={epoch}
      setEpoch={setEpoch}

      onClick={glb => {
        const avatarContentPath = contentPath[contentPath.length - 2];
        const newContentPath = [
          ...contentPath.slice(0, -2),
          {
            ...avatarContentPath,
            item: {
              ...avatarContentPath.item,
              glb,
            },
          },
        ];
        setContentPath(newContentPath);
      }}

      debug={debug}
    />
  );
}; */
export const ChooseAssetContent = ({
  localStorageManager,

  supabaseClient,
  sessionUserId,
  user,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    assetType,
    onChange,
  } = contentPathElement;

  return (
    <AssetListContent
      localStorageManager={localStorageManager}

      supabaseClient={supabaseClient}
      sessionUserId={sessionUserId}
      user={user}

      contentPath={contentPath}
      setContentPath={setContentPath}

      assetType={assetType}

      epoch={epoch}
      setEpoch={setEpoch}

      onClick={(itemValue) => {
        onChange({
          itemValue,
          contentPath,
          setContentPath,
        });
      }}

      debug={debug}
    />
  );
};
export const EditNpcWearablesContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  // wearItem,

  debug,
}) => {
  // const contentPathElement = contentPath[contentPath.length - 1];
  // const {
  //   items = [],
  // } = contentPathElement;

  // console.log('contentPath', contentPath);

  //

  return (
    <div className={styles.wearablesUi}>
      <AssetListContent
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        contentPath={contentPath}
        setContentPath={setContentPath}

        epoch={epoch}
        setEpoch={setEpoch}

        onClick={item => {
          const assetContentPath = contentPath[contentPath.length - 2];
          const newContentPath = [
            ...contentPath.slice(0, -2),
            {
              ...assetContentPath,
              item: {
                ...assetContentPath.item,
                wearables: {
                  ...assetContentPath.item.wearables,
                  item,
                },
              },
            },
          ];
          setContentPath(newContentPath);
        }}

        debug={debug}
      />
    </div>
  );
};
export const NpcMemoriesContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  engine,

  // epoch,
  // setEpoch,

  // debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;
  const name = item?.name ?? 'Unnamed';

  //

  const [memories, setMemories] = useState([]);
  const [epoch, setEpoch] = useState(0);
  const [query, setQuery] = useState('');
  const [loading, setLoading] = useState(false);

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  useEffect(() => {
    setLoading(true);

    const abortController = new AbortController();

    if (query) {
      (async () => {
        try {
          const {
            signal,
          } = abortController;

          const newMemories = await engine.characterMemoriesManager.searchMemories(query, {
            name,
            signal,
          });
          setMemories(newMemories);
          setLoading(false);
        } catch (err) {
          console.warn('err', err);
        }
      })();
    } else {
      (async () => {
        try {
          const newMemories = await engine.characterMemoriesManager.getMemoriesByName(name);
          setMemories(newMemories);
          setLoading(false);
        } catch (err) {
          console.warn('err', err);
        }
      })();
    }

    return () => {
      // setLoading(false);
      abortController.abort(abortError);
    };
  }, [
    query,
    epoch,
  ]);

  //

  return (
    <div className={styles.memoriesUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} disabled value={name} />
      </label>

      <div className={styles.label}>
        <div className={styles.text}>Insert</div>
        <div className={styles.buttons}>
          <button className={styles.button} onClick={e => {
            const newContentPath = [
              ...contentPath,
              {
                type: 'editNpcMemory',
                item: {
                  ...item,
                },
              },
            ];
            setContentPath(newContentPath);
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Add Memory</span>
          </button>

          <button className={styles.button} onClick={e => {
            e.preventDefault();
            e.stopPropagation();

            const input = document.createElement('input');
            input.type = 'file';
            const mimeTypes = [
              'txt',
              'md',
            ];
            input.accept = mimeTypes.map(mimeType => '.' + mimeType).join(',');
            input.multiple = true;
            input.addEventListener('change', async e => {
              const files = Array.from(e.target.files);

              console.log('import files knowledge', {
                files,
              });
            });
            input.click();
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Import data</span>
          </button>
        </div>
      </div>

      <label className={styles.label}>
        <div className={styles.text}>Search</div>
        <input type='text' className={styles.input} value={query} onChange={e => {
          setQuery(e.target.value);
        }} />
      </label>

      <label className={styles.label}>
        {loading ? <div className={styles.text}>Loading...</div> : <>
          <div className={styles.text}>Memories</div>
          {memories.length > 0 ? <div className={styles.memories}>
            {memories.map((memory, i) => {
              return (
                <div className={styles.memory} key={i}>
                  <div className={styles.text}>
                    {memory.message}
                  </div>

                  <nav className={styles.btn} onClick={async e => {
                    e.preventDefault();
                    e.stopPropagation();

                    const {
                      id,
                      name,
                      command,
                      args,
                      message,
                    } = memory;

                    const newContentPath = [
                      ...contentPath,
                      {
                        type: 'editNpcMemory',
                        item: {
                          id,
                          name,
                          command,
                          args,
                          message,
                        },
                      },
                    ];
                    setContentPath(newContentPath);
                  }}>
                    <span>Edit</span>
                    <img className={styles.chevron} src='/images/chevron.png' draggable={false} />
                  </nav>

                  {(memory.user_id === sessionUserId) && <nav className={styles.iconBtn} onClick={async e => {
                    e.preventDefault();
                    e.stopPropagation();

                    await engine.characterMemoriesManager.removeMemory(memory.id);

                    refreshItems();
                  }}>
                    <img className={styles.img} src='/assets/x.svg' draggable={false} />
                  </nav>}
                </div>
              );
            })}
          </div> : <div className={styles.placeholder}>
            Empty
          </div>}
        </>}
      </label>
    </div>
  );
};
export const EditNpcMemoryContent = ({
  localStorageManager,
  supabaseClient,
  sessionUserId,

  engine,

  contentPath,
  setContentPath,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;
  const name = item?.name ?? 'Unnamed';

  // console.log('load memory', item);

  //

  const [id, setId] = useState(() => item?.id ?? crypto.randomUUID());
  const [command, setCommand] = useState(() => item?.command ?? '');
  const [args, setArgs] = useState(() => item?.args ? item?.args.join(',') : []);
  const [message, setMessage] = useState(() => item?.message ?? '');
  const [adding, setAdding] = useState(false);

  const hadItem = !!item?.id;
  const disabled = !message || adding;

  //

  return (
    <div className={styles.memoriesUi}>
      <label className={styles.label}>
        <div className={styles.text}>Name</div>
        <input type='text' className={styles.input} disabled value={name} />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Command</div>
        <input type='text' className={styles.input} value={command} onChange={e => {
          setCommand(e.target.value);
        }} placeholder='SAY' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Args</div>
        <input type='text' className={styles.input} value={args} onChange={e => {
          setArgs(e.target.value);
        }} placeholder='[]' />
      </label>

      <label className={styles.label}>
        <div className={styles.text}>Message</div>
        <textarea className={styles.textarea} value={message} onChange={e => {
          setMessage(e.target.value);
        }} />
      </label>

      <button className={classnames(
        styles.button,
        disabled && styles.disabled,
      )} disabled={disabled} onClick={async e => {
        e.preventDefault();
        e.stopPropagation();

        setAdding(true);

        try {
          const memory = {
            id,
            user_id: sessionUserId,
            name,
            command,
            args: args.split(',').map(arg => arg.trim()),
            message,
            embedding: null,
          };
          await engine.characterMemoriesManager.upsertMemory(memory);

          const newContentPath = contentPath.slice(0, -1);
          setContentPath(newContentPath);
        } finally {
          setAdding(false);
        }
      }}>
        <div className={styles.background} />

        {!hadItem ?
          <span className={styles.text}>Add Memory</span>
          :
          <span className={styles.text}>Update Memory</span>
        }
      </button>
    </div>
  );
};

export const EditContent = ({
  engine,

  contentPath,
  setContentPath,

  supabaseClient,
  sessionUserId,

  // changed,
  // setChanged,

  sceneTracker,

  appManagerName,
}) => {
  // get the item from the content path element
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item = null,
  } = contentPathElement;

  //

  const [name, setName] = useState(() => sceneTracker.getName());
  const [description, setDescription] = useState(() => sceneTracker.getDescription());
  const [previewImg, setPreviewImg] = useState(() => item?.previewImg ?? sceneTracker.getPreviewImg());
  const [apps, setApps] = useState(() => sceneTracker.getApps());

  // console.log('load preview', previewImg);

  // bind item
  useEffect(() => {
    const previewImg = item?.previewImg;
    if (previewImg) {
      console.log('item set preview image', previewImg);
      setPreviewImg(previewImg);
    }
  }, [item]);

  // bind scene tracker
  useEffect(() => {
    const nameupdate = e => {
      setName(e.data.name);
    };
    sceneTracker.addEventListener('nameupdate', nameupdate);
    const descriptionupdate = e => {
      setDescription(e.data.description);
    };
    sceneTracker.addEventListener('descriptionupdate', descriptionupdate);
    const previewimgupdate = e => {
      console.log('update preview', e.data.previewImg);
      setPreviewImg(e.data.previewImg);
    };
    sceneTracker.addEventListener('previewimgupdate', previewimgupdate);
    const appsupdate = e => {
      setApps(e.data.apps);
    };
    sceneTracker.addEventListener('appsupdate', appsupdate);

    setName(sceneTracker.getName());
    setDescription(sceneTracker.getDescription());
    setPreviewImg(item?.previewImg ?? sceneTracker.getPreviewImg());
    setApps(sceneTracker.getApps());

    return () => {
      sceneTracker.removeEventListener('nameupdate', nameupdate);
      sceneTracker.removeEventListener('descriptionupdate', descriptionupdate);
      sceneTracker.removeEventListener('previewimgupdate', previewimgupdate);
      sceneTracker.removeEventListener('appsupdate', appsupdate);
    };
  }, [
    sceneTracker,
  ]);

  // bind transform controls physics hover
  useEffect(() => {
    if (engine) {
      const { transformControlsManager } = engine;
      transformControlsManager.setControlsEnabled(true);

      const select = e => {
        e.data.app && setSelectedApp(e.data.app);
      };
      transformControlsManager.addEventListener('select', select);

      return () => {
        transformControlsManager.setControlsEnabled(false);
        transformControlsManager.removeEventListener('select', select);
      };
    }
  }, [
    engine,
  ]);

  //

  const setSelectedApp = app => {
    const newContentPath = [
      ...contentPath,
      {
        type: 'editApp',
        item: {
          app,
        },
      },
    ];
    setContentPath(newContentPath);
  };

  //

  const screenshotSize = 512;
  const _uploadScreenshot = () => {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = '.png,.jpg,.jpeg';
    input.addEventListener('change', async e => {
      let screenshotBlob = e.target.files[0];
      screenshotBlob = await resizeImage(screenshotBlob, screenshotSize);

      // upload
      const supabaseFsWorker = new SupabaseFsWorker({
        supabase: supabaseClient.supabase,
        bucketName: 'public',
      });

      const fileName = sceneTracker.getId() + '_' + makeId(8) + '.png';
      const keyPath = ['screenshots', appManagerName].concat(fileName);
      const imageUrl = await supabaseFsWorker.writeFile(keyPath, screenshotBlob);
      setPreviewImg(imageUrl);
    });
    input.click();
  };

  //

  return (
    <div className={styles.sceneEditor}>
      {sceneTracker.editable ? <>
        <div className={styles.subheader}>
          <span className={styles.text}>Scene cover</span>
        </div>

        <div className={styles.header}>
          <div className={styles.section}>
            {previewImg ?
              <img src={cacheBust(previewImg)} className={styles.screenshot} onClick={e => {
                e.preventDefault();
                e.stopPropagation();

                _uploadScreenshot();
              }} />
              :
              <div className={styles.screenshotPlaceholder} onClick={e => {
                e.preventDefault();
                e.stopPropagation();

                _uploadScreenshot();
              }} />
            }
          </div>

          <div className={styles.section}>
            <input type='text' className={styles.text} value={name} onChange={e => {
              setName(e.target.value);
            }} placeholder='Enter a name' />
            <textarea
              className={styles.textarea}
              value={description}
              onChange={e => {
                setDescription(e.target.value);
              }}
              placeholder='Leave a description'
            />
          </div>
        </div>
        <div className={styles.header}>
          <button className={classnames(
            styles.button,
          )} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            const { engineRenderer } = engine;
            const { renderer } = engineRenderer;
            const canvas = renderer.domElement;

            const popSize = engineRenderer.pushSize(screenshotSize, screenshotSize);
            engineRenderer.render();
            const screenshotBlobPromise = new Promise((accept, reject) => {
              canvas.toBlob(accept, 'image/jpeg');
            });
            popSize();

            // get blob
            let screenshotBlob = await screenshotBlobPromise;
            // screenshotBlob = await resizeImage(screenshotBlob, screenshotSize);

            // upload
            const supabaseFsWorker = new SupabaseFsWorker({
              supabase: supabaseClient.supabase,
              bucketName: 'public',
            });

            const fileName = sceneTracker.getId() + '_' + makeId(8) + '.png';
            const keyPath = ['screenshots', appManagerName].concat(fileName);
            const imageUrl = await supabaseFsWorker.writeFile(keyPath, screenshotBlob);
            setPreviewImg(imageUrl);
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Screenshot</span>
          </button>
          <button className={classnames(
            styles.button,
          )} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            _uploadScreenshot();
          }}>
            <div className={styles.background} />
            <span className={styles.text}>File...</span>
          </button>
          <button className={classnames(
            styles.button,
          )} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            const newContentPath = [
              ...contentPath,
              {
                type: 'chooseAsset',
                assetType: 'image',
                value: null,
                onChange: ({ itemValue }) => {
                  const { start_url } = itemValue;

                  const newContentPath = [
                    ...contentPath.slice(0, -1),
                    {
                      ...contentPath[contentPath.length - 1],
                      item: {
                        previewImg: start_url,
                      },
                    },
                  ];
                  setContentPath(newContentPath);
                },
              },
            ];
            setContentPath(newContentPath);
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Gallery...</span>
          </button>
        </div>

        {apps.length === 0 && <div className={styles.inset}>
          Scene is empty! <a className={styles.anchor} onClick={e => {
            setContentPath([
              {
                type: 'apps',
              },
            ]);
          }}>Add some apps?</a>
        </div>}

        <div className={styles.objects}>
          {apps.map((app, i) => {
            return (
              <div className={styles.objectsObject} onClick={e => {
                setSelectedApp(app);
              }} key={i}>
                <div className={styles.type}>{app.appType}</div>
                <div className={styles.name}>{app.name}</div>
                <div className={styles.description}>{app.description}</div>
              </div>
            );
          })}
        </div>
        <div className={styles.header}>
          <button className={classnames(
            styles.button,
          )} onClick={async e => {
            e.preventDefault();
            e.stopPropagation();

            // choose from input[type=file]
            const input = document.createElement('input');
            input.type = 'file';
            // input.accept = '.vrm';
            input.addEventListener('change', async e => {
              const f = e.target.files[0];
              if (f.type === 'application/json') {
                // read the file json
                const s = await f.text();
                const json = JSON.parse(s);
                await sceneTracker.setSceneJson(json);
              } else {
                console.warn('not json', f);
              }
            });
            input.click();
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Import</span>
          </button>

          <button className={classnames(
            styles.button,
          )} onClick={async e => {
            const json = await sceneTracker.getSceneJson();

            // open json in new tab via blob
            const blob = new Blob([
              JSON.stringify(json, null, 2),
            ], {
              type: 'application/json',
            });
            const url = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            // a.target = '_blank';
            a.download = 'scene.json';
            a.click();
          }}>
            <div className={styles.background} />
            <span className={styles.text}>Export</span>
          </button>

          <button
            className={classnames(
              styles.button,
              // !changed ? styles.disabled : null,
            )}
            onClick={async e => {
              await sceneTracker.save({
                name,
                description,
                preview_url: previewImg,
              });

              // setChanged(false);
            }}
          //disabled={!changed}
          >
            <div className={styles.background} />
            <span className={styles.text}>Save</span>
          </button>
        </div>
      </> : <>
        <div className={styles.inset}>
          Scene is not editable!
        </div>
      </>}
    </div>
  );
};

export const AppsContent = ({
  contentPath,
  setContentPath,

  debug,
}) => {
  const iconTabs = Object.keys(assetTypes).map(assetType => {
    const assetTypeSpec = assetTypes[assetType];
    if (!assetTypeSpec.debug || debug) {
      return {
        ...assetTypeSpec,
        value: assetType,
      };
    } else {
      return null;
    }
  }).filter(o => o !== null);

  return (
    <IconTabs
      iconTabs={iconTabs}

      contentPath={contentPath}
      setContentPath={setContentPath}
    />
  );
};

//

export const EditAppContent = ({
  engine,

  contentPath,
  setContentPath,

  appManagerName,

  // changed,
  // setChanged,

  // epoch,
  // setEpoch,
  sceneTracker,

  debug,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item,
  } = contentPathElement;
  const selectedApp = item.app;

  const assetTypeSpec = assetTypes[selectedApp.appType];
  if (!assetTypeSpec) {
    console.warn('missing asset type spec', selectedApp, assetTypes);
  }
  // const {itemKey} = assetTypeSpec.edit;
  // const value = AssetPicker.getValue({
  //   item: selectedApp,
  //   itemKey,
  // });
  const {
    start_url,
  } = selectedApp;
  const value = start_url ? {
    name: basename(start_url),
  } : null;
  // console.log('got value', {
  //   item,
  //   // itemKey,
  //   selectedApp,
  //   spec: selectedApp.spec,
  //   start_url,
  //   start_url2: selectedApp.start_url,
  //   value,
  //   epoch,
  //   setEpoch,
  // });
  const onChange = ({
    itemValue,
  }) => {
    selectedApp.setStartUrl(itemValue.start_url);
    // console.log('change item value', {
    //   selectedApp,
    //   itemValue,
    //   start_url: itemValue.start_url,
    //   start_url2: selectedApp.start_url,
    // });

    setContentPath(contentPath);
  };

  // bind selected app
  // useEffect(() => {
  //   if (selectedApp) {
  //     const contentupdate = e => {
  //       setEpoch(epoch + 1);
  //     };
  //     selectedApp.addEventListener('contentupdate', contentupdate);

  //     return () => {
  //       selectedApp.removeEventListener('contentupdate', contentupdate);
  //     };
  //   }
  // }, [
  //   selectedApp,
  //   epoch,
  // ]);

  // bind keyboard controls
  useEffect(() => {
    if (engine) {
      const { transformControlsManager } = engine;

      const keydown = e => {
        switch (e.key) {
          case 'x': {
            if (selectedApp) {
              // setSelectedApp(null);
              const newContentPath = contentPath.slice(0, -1);
              setContentPath(newContentPath);

              const appManager = engine.appManagerContext.getAppManager(appManagerName);
              appManager.removeApp(selectedApp);

              transformControlsManager.clearHoveredPhysicsObject();
            }
            break;
          }
        }
      };
      globalThis.addEventListener('keydown', keydown);

      return () => {
        globalThis.removeEventListener('keydown', keydown);
      };
    }
  }, [
    engine,
    selectedApp,
  ]);

  //

  const {
    componentUiTracker,
  } = engine;
  const Components = componentUiTracker.getAppComponentUiFn(selectedApp);

  // handle onChange debounced
  const [sceneChanged, setSceneChanged] = useState(false);
  const [sceneChangedTimeout, setSceneChangedTimeout] = useState(null);
  const [deleted, setDeleted] = useState(false);

  useEffect(() => {
    if (!deleted) return;
    setContentPath(contentPath.slice(0, -1));
    setDeleted(false);
  }, [deleted]);

  // if scene is changed (debounced), save the scene
  useEffect(() => {
    if (sceneChanged) {
      if (sceneChangedTimeout !== null) {
        clearTimeout(sceneChangedTimeout);
        setSceneChangedTimeout(null);
      }
      const timeout = setTimeout(async () => {
        await sceneTracker.save();
        setSceneChanged(false);
      }, 1000);
      setSceneChangedTimeout(timeout);
    }
  }, [sceneChanged]);

  //

  return (
    <div className={styles.sceneEditor}>
      {/* <div className={styles.subheader}>
        <span className={styles.text}>Edit App</span>
      </div> */}

      <div className={styles.selectedApp}>
        <div className={classnames(
          styles.wrap,
          styles.head,
        )}>
          <div className={styles.name}>{selectedApp.name}</div>
          <div className={styles.description}>{selectedApp.description}</div>
        </div>

        {(assetTypeSpec?.edit) && <div className={styles.wrap}>
          <div className={styles.h}>Asset</div>
          <AssetPicker
            contentPath={contentPath}
            setContentPath={setContentPath}

            assetType={assetTypeSpec.edit.assetType}
            value={value}
            onChange={onChange}
          />
        </div>}

        <div className={styles.wrap}>
          <div className={styles.h}>Transform</div>
          <TransformControlsUi
            engine={engine}
            app={selectedApp}
            // setApp={setSelectedApp}

            onChange={() => {
              // setChanged(true);
              setSceneChanged(true);
            }}
          />
        </div>

        <div className={styles.wrap}>
          {Components && <>
            <div className={styles.h}>
              Components
            </div>
            <Components
              contentPath={contentPath}
              setContentPath={setContentPath}
              debug={debug}
            />
          </>}
        </div>

        <div className={styles.wrap}>
          <div className={styles.row}>
            <button className={styles.button} onClick={e => {
              console.log('remove app', selectedApp)
              // setSelectedApp(null);
              const appManager = engine.appManagerContext.getAppManager(appManagerName);
              appManager.removeApp(selectedApp);
              sceneTracker.save();
              setDeleted(true);
              console.log('removed app', selectedApp)
            }}>
              <div className={styles.background} />
              <span className={styles.text}>Remove</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

//

const storeTypes = [
  'vrm',
  'glb',
  'npc',
  'image',
  'script',
];
const PillTabs = ({
  tabs,
  tab,
  setTab,
}) => {
  return (
    <div className={styles.pillTabs}>
      {tabs.map((t, i) => {
        return (
          <div className={classnames(
            styles.pill,
            t === tab ? styles.selected : null,
          )} onClick={e => {
            setTab(t);
          }} key={t}>
            {t}
          </div>
        );
      })}
    </div>
  );
};
const AssetPreview = ({
  item,
}) => {
  const [appStores, setAppStores] = useState(() => {
    const {
      start_url,
    } = item;
    return {
      preview: {
        objects: [
          {
            start_url,
          },
          {
            start_url: '/core-modules/asset-preview-camera/index.js',
          },
          {
            start_url: '/core-modules/asset-preview-background/index.js',
          },
          {
            start_url: '/core-modules/floor/index.js',
          },
        ],
      },
    };
  });
  const [engine, setEngine] = useState(null);
  const [engineLoading, setEngineLoading] = useState(false);
  const [context, setContext] = useState(null);
  const [canvas, setCanvas] = useState(null);
  const canvasRef = useRef();

  //

  useEffect(() => {
    setCanvas(canvasRef.current);
  }, [
    canvasRef.current,
  ]);

  //

  return (
    <div className={classnames(
      styles.assetPreview,
      engine ? styles.loaded : null,
    )}>
      <canvas ref={canvasRef} className={classnames(
        styles.canvas,
      )} />
      <div className={classnames(
        styles.placeholder,
      )}>
        <div className={styles.inner}>
          Loading preview...
        </div>
      </div>

      {canvas && <EngineProvider
        canvas={canvas}
        appStores={appStores}
        // playerSpec={user.playerSpec}

        engine={engine}
        setEngine={setEngine}

        engineLoading={engineLoading}
        setEngineLoading={setEngineLoading}

        onContext={context => {
          setContext(context);
        }}
      />}
    </div>
  );
};

//

const StoreHeader = ({
  type,
  setType,
  children,
}) => {
  return (
    <>
      <div className={styles.h}>{children}</div>
      <PillTabs
        tabs={storeTypes}

        tab={type}
        setTab={setType}
      />
    </>
  );
};
const StoreSidebarSellUi = ({
  item,
}) => {
  const [price, setPrice] = useState(1);
  const [count, setCount] = useState(1);

  //

  return (
    <div className={styles.side}>
      <label className={styles.label}>
        <span className={styles.text}>Type</span>
        <div className={classnames(
          styles.pill,
          styles.selected,
        )}>{item.type}</div>
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Price</span>
        <input type='number' step={1} value={price} onChange={e => {
          setPrice(e.target.value);
        }} />
      </label>

      <label className={styles.label}>
        <span className={styles.text}>Count</span>
        <input type='number' step={1} value={count} onChange={e => {
          setCount(e.target.value);
        }} />
      </label>

      <button className={styles.button} onClick={async e => {
        const id = crypto.randomUUID();
        const {
          type,
          start_url,
        } = item;
        const preview_url = '/images/ui/buster-sword.svg';
        const storeItem = {
          id,
          type,
          name,
          description,
          count,
          preview_url,
          start_url,
        };

        await supabaseClient.supabase
          .from('store')
          .insert(storeItem);
        refreshItems();
      }}>
        <div className={styles.background} />
        <span className={styles.text}>Post</span>
      </button>
    </div>
  );
};
const StoreSidebarMintUi = ({
  item,
}) => {
  // const [price, setPrice] = useState(1);
  const [count, setCount] = useState(1);

  //

  return (
    <div className={styles.side}>
      <label className={styles.label}>
        <span className={styles.text}>Type</span>
        <div className={classnames(
          styles.pill,
          styles.selected,
        )}>{item.type}</div>
      </label>

      {/* <label className={styles.label}>
        <span className={styles.text}>Price</span>
        <input type='number' step={1} value={price} onChange={e => {
          setPrice(e.target.value);
        }} />
      </label> */}

      <label className={styles.label}>
        <span className={styles.text}>Count</span>
        <input type='number' step={1} value={count} onChange={e => {
          setCount(e.target.value);
        }} />
      </label>

      <button className={styles.button} onClick={async e => {
        const id = crypto.randomUUID();
        const {
          type,
          start_url,
        } = item;
        const preview_url = '/images/ui/buster-sword.svg';
        const storeItem = {
          id,
          type,
          name,
          description,
          count,
          preview_url,
          start_url,
        };

        await supabaseClient.supabase
          .from('inventory')
          .insert(storeItem);
        refreshItems();
      }}>
        <div className={styles.background} />
        <span className={styles.text}>Mint</span>
      </button>
    </div>
  );
};
const StoreAssetEditUi = ({
  item,
  StoreSidebarUiComponent,
}) => {
  const [name, setName] = useState(item?.name || '');
  const [description, setDescription] = useState(item?.description || '');

  //

  return (
    <>
      <div className={styles.row}>
        <AssetPreview
          item={item}
        />
      </div>

      <div className={styles.wrap}>
        <div className={styles.main}>
          <label className={styles.label}>
            <span className={styles.text}>Name</span>
            <input type='text' value={name} onChange={e => {
              setName(e.target.value);
            }} />
          </label>

          <label className={styles.label}>
            <span className={styles.text}>Description</span>
            <textarea value={description} onChange={e => {
              setDescription(e.target.value);
            }} />
          </label>
        </div>

        <StoreSidebarUiComponent
          item={item}
        />
      </div>
    </>
  );
};
const StoreAssetSelectUi = ({
  headerText,
  // sidebarText,
  SidebarComponent,

  tableName,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  supabaseClient,
  sessionUserId,

  onClick,
}) => {
  const [type, setType] = useState(storeTypes[0]);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  //

  useEffect(() => {
    const abortController = new AbortController();
    const { signal } = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadItemsType(type, tableName)({
          signal,
          supabaseClient,
          sessionUserId,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, [
    type,
    epoch,
  ]);

  //

  return (
    <div className={styles.storeUi}>
      <StoreHeader
        type={type}
        setType={setType}
      >{headerText}</StoreHeader>

      <div className={styles.wrap}>
        <div className={styles.main}>
          {loading && <div className={styles.placeholder}>
            Loading...
          </div>}

          {!loading && items.length === 0 && <div className={styles.placeholder}>
            No items
          </div>}

          {!loading && items.map(item => {
            return (
              <div className={styles.item} onClick={e => {
                e.preventDefault();
                e.stopPropagation();

                onClick && onClick(item);
              }} key={item.id}>
                <img src={item.preview_url} className={styles.previewImg} draggable={false} />
                <div className={styles.name}>{item.name}</div>
              </div>
            );
          })}
        </div>

        <SidebarComponent
          contentPath={contentPath}
          setContentPath={setContentPath}
        />
      </div>
    </div>
  );
};

//

const StoreBuySidebarUi = ({
  item,
  contentPath,
  setContentPath,
}) => {
  return (
    <div className={styles.sidebar}>
      <div className={styles.item} onClick={e => {
        e.preventDefault();
        e.stopPropagation();

        setContentPath([
          contentPath[0],
          {
            type: 'storeSell',
          },
        ]);
      }}>
        <img src='/assets/icons/plus.svg' className={classnames(
          styles.previewImg,
          styles.placeholderImg,
        )} draggable={false} />
        <div className={styles.name}>Sell item</div>
      </div>
    </div>
  );
}
export const StoreBuyContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,
}) => {
  const [type, setType] = useState(storeTypes[0]);
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadQueue, setLoadQueue] = useState(() => new QueueManager());

  //

  const refreshItems = () => {
    setEpoch(epoch + 1);
  };

  //

  useEffect(() => {
    const abortController = new AbortController();
    const { signal } = abortController;

    loadQueue.waitForTurn(async () => {
      setLoading(true);

      try {
        const newItems = await loadItemsType(type, 'store')({
          signal,
          supabaseClient,
          sessionUserId,
        });
        if (signal.aborted) return;

        setItems(newItems);
      } catch (err) {
        throw err;
      } finally {
        setLoading(false);
      }
    });

    return () => {
      abortController.abort();
    };
  }, [
    epoch,
    type,
  ]);

  //

  return (
    <div className={styles.storeUi}>
      <StoreAssetSelectUi
        headerText='Buy item'
        // sidebarText='Choose an item to buy'
        SidebarComponent={StoreBuySidebarUi}

        tableName='store'

        contentPath={contentPath}
        setContentPath={setContentPath}

        epoch={epoch}
        setEpoch={setEpoch}

        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        onClick={item => {
          setContentPath([
            ...contentPath,
            {
              type: 'storeBuyItem',
              item,
            },
          ]);
        }}
      />
    </div>
  );
};

//

const StoreSellSidebarUi = () => {
  return (
    <div className={styles.side}>
      Choose an item to sell
    </div>
  );
};
export const StoreSellContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  onClick,
}) => {
  return (
    <div className={styles.storeUi}>
      <StoreAssetSelectUi
        headerText='Sell item'
        // sidebarText='Choose an item to sell'
        SidebarComponent={StoreSellSidebarUi}

        tableName='assets'

        epoch={epoch}
        setEpoch={setEpoch}

        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        onClick={item => {
          setContentPath([
            ...contentPath,
            {
              type: 'storeEdit',
              item,
              subType: 'sell',
            },
          ]);
        }}
      />
    </div>
  );
};

//

const StoreTradeSidebarUi = () => {
  return (
    <div className={styles.side}>
      Choose a trade
    </div>
  );
};
export const StoreTradeContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  onClick,
}) => {
  return (
    <div className={styles.storeUi}>
      <StoreAssetSelectUi
        headerText='Trade item'
        // sidebarText='Choose an item to trade'
        SidebarComponent={StoreTradeSidebarUi}

        tableName='trades'

        epoch={epoch}
        setEpoch={setEpoch}

        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        onClick={item => {
          setContentPath([
            ...contentPath,
            {
              type: 'storeTradeItem',
              item,
            },
          ]);
        }}
      />
    </div>
  );
};

//

const storeEditTypes = {
  'sell': {
    StoreSidebarUiComponent: StoreSidebarSellUi,
  },
  'mint': {
    StoreSidebarUiComponent: StoreSidebarMintUi,
  },
};
export const StoreEditContent = ({
  contentPath,
  setContentPath,
}) => {
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    item,
    subType,
  } = contentPathElement;

  //

  if (!item) {
    console.warn('item not set in content path!', contentPath);
    return null;
  }

  //

  const {
    StoreSidebarUiComponent,
  } = storeEditTypes[subType];

  //

  return (
    <div className={styles.storeUi}>
      <StoreAssetEditUi
        item={item}
        StoreSidebarUiComponent={StoreSidebarUiComponent}
      />
    </div>
  );
};

//

const StoreMintSidebarUi = () => {
  return (
    <div className={styles.side}>
      Choose an item to mint from your inventory
    </div>
  );
};
export const StoreMintContent = ({
  supabaseClient,
  sessionUserId,

  contentPath,
  setContentPath,

  epoch,
  setEpoch,

  // onClick,
}) => {
  return (
    <div className={styles.storeUi}>
      <StoreAssetSelectUi
        headerText='Mint item'
        // sidebarText='Choose an item to mint'
        SidebarComponent={StoreMintSidebarUi}

        tableName='assets'

        epoch={epoch}
        setEpoch={setEpoch}

        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        onClick={item => {
          setContentPath([
            ...contentPath,
            {
              type: 'storeEdit',
              item,
              subType: 'mint',
            },
          ]);
        }}
      />
    </div>
  );
};

//// Start of code for IC storage integration

/**
 * CreateICUploadContent.js
 *
 * A React component for uploading, viewing, and deleting images on the Internet Computer (IC). This component utilizes the IC's
 * decentralized architecture for secure and scalable content management, leveraging DFINITY's AssetManager for asset operations
 * and the IC's authentication mechanisms for user identification.
 *
 * Functional Overview:
 * - **User Authentication**: Implements user login through the Internet Computer's Plug Wallet, ensuring secure and decentralized
 *   identity verification. This step is crucial for associating uploaded content with the user's identity.
 *
 * - **Image Upload**: Allows users to select images from their device and upload them to the IC. The upload process uses the
 *   AssetManager canister to store images securely on the IC, demonstrating decentralized file management.
 *
 * - **Viewing Uploaded Images**: Users can view a list of their uploaded images, fetched directly from the IC. This showcases
 *   real-time asset retrieval from a decentralized network.
 *
 * - **Image Deletion**: Provides the ability to delete images. This sends a request to the AssetManager canister to remove the
 *   image from storage, illustrating direct manipulation of decentralized assets.
 *
 */

// Importing necessary modules from React
import {
  useCallback
} from 'react';

// Importing necessary modules for IC integration
import {
  AssetManager
} from '@dfinity/assets'; // AssetManager module for interacting with Internet Computer canisters
import {
  HttpAgent,
  Actor
} from "@dfinity/agent"; // HttpAgent and Actor module for interacting with Internet Computer canisters
import {
  plugLogin
} from '../ic-wallet-auth-ui/ICWalletAuthUi.jsx'; // Custom module for authentication using IC wallet UI

// Importing interfaces for cycles top-up feature..
import * as cycles from "./interfaces/cmc/cmc";
import * as ledger from "./interfaces/ledger/index";
import * as distro from "./interfaces/distro/index";

// Function component for creating IC upload content
export const CreateICUploadContent = () => {
  // State variables for managing current user, loading status, error, file, upload status, assets, selected asset, and confirmation dialog visibility
  const [currentUser, setCurrentUser] = useState(null); // State for storing current user's information
  const [globalLoading, setGlobalLoading] = useState(false); // State for global loading status
  const [loadingMessage, setLoadingMessage] = useState(''); // State for loading message
  const [error, setError] = useState(null); // State for error message
  const [file, setFile] = useState(null); // State for currently selected file
  const [uploadStatus, setUploadStatus] = useState(''); // State for upload status
  const [assets, setAssets] = useState([]); // State for storing list of assets
  const [selectedAsset, setSelectedAsset] = useState(null); // State for currently selected asset
  const [showConfirmation, setShowConfirmation] = useState(false); // State for confirmation dialog visibility

  // Effect hook to check connection and authenticate user when component mounts
  useEffect(() => {
    const checkConnectionAndAuthenticate = async () => {
      try {
        // Attempt to log in using the plugLogin function, which should return a user object containing the principal, agent, and provider if successful.
        const userObject = await plugLogin();
        console.log(userObject);
        // Update the state with the returned user object to store the current user's information.
        setCurrentUser(userObject);
        console.log("Logged In Successfully!");
        console.log("Loading list...");
        // Call the loadList function, passing the principal and agent from the user object. This function is responsible for fetching the list of assets associated with the user.
        await loadList(userObject.principal, userObject.agent);
        console.log("Successful");
      } catch (err) {
        console.error('Error authenticating user:', err);
      }
    };

    // Invoke the checkConnectionAndAuthenticate function when component mounts
    checkConnectionAndAuthenticate();
  }, []);

  // Function to load asset list from the Internet Computer
  const loadList = useCallback(async (principal, agent) => {
    console.log('Loading asset list...');
    setGlobalLoading(true); // Set global loading status to true
    setLoadingMessage('Loading assets...'); // Set loading message
    try {
      // Create an instance of AssetManager with the user's principal and agent
      const actor = await createAssetActor(principal, agent);
      // Retrieve the list of assets from the IC canister
      console.log("Retrieving list...");
      const list = await actor.list();
      console.log('Asset list loaded:', list);
      // Filter assets by user's principal
      const filteredList = list.filter((file) => file.key.startsWith(`/${principal}/`));
      // Map asset list to include URL for each asset
      setAssets(
        filteredList.map((file) => ({
          key: file.key,
          url: `https://zks6t-giaaa-aaaap-qb7fa-cai.raw.icp0.io/${file.key.slice(1)}`, // Construct URL for accessing the asset
        }))
      );
      setError(null); // Clear any previous error messages
    } catch (err) {
      setError('Failed to load assets.'); // Set error message if asset loading fails
      console.error('Error loading asset list:', err);
    } finally {
      setGlobalLoading(false); // Set global loading status to false
    }
  }, [currentUser]);

  // Function to create asset actor for interacting with IC canister
  const createAssetActor = useCallback(async (principal, agent) => {
    console.log('Creating asset actor...');
    // Check if principal or agent is not set
    if (!principal || !agent) {
      throw new Error('Principal or Agent is not set.');
    }
    // Return a new AssetManager instance initialized with canister ID and agent
    return new AssetManager({
      canisterId: 'zks6t-giaaa-aaaap-qb7fa-cai', // Canister ID for the asset manager canister on the Internet Computer
      agent: agent,
    });
  }, []);

  // Function to handle file change event
  const handleFileChange = (event) => {
    setFile(event.target.files[0]); // Set the selected file
    setUploadStatus(''); // Clear any previous upload status
  };

  // Function to handle file upload to the Internet Computer
  const handleFileUpload = useCallback(async () => {
    console.log('Uploading file...');
    // Check if file, currentUser, or currentUser's agent is not set
    if (!file || !currentUser || !currentUser.agent) {
      setUploadStatus('Please select a file and make sure you are authenticated.');
      return;
    }

    setGlobalLoading(true); // Set global loading status to true
    setLoadingMessage('Uploading file...'); // Set loading message
    try {
      setUploadStatus('Uploading file...'); // Update upload status
      // Create asset actor and store the file in the IC canister
      const actor = await createAssetActor(currentUser.principal, currentUser.agent);
      const key = `${currentUser.principal}/${file.name}`;
      const arrayBuffer = await file.arrayBuffer();
      const uint8Array = new Uint8Array(arrayBuffer);
      await actor.store(uint8Array, {
        fileName: key
      }); // Store the file in the IC canister
      await loadList(currentUser.principal, currentUser.agent); // Reload list after successful upload
      setUploadStatus('File uploaded successfully!'); // Set upload status
      setFile(null); // Clear the file input after successful upload
    } catch (err) {
      setError('Failed to upload file.'); // Set error message if upload fails
      console.error('Error uploading file:', err);
    } finally {
      setGlobalLoading(false); // Set global loading status to false
    }
  }, [file, currentUser, createAssetActor, loadList]);

  // Function to handle asset deletion from the Internet Computer
  const handleDeleteAsset = useCallback(async () => {
    if (!selectedAsset) return;

    setGlobalLoading(true); // Set global loading status to true
    setLoadingMessage('Deleting asset...'); // Set loading message
    setUploadStatus('Deleting file...'); // Update upload status
    try {
      // Create asset actor and delete the selected asset from the IC canister
      const actor = await createAssetActor(currentUser.principal, currentUser.agent);
      await actor.delete(selectedAsset.key); // Delete the asset from the IC canister
      await loadList(currentUser.principal, currentUser.agent); // Reload list after deletion
      setUploadStatus('Asset deleted successfully!'); // Set upload status
    } catch (err) {
      setError('Failed to delete asset.'); // Set error message if deletion fails
      console.error('Error deleting asset:', err);
    } finally {
      setGlobalLoading(false); // Set global loading status to false
      setShowConfirmation(false); // Close the confirmation dialog after deletion
    }
  }, [currentUser, selectedAsset, createAssetActor, loadList]);

  /**
   * Functions to handle canister top-up functions
   */

  // Define a whitelist of canister IDs for security purposes
  const whitelist = ["zks6t-giaaa-aaaap-qb7fa-cai", "jeb4e-myaaa-aaaak-aflga-cai"];

  // Function to create actors for interacting with the Cycles, Ledger, and Distro canisters
  const createActors = async () => {
    // Create actors for cycles, ledger, and distro canisters
    const cyclesActor = Actor.createActor(cycles.idlFactory, {
      agent: currentUser.agent,
      canisterId: "rkp4c-7iaaa-aaaaa-aaaca-cai" // Canister ID for cycles actor
    });
    const ledgerActor = Actor.createActor(ledger.idlFactory, {
      agent: currentUser.agent,
      canisterId: "ryjl3-tyaaa-aaaaa-aaaba-cai" // Canister ID for ledger actor
    });
    const distroActor = Actor.createActor(distro.idlFactory, {
      agent: currentUser.agent,
      canisterId: "jeb4e-myaaa-aaaak-aflga-cai" // Canister ID for distro actor
    });

    return {
      cycles: cyclesActor,
      ledger: ledgerActor,
      distro: distroActor
    };
  };

  // Function to verify a transaction on the ledger
  const verifyTransaction = async (block_height, amount_sent, actor) => {
    // Create a basic HTTP agent
    const basicAgent = new HttpAgent({
      host: "https://ic0.app",
    });
    const ledgerActor = actor;

    // Query the ledger for transaction details
    const result = await ledgerActor.query_blocks({
      start: block_height,
      length: 1
    });
    console.log("The result of the query is: ", result);

    // Extract transfer information from the transaction
    const transferInfo = result.blocks[0].transaction.operation[0].Transfer;
    console.log("The transfer info is: ", transferInfo);

    // Parse transferred amount from the transaction
    const transferredAmount = Number(transferInfo.amount.e8s);
    console.log("The transferred amount is: ", transferredAmount);

    // Verify if transferred amount matches the expected amount
    if (transferredAmount === amount_sent) {
      return true; // Transaction verified
    } else {
      return false; // Transaction not verified
    }
  };

  // Main function to handle the cycles top-up process
  const cyclesTopUp = async () => {
    // Create actors for interacting with canisters
    const actors = await createActors();

    // Convert ICP to cycles
    console.log("Converting rate...");
    const conversionRate = await actors.cycles.get_icp_xdr_conversion_rate();
    const actualRate = conversionRate.data.xdr_permyriad_per_icp.toString();
    const requiredZeros = "00000000";
    const finalRate = Number(actualRate + requiredZeros);

    // Amount of ICP requested from the user.
    const amountOfICP = 0.01;
    const amountInXDR = amountOfICP * finalRate;
    console.log("The amount in XDR is: ", amountInXDR);

    console.log("Handling plug payment...");
    // Note: Change to your Plug Address.
    const to = "7zdi6-6h2gk-g4j54-cigti-iiu4u-lj4vy-bewjf-oouoc-dnlck-fyfy5-aae";
    const amount = amountOfICP * 100000000;
    console.log(amount);
    const memo = "Testing";
    const result = await window.ic.plug.requestTransfer({
      to,
      amount,
      memo
    });
    console.log("The result of the transfer is: ", result);

    console.log("Verifying the transaction...");
    const verified = await verifyTransaction(result.height, amount, actors.ledger);
    if (verified) {
      console.log("The transaction was verified!");
    } else {
      console.log("The transaction was not verified!");
      return;
    }

    console.log("Adding cycles...");
    const balancesBefore = await actors.distro.getBalances();
    console.log("The current balances of the canisters are: ", balancesBefore);
    const topupResult = await actors.distro.addCyclesToAll(amountInXDR);
    console.log("The result of the topup is: ", topupResult);
    const balancesAfter = await actors.distro.getBalances();
    console.log("The new balances of the canisters are: ", balancesAfter);
  };

  /**
  * Return JSX for IC upload content - inline styles are included for demo purposes
  * The styles should be seperated into a seperate CSS file for production use 
  */
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '40px',
        borderRadius: '10px',
        boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
        maxWidth: '600px',
        margin: 'auto',
        backgroundColor: '#333',
        color: '#fff'
      }}
    >
      <h2
        style={{
          marginBottom: '20px',
          fontSize: '24px',
          fontWeight: 'bold',
          textAlign: 'center'
        }}
      >
        IC Upload Content
      </h2>
      {globalLoading && (
        <div
          style={{
            margin: '20px 0',
            padding: '10px',
            backgroundColor: '#888',
            borderRadius: '5px',
            boxShadow: '0 2px 4px rgba(0, 0, 0, 0.3)',
            textAlign: 'center'
          }}
        >
          Loading...
        </div>
      )}
      {error && (
        <div
          style={{
            color: 'red',
            margin: '20px 0',
            textAlign: 'center'
          }}
        >
          {error}
        </div>
      )}
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center',
          gap: '20px'
        }}
      >
        {assets.map((asset) => (
          <div
            key={asset.key}
            style={{
              position: 'relative',
              borderRadius: '10px',
              overflow: 'hidden',
              boxShadow: selectedAsset === asset ? '0 0 0 2px #4CAF50' : '0 2px 4px rgba(0, 0, 0, 0.3)',
              marginBottom: '10px'
            }}
          >
            <img
              src={asset.url}
              alt="Uploaded Asset"
              style={{
                maxWidth: '200px',
                maxHeight: '200px',
                objectFit: 'cover',
                borderRadius: '10px',
                cursor: 'pointer'
              }}
              onClick={() => setSelectedAsset(asset)}
            />
            <button
              onClick={() => { setSelectedAsset(asset); setShowConfirmation(true); }}
              style={{
                position: 'absolute',
                top: '10px',
                right: '10px',
                padding: '5px',
                borderRadius: '50%',
                backgroundColor: '#ff4d4f',
                color: '#fff',
                fontSize: '14px',
                fontWeight: 'bold',
                cursor: 'pointer',
                border: 'none',
                outline: 'none',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.3)',
                width: '30px',
                height: '30px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              X
            </button>
          </div>
        ))}
      </div>
      <label
        htmlFor="file-upload"
        style={{
          cursor: 'pointer',
          margin: '20px 0',
          padding: '15px 30px',
          backgroundColor: '#4CAF50',
          color: '#fff',
          borderRadius: '5px',
          fontSize: '16px',
          fontWeight: 'bold',
          textTransform: 'uppercase',
          transition: 'background-color 0.3s',
          boxShadow: '0 2px 4px rgba(0, 0, 0, 0.3)',
          display: 'flex',
          alignItems: 'center'
        }}
      >
        <span style={{ marginRight: '10px' }}>Choose File</span>
        <input
          id="file-upload"
          type="file"
          onChange={handleFileChange}
          style={{
            display: 'inline-block',
            opacity: 0,
            position: 'absolute',
            left: '-9999px'
          }}
        />
        {file && <span style={{ fontSize: '14px', marginLeft: '10px' }}>{file.name}</span>}
      </label>
      <button
        onClick={handleFileUpload}
        style={{
          padding: '15px 30px',
          borderRadius: '5px',
          border: 'none',
          backgroundColor: '#4CAF50',
          color: '#fff',
          fontSize: '18px',
          fontWeight: 'bold',
          textTransform: 'uppercase',
          cursor: 'pointer',
          transition: 'background-color 0.3s',
          boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
          marginTop: '20px'
        }}
      >
        {uploadStatus === 'Uploading file...' ? 'Uploading file...' : 'Upload to IC'}
      </button>
      <button
        onClick={cyclesTopUp}
        style={{
          padding: '15px 30px',
          borderRadius: '5px',
          border: 'none',
          backgroundColor: '#4CAF50',
          color: '#fff',
          fontSize: '18px',
          fontWeight: 'bold',
          textTransform: 'uppercase',
          cursor: 'pointer',
          transition: 'background-color 0.3s',
          boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
          marginTop: '20px'
        }}
      >
        Donate Cycles
      </button>

      <div
        style={{
          marginTop: '20px',
          textAlign: 'center'
        }}
      >
        {uploadStatus}
      </div>
      {showConfirmation && (
        <div
          style={{
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div
            style={{
              backgroundColor: '#222',
              padding: '20px',
              borderRadius: '10px',
              boxShadow: '0 4px 8px rgba(0, 0, 0, 0.3)',
              maxWidth: '400px',
              textAlign: 'center'
            }}
          >
            <p>Are you sure you want to delete the asset?</p>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                marginTop: '20px'
              }}
            >
              <button
                onClick={() => setShowConfirmation(false)}
                style={{
                  padding: '10px 20px',
                  borderRadius: '5px',
                  backgroundColor: '#888',
                  color: '#fff',
                  fontSize: '16px',
                  marginRight: '20px',
                  cursor: 'pointer',
                  border: 'none',
                  outline: 'none'
                }}
              >
                Cancel
              </button>
              <button
                onClick={() => { handleDeleteAsset(selectedAsset.key); setShowConfirmation(false); }}
                style={{
                  padding: '10px 20px',
                  borderRadius: '5px',
                  backgroundColor: '#ff4d4f',
                  color: '#fff',
                  fontSize: '16px',
                  cursor: 'pointer',
                  border: 'none',
                  outline: 'none'
                }}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

//// End of code for IC storage integration 
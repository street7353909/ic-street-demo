
import React, {
  useState,
} from 'react';
import classnames from 'classnames';

import {
  ContentPathWindow,
} from '../content-path/ContentPath.jsx';

import {
  MapUi,
} from '../map-ui/MapUi.jsx';

// import adventureStyles from '../../../styles/Adventure.module.css';
import inventoryUiStyles from '../../../styles/InventoryUi.module.css';
import marketplaceUiStyles from '../../../styles/MarketplaceUi.module.css';

export const SceneEditorWindow = ({
  ...props
}) => {
  const {sessionUserId} = props;
  if (!sessionUserId)  {
    return (
      <div className={classnames(
        inventoryUiStyles.inventoryUi,
      )}>
        <div className={inventoryUiStyles.row}>
          <div className={inventoryUiStyles.placeholder}>
            Need to sign in
          </div>
        </div>
      </div>
    );
  } else {
    const tabs = [
      {
        label: 'Edit',
        value: 'edit',
        item: null,
      },
      {
        label: 'Apps',
        value: 'apps',
        item: null,
      },
    ];

    const [epoch, setEpoch] = useState(0);

    return (
      <ContentPathWindow
        tabs={tabs}

        epoch={epoch}
        setEpoch={setEpoch}

        {...props}
      />
    );    
  }
};

//

export const ProfileWindow = ({
  ...props
}) => {
  const {
    contentPath,
  } = props;
  const contentPathElement = contentPath[contentPath.length - 1];
  const {
    childContentPath: [
      {
        item,
      },
    ],
  } = contentPathElement;

  const tabs = [
    {
      label: 'Profile',
      value: 'profile',
      item,
    },
  ];

  //

  return (
    <ContentPathWindow
      tabs={tabs}

      {...props}
    />
  );
};

//

export const WorldsWindow = ({
  ...props
}) => {
  const {
    user,
  } = props;
  const tabs = [
    {
      label: 'Worlds',
      value: 'worlds',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      tabs={tabs}

      contentPath={contentPath}
      setContentPath={setContentPath}

      {...props}
    />
  );
};

//

export const InventoryWindow = ({
  ...props
}) => {
  const tabs = [
    {
      label: 'Inventory',
      value: 'inventory',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      className={inventoryUiStyles.jigsawInventoryUi}

      tabs={tabs}
      showTabs={false}

      contentPath={contentPath}
      setContentPath={setContentPath}

      {...props}
    />
  );
};

//

export const MapWindow = ({
  ...props
}) => {
  return (
    <MapUi
      {...props}
    />
  );
};

//

export const AiAgentsWindow = ({
  ...props
}) => {
  const tabs = [
    {
      label: 'AI Agents',
      value: 'aiAgents',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      tabs={tabs}

      contentPath={contentPath}
      setContentPath={setContentPath}

      {...props}
    />
  );
};

//

/* export const GenAiWindow = ({
  ...props
}) => {
  const tabs = [
    {
      label: 'Gen AI',
      value: 'genAi',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      tabs={tabs}

      contentPath={contentPath}
      setContentPath={setContentPath}

      {...props}
    />
  );
}; */

//

export const SettingsWindow = ({
  ...props
}) => {
  const tabs = [
    {
      label: 'Settings',
      value: 'settings',
      item: null,
    },
  ];

  const [contentPath, setContentPath] = useState(() => [
    {
      type: tabs[0].value,
      item: tabs[0].item,
    },
  ]);

  return (
    <ContentPathWindow
      tabs={tabs}

      contentPath={contentPath}
      setContentPath={setContentPath}

      {...props}
    />
  );
};

//

export const MarketplaceWindow = ({
  ...props
}) => {
  const tabs = [
    {
      label: 'Store',
      value: 'storeBuy',
      item: null,
    },
    {
      label: 'Trade',
      value: 'storeTrade',
      item: null,
    },
    {
      label: 'Mint',
      value: 'storeMint',
      item: null,
    },
  ];

  return (
    <ContentPathWindow
      tabs={tabs}

      className={marketplaceUiStyles.full}

      {...props}
    />
  );
};
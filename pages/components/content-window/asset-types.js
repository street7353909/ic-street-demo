import * as THREE from 'three';

export const assetTypes = {
  'vrm': {
    icon: '/ui/assets/icons/posture.svg',
    label: 'VRM',
    category: 'media',
    create: {
      // type: 'upload',
      // mimeTypes: [
      //   'vrm',
      // ],
      type: 'custom',
      nextType: 'createVrm',
    },
    edit: {
      assetType: 'vrm',
      itemKey: 'start_url',
    },
    drop: {
      metadata: {
        components: [
          {
            key: 'physics',
            value: 'true',
          },
        ],
      },
    },
    buttons: [
      'createNpc',
      'sell',
      'wearAvatar',
    ],
  },
  'glb': {
    icon: '/assets/icons/backpack.svg',
    label: 'GLB',
    category: 'media',
    create: {
      type: 'upload',
      mimeTypes: [
        'glb',
      ],
    },
    edit: {
      assetType: 'glb',
      itemKey: 'start_url',
    },
    buttons: [
      'createWearable',
      'sell',
    ],
  },
  'image': {
    icon: '/ui/assets/icons/graphics.svg',
    label: 'Image',
    category: 'media',
    create: {
      type: 'upload',
      mimeTypes: [
        'png',
        'jpg',
        'jpeg',
        'gif',
      ],
      // type: 'custom',
      // nextType: 'createImage',
    },
    edit: {
      assetType: 'image',
      itemKey: 'start_url',
    },
    buttons: [
      'sell',
    ],
  },
  'npc': {
    icon: '/ui/assets/icons/personas.svg',
    label: 'NPC',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createNpc',
    },
    edit: {
      assetType: 'vrm',
      itemKey: 'start_url',
    },
    drop: {
      metadata: {
        quaternionOffset: new THREE.Quaternion()
          .setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI)
          .toArray(),
      },
    },
    buttons: [
      'editNpc',
      'sell',
    ],
  },
  'audio': {
    icon: '/images/audio.svg',
    label: 'Audio',
    category: 'media',
    create: {
      // type: 'upload',
      // mimeTypes: [
      //   'mp3',
      //   'ogg',
      //   'wav',
      // ],
      type: 'custom',
      nextType: 'createAudio',
    },
    buttons: [
      'sell',
    ],
  },
  'video': {
    icon: '/images/video.svg',
    label: 'Video',
    category: 'media',
    create: {
      // type: 'upload',
      // mimeTypes: [
      //   'mp4',
      //   'webm',
      // ],
      type: 'custom',
      nextType: 'createVideo',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1, 0],
      },
    },
    buttons: [
      'sell',
    ],
  },
  'voice': {
    icon: '/images/voice.svg',
    label: 'Voice',
    category: 'media',
    create: {
      type: 'custom',
      nextType: 'createVoice',
    },
    buttons: [
      'sell',
      'testVoice',
      'editVoice',
    ],
  },
  'director': {
    icon: '/images/megaphone.svg',
    label: 'Director',
    category: 'media',
    create: {
      type: 'custom',
      nextType: 'createDirector',
    },
    buttons: [
      'sell',
      'editDirector',
    ],
    debug: true,
  },
  'lore': {
    icon: '/images/lore.svg',
    label: 'Lore',
    category: 'media',
    create: {
      type: 'custom',
      nextType: 'createLore',
    },
    buttons: [
      'sell',
      'editLore',
    ],
    debug: true,
  },
  'camera': {
    icon: '/assets/video-camera.svg',
    label: 'Camera',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createCamera',
    },
    buttons: [
      'setCamera',
      'editCamera',
    ],
  },
  'stream': {
    icon: '/images/stream.svg',
    label: 'Stream',
    category: 'connection',
    create: {
      type: 'drop',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1.5, 0],
      },
    },
    buttons: [
      // 'sell',
    ],
  },
  'portal': {
    icon: '/images/metaverse-portal.svg',
    label: 'Portal',
    category: 'entity',
    create: {
      type: 'drop',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1, 0],
      },
    },
    buttons: [
      'sell',
    ],
  },
  'text': {
    icon: '/images/text.svg',
    label: 'Text',
    category: 'ui',
    create: {
      type: 'drop',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1, 0],
        components: {
          text: 'Hello world!',
        },
      },
    },
    buttons: [
      'sell',
    ],
  },
  'html': {
    icon: '/images/html.svg',
    label: 'HTML',
    category: 'ui',
    create: {
      type: 'drop',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1, 0],
        components: {
          src: `data:text/html,${encodeURIComponent('<html><body><h1>Hello, world!</h1></body></html>')}`
        },
      },
    },
    buttons: [
      'sell',
    ],
  },
  'light': {
    icon: '/assets/icons/light.svg',
    label: 'Light',
    category: 'environment',
    create: {
      type: 'drop',
    },
    buttons: [
      'sell',
    ],
  },
  'wearable': {
    icon: '/images/grab.svg',
    label: 'Wearable',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createWearable',
    },
    edit: {
      assetType: 'glb',
      itemKey: 'glbUrl',
    },
    drop: {
      metadata: {
        positionOffset: [0, 1, 0],
      },
    },
    buttons: [
      'sell',
      'editWearable',
      'wearItem',
    ],
    debug: true,
  },
  'sittable': {
    icon: '/images/seat.svg',
    label: 'Sittable',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createSittable',
    },
    edit: {
      assetType: 'glb',
      itemKey: 'glbUrl',
    },
    buttons: [
      'sell',
      'editSittable',
      'wearItem',
    ],
    debug: true,
  },
  'mount': {
    icon: '/images/saddle.svg',
    label: 'Mount',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createMount',
    },
    edit: {
      assetType: 'glb',
      itemKey: 'glbUrl',
    },
    buttons: [
      'sell',
      'editMount',
      'wearItem',
    ],
    debug: true,
  },
  'vehicle': {
    icon: '/images/vehicle.svg',
    label: 'Vehicle',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createVehicle',
    },
    edit: {
      assetType: 'glb',
      itemKey: 'glbUrl',
    },
    buttons: [
      'sell',
      'editVehicle',
      'wearItem',
    ],
    debug: true,
  },
  'mob': {
    icon: '/images/monster.svg',
    label: 'Mob',
    category: 'entity',
    create: {
      type: 'custom',
      nextType: 'createMob',
    },
    edit: {
      assetType: 'glb',
      itemKey: 'glbUrl',
    },
    buttons: [
      'sell',
      // 'wearItem',
      'editMob',
    ],
    debug: true,
  },
  'skill': {
    icon: '/images/skill.svg',
    label: 'Skill',
    category: 'code',
    create: {
      type: 'custom',
      nextType: 'createSkill',
    },
    buttons: [
      'sell',
      'editSkill',
      'wearItem',
    ],
  },
  'script': {
    icon: '/images/code.svg',
    label: 'Script',
    category: 'code',
    create: {
      type: 'custom',
      nextType: 'createScript',
    },
    buttons: [
      'sell',
      'editScript',
      'wearItem',
    ],
  },

  'panel3D': {
    icon: '/images/comic.svg',
    label: 'Panel3D',
    create: {
      type: 'custom',
      nextType: 'createPanel3D',
    },
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
    debug: true,
  },
  'skybox3D': {
    icon: '/images/360.svg',
    label: 'Skybox3D',
    create: {
      type: 'custom',
      nextType: 'createSkybox3D',
    },
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
    debug: false,
  },
  'item360': {
    icon: '/images/item.svg',
    label: 'Item360',
    create: {
      type: 'custom',
      nextType: 'createItem360',
    },
    drop: {
      metadata: {
        quaternionOffset: new THREE.Quaternion()
          .setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI)
          .toArray(),
      },
    },
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
    debug: false,
  },
  'character360': {
    icon: '/images/character.svg',
    label: 'Character360',
    create: {
      type: 'custom',
      nextType: 'createCharacter360',
    },
    drop: {
      metadata: {
        quaternionOffset: new THREE.Quaternion()
          .setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI)
          .toArray(),
      },
    },
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
    debug: false,
  },
  'mob360': {
    icon: '/images/monster2.svg',
    label: 'Mob360',
    create: {
      type: 'custom',
      nextType: 'createMob360',
    },
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
    debug: true,
  },
  'music': {
    icon: '/images/music.svg',
    label: 'Music',
    category: 'media',
    create: {
      type: 'custom',
      nextType: 'createMusic',
    },
    buttons: [
      'playAudio',
      // 'sell',
    ],
    debug: true,
  },

  'nft': {
    icon: '/images/ethereum.svg',
    label: 'NFT',
    category: 'import',
    buttons: [
      // 'sell',
    ],
    debug: true,
  },

  'prefabs': {
    type: 'prefabs',
    icon: '/images/dots.svg',
    label: 'Prefabs',
    category: 'import',
    buttons: [
      // 'sell',
      // 'wearItem',
    ],
  },
  
};

// Add this new asset type in your existing assetTypes object
assetTypes['icUpload'] = {
  icon: '/images/dots.svg', // Assuming you have an appropriate icon
  label: 'Open IC Vault',
  category: 'media',
  create: {
    type: 'custom',
    nextType: 'createICUpload', // This will be handled by your React component logic
  },
  buttons: [
    // Define any specific actions you want to allow for IC uploaded assets
  ],
};

export const categoryAssetTypes = (() => {
  const result = [];
  for (const k in assetTypes) {
    const assetType = assetTypes[k];
    const {
      category,
    } = assetType;
    if (!result[category]) {
      result[category] = [];
    }
    result[category].push(assetType);
  }
  return result;
})();

export const wearableAssetTypes = [
  'wearable',
  'sittable',
  'mount',
  'voice',
  'skill',
  'script',
];

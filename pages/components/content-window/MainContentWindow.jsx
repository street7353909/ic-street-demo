import React, {
  useState,
  // useEffect,
  // useRef,
} from 'react';

import {
  ProfileWindow,
  WorldsWindow,
  InventoryWindow,
  SceneEditorWindow,
  MarketplaceWindow,
  MapWindow,
  AiAgentsWindow,
  // GenAiWindow,
  SettingsWindow,
} from '../content-window/ContentWindows.jsx';
import {
  CreateNpcContent,
  EditNpcWearablesContent,
  NpcMemoriesContent,
  EditNpcMemoryContent,

  EditProfileCharacterContent,

  CreateVoiceContent,

  CreateCameraContent,

  CreateImageContent,
  GenerateImageContent,

  CreateAudioContent,
  ImportAudioContent,

  CreateVideoContent,
  ImportVideoContent,

  CreateWearableContent,
  CreateSittableContent,
  CreateMountContent,
  CreateVehicleContent,
  CreateMobContent,
  CreateSkillContent,
  CreateScriptContent,

  AssetListContent,
  ChooseAssetContent,

  ChooseVoiceContent,
  ChooseVoicePresetContent,
  ChooseVoiceCustomContent,
  ChooseVoicePackContent,

  PrefabAssetContent,

  CreateVrmContent,
  ImportVrmContent,

  /////////////////////////////////////////////////////////////

  // Code for IC image storage integration
  CreateICUploadContent,

  /////////////////////////////////////////////////////////////

  // GenAiContent,
  CreatePanel3DContent,
  CreateMusicContent,
  CreateItem360Content,
  CreateCharacter360Content,
  CreateMob360Content,
  CreateSkybox3DContent,
  CreateWorldContent,
  ProfileContent,
  InventoryContent,
  WorldsContent,
  AiAgentsContent,
  SettingsContent,

  EditContent,
  AppsContent,

  EditAppContent,

  StoreBuyContent,
  StoreSellContent,
  StoreTradeContent,
  StoreMintContent,
  StoreEditContent,
} from '../content-window/ContentComponents.jsx';

import {
  ContentPathUi,
} from '../content-path/ContentPath.jsx';

//

export const contentPathMap = {
  'profileWindow': ProfileWindow,
  'worldsWindow': WorldsWindow,
  'inventoryWindow': InventoryWindow,
  'sceneEditorWindow': SceneEditorWindow,
  'marketplaceWindow': MarketplaceWindow,
  'mapWindow': MapWindow,
  'aiAgentsWindow': AiAgentsWindow,
  // 'genAiWindow': GenAiWindow,
  'settingsWindow': SettingsWindow,

  'profile': ProfileContent,
  'inventory': InventoryContent,
  'worlds': WorldsContent,
  'aiAgents': AiAgentsContent,
  'settings': SettingsContent,

  'edit': EditContent,
  'apps': AppsContent,
  'editApp': EditAppContent,

  'prefabs': PrefabAssetContent,

  'createVrm': CreateVrmContent,
  'importVrm': ImportVrmContent,

  /////////////////////////////////////////////////////////////

  // Code for IC image storage integration
  'createICUpload': CreateICUploadContent,

  /////////////////////////////////////////////////////////////


  // 'genAi': GenAiContent,
  'createPanel3D': CreatePanel3DContent,
  'createMusic': CreateMusicContent,
  'createItem360': CreateItem360Content,
  'createCharacter360': CreateCharacter360Content,
  'createSkybox3D': CreateSkybox3DContent, // XXX
  'createMob360': CreateMob360Content,

  'createNpc': CreateNpcContent,
  'editNpcWearables': EditNpcWearablesContent,
  'npcMemories': NpcMemoriesContent,
  'editNpcMemory': EditNpcMemoryContent,

  'editProfileCharacter': EditProfileCharacterContent,

  'createVoice': CreateVoiceContent,

  'createCamera': CreateCameraContent,

  'createImage': CreateImageContent,
  'generateImage': GenerateImageContent,

  'createAudio': CreateAudioContent,
  'importAudio': ImportAudioContent,

  'createVideo': CreateVideoContent,
  'importVideo': ImportVideoContent,

  'createWearable': CreateWearableContent,
  'createSittable': CreateSittableContent,
  'createMount': CreateMountContent,
  'createVehicle': CreateVehicleContent,
  'createMob': CreateMobContent,
  'createSkill': CreateSkillContent,
  'createScript': CreateScriptContent,

  'assetList': AssetListContent,
  'chooseAsset': ChooseAssetContent,

  'chooseVoice': ChooseVoiceContent,
  'chooseVoicePreset': ChooseVoicePresetContent,
  'chooseVoiceCustom': ChooseVoiceCustomContent,
  'chooseVoicePack': ChooseVoicePackContent,

  'createWorld': CreateWorldContent,

  'storeBuy': StoreBuyContent,
  'storeSell': StoreSellContent,
  'storeTrade': StoreTradeContent,
  'storeMint': StoreMintContent,

  'storeEdit': StoreEditContent,
};

//

export const MainContentWindow = ({
  contentPath,
  setContentPath,

  ...props
}) => {
  const [epoch, setEpoch] = useState(0);

  return (
    <>
      <ContentPathUi
        contentPath={contentPath}
        setContentPath={setContentPath}
        contentPathMap={contentPathMap}

        epoch={epoch}
        setEpoch={setEpoch}

        {...props}
      />
    </>
  );
}

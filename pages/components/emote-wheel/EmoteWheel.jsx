import * as THREE from 'three';
import React, {useEffect, useRef, useState} from 'react';
import classnames from 'classnames';
import {
  MouseDot,
} from '../mouse-dot/MouseDot.jsx';

import styles from './EmoteWheel.module.css';

import {
  emotes,
  emoteIcons,
} from '../../../packages/engine/managers/emote/emotes.js';

import {
  mod,
  loadImage,
  imageToCanvas,
} from '../../../packages/engine/util.js';
const modPi2 = angle => mod(angle, Math.PI*2);

//

const localVector2D = new THREE.Vector2();

//

const chevronImgSrc = `/images/chevron2.svg`;

const size = 500;
const pixelRatio = window.devicePixelRatio;
const pixelSize = size * pixelRatio;

const numSlices = emotes.length;
const sliceSize = Math.PI*2/numSlices;
const interval = Math.PI*0.01;

// const centerCoords = [
//   size / 2,
//   size / 2,
// ];

const outerRadius = pixelSize/2;
const innerRadius = pixelSize/4;
const radiusCenter = (outerRadius + innerRadius) / 2;

const outerRadiusSoft = pixelSize/4 - 10;
const innerRadiusSoft = pixelSize/8;
const radiusCenterSoft = (outerRadiusSoft + innerRadiusSoft) / 2;

const iconSize = 80;
const chevronSize = 50;

export const EmoteWheel = ({
  engine,
}) => {
  const [open, setOpen] = useState(false);
  const [down, setDown] = useState(false);
  // const [selectedSlice, _setSelectedSlice] = useState(-1);
  // const [selectedDepth, setSelectedDepth] = useState(-1);
  const [selectedState, _setSelectedState] = useState(() => [-1, -1]);
  const [coords, _setCoords] = useState(() => [0, 0]);
  const [emoteIconImages, setEmoteIconImages] = useState(null);
  const [chevronImage, setChevronImage] = useState(null);
  const canvasRef = useRef();
  const mousedotRef = useRef();

  //

  const setSelectedState = (newSelectedState) => {
    selectedState[0] = newSelectedState[0];
    selectedState[1] = newSelectedState[1];
    _render();
  };

  const setCoords = (newCoords) => {
    coords[0] = newCoords[0];
    coords[1] = newCoords[1];
    _updateCoords();
  }
  const _updateCoords = () => {
    localVector2D.fromArray(coords);
    const centerDistance = localVector2D.length() * pixelRatio;
    const angle = modPi2(Math.atan2(coords[1], coords[0]) + Math.PI/2);
    const sliceIndex = Math.floor(angle/sliceSize);

    if (centerDistance > innerRadius) {
      setSelectedState([sliceIndex, 1]);
    } else if (centerDistance > innerRadiusSoft) {
      setSelectedState([sliceIndex, 0]);
    } else {
      setSelectedState([-1, -1]);
    }

    const mouseDotEl = mousedotRef.current;
    if (mouseDotEl) {
      mouseDotEl.style.transform = `translate(${coords[0]}px, ${coords[1]}px)`;
    }
  };

  //

  const {sounds} = engine;

  // bind io events
  useEffect(() => {
    if (engine) {
      const {
        ioManager,
        emoteManager,
      } = engine;

      const keydown = e => {
        const {pointerLockManager} = engine;
    
        if (!open) {
          // if (game.inputFocused()) return true;
    
          if (!e.repeat) {
            if (/q/i.test(e.key) && !e.ctrlKey) {
              // cameraManager.requestPointerLock();
    
              // IoBus.request({
              //   contentWindow: globalThis.parent,
              //   method: 'requestPointerLock',
              // });
              pointerLockManager.requestPointerLock();
    
              // IoBus.request({
              //   contentWindow: globalThis.parent,
              //   method: 'playSoundName',
              //   args: {
              //     name: 'menuOk',
              //   },
              // });
              sounds.playSoundName('menuOk');
    
              // const emote = _getSelectedEmote();
              // emote && emoteManager.triggerEmote(emote);
              // sounds.playSoundName('menuOk');
    
              setOpen(true);
              setDown(false);
              setCoords([0, 0]);
    
              return true;
            }
          }
        }
        return false;
      };
      // const keypress = e => {
      //   const {key} = e;
      // };
      function keyup(e) {
        // console.log('emote wheel keyup', open, e.key);
        
        if (open) {
          if (/q/i.test(e.key)) {
            const emote = _getSelectedEmote();
            if (emote) {
              // IoBus.request({
              //   contentWindow: globalThis.parent,
              //   method: 'triggerEmote',
              //   args: {
              //     emote,
              //   },
              // });
              
              // emoteManager.triggerEmote(emoteName, player = this.playersManager.getLocalPlayer());
              // console.log('trigger emote 1', {emote});
              emoteManager.triggerEmote(
                emote,
              );
    
              // IoBus.request({
              //   contentWindow: globalThis.parent,
              //   method: 'playSoundName',
              //   args: {
              //     name: 'menuBack',
              //   },
              // });
              sounds.playSoundName('menuBack');
            }
    
            setOpen(false);
            setDown(false);
          }
        }
    
        return false;
      }
      function mousemove(e) {
        if (open) {
          const {movementX, movementY} = e;
      
          const [x, y] = coords;
          setCoords([
            x + movementX,
            y + movementY,
          ]);

          return true;
        } else {
          return false;
        }
      }
      function mousedown(e) {
        setDown(true);
    
        return false;
      }
      function mouseup(e) {
        setDown(false);

        if (open) {
          setOpen(false);
    
          const emote = _getSelectedEmote();
          if (emote) {
            emoteManager.triggerEmote(
              emote,
            );
    
            sounds.playSoundName('menuNext');
          }
        }
    
        return false;
      }
      function wheel(e) {
        // nothing
        return false;
      }

      ioManager.registerEventHandler('keydown', keydown);
      // ioManager.registerEventHandler('keypress', keypress);
      ioManager.registerEventHandler('keyup', keyup);
      ioManager.registerEventHandler('mousemove', mousemove);
      ioManager.registerEventHandler('mousedown', mousedown);
      ioManager.registerEventHandler('mouseup', mouseup);
      ioManager.registerEventHandler('wheel', wheel);

      return () => {
        ioManager.unregisterEventHandler('keydown', keydown);
        // ioManager.unregisterEventHandler('keypress', keypress);
        ioManager.unregisterEventHandler('keyup', keyup);
        ioManager.unregisterEventHandler('mousemove', mousemove);
        ioManager.unregisterEventHandler('mousedown', mousedown);
        ioManager.unregisterEventHandler('mouseup', mouseup);
        ioManager.unregisterEventHandler('wheel', wheel);
      };
    }
  }, [
    engine,
    open,
    // coords,
    // selectedSlice,
    // selectedDepth,
    mousedotRef.current
  ]);

  // const setSelectedSlice = v => {
  //   if (v !== selectedSlice) {
  //     _setSelectedSlice(v);
  //     // XXX this needs to be quieter (configurable per-sound volume)
  //     // sounds.playSoundName('menuClick');
  //   }
  // };

  const _getSelectedEmote = () => {
    const [
      selectedSlice,
      selectedDepth,
    ] = selectedState;

    if (selectedSlice !== -1 && selectedDepth !== -1) {
      return emotes[selectedSlice] + (selectedDepth === 0 ? 'Soft' : '');
    } else {
      return null;
    }
  };

  useEffect(() => {
    (async () => {
      const emoteIconImages = await Promise.all(emoteIcons.map(async icon => {
        const img = await loadImage(`/images/poses/${icon}`);
        const canvas = imageToCanvas(img, iconSize, iconSize);
        return canvas;
      }));
      setEmoteIconImages(emoteIconImages);
    })();
  }, []);
  useEffect(() => {
    (async () => {
      const img = await loadImage(chevronImgSrc);
      const canvas = imageToCanvas(img, chevronSize, chevronSize);
      setChevronImage(canvas);
    })();
  }, []);

  const _render = () => {
    const wheelCanvas = canvasRef.current;
    if (wheelCanvas && emoteIconImages && chevronImage) {
      const [
        selectedSlice,
        selectedDepth,
      ] = selectedState;
      // console.log('render', {
      //   selectedState,
      //   selectedSlice,
      //   selectedDepth,
      // });

      const ctx = wheelCanvas.getContext('2d');
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';

      ctx.clearRect(0, 0, pixelSize, pixelSize);
    
      for (let i = 0; i < numSlices; i++) {
        const startAngle = i*sliceSize + interval - Math.PI/2;
        const endAngle = (i+1)*sliceSize - interval - Math.PI/2;
        
        {
          const selected = i === selectedSlice && selectedDepth === 0;
          ctx.fillStyle = selected ? '#4fc3f7' : '#111';
          ctx.beginPath();
          ctx.arc(pixelSize/2, pixelSize/2, outerRadiusSoft, startAngle, endAngle, false);
          ctx.arc(pixelSize/2, pixelSize/2, innerRadiusSoft, endAngle, startAngle, true);
          ctx.fill();
        }
        {
          const selected = i === selectedSlice && selectedDepth === 1;
          ctx.fillStyle = selected ? '#4fc3f7' : '#111';
          ctx.beginPath();
          ctx.arc(pixelSize/2, pixelSize/2, outerRadius, startAngle, endAngle, false);
          ctx.arc(pixelSize/2, pixelSize/2, innerRadius, endAngle, startAngle, true);
          ctx.fill();
        }
        {
          const midAngle = (startAngle + endAngle)/2;

          // soft chevron
          {
            ctx.save();
            ctx.globalAlpha = 0.3;
            ctx.translate(
              pixelSize/2 + Math.cos(midAngle)*radiusCenterSoft,
              pixelSize/2 + Math.sin(midAngle)*radiusCenterSoft
            );
            ctx.rotate(midAngle + Math.PI);
            ctx.translate(-chevronSize/2, -chevronSize/2);
            ctx.drawImage(
              chevronImage,
              0,
              0
            );
            ctx.restore();
          }
          
          // hard icon
          ctx.drawImage(
            emoteIconImages[i],
            pixelSize/2 + Math.cos(midAngle)*radiusCenter - iconSize/2,
            pixelSize/2 + Math.sin(midAngle)*radiusCenter - iconSize/2 - pixelSize/30
          );

          // hard label
          ctx.font = (pixelSize/30) + 'px Muli';
          ctx.fillStyle = '#FFF';
          ctx.fillText(
            emotes[i],
            pixelSize/2 + Math.cos(midAngle)*radiusCenter,
            pixelSize/2 + Math.sin(midAngle)*radiusCenter + pixelSize/20
          );
        }
      }
    }
  };
  useEffect(() => {
    _render();
  }, [canvasRef.current, emoteIconImages, chevronImage, open]);

  //

	return (
    <div className={classnames(
      styles.emoteWheel,
      open ? styles.open : null,
    )}>
      <div className={styles.container} >
        <canvas
          className={styles.canvas}
          width={pixelSize}
          height={pixelSize}
          ref={canvasRef}
        />
        {/* <LightArrow
          enabled={open}
          down={down}
          ax={coords[0] + centerCoords[0]}
          ay={coords[1] + centerCoords[1]}
        /> */}
        <MouseDot
          enabled={open}
          down={down}
          ref={mousedotRef}
        />
      </div>
      {/* <Io
        fns={{
          keydown,
          keypress,
          keyup,
          mousemove,
          mousedown,
          mouseup,
          wheel,
        }}
      /> */}
    </div>
  );
};
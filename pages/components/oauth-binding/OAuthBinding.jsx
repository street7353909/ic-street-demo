import {
  useState,
  useEffect,
  // useRef,
  // createContext,
  // useContext,
} from 'react';

//

export const OAuthBinding = ({
  supabaseClient,
}) => {
  const [profile, setProfile] = useState(() => supabaseClient.getProfile());

  // update profile when supabase client changes it
  useEffect(() => {
    const profileupdate = e => {
      setProfile(e.data.profile);
    };
    supabaseClient.addEventListener('profileupdate', profileupdate);

    return () => {
      supabaseClient.removeEventListener('profileupdate', profileupdate);
    };
  }, [
    supabaseClient,
  ]);

  // handle oauth
  useEffect(() => {
    const u = new URL(location.href);
    const {searchParams} = u;
    const state = searchParams.get('state');
    let match;
    // the first paramater is the real redirect we will use in the browser
    // the second parameter is the redirect we will use in the oauth flow
    if (match = state && state.match(/^vroidhub (.+?) (.+?)$/)) {
      const u2 = new URL(match[1]);
      u2.searchParams.set('state', `vroidhub2 ${match[1]} ${match[2]}`);
      const code = searchParams.get('code');
      u2.searchParams.set('code', code);
      location.href = u2;
    } else if (match = state && state.match(/^vroidhub2 (.+?) (.+?)$/)) {
      const code = searchParams.get('code');
      const redirect_uri = match[2];

      if (!!profile?.user && profile?.user?.oauth?.code !== code) {
        console.log('got vroid hub oauth redirect yes', {
          code,
          redirect_uri,
        });

        (async () => {
          const endpointUrl = `https://ai-proxy.upstreet.ai/api/vroidHub/oauth`;
          const u2 = new URL(endpointUrl);
          u2.searchParams.set('code', code);
          u2.searchParams.set('redirect_uri', redirect_uri);
          const res = await fetch(u2, {
            method: 'POST',
            redirect: 'manual',
          });
          if (res.ok) {
            const j = await res.json();
            const {access_token, error} = j;

            if (access_token) {
              const newOAuth = {
                ...profile?.user?.oauth,
                vroidHub: {
                  code,
                  access_token,
                },
              };

              await supabaseClient.updateUser({
                oauth: newOAuth,
              });
              console.log('update user', {newOAuth});
            } else {
              console.log('vroid hub login error', {error});
            }
          } else {
            const text = await res.text();
            console.warn('got oauth error', res.status, text, profile);
          }

          // either way, clear the state from the url using history.replaceState
          const u3 = new URL(location.href);
          u3.searchParams.delete('state');
          u3.searchParams.delete('code');
          history.replaceState({}, '', u3);
        })();
      }
    }
  }, [
    profile,
  ]);

  return null;
};
// import * as THREE from 'three';

export default ctx => {
  const {
    useEngineRenderer,
    useCameraManager,
  } = ctx;

  const engineRenderer = useEngineRenderer();
  const cameraManager = useCameraManager();
  cameraManager.setControllerFn(() => {
    const {
      camera,
    } = engineRenderer;

    const now = performance.now();
    const rotationTime = 10 * 1000;
    const radius = 3;
    camera.position.set(
      Math.cos(now / rotationTime) * radius,
      1.6,
      Math.sin(now / rotationTime) * radius,
    );
    camera.lookAt(0, 1.6, 0);
    camera.updateMatrixWorld();

    // const camera = new THREE.PerspectiveCamera(45, 1, 0.1, 1000);
    // camera.position.set(0, 1.6, 0);
    // camera.rotation.order = 'YXZ';
    // camera.rotation.y = Math.PI;
    // camera.rotation.x = Math.PI * 0.5;
    // return camera;
  });
};
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import {
  mod
} from '../../../packages/engine/util.js';

import {
  LightArrow,
} from '../light-arrow/LightArrow.jsx';

import styles from '../../../styles/MainMenu.module.css';

const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

//

const Option = ({
  icon,
  option,
  selected,
  setSelected,
  active,
  setActive,
  submit,
  isAlt=false,
}) => {
  return (
    <li
      className={classnames(
        styles.option,
        selected ? styles.selected : null,
        (selected && active) ? styles.active : null,
        isMobile ? styles.mobile : null,
        isAlt ? styles.alt : null,
      )}
      onMouseMove={setSelected}
      onMouseLeave={() => setActive(false)}
      onMouseDown={() => setActive(true)}
      onMouseUp={() => setActive(false)}
      onClick={submit}
    >
    <div className={classnames(isMobile ? styles.backgroundMobile : null, styles.background, isAlt ? styles.alt : null)}></div>
      {selected && !isMobile ? <LightArrow
        className={styles.lightArrow}
        direction='down'
      /> : null}
      <div className={classnames(styles.text, isMobile ? styles.textMobile : null, isAlt ? styles.altButton : null)}>{icon && <img className={classnames(styles.text, styles.icon, isAlt ? styles.iconAlt : null)} src={isAlt ? icon.replace('.svg', '_alt.svg') : icon} />}<span className={styles.textInner}>{option}</span></div>
    </li>
  );
};

//

export const MainMenu = ({
  enabled = true,
  className,
  // loginValue,
  options,
  onCursorChange,
  onKeyPathChange,
  onSelect,
} = {}) => {
  const [cursorPosition, setCursorPosition] = useState(0);
  const [keyPath, setKeyPath] = useState([]);
  const [currentOptions, setCurrentOptions] = useState(() => options);
  const [active, setActive] = useState(false);

  // keyPath handling
  useEffect(() => {
    let result = options;
    for (let i = 0; i < keyPath.length; i++) {
      const key = keyPath[i];
      const nextOptions = result[key].options;
      if (nextOptions) {
        result = result[key].options;
      } else {
        break;
      }
    }
    setCurrentOptions(result);
  }, [
    keyPath,
    currentOptions,
  ]);

  // initialize cursor position
  useEffect(() => {
    if (enabled) {
      setCursorPosition(0);
    }
  }, [
    enabled,
  ]);

  // key up/down handling
  useEffect(() => {
    const keydown = e => {
      switch (e.key) {
        case 'ArrowUp':
        case 'ArrowLeft':
        {
          const nextCursorPosition = mod(cursorPosition - 1, currentOptions.length);
          setCursorPosition(nextCursorPosition);
          onCursorChange && onCursorChange(nextCursorPosition);
          break;
        }
        case 'ArrowDown':
        case 'ArrowRight':
        {
          const nextCursorPosition = mod(cursorPosition + 1, currentOptions.length);
          setCursorPosition(nextCursorPosition);
          onCursorChange && onCursorChange(nextCursorPosition);
          break;
        }
        case 'Enter': {
          enabled && setActive(true);
          break;
        }
      }
    };
    globalThis.addEventListener('keydown', keydown);
    const keyup = e => {
      switch (e.key) {
        case 'Enter': {
          if (enabled) {
            setActive(false);
            select();
          }
          break;
        }
        case 'Escape':
        case 'Backspace':
        {
          if (keyPath.length > 0) {
            const newKeyPath = keyPath.slice(0, -1);
            setKeyPath(newKeyPath);
            onKeyPathChange && onKeyPathChange(newKeyPath);

            const nextCursorPosition = keyPath[keyPath.length - 1];
            setCursorPosition(nextCursorPosition);
            onCursorChange && onCursorChange(nextCursorPosition);
          }
          break;
        }
      }
    };
    globalThis.addEventListener('keyup', keyup);

    return () => {
      globalThis.removeEventListener('keydown', keydown);
      globalThis.removeEventListener('keyup', keyup);
    };
  }, [
    enabled,
    keyPath,
    currentOptions,
    cursorPosition,
  ]);

  const select = (_cursorPosition = cursorPosition) => {
    onSelect && onSelect(_cursorPosition);

    const currentOption = currentOptions[cursorPosition];
    const {
      handler,
      options: suboptions,
    } = currentOption;
    if (handler) {
      handler();
    } else if (suboptions) {
      const newKeyPath = [
        ...keyPath,
        _cursorPosition,
      ];
      setKeyPath(newKeyPath);
      onKeyPathChange && onKeyPathChange(newKeyPath);

      setCursorPosition(0);
      onCursorChange && onCursorChange(0);
    } else {
      console.warn('invalid option spec', currentOption);
      throw new Error('invalid option spec');
    }
  };

  //

  return (
    <div className={classnames(
      styles.mainMenu,
      className,
    )}>
      <div className={styles.wrap}>
        <ul className={classnames(isMobile ? styles.optionsMobile : null, styles.options)}>
          {currentOptions.map((option, i) => (
            <Option
              option={option.label}
              selected={i === cursorPosition}
              active={active}
              setActive={setActive}
              setSelected={e => {
                if (cursorPosition !== i) {
                  setCursorPosition(i);
                  onCursorChange && onCursorChange(i);
                }
              }}
              submit={e => {
                isMobile && setCursorPosition(i);
                select(i);
              }}
              key={option.label}
            />
          ))}
        </ul>
      </div>
    </div>
  );
};

import React, {
  useState,
  useEffect,
  useRef,
  // createContext,
  // useContext,
} from 'react';
// import classnames from 'classnames';

// export const LoginContext = createContext();
export const LoginProvider = ({
  supabaseClient,
  children,
}) => {
  const [profile, setProfile] = useState(() => supabaseClient.getProfile());

  // update profile when supabase client changes it
  useEffect(() => {
    const profileupdate = e => {
      setProfile(e.data.profile);
    };
    supabaseClient.addEventListener('profileupdate', profileupdate);

    setProfile(supabaseClient.getProfile());

    return () => {
      supabaseClient.removeEventListener('profileupdate', profileupdate);
    };
  }, [
    supabaseClient,
  ]);

  return children(profile);
};

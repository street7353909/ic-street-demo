export const dragStartType = type => (e, item = {}, metadata = {}) => {
  const jsonFile = JSON.stringify(item, null, 2);
  e.dataTransfer.setData(`application/${type}`, jsonFile);

  const metadataFile = JSON.stringify(metadata, null, 2);
  e.dataTransfer.setData(`application/metadata`, metadataFile);
};
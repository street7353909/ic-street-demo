import * as THREE from 'three';
import {
  sha256,
} from 'js-sha256';
import physicsManager from '../../../packages/engine/physics/physics-manager.js';

import {
  SupabaseFsWorker,
} from '../../../packages/engine/supabase-fs-worker.js';

import {
  getScreenshotBlob,
} from '../../../packages/engine/preview-screenshot.js';

//

const localVector = new THREE.Vector3();
const localVector2 = new THREE.Vector3();
const localVector2D = new THREE.Vector2();
const localQuaternion = new THREE.Quaternion();
const localQuaternion2 = new THREE.Quaternion();
const localEuler = new THREE.Euler();
const localMatrix = new THREE.Matrix4();
const localRaycaster = new THREE.Raycaster();

const zeroVector = new THREE.Vector3();
const upVector = new THREE.Vector3(0, 1, 0);

// helper methods

const normalizeExt = ext => {
  switch (ext) {
    case 'png':
    case 'jpg':
    case 'jpeg':
    {
      ext = 'image';
      break;
    }
  }
  return ext;
};

// drop methods

export const handleDropFn = ({
  engine,
  supabaseClient,
  sessionUserId,
  appManagerName,
}) => {
  // if (typeof appManagerName !== 'string') {
  //   debugger;
  // }
  return async e => {

  let {files, items} = e.dataTransfer;
  files = Array.from(files);
  items = Array.from(items);

  const normalFiles = [];
  const metadataFiles = [];
  for (const file of files) {
    if (file.type === 'application/metadata') {
      metadataFiles.push(file);
    } else {
      normalFiles.push(file);
    }
  }

  const normalItems = [];
  const metadataItems = [];
  for (const item of items) {
    if (item.type === 'application/metadata') {
      metadataItems.push(item);
    } else {
      normalItems.push(item);
    }
  }

  const file = normalFiles[0];
  const item = normalItems[0];
  const metadataFile = metadataFiles[0];
  const metadataItem = metadataItems[0];

  // console.log('drop files/items', {
  //   files: Array.from(files).map(f => [f.kind, f.type, f.name]),
  //   items: Array.from(items).map(i => [i.kind, i.type, i.name]),
  // });

  let f;
  let source;
  let metadata;

  const filePromise = (async () => {
    if (file) {
      f = file;
      source = 'file';
    } else if (item) {
      let ext = item.type.match(/([^\/]*)$/)[1];
      ext = normalizeExt(ext);
      const fileName = item.type.replace(/([^\/]+)$/, 'file.$1');

      // read string
      const s = await new Promise((accept, reject) => {
        item.getAsString(accept);
      });

      f = new File([s], fileName, {
        type: 'application/' + ext,
      });
      source = 'json';
    } else {
      throw new Error('no file dropped');
    }
  })();

  // get the metadata, if any
  // console.log('got metadata 1');
  const metadataPromise = (async () => {
    if (metadataFile) {
      // console.log('metadata file 1', metadataFile.json);
      metadata = await metadataFile.json();
    } else if (metadataItem) {
      // console.log('metadata file 2', metadataItem, metadataItem.getAsString);
      const metadataString = await new Promise((accept, reject) => {
        metadataItem.getAsString(accept);
      });
      // console.log('got string', {metadataString});
      metadata = JSON.parse(metadataString);
    } else {
      metadata = null;
    }
  })();

  await Promise.all([
    filePromise,
    metadataPromise,
  ]);

  // console.log('import source/metadata', {
  //   source,
  //   metadata,
  // });
  // debugger;

  const positionOffset = metadata?.positionOffset ?? null;
  const quaternionOffset = metadata?.quaternionOffset ?? null;
  const components = metadata?.components ?? [];

  // handle file upload to inventory
  if (source === 'file') {
    // console.log('load file', f);

    const match = f.name.match(/^(.+)\.([^\.]*)$/)
    let ext = match[2];
    ext = normalizeExt(ext);
    const fileMetadata = await importFileType(ext)(f, supabaseClient, sessionUserId);
    await addAsset(fileMetadata);
    // refreshItems();
  } else if (source === 'json') {
    // console.log('load internal', f);

    const physicsScene = physicsManager.getScene();
    const {engineRenderer, appManagerContext} = engine;
    const {renderer, camera} = engineRenderer;
    const canvas = renderer.domElement;

    const pixelRatio = renderer.getPixelRatio();

    const localMouse = localVector2D.set(
      e.clientX / canvas.width * pixelRatio * 2 - 1,
      -(e.clientY / canvas.height * pixelRatio) * 2 + 1,
    );
    localRaycaster.setFromCamera(localMouse, camera);

    const p = localRaycaster.ray.origin.clone();
    const q = localQuaternion.setFromRotationMatrix(
      localMatrix.lookAt(
        zeroVector,
        localRaycaster.ray.direction,
        upVector,
      ),
    );

    const intersection = physicsScene.raycast(p, q);
    if (intersection) {
      const appManager = appManagerContext.getAppManager(appManagerName);
      const position = localVector.fromArray(intersection.point);
      
      if (positionOffset) {
        position.add(
          localVector2.fromArray(positionOffset)
        );
      }
      if (quaternionOffset) {
        q.premultiply(
          localQuaternion2.fromArray(quaternionOffset)
        );
      }

      localEuler.setFromQuaternion(q, 'YXZ');
      localEuler.x = 0;
      localEuler.z = 0;
      const quaternion = localQuaternion2.setFromEuler(localEuler);

      dropFile(f, appManager, position, quaternion, components);
    } else {
      console.warn('no intersection', {
        p,
        q,
      });
    }
  } else {
    console.warn('unknown file source: ' + f.name + ' ' + source);
  }
};
};

// file import methods

const cleanSupabaseName = name => {
  return name
    .replace(/[\u007f-\uffff]/g, c => {
      // return '\\u'+('0000'+c.charCodeAt(0).toString(16)).slice(-4);
      return '_u'+('0000'+c.charCodeAt(0).toString(16)).slice(-4);
    })
    .replace(/\[+/g, '(')
    .replace(/\]+/g, ')');
};
export const importFileType = ext => async (f, supabaseClient, sessionUserId) => {
  // get the name part
  // const name = f.name.replace(/\.[^\.]*$/, '');
  let {name} = f;
  // escape unicode with its code point
  name = cleanSupabaseName(name);

  // compute the sha256 hash of the file
  const sha256Sum = await (async () => {
    const arrayBuffer = await f.arrayBuffer();
    // get the sha hash of the file
    const hash = sha256.create();
    hash.update(arrayBuffer);
    const sha256Sum = hash.hex();
    return sha256Sum;
  })();

  // initialize the supabase fs worker
  const supabaseFsWorker = new SupabaseFsWorker({
    supabase: supabaseClient.supabase,
    bucketName: 'public',
  });

  //

  const [
    start_url,
    preview_url,
  ] = await Promise.all([
    (async () => {
      // upload the data file to supabase storage
      const keyPath = ['assets', sha256Sum, name];
      const start_url = await supabaseFsWorker.writeFile(keyPath, f);
      return start_url;
    })(),
    (async () => {
      try {
        // take the screenshot
        const blob = await getScreenshotBlob(f, ext);
        const u = URL.createObjectURL(blob);

        // upload the preview image file to supabase storage
        const previewFileName = `${name}.png`;
        const keyPath = ['previews', sha256Sum, previewFileName];
        const preview_url = await supabaseFsWorker.writeFile(keyPath, blob);

        return preview_url;
      } catch(err) {
        console.warn('failed to take screenshot', err);
        return '/assets/icons/backpack.svg';
      }
    })(),
  ]);

  return {
    start_url,
    preview_url,

    name,
    ext,

    supabaseClient,
    sessionUserId,
  };
};
export const addAsset = async ({
  id = crypto.randomUUID(),
  start_url,
  preview_url,

  name,
  ext,

  supabaseClient,
  sessionUserId,
}) => {
  const asset = {
    id,
    type: ext,
    name,
    start_url,
    preview_url,
    user_id: sessionUserId,
  };
  const result = await supabaseClient.supabase
    .from('assets')
    .upsert(asset);
};

// Updated to work with URLs for loading our Internet Computer assets.
const dropFile = async (f, appManager, position, quaternion, components) => {
  // Helper function to check if a string is JSON
  const isJSON = str => {
    try {
      JSON.parse(str);
      return true;
    } catch {
      return false;
    }
  };

  // Read the file content
  const content = await new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = e => {
      resolve(e.target.result);
    };
    reader.onerror = error => reject(error);
    reader.readAsText(f);
  });

  // Check if the content is JSON
  if (isJSON(content)) {
    // If it's JSON, parse it
    const j = JSON.parse(content);
    const app = await appManager.addAppAsync({
      start_url: j.start_url,
      position,
      quaternion,
      components,
    });
  } else {
    // If it's not JSON, assume it's a URL and use it directly
    const app = await appManager.addAppAsync({
      start_url: content,
      position,
      quaternion,
      components,
    });
  }
};

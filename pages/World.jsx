import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
  useContext,
} from 'react';
import classnames from 'classnames';

import {
  HelperUi,
} from './components/helper-ui/HelperUi.jsx';
import {
  IoBusEventSource,
} from './components/io-bus/IoBusEventSource.jsx';
import {
  DragAndDrop,
} from './components/drag-and-drop/DragAndDrop.jsx';
import {
  OAuthBinding,
} from './components/oauth-binding/OAuthBinding.jsx';

import {
  EngineContext,
} from '../packages/engine/engine-context.js';

import {
  ChatUi,
} from './components/chat-ui/ChatUi.jsx';
import {
  GameHeader,
} from './components/game-header/GameHeader.jsx';
import {
  StoryUi,
} from './components/story-ui/StoryUi.jsx';
import {
  CrosshairUi,
} from './components/crosshair-ui/CrosshairUi.jsx';
import {
  LoadingUi,
} from './components/loading-ui/LoadingUi.jsx';

import {
  EngineProvider,
} from '../packages/engine/clients/engine-client.js';

import {
  handleDropFn,
} from './components/drag-and-drop/drop.js';

import {
  multiplayerEndpointUrl,
} from '../packages/engine/endpoints.js';

import {
  EmoteWheel,
} from './components/emote-wheel/EmoteWheel.jsx';

import {
  // LoginContext,
  LoginProvider,
  // LoginConsumer,
} from './components/login-provider/LoginProvider.jsx';

import {
  isProd,
  isMultiplayer,
} from '../packages/engine/env.js';

import styles from '../styles/Adventure.module.css';

//

const WorldBinding = ({
  // engine,
  supabaseClient,

  landObjects,
  setLandObjects,

  world,
}) => {
  // bind world database state
  useEffect(() => {
    if (supabaseClient) {
      (async () => {
        const newObjects = await (async () => {
          const {supabase} = supabaseClient;
          const {
            data,
          } = await supabase
            .from('worlds')
            .select('*')
            .eq('name', world)
            .maybeSingle();
          // console.log('load world data 1', data);
          const {
            objects: newObjects = [],
          } = (data || {});
          return newObjects;
        })();

        const subArray2 = [];
        for (const object of newObjects) {
          subArray2.push(object);
        }
        if (subArray2.length === 0) {
          subArray2.push({
            start_url: '/core-modules/floor/index.js',
          });
          subArray2.push({
            start_url: '/core-modules/asset-preview-background/index.js',
          });
        }
        setLandObjects(subArray2);
      })();
    } else {
      setLandObjects([]);
    }
  }, [
    world,
    supabaseClient,
  ]);

  //

  return null;
};

//

const WorldAppContent = ({
  world,

  // localStorageManager,
  context,

  sessionLoaded,
  // supabaseClient,
  sessionUserId,
  address,
  // defaultUser,
  user,
  isDefaultUser,

  debug,
}) => {
  const [engine, setEngine] = useState(null);
  // const [engineLoading, setEngineLoading] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const appManagerName = 'world:' + world;
  const [appStores, setAppStores] = useState(() => {
    return {
      [appManagerName]: {
        objects: [],
        editable: true,
      },
    };
  });
  const landObjects = appStores[appManagerName];
  const setLandObjects = (landObjects) => {
    const newAppStores = structuredClone(appStores);
    newAppStores[appManagerName].objects = landObjects;
    setAppStores(newAppStores);
  };

  const [tracker, setTracker] = useState(null);

  const [contentPath, setContentPath] = useState([]);
  const [changed, setChanged] = useState(false);

  const [canvas, setCanvas] = useState(null);
  const canvasRef = useRef();

  const {
    localStorageManager,
    supabaseClient,
  } = context;

  //

  // initialize scene tracker
  useEffect(() => {
    if (engine) {
      const appManager = engine.appManagerContext.getAppManager(appManagerName);
      const newTracker = new SceneTracker({
        appManager,
        world,
        supabaseClient,
      });
      setTracker(newTracker);
    }
  }, [
    engine,
  ]);

  // bind canvas
  useEffect(() => {
    if (canvasRef.current) {
      setCanvas(canvasRef.current);
    }
  }, [canvasRef]);

  // bind loading manager
  useEffect(() => {
    if (engine) {
      (async () => {
        await engine.engineRenderer.waitForRender();
        setLoaded(true);
      })();
    }
  }, [
    engine,
  ]);

  // bind multiplayer
  useEffect(() => {
    if (engine && isMultiplayer) {
      (async () => {
        const newMultiplayer = engine.createMultiplayer();

        await newMultiplayer.connectMultiplayer({
          endpoint_url: multiplayerEndpointUrl,
          room: world,
        });
      })();
    }
  }, [
    engine,
  ]);

  //

  return (
    <>
      <LoadingUi
        loadingManager={context.loadingManager}
        loaded={loaded}
      />

      <canvas className={classnames(
        styles.canvas,
      )} ref={canvasRef} />
      <IoBusEventSource engine={engine} />

      {engine ? <CrosshairUi
        engine={engine}
      /> : null}

      <ChatUi
        engine={engine}
        supabaseClient={supabaseClient}
        worldId={appManagerName}
      />

      {engine && <EmoteWheel
        engine={engine}
      />}

      {engine && <StoryUi
        engine={engine}
      />}

      <HelperUi
        localStorageManager={localStorageManager}
        loaded={loaded}
      />

      <GameHeader
        engine={engine}
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}
        address={address}
        user={user}
        isDefaultUser={isDefaultUser}
        // accountManager={accountManager}

        contentPath={contentPath}
        setContentPath={setContentPath}

        changed={changed}
        setChanged={setChanged}

        // getSceneJson={getSceneJson}
        // setSceneJson={setSceneJson}
        // onSave={onSave}
        sceneTracker={tracker}

        appManagerName={appManagerName}

        hidable
        debug={debug}

        loaded={loaded}
      />

      {/* {sessionLoaded && <ProfileBinding
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}

        defaultUser={defaultUser}
        user={user}
        setUser={setUser}

        accountManager={accountManager}
        setAccountManager={setAccountManager}
      />} */}

      <WorldBinding
        supabaseClient={supabaseClient}

        landObjects={landObjects}
        setLandObjects={setLandObjects}

        world={world}
      />

      <DragAndDrop
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}

        onDrop={async (e) => {
          await handleDropFn({
            engine,
            supabaseClient,
            sessionUserId,
            appManagerName,
          })(e);

          setChanged(true);
          tracker.save({});
        }}
      />

      {canvas ? <EngineProvider
        canvas={canvas}
        context={context}

        appStores={appStores}
        playerSpec={user ? user.playerSpec : undefined}

        engine={engine}
        setEngine={setEngine}
      /> : null}
    </>
  );
};

//

class SceneTracker extends EventTarget {
  constructor({
    supabaseClient,
    appManager,
    world,
  }) {
    super();

    this.supabaseClient = supabaseClient;
    this.appManager = appManager;
    this.world = world;

    this.name = '';
    this.description = '';
    this.previewImg = '';
    this.editable = !!world;

    this.#listen();
  }

  #listen() {
    if (this.world) {
      (async () => {
        const result = await this.supabaseClient.supabase
          .from('worlds')
          .select('*')
          .eq('name', this.world)
          .maybeSingle();
        const {
          data,
        } = result;
        const {
          name,
          description,
          preview_url: previewImg,
        } = data;

        this.name = name;
        this.description = description;
        this.previewImg = previewImg;

        this.dispatchEvent(new MessageEvent('nameupdate', {
          data: {
            name,
          },
        }));
        this.dispatchEvent(new MessageEvent('descriptionupdate', {
          data: {
            description,
          },
        }));
        this.dispatchEvent(new MessageEvent('previewimgupdate', {
          data: {
            previewImg,
          },
        }));
      })();
    }
  }

  getId() {
    return ['world', this.world].join(':');
  }

  getName() {
    return this.name;
  }
  getDescription() {
    return this.description;
  }
  getPreviewImg() {
    return this.previewImg;
  }
  getApps() {
    return this.appManager.getApps();
  }

  async getSceneJson() {
    const {
      data: oldLand,
    } = await this.supabaseClient.supabase
      .from('worlds')
      .select('*')
      .eq('name', this.world)
      .maybeSingle();

    const apps = this.getApps();
    let objects = apps.map(app => app.toJson());
    const land = {
      ...oldLand,
      objects,
    };
    return land;
  }
  async setSceneJson(json) {
    await this.appManager.loadJson(json);
  }
  async save(o) {
    let json = await this.getSceneJson();
    json = {
      ...json,
      ...o,
    };

    const result = await this.supabaseClient.supabase
      .from('worlds')
      .upsert(json);
  }
}

//

export const WorldApp = ({
  world = '',
  // avatar,

  debug,
}) => {
  const [context, setContext] = useState(() => new EngineContext());

  const {
    supabaseClient,
  } = context;

  //

  return (
    <>
      <OAuthBinding
        supabaseClient={supabaseClient}
      />
      <LoginProvider
        supabaseClient={supabaseClient}
      >
        {loginValue => {
          let {
            loaded: sessionLoaded,
            sessionUserId,
            address,
            user,
            isDefaultUser,
          } = loginValue;
          // if (defaultUser && avatar) {
          //   defaultUser = mergeUserSpecAvatarUrl(defaultUser, avatar);
          // }

          return (
            <WorldAppContent
              world={world}
              // avatar={avatar}

              context={context}

              debug={debug}

              sessionLoaded={sessionLoaded}
              sessionUserId={sessionUserId}
              address={address}
              // defaultUser={defaultUser}
              user={user}
              isDefaultUser={isDefaultUser}

              // localStorageManager={localStorageManager}
            />
          );
        }}
      </LoginProvider>
    </>
  );
};

// import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
  useContext,
} from 'react';
import classnames from 'classnames';

import {
  HelperUi,
} from './components/helper-ui/HelperUi.jsx';
import {
  IoBusEventSource,
} from './components/io-bus/IoBusEventSource.jsx';
import {
  DragAndDrop,
} from './components/drag-and-drop/DragAndDrop.jsx';
import {
  EngineContext,
} from '../packages/engine/engine-context.js';
import {
  EngineProvider,
} from '../packages/engine/clients/engine-client.js';
import {
  OAuthBinding,
} from './components/oauth-binding/OAuthBinding.jsx';

import {
  ChatUi,
} from './components/chat-ui/ChatUi.jsx';
import {
  GameHeader,
} from './components/game-header/GameHeader.jsx';
import {
  StoryUi,
} from './components/story-ui/StoryUi.jsx';
import {
  CrosshairUi,
} from './components/crosshair-ui/CrosshairUi.jsx';
import {
  LoadingUi,
} from './components/loading-ui/LoadingUi.jsx';

import {
  handleDropFn,
} from './components/drag-and-drop/drop.js';

import {
  EmoteWheel,
} from './components/emote-wheel/EmoteWheel.jsx';

import {
  // LoginContext,
  LoginProvider,
  // LoginConsumer,
} from './components/login-provider/LoginProvider.jsx';

import styles from '../styles/Adventure.module.css';

// import '../hack.js';

//

/* const TitleCard = ({
  title = 'Title card',
  description = 'Here is a description.',
} = {}) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setOpen(!open);
    }, 2000);

    return () => {
      clearTimeout(timeout);
    };
  }, [
    open,
  ]);

  return (
    <div className={classnames(
      styles.titleCard,
      open && styles.open,
    )}>
      <div className={styles.bookmark}>
        <div className={styles.notch} />
      </div>
      <div className={styles.title}>{title}</div>
      <div className={styles.description}>{description}</div>
    </div>
  );
}; */

//

class StaticSceneTracker extends EventTarget {
  constructor({
    appManager,
  }) {
    super();

    this.appManager = appManager;
  }
  editable = false;
  getName() {
    return '';
  }
  getDescription() {
    return '';
  }
  getPreviewImg() {
    return '';
  }
  getApps() {
    return this.appManager.getApps();
  }
}

//

const QuestAppContent = ({
  quest,

  context,

  sessionLoaded,
  sessionUserId,
  address,
  user,
  isDefaultUser,

  debug,
}) => {
  const [engine, setEngine] = useState(null);
  // const [engineLoading, setEngineLoading] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const [appStores, setAppStores] = useState(() => {
    return {
      'quest': {
        objects: (quest ? [
          {
            start_url: '/metazine/metazine.zine',
          },
        ] : [
          {
            start_url: '/core-modules/floor/index.js',
          },
          {
            start_url: '/core-modules/asset-preview-background/index.js',
          },
          {
            type: 'application/portal',
            content: {},
            components: [
              {
                key: 'destination',
                value: '/quest/1',
              },
            ],
            position: [0, 1, -3],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Enter portal for quest',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [0, 2.2, -3],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Top',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [0, 2.2, 0],
            quaternion: [0.7071067811865475, 0, 0, 0.7071067811865476],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Bottom',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [0, 0.1, 0],
            quaternion: [-0.7071067811865475, -0, -0, 0.7071067811865476],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Left',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [-1 + 0.1, 0.75, 0],
            quaternion: [0, 0.7071067811865475, 0, 0.7071067811865476],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Right',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [1 - 0.1, 0.75, 0],
            quaternion: [-0, -0.7071067811865475, -0, 0.7071067811865476],
          },
          {
            type: 'application/text',
            content: {},
            components: {
              font: '/fonts/WinchesterCaps.ttf',
              text: 'Back',
              fontSize: 0.5,
              color: 0xFFFFFF,
              anchorX: 'center',
              anchorY: 'middle',
            },
            position: [0, 0.75, 1 - 0.1],
            quaternion: [0, 1, 0, 0],
          },
        ])
          // all
          .concat([
            {
              type: 'application/background',
              content: {
                color: 0x111111,
              },
            },
          ]),
        editable: true,
      },
    };
  });

  const [tracker, setTracker] = useState(null);

  const [contentPath, setContentPath] = useState([]);
  const [changed, setChanged] = useState(false);

  const [canvas, setCanvas] = useState(null);
  const canvasRef = useRef();

  const {
    localStorageManager,
    supabaseClient,
  } = context;

  //

  const appManagerName = 'quest';
  useEffect(() => {
    if (engine) {
      const appManager = engine.appManagerContext.getAppManager(appManagerName);
      const newTracker = new StaticSceneTracker({
        appManager,
      });
      setTracker(newTracker);
    }
  }, [
    engine,
  ]);

  // bind canvas
  useEffect(() => {
    if (canvasRef.current) {
      setCanvas(canvasRef.current);
    }
  }, [canvasRef]);

  // bind loaded
  useEffect(() => {
    if (engine) {
      (async () => {
        await engine.engineRenderer.waitForRender();
        setLoaded(true);
      })();
    }
  }, [
    engine,
  ]);

  // if (engine) {
  //   globalThis.testVqaEyesBound = () => globalThis.testVqaEyes({engine});
  //   globalThis.testVqaEyes360Bound = () => globalThis.testVqaEyes360({engine});
  // }

  //

  return (
    <>
      {/* <TitleCard /> */}

      <LoadingUi
        loadingManager={context.loadingManager}
        loaded={loaded}
      />
      <canvas className={classnames(
        styles.canvas,
      )} ref={canvasRef} />
      <IoBusEventSource engine={engine} />

      {engine ? <CrosshairUi
        engine={engine}
      /> : null}

      <ChatUi
        engine={engine}
        supabaseClient={supabaseClient}
        worldId={appManagerName}
      />

      {engine && <EmoteWheel
        engine={engine}
      />}

      {engine && <StoryUi
        engine={engine}
      />}

      <HelperUi
        localStorageManager={localStorageManager}
        loaded={loaded}
      />

      <GameHeader
        engine={engine}
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}
        address={address}
        user={user}
        isDefaultUser={isDefaultUser}

        contentPath={contentPath}
        setContentPath={setContentPath}

        changed={changed}
        setChanged={setChanged}

        sceneTracker={tracker}

        appManagerName={appManagerName}

        hidable
        questable
        debug={debug}

        loaded={loaded}
      />

      <DragAndDrop
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}

        onDrop={async (e) => {
          await handleDropFn({
            engine,
            supabaseClient,
            sessionUserId,
            appManagerName: 'quest',
          })(e);

          setChanged(true);
        }}
      />

      {canvas ? <EngineProvider
        canvas={canvas}
        context={context}

        appStores={appStores}
        playerSpec={user ? user.playerSpec : undefined}

        engine={engine}
        setEngine={setEngine}
      /> : null}
    </>
  );
};

//

export const QuestApp = ({
  quest = '',
  debug,
}) => {
  const [context, setContext] = useState(() => new EngineContext());

  const {
    supabaseClient,
  } = context;

  return (
    <>
      <OAuthBinding
        supabaseClient={supabaseClient}
      />
      <LoginProvider
        supabaseClient={supabaseClient}
      >
        {loginValue => {
          let {
            loaded: sessionLoaded,
            sessionUserId,
            address,
            user,
            isDefaultUser,
          } = loginValue;

          return (
            <QuestAppContent
              quest={quest}

              context={context}

              debug={debug}

              sessionLoaded={sessionLoaded}
              sessionUserId={sessionUserId}
              address={address}
              user={user}
              isDefaultUser={isDefaultUser}
            />
          );
        }}
      </LoginProvider>
    </>
  );
};
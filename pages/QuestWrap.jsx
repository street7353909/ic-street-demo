import React from 'react';
import ReactDOM from 'react-dom/client';

import {QuestApp} from './Quest.jsx';
import '../styles/globals.css';

//

const match = window.location.pathname.match(/^\/q(?:uest)?\/(.+)$/);
const quest = decodeURIComponent(match?.[1] ?? '');
const urlParams = new URLSearchParams(window.location.search);
const debug = urlParams.get('debug') !== null;

//

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <QuestApp
    quest={quest}
    debug={debug}
  />
);

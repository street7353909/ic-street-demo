import React, {
  useEffect,
} from 'react';

//

export const ChatManagerMessagesBinding = ({
  engine,
  messages,
  setMessages,
}) => {
  // load props from engine
  useEffect(() => {
    if (engine) {
      const chatManager = engine.chatManager;

      // messages
      const updateMessages = () => {
        let messages = chatManager.getMessages();
        messages = messages.slice();
        setMessages(messages);
      };

      // initial
      updateMessages();

      // listen
      chatManager.addEventListener('loadmessages', updateMessages);
      chatManager.addEventListener('message', updateMessages);
      chatManager.addEventListener('messageremove', updateMessages);

      // cleanup
      return () => {
        chatManager.removeEventListener('loadmessages', updateMessages);
        chatManager.removeEventListener('message', updateMessages);
        chatManager.removeEventListener('messageremove', updateMessages);
      };
    }
  }, [
    engine,
  ]);
};
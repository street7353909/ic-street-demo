import * as THREE from 'three';
import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import classnames from 'classnames';

import {
  chunkSize,
  // segments,
  gridHeight,
  // customChunkType,
} from '../packages/engine/managers/land/constants.js';

import {
  GameHeader,
} from './components/game-header/GameHeader.jsx';

import {
  mapObjectsChunkToWorldCoords,
  mapObjectsWorldToChunkCoords,
} from '../packages/engine/managers/land/land-manager.js';

import {
  IoBusEventSource,
} from './components/io-bus/IoBusEventSource.jsx';
import {
  DragAndDrop,
} from './components/drag-and-drop/DragAndDrop.jsx';
import {
  handleDropFn,
} from './components/drag-and-drop/drop.js';
import {
  EmoteWheel,
} from './components/emote-wheel/EmoteWheel.jsx';

import {
  ChatUi,
} from './components/chat-ui/ChatUi.jsx';
import {
  StoryUi,
} from './components/story-ui/StoryUi.jsx';
import {
  CrosshairUi,
} from './components/crosshair-ui/CrosshairUi.jsx';
import {
  LoadingUi,
} from './components/loading-ui/LoadingUi.jsx';

import {QueueManager} from '../packages/engine/managers/queue/queue-manager.js';

import {
  exposeAgentInterface,
} from '../packages/engine/agent-interface.js';

import {
  multiplayerEndpointUrl,
} from '../packages/engine/endpoints.js';

import {
  EngineContext,
} from '../packages/engine/engine-context.js';
import {
  EngineProvider,
} from '../packages/engine/clients/engine-client.js';

import {
  LoginProvider,
  // LoginConsumer,
} from './components/login-provider/LoginProvider.jsx';
// import {
//   OAuthProvider,
// } from './components/oauth-provider/OAuthProvider.jsx';

import styles from '../styles/Adventure.module.css';
import {
  getRandomString,
  mergeUserSpecAvatarUrl,
} from '../packages/engine/util.js';

import MobileControls from './components/mobile-controls/mobile-controls.jsx';
import '../styles/globals.css';

// import '../hack.js';

//

const LocationBinding = ({
  engine,

  coords,
  setCoords,
}) => {
  // bind coordinate update
  useEffect(() => {
    if (engine) {
      const localPlayer = engine.playersManager.getLocalPlayer();
      let frame;
      const recurse = () => {
        frame = requestAnimationFrame(() => {
          recurse();

          const {position} = localPlayer;
          const x = Math.floor(position.x / chunkSize);
          const z = Math.floor(position.z / chunkSize);

          if (x !== coords[0] || z !== coords[1]) {
            setCoords([x, z]);

            // clear transform controls hovered physics object on coord change
            const {
              transformControlsManager,
            } = engine;
            transformControlsManager.clearSelectedPhysicsObject();
          }
        });
      };
      recurse();

      return () => {
        cancelAnimationFrame(frame);
      };
    }
  }, [
    engine,
    coords,
  ]);

  return null;
}

//

/* const CameraTargetUi = ({
  engine,
}) => {
  const [app, setApp] = useState(null);
  const [object, setObject] = useState(null);

  // bind focusupdate
  useEffect(() => {
    if (engine) {
      const {cameraTargetingManager} = engine;
      const focusupdate = e => {
        const {
          app,
          object,
        } = e;
        setApp(app);
        setObject(object);
      };
      cameraTargetingManager.addEventListener('focusupdate', focusupdate);

      return () => {
        cameraTargetingManager.removeEventListener('focusupdate', focusupdate);
      };
    }
  }, [
    engine,
  ]);

  // bind keys
  useEffect(() => {
    if (engine) {
      const {playersManager} = engine;
      const localPlayer = playersManager.getLocalPlayer();

      const keydown = e => {
        switch (e.key) {
          case 'g': {
            if (object) {
              e.preventDefault();
              e.stopPropagation();

              const targetObject = object;
              const targetPosition = targetObject.position;
              const bbox2 = targetObject.physicsMesh ?
                new THREE.Box3()
                  .setFromBufferAttribute(targetObject.physicsMesh.geometry.attributes.position)
                  .applyMatrix4(targetObject.physicsMesh.matrixWorld)
              :
                null;

              const timestamp = performance.now();
              localPlayer.characterBehavior.clearWaypointActions();
              localPlayer.characterBehavior.addWaypointAction(
                targetPosition,
                timestamp,
                {
                  boundingBox: bbox2,
                },
              );
            }
            break;
          }
        }
      };
      globalThis.addEventListener('keydown', keydown);
      const click = e => {
        const {
          pointerLockManager,
          storyManager,
        } = engine;
        if (
          object &&
          pointerLockManager.pointerLockElement &&
          !storyManager.getConversation()
        ) {
          storyManager.inspectPhysicsId(object.physicsId);
        }
      };
      globalThis.addEventListener('click', click);

      return () => {
        globalThis.removeEventListener('keydown', keydown);
        globalThis.removeEventListener('click', click);
      };
    }
  }, [
    engine,
    app,
    object,
  ]);

  //

  return object ? (
    <div className={styles.cameraTargetUi}>
      <div className={styles.name}>{object.name}</div>
      <div className={styles.description}>{object.description}</div>
    </div>
  ) : null;
}; */

//

const directionCandidates = [
  {
    name: 'N',
    direction: new THREE.Vector3(0, 0, -1),
  },
  {
    name: 'NE',
    direction: new THREE.Vector3(1, 0, -1).normalize(),
  },
  {
    name: 'E',
    direction: new THREE.Vector3(1, 0, 0),
  },
  {
    name: 'SE',
    direction: new THREE.Vector3(1, 0, 1).normalize(),
  },
  {
    name: 'S',
    direction: new THREE.Vector3(0, 0, 1),
  },
  {
    name: 'SW',
    direction: new THREE.Vector3(-1, 0, 1).normalize(),
  },
  {
    name: 'W',
    direction: new THREE.Vector3(-1, 0, 0),
  },
  {
    name: 'NW',
    direction: new THREE.Vector3(-1, 0, -1).normalize(),
  },
];
const getPolarDirection = q => {
  const dir = new THREE.Vector3(0, 0, -1).applyQuaternion(q);
  let closestDirection = null;
  let closestAngle = Infinity;
  for (const directionCandidate of directionCandidates) {
    const angle = dir.angleTo(directionCandidate.direction);
    if (angle < closestAngle) {
      closestDirection = directionCandidate.name;
      closestAngle = angle;
    }
  }
  return closestDirection;
}
const LocationUrlTracker = ({
  engine,
}) => {
  // bind coordinate update
  useEffect(() => {
    if (engine) {
      const coordinateUpdateInterval = 1000;

      const localPlayer = engine.playersManager.getLocalPlayer();
      const interval = setInterval(() => {
        const {position} = localPlayer;
        const x = Math.floor(position.x);
        const z = Math.floor(position.z);

        const dir = getPolarDirection(localPlayer.quaternion);

        const u = new URL(window.location);
        u.hash = `#${x},${z},${dir}`;
        window.history.replaceState({}, '', u.toString());
      }, coordinateUpdateInterval);

      return () => {
        clearInterval(interval);
      };
    }
  }, [
    engine,
  ]);

  return null;
};

//

const mapObjectsToAppSpecs = (objects) => objects.map(object => {
  const {
    position,
    quaternion,
    scale,
  } = object;
  return {
    ...object,
    position: position ? new THREE.Vector3().fromArray(position) : null,
    quaternion: quaternion ? new THREE.Quaternion().fromArray(quaternion) : null,
    scale: scale ? new THREE.Vector3().fromArray(scale) : null,
  };
});
const mapAppSpecsWithInstanceId = appSpecs => appSpecs.map(appSpec => {
  return {
    ...appSpec,
    instanceId: getRandomString(),
  };
});

//

const LandUpdateBinding = ({
  engine,
  supabaseClient,
  sessionUserId,

  appManagerName,

  coords,
  setCoords,

  // changed,
  // setChanged,

  // updatingApps,
  // setUpdatingApps,

  landTracker,
  setLandTracker,
  sceneTracker,
  setSceneTracker,

  range,
}) => {
  const [queueManager, setQueueManager] = useState(() => new QueueManager());
  // const [loadingAppSpecsSet, setLoadingAppSpecsSet] = useState(() => new Set());
  // const [removingAppSpecsSet, setRemovingAppSpecsSet] = useState(() => new Set());

  //

  useEffect(() => {
    const hashchange = e => {
      location.reload();
    };
    globalThis.addEventListener('hashchange', hashchange);

    return () => {
      globalThis.removeEventListener('hashchange', hashchange);
    };
  }, []);

  //

  const bindLandTrackerEvents = (tracker) => {
    const {
      engineRenderer,
      appManagerContext,
    } = engine;
    const {
      scene,
    } = engineRenderer;

    //

    // load new land
    const landAdd = async e => {
      await queueManager.waitForTurn(async () => {
        let {
          land: {
            coords,
            objects: addObjects,
          },
        } = e.data;

        const coordsString = `land:${coords[0]}:${coords[1]}`;
        // console.log('load new land', coordsString);
        const coordsAppManager = appManagerContext.createAppManager(coordsString);
        scene.add(coordsAppManager);

        let appSpecs = mapObjectsChunkToWorldCoords(addObjects, coords);
        appSpecs = mapObjectsToAppSpecs(appSpecs);
        appSpecs = mapAppSpecsWithInstanceId(appSpecs);

        // setUpdatingApps(true);

        const promises = [];
        for (const appSpec of appSpecs) {
          // loadingAppSpecsSet.add(appSpec.instanceId);
          const p = coordsAppManager.addAppAsync(appSpec);
          // p.finally(() => {
          //   loadingAppSpecsSet.delete(appSpec.instanceId);
          // });
          promises.push(p);
        }
        const apps = await Promise.all(promises);

        // setChanged(false);
        // setUpdatingApps(false);
      });
    };
    tracker.addEventListener('landadd', landAdd);

    // unload old land
    const landRemove = async e => {
      await queueManager.waitForTurn(async () => {
        let {
          land: {
            coords,
          },
        } = e.data;

        const coordsString = `land:${coords[0]}:${coords[1]}`;
        // console.log('remove old land', coordsString);
        const coordsAppManager = appManagerContext.getAppManager(coordsString);

        const apps = coordsAppManager.getApps();

        // setUpdatingApps(true);

        await Promise.resolve();

        for (const app of apps) {
          // removingAppSpecsSet.add(app.instanceId);
          coordsAppManager.removeApp(app);
          // removingAppSpecsSet.delete(app.instanceId);
        }

        scene.remove(coordsAppManager);
        appManagerContext.destroyAppManager(coordsAppManager);

        // setChanged(false);
        // setUpdatingApps(false);

        // landCoordToAppsMap.delete(key);
      });
    }
    tracker.addEventListener('landremove', landRemove);

    return {
      cancel() {
        tracker.removeEventListener('landadd', landAdd);
        tracker.removeEventListener('landremove', landRemove);
        tracker.cancel();
      },
    };
  };

  // bind land tracker for range
  useEffect(() => {
    if (engine) {
      const {
        landManager,
      } = engine;
      const landTracker = landManager.createLandTracker({
        supabaseClient,
        range: range ?? 1,
      });
      setLandTracker(landTracker);

      const {
        cancel,
      } = bindLandTrackerEvents(landTracker);

      let frame;
      const _recurse = () => {
        frame = requestAnimationFrame(_recurse);

        landTracker.update();
      };
      frame = requestAnimationFrame(_recurse);

      return () => {
        cancel();
        cancelAnimationFrame(frame);
      };
    }
  }, [
    engine,
  ]);

  // bind scene tracker tracker for range
  useEffect(() => {
    if (engine) {
      const {
        landManager,
        appManagerContext,
        appTracker,
      } = engine;
      const sceneTracker = landManager.createSceneTracker({
        supabaseClient,
        sessionUserId,
        appManagerContext,
        appTracker,
        appManagerName,
      });
      setSceneTracker(sceneTracker);

      let frame;
      const _recurse = () => {
        frame = requestAnimationFrame(_recurse);

        sceneTracker.update();
      };
      frame = requestAnimationFrame(_recurse);

      return () => {
        sceneTracker.cancel();
        cancelAnimationFrame(frame);
      };
    }
  }, [
    engine,
    appManagerName,
  ]);

  // bind coord app manager editability
  useEffect(() => {
    if (engine && coords) {
      const {
        appManagerContext,
        // appTracker,
      } = engine;

      const appManagerName = `land:${coords[0]}:${coords[1]}`;

      // initialize app manager editability
      const appManagers = appManagerContext.getAppManagers();
      for (const appManager of appManagers) {
        appManager.setEditable(appManager.name === appManagerName);
      }

      // track app manager editability
      const appManager = appManagerContext.getAppManager(appManagerName);
      if (!appManager) {
        const appmanageradd = e => {
          const {
            appManager,
          } = e.data;

          if (appManager.name === appManagerName) {
            appManagerContext.removeEventListener('appmanageradd', appmanageradd);

            appManager.setEditable(true);
          }
        };
        appManagerContext.addEventListener('appmanageradd', appmanageradd);

        return () => {
          appManagerContext.removeEventListener('appmanageradd', appmanageradd);
        };
      }
    }
  }, [
    engine,
    coords,
  ]);

  //

  return null;
};

//

export const AdventureApp = ({
  avatar,
  range,
  multiplayer,
  beta,
  agent,
  hidable,
  debug,
  music,
}) => {
  const [context, setContext] = useState(() => new EngineContext());

  //

  return (
    <LoginProvider
      supabaseClient={context.supabaseClient}
    >
      {/* <LoginConsumer> */}
        {loginValue => {
          return (
            <AdventureContent
              avatar={avatar}
              range={range}
              multiplayer={multiplayer}
              beta={beta}
              agent={agent}
              hidable={hidable}
              debug={debug}
              music={music}

              context={context}

              // localStorageManager={localStorageManager}
              loginValue={loginValue}
            />
          );
        }}
      {/* </LoginConsumer> */}
    </LoginProvider>
  );
};

export const AdventureContent = ({
  // difficulty,
  avatar,
  range,
  multiplayer,
  beta,
  agent,
  hidable,
  debug,
  music,

  context,

  // localStorageManager,
  loginValue,
}) => {
  const [engine, setEngine] = useState(null);
  const [appStores, setAppStores] = useState(() => {
    return {
      'adventure': {
        objects: [
          {
            type: 'application/spawnpoint',
            content: (() => {
              const u = new URL(location.href);
              // match #x,z
              const hash = u.hash.slice(1);
              const match = hash.match(/^(-?\d+),(-?\d+)(?:,(N|NE|E|SE|S|SW|W|NW))?$/);

              const x = (match ? parseInt(match[1], 10) : 0) || 0;
              const z = (match ? parseInt(match[2], 10) : 0) || 0;
              const dir = (match ? match[3] : '') || 'N';

              const position = [x, gridHeight, z];

              const directionSpec = directionCandidates.find(directionCandidate => directionCandidate.name === dir);
              const quaternion = new THREE.Quaternion().setFromUnitVectors(
                new THREE.Vector3(0, 0, -1),
                directionSpec.direction,
              ).toArray();

              return {
                position,
                quaternion,
              };
            })(),
          },
          {
            start_url: '/core-modules/terrain/index.js',
            components: beta ? [
              {
                key: 'layers',
                value: {
                  avatar: true,
                  object: true,
                  mob: true,
                },
              },
            ] : [],
          },

          {
            type: 'application/wind',
            content: {
              windType: 'directional',
              direction: [-1, 0, 0],
              windForce: 0.5,
              noiseScale: 1,
              windFrequency: 1
            },
          },
          {
            start_url: '/audio/soundscape-robust-summer-after.mp3',
            components: [
              {key: 'volume', value: 0.2},
            ],
          },

          // testing
          /*
          {
            position: [0, 12, -3],
            quaternion: [0, 1, 0, 0],
            start_url: '/characters/solarwitch.npc',
          },
          {
            position: [0, 10, -3],
            start_url: '/core-modules/silk/index.js',
          },
          {
            position: [0, 10, -2],
            start_url: '/core-modules/ores/index.js',
          },
          {
            position: [0, 10, -5],
            start_url: '/core-modules/title-deed/title-deed.item',
          },
          {
            position: [0, 10, -6],
            start_url: '/core-modules/button/index.js',
          },
          {
            position: [20.683387756347656, 26.5, -100.8043441772461],
            quaternion: [0, 0, 0, 1],
            start_url: '/models/The_Basilik_v3.glb',
          }, */
        ].concat(beta ? [
          {
            position: [0, 70, 0],
            quaternion: [0, 0.7071067811865475, 0, 0.7071067811865476],
            start_url: '/core-modules/floating-treehouse/index.js',
          },
          {
            position: [0, 0, 0],
            start_url: '/core-modules/lisk/index.js',
          },
          {
            position: [-69.12263488769531, 36.84118765592575, -19.192594528198242],
            quaternion: [0.37533027751786524, 0.07465783405034258, -0.9061274463528878, -0.1802399555017369],
            start_url: '/core-modules/silsword/index.js',
          },
          {
            position: [32.683387756347656, 30.6351997256279, -94.8043441772461],
            quaternion: [0, 0, 0, 1],
            start_url: '/core-modules/origin-tablet/Origin_Tablets_Pillar_V2_avaer_hax.glb',
          },
          {
            position: [-69.12263488769531, 30.84118765592575, -19.192594528198242],
            quaternion: [0, 0, 0, 1],
            start_url: '/core-modules/street-green/index.js',
          },
          {
            position: [-69.12263488769531, 30.84118765592575, -3.192594528198242],
            quaternion: [0, 0, 0, 1],
            start_url: '/core-modules/hovercraft/hovercraft.glb',
          },
        ] : [
          {
            position: [chunkSize/2, 70, 0],
            quaternion: [0, 0, 0, 1],
            start_url: '/core-modules/street-green/index.js',
          },
        ]).concat(music ? [{
          start_url: '/audio/calm-jrpg-style-demo2.mp3',
          components: [
            {key: 'volume', value: 0.1},
          ],
        },
        ] : []),
        editable: false,
      },
    };
  });
  const [coords, setCoords] = useState([0, 0]);

  const [loaded, setLoaded] = useState(false);
  const [titleScreenEntered, setTitleScreenEntered] = useState(false);

  const [contentPath, setContentPath] = useState([]);

  const [landTracker, setLandTracker] = useState(null);
  const [sceneTracker, setSceneTracker] = useState(null);

  const [canvas, setCanvas] = useState(null);
  const canvasRef = useRef();

  const {
    supabaseClient,
    localStorageManager,
  } = context;

  //

  // useEffect(() => {
  //   if (engine) {
  //     globalThis.testVqaEyesBound = () => globalThis.testVqaEyes({engine});
  //     globalThis.testVqaEyes360Bound = () => globalThis.testVqaEyes360({engine});
  //   }
  // }, [engine]);

  //

  const appManagerName = `land:${coords[0]}:${coords[1]}`;

  //

  let {
    loaded: sessionLoaded,
    // supabaseClient,
    sessionUserId,
    address,
    user,
    isDefaultUser,
  } = loginValue;

  //

  // bind canvas
  useEffect(() => {
    if (canvasRef.current) {
      setCanvas(canvasRef.current);
    }
  }, [canvasRef]);

  // initialize multiplayer
  useEffect(() => {
    if (engine && multiplayer) {
      const roomName = typeof multiplayer === 'string' ?
        multiplayer
      :
        'multiplayer'
      ;

      (async () => {
        const newMultiplayer = engine.createMultiplayer();

        await newMultiplayer.connectMultiplayer({
          endpoint_url: multiplayerEndpointUrl,
        });
        const multiplayerTracker = newMultiplayer.createTracker({
          getKeySpec: () => {
            const {
              playersManager,
            } = engine;
            const localPlayer = playersManager.getLocalPlayer();
            const position = localPlayer.position;

            const {x, y, z} = position;
            const realmsKeys = [];
            let rootRealmKey;
            for (let dx = -1; dx <= 1; dx++) {
              for (let dz = -1; dz <= 1; dz++) {
                const key = `${roomName}:${Math.floor((x + dx * chunkSize) / chunkSize)},${Math.floor((z + dz * chunkSize) / chunkSize)}`;
                realmsKeys.push(key);

                if (dx === 0 && dz === 0) {
                  rootRealmKey = key;
                }
              }
            }
            return {
              realmsKeys,
              rootRealmKey,
            };
          },
        });
      })();
    }
  }, [engine]);

  // bind title screen controls
  useEffect(() => {
    if (engine) {
      const pointerlockchange = e => {
        const {
          pointerLockElement,
        } = e.data;
        !titleScreenEntered && pointerLockElement && setTitleScreenEntered(true);
      };
      engine.pointerLockManager.addEventListener('pointerlockchange', pointerlockchange);

      return () => {
        engine.pointerLockManager.removeEventListener('pointerlockchange', pointerlockchange);
      };
    }
  }, [engine]);

  // bind loading manager
  useEffect(() => {
    if (engine) {
      (async () => {
        await engine.engineRenderer.waitForRender();
        setLoaded(true);
      })();
    }
  }, [
    engine,
  ]);

  // expose the agent interface
  useEffect(() => {
    if (engine && agent) {
      const {
        playersManager,
        chatManager,
        loreManager,
      } = engine;
      exposeAgentInterface({
        playersManager,
        chatManager,
        loreManager,
      });
    }
  }, [
    engine,
  ]);

  //

  // const appManager = engine?.appManagerContext.getAppManager(appManagerName);
  // const chatWorldId = appManager?.isEditable() ? appManagerName : '';
  // console.log('chat world id', {appManagerName, appManager, chatWorldId});
  // globalThis.appManagerContext = engine?.appManagerContext;

  //

  return (
    <>
      <LoadingUi
        loadingManager={context.loadingManager}
        loaded={loaded}
      />

      <canvas className={classnames(
        styles.canvas,
      )} ref={canvasRef} />
      <IoBusEventSource engine={engine} />
      <GameHeader
        engine={engine}

        loaded={loaded}
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}
        address={address}
        user={user}
        isDefaultUser={isDefaultUser}

        contentPath={contentPath}
        setContentPath={setContentPath}

        sceneTracker={sceneTracker}
        landTracker={landTracker}
        appManagerName={appManagerName}

        mappable
        hidable={hidable}
        debug={debug}
      >
      </GameHeader>

      {engine ? <CrosshairUi
        engine={engine}
      /> : null}

      {/* <CameraTargetUi
        engine={engine}
      /> */}

      <ChatUi
        engine={engine}
        supabaseClient={supabaseClient}
        worldId={appManagerName}
      />

      <LocationUrlTracker
        engine={engine}
      />

      <LocationBinding
        engine={engine}

        coords={coords}
        setCoords={setCoords}
      />

      <LandUpdateBinding
        engine={engine}
        supabaseClient={supabaseClient}
        sessionUserId={sessionUserId}

        appManagerName={appManagerName}

        coords={coords}
        setCoords={setCoords}

        landTracker={landTracker}
        setLandTracker={setLandTracker}
        sceneTracker={sceneTracker}
        setSceneTracker={setSceneTracker}

        range={range}
      />

      <DragAndDrop
        localStorageManager={localStorageManager}
        supabaseClient={supabaseClient}

        onDrop={handleDropFn({
          engine,
          supabaseClient,
          sessionUserId,
          appManagerName,
        })}
      />

      {engine && <EmoteWheel
        engine={engine}
      />}

      {engine && <StoryUi
        engine={engine}
      />}

      {canvas && <EngineProvider
        canvas={canvas}
        context={context}

        appStores={appStores}
        playerSpec={user ? user.playerSpec : undefined}

        engine={engine}
        setEngine={setEngine}
      />}
    </>
  );
};

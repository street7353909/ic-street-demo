import React from 'react';
import ReactDOM from 'react-dom/client';

import {WorldApp} from './World.jsx';
import '../styles/globals.css';

// const match = window.location.pathname.match(/^\/w(?:orld)?\/(.+)$/);
// const world = decodeURIComponent(match?.[1] ?? '');

// // if world is null, kick back to home page
// if (!world) {
//   window.location.href = '/';
// }

const urlParams = new URLSearchParams(window.location.search);
const world = urlParams.get('name') || '';
const avatar = urlParams.get('avatar');
const debug = urlParams.get('debug') !== null;

//

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <WorldApp
    world={world}
    avatar={avatar}
    debug={debug}
  />
);

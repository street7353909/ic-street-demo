import * as THREE from 'three';
import {
  SupabaseClient,
} from './packages/engine/clients/supabase-client.js';
import {
  imageCaptioning,
} from './packages/engine/vqa.js';
import {
  QueueManager,
} from './packages/engine/managers/queue/queue-manager.js';
import {
  generateGif,
} from './packages/engine/generate-image.js';
// import {
//   getSilkContractsAsync,
// } from './packages/engine/ethereum/contracts.js';
import CubemapToEquirectangular from './packages/cubemap-to-equirectanglar/build/CubemapToEquirectangular.js';
import {
  llava,
  imageFind,
  imageSegmentation,
  // imageSegmentationMulti,
  getTopSegmentBoxes,
} from './packages/engine/vqa.js';
// import {
//   drawImageSegments,
// } from './packages/gen/src/generators/scene-generator.js';
import {
  Alchemy,
  Network,
} from 'alchemy-sdk';
import uuidByString from 'uuid-by-string';

import {
  remark,
} from 'remark';
import strip from 'strip-markdown';

import {
  ALCHEMY_API_KEY,
  VROID_HUB_CLIENT_ID, 
} from './packages/engine/constants/auth.js';

import {
  DiscordClient,
} from './packages/engine/clients/discord-client.js';
import {
  TwitchVideoClient,
  TwitchChatClient,
} from './packages/engine/clients/twitch-client.js';
import { AutoVoiceEndpoint } from './packages/engine/audio/voice-output/voice-endpoint-voicer.js';

// import {pipeline} from '@xenova/transformers';

// import {PicketProvider} from '@picketapi/picket-react';

//
// alchemy
//

globalThis.testAlchemyToken = async (
  contractAddresss = '0x3999877754904d8542ad1845d368fa01a5e6e9a5', // Vipe Heroes
  tokenId = '1614',
) => {
  // Optional config object, but defaults to the API key 'demo' and Network 'eth-mainnet'.
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  // use alchemy to get metadata for contract 0x3999877754904d8542ad1845d368fa01a5e6e9a5
  const nft = await alchemy.nft.getNftMetadata(
    contractAddresss,
    tokenId,
  );

  const {
    tokenUri,
  } = nft;
  const {
    gateway,
  } = tokenUri;

  const res = await fetch(gateway);
  const j = await res.json();

  const {
    asset,
  } = j;
  const match = asset.match(/^ipfs:\/\/(.+)$/);
  const ipfsPath = match[1];

  const u = `https://alchemy.mypinata.cloud/ipfs/${ipfsPath}`;
  const res2 = await fetch(u);
  const blob = await res2.blob();
  console.log('got blob', u, {
    blob,
  });
};
globalThis.testListVrmNfts = async (
  contractAddresss = '0x3999877754904d8542ad1845d368fa01a5e6e9a5', // Vipe Heros
  limit = 1000,
) => {
  // Optional config object, but defaults to the API key 'demo' and Network 'eth-mainnet'.
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  // use alchemy to get metadata for contract 0x3999877754904d8542ad1845d368fa01a5e6e9a5
  let nfts = [];
  let pageKey;
  for (;;) {
    const o = await alchemy.nft.getNftsForContract(
      contractAddresss,
      {
        pageKey,
      },
    );
    const {
      nfts: _nfts,
      pageKey: _pageKey,
    } = o;
    if (
      _nfts.length > 0 && // returned result and
      (nfts.length === 0 || _nfts[0].tokenId !== '0') // first or the result did not start at zero
    ) {
      nfts.push(..._nfts);
      if (nfts.length >= limit) {
        nfts = nfts.splice(0, limit);
        break;
      } else {
        console.log('got more nfts', _nfts, _nfts.length, nfts.length);
        pageKey = _pageKey;
      }
    } else {
      break;
    }
  }

  // load the avatar

  const firstNft = nfts[0];
  const {tokenId} = firstNft;
  const nft = await alchemy.nft.getNftMetadata(
    contractAddresss,
    tokenId,
  );

  const {
    tokenUri,
  } = nft;
  const {
    gateway,
  } = tokenUri;

  const res = await fetch(gateway);
  const j = await res.json();

  const {
    asset,
  } = j;
  const match = asset.match(/^ipfs:\/\/(.+)$/);
  const ipfsPath = match[1];

  const u = `https://alchemy.mypinata.cloud/ipfs/${ipfsPath}`;
  const res2 = await fetch(u);
  const blob = await res2.blob();
  // console.log('got blob', u, {
  //   blob,
  // });
  return blob;
};
globalThis.testDownloadVrmNfts = async (
  contractAddresss = '0x3999877754904d8542ad1845d368fa01a5e6e9a5', // Vipe Heros
  limit = 1000,
) => {
  // Optional config object, but defaults to the API key 'demo' and Network 'eth-mainnet'.
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  // download the avatars
  const queueManager = new QueueManager({
    parallelism: 8,
  });

  const startTokenId = 2240;
  const endTokenId = 2500;
  for (let tokenId = startTokenId; tokenId < endTokenId; tokenId++) {
    queueManager.waitForTurn(async () => {
      const nft = await alchemy.nft.getNftMetadata(
        contractAddresss,
        tokenId,
      );
      console.log('got nft', tokenId, nft);

      const {
        tokenUri,
      } = nft;
      const {
        gateway,
      } = tokenUri;

      const res = await fetch(gateway);
      const j = await res.json();

      const {
        asset,
      } = j;
      const match = asset.match(/^ipfs:\/\/(.+)$/);
      const ipfsPath = match[1];

      const u = `https://alchemy.mypinata.cloud/ipfs/${ipfsPath}`;
      const blob = await (async () => {
        const res2 = await fetch(u);
        const blob = await res2.blob();
        return blob;
      })();
      const u2 = URL.createObjectURL(blob);
      // force download with anchor tag
      console.log('got u', {u: u2});
      const a = document.createElement('a');
      a.href = u2;
      a.download = `vipe_${tokenId}.vrm`;
      // document.body.appendChild(a);
      a.click();
    });
  }
};
globalThis.testDownloadImageNfts = async (
  contractAddresss = '0x3999877754904d8542ad1845d368fa01a5e6e9a5', // Vipe Heros
  limit = 1000,
) => {
  // Optional config object, but defaults to the API key 'demo' and Network 'eth-mainnet'.
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  // download the avatars
  const queueManager = new QueueManager({
    parallelism: 8,
  });

  const startTokenId = 1240;
  const endTokenId = 2500;
  for (let tokenId = startTokenId; tokenId < endTokenId; tokenId++) {
    queueManager.waitForTurn(async () => {
      const nft = await alchemy.nft.getNftMetadata(
        contractAddresss,
        tokenId,
      );
      console.log('got nft', tokenId, nft);

      const {
        media,
      } = nft;
      const imgSrcRaw = media[0].raw;

      const match = imgSrcRaw.match(/^ipfs:\/\/(.+)$/);
      const ipfsPath = match[1];
      const u = `https://alchemy.mypinata.cloud/ipfs/${ipfsPath}`;
      const blob = await (async () => {
        const res2 = await fetch(u);
        const blob = await res2.blob();
        return blob;
      })();
      const u2 = URL.createObjectURL(blob);
      // force download with anchor tag
      console.log('got u', {u: u2});
      const a = document.createElement('a');
      a.href = u2;
      a.download = `vipe_${tokenId}.png`;
      // document.body.appendChild(a);
      a.click();
    });
  }
};
globalThis.testAlchemyContract = async (
  contractAddresss = '0x543d43f390b7d681513045e8a85707438c463d80', // Genesis Pass
) => {
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  // use alchemy to get metadata for contract 0x3999877754904d8542ad1845d368fa01a5e6e9a5
  const nfts = [];
  globalThis.nfts = nfts;
  let pageKey;
  for (;;) {
    const o = await alchemy.nft.getNftsForContract(
      contractAddresss,
      {
        pageKey,
      },
    );
    const {
      nfts: _nfts,
      pageKey: _pageKey,
    } = o;
    if (_nfts.length > 0 && (nfts.length === 0 || _nfts[0].tokenId !== '0')) {
      nfts.push(..._nfts);
      console.log('got more nfts', _nfts, _nfts.length, nfts.length);
      pageKey = _pageKey;
    } else {
      break;
    }
    break;
  }
  console.log('got all nfts', nfts);

  const nftsByOwner = new Map();
  globalThis.nftsByOwner = nftsByOwner;
  for (const nft of nfts) {
    const {
      tokenId,
    } = nft;
    const ownerSpec = await alchemy.nft.getOwnersForNft(contractAddresss, tokenId);
    console.log('got owner spec', ownerSpec);
    const {
      owners,
    } = ownerSpec;
    if (owners.length > 1) {
      console.warn('got multiple owners for token', {
        nft,
        owners,
      });
    }
    const owner = owners[0];
    if (!nftsByOwner.has(owner)) {
      nftsByOwner.set(owner, new Set());
    }
    nftsByOwner.get(owner).add(nft);

    // const startBlock = '0xe94c3d';
    // const startBlockInt = BigInt(startBlock);

    // console.log('nfts by owner', owner, nftsByOwner.get(owner));
  }
  console.log('got all nfts by owner', nftsByOwner);
};
globalThis.testContractOwnerTiming = async ({
  contractAddresss = '0x543d43f390b7d681513045e8a85707438c463d80', // Genesis Pass
  contractData = null, // from the above call
} = {}) => {
  const settings = {
    apiKey: ALCHEMY_API_KEY, // Replace with your Alchemy API key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);

  const contractMetadata = await alchemy.nft.getContractMetadata(contractAddresss);
  console.log('got contract metadata', contractMetadata);
  const {
    deployedBlockNumber,
  } = contractMetadata;

  const transfers = [];
  let pageKey = undefined;
  for (;;) {
    const res = await alchemy.core.getAssetTransfers({
      fromBlock: '0x' + deployedBlockNumber.toString(16),
      // toAddress: owner,
      excludeZeroValue: true,
      category: ["erc721", "erc1155"],
      contractAddresses: [contractAddresss],
      pageKey,
    });
    console.log('got res', res);
    await new Promise((accept, reject) => {
      setTimeout(accept, 200);
    });
    const {
      pageKey: _pageKey,
      transfers: _transfers,
    } = res;
    pageKey = _pageKey;
    // console.log('got page key', pageKey);
    transfers.push(..._transfers);
    if (!pageKey) {
      break;
    }
  }
  return transfers;
};



//
// animatediff
//

globalThis.loadAnimatediffImage = async ({
  prompt = `girl running through green field with pokemon`,
  n_prompt = `bad quality, blurry, low resolution`,
} = {}) => {
  const fd = new FormData();
  fd.append('prompt', prompt);
  fd.append('n_prompt', n_prompt);
  fd.append('model', `flat2dAnimergeV3F16.vzgC.safetensors`);
  const res = await fetch('https://ai-proxy.isekai.chat/animatediff', {
    method: 'POST',
    body: fd,
  });
  if (!res.ok) {
    throw new Error('failed to fetch animatediff');
  }
  const blob = await res.blob();
  const img = await new Promise((accept, reject) => {
    const img = new Image();
    const u = URL.createObjectURL(blob);
    const cleanup = () => {
      URL.revokeObjectURL(u);
    };
    img.src = u;
    img.onload = () => {
      accept();
      cleanup();
    };
    img.onerror = err => {
      reject(err);
      cleanup();
    };
  });
  img.style.cssText = `\
position: fixed;
top: 0;
left: 0;
width: 512px;
`;
  document.body.appendChild(img);
};

//
// embedding
//

const extractMdText = async text => {
  return await remark()
    .use(strip)
    .process(text)
    .then((file) => String(file));
}
function extractImagesFromMarkdown(mdString) {
  // This regex pattern matches markdown image format ![alt text](url)
  const imageRegex = /!\[.*?\]\((.*?)\)/g;
  let matches;
  const images = [];
  while ((matches = imageRegex.exec(mdString)) !== null) {
    images.push(matches[1]);
  }
  return images;
}
globalThis.testEmbeddingFiles = async () => {
  const input = document.createElement('input');
  input.type = 'file';
  input.multiple = true;
  input.click();
  const files = await new Promise((accept, reject) => {
    input.addEventListener('change', async (e) => {
      const {files} = e.target;
      accept(files);
    });
  });
  const file = files[0];
  const text = await file.text();
  // console.log('got files', files);

  const rawMarkdown = await extractMdText(text);
  const images = extractImagesFromMarkdown(text);

  const markdownSplits = await split(rawMarkdown);
  console.log('got markdown splits', markdownSplits);

  //

  const imageEmbeddings = [];
  for (const image of images) {
    // if (!/\.gif/.test(image)) {
      let blob = null;
      try {
        const res = await fetch(image);
        blob = await res.blob();
      } catch(err) {
        console.warn(err);
      }
      if (blob === null) {
        continue;
      }

      const caption = await imageCaptioning(blob);
      console.log('got image caption', JSON.stringify(caption));

      const dataUrl = await new Promise((accept, reject) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
          const {result} = reader;
          accept(result);
        });
        reader.readAsDataURL(blob);
      });

      const embedding = await embed(caption);

      const imageEmbedding = {
        dataUrl,
        caption,
        embedding,
      };
      imageEmbeddings.push(imageEmbedding);
    // }
  }
  console.log('got image embeddings', imageEmbeddings);

  //

  const supabaseClient = new SupabaseClient();
  const {supabase} = supabaseClient;
  const {data: {user}} = await supabase.auth.getUser();
  const user_id = user.id;

  //

  // upload markdown text
  for (const paragraphLines of markdownSplits) {
    const content = paragraphLines.join('\n');
    const id = uuidByString(content);

    const embedding = await embed(content);

    const loreItem = {
      id,
      user_id,
      type: 'text',
      content,
      embedding,
    };
    console.log('upload text lore', loreItem);
    await supabase.from('lore')
      .upsert(loreItem);
  }

  // upload image embeddings
  for (const imageEmbedding of imageEmbeddings) {
    const {dataUrl, caption, embedding} = imageEmbedding;
    const id = uuidByString(dataUrl);

    const loreItem = {
      id,
      user_id,
      type: 'image',
      content: {
        dataUrl,
        caption,
      },
      embedding,
    };
    console.log('upload image lore', loreItem);
    await supabase.from('lore')
      .upsert(loreItem);
  } 

  //

  return {
    rawMarkdown,
    markdownSplits,
    imageEmbeddings,
  };
};
/* globalThis.testEmbeddingString = async (content = 'hello world') => {
  const embedding = await embed(content);

  const supabaseClient = new SupabaseClient();
  const {supabase} = supabaseClient;

  const {data: {user}} = await supabase.auth.getUser();
  const user_id = user.id;

  const loreItem = {
    id: crypto.randomUUID(),
    user_id,
    content,
    embedding,
  };
  await supabase.from('lore')
    .upsert(loreItem);

  const {
    data: documents,
  } = await supabase.rpc('match_lore', {
    query_embedding: embedding, // Pass the embedding you want to compare
    match_threshold: 0.7, // Choose an appropriate threshold for your data
    match_count: 10, // Choose the number of matches
  });
  console.log('documents', documents);
}; */

globalThis.generateGif = async (prompt = 'flying through the lush jungle, anime style') => {
  const gifBlob = await generateGif({
    prompt,
  });
  const url = URL.createObjectURL(gifBlob);
  const image = new Image();
  image.src = url;
  image.style.cssText = `\
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1;
  `;
  document.body.appendChild(image);
};

globalThis.testVqaEyes = async ({
  engine,
  prompt = 'what is this?',
}) => {
  const {engineRenderer} = engine;
  const {
    renderer,
    scene,
    camera,
  } = engineRenderer;
  const canvas = renderer.domElement;

  // take a screenshot
  const blob = await new Promise((accept, reject) => {
    renderer.domElement.toBlob(accept, 'image/png');
  });
  const result = await imageCaptioning(blob, prompt);
  console.log(JSON.stringify(result));
};
globalThis.testVqaEyes360 = async ({
  engine,
}) => {
  const {engineRenderer, playersManager} = engine;
  const {renderer, scene, rootScene, camera} = engineRenderer;

  const localPlayer = playersManager.getLocalPlayer();
  const position = localPlayer.position.clone();

  //

  const depthVertexShader = `\
    precision highp float;
    precision highp int;

    varying vec4 vWorldPosition;

    void main() {
      // Transform vertex into world coordinates
      vWorldPosition = modelMatrix * vec4(position, 1.0);
      gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }
  `;
  const depthFragmentShader = `\
    // uniform vec3 uColor;
    // uniform float uTime;
    uniform float cameraNear;
    uniform float cameraFar;

    varying vec4 vWorldPosition;
    // uniform vec3 cameraPosition;

    // varying vec3 vViewPosition;
    // varying vec2 vUv;

    // varying vec3 vPos;
    // varying vec3 vNormal;

    #define FLOAT_MAX  1.70141184e38
    #define FLOAT_MIN  1.17549435e-38

    lowp vec4 encode_float(highp float v) {
      highp float av = abs(v);

      //Handle special cases
      if(av < FLOAT_MIN) {
        return vec4(0.0, 0.0, 0.0, 0.0);
      } else if(v > FLOAT_MAX) {
        return vec4(127.0, 128.0, 0.0, 0.0) / 255.0;
      } else if(v < -FLOAT_MAX) {
        return vec4(255.0, 128.0, 0.0, 0.0) / 255.0;
      }

      highp vec4 c = vec4(0,0,0,0);

      //Compute exponent and mantissa
      highp float e = floor(log2(av));
      highp float m = av * pow(2.0, -e) - 1.0;
      
      //Unpack mantissa
      c[1] = floor(128.0 * m);
      m -= c[1] / 128.0;
      c[2] = floor(32768.0 * m);
      m -= c[2] / 32768.0;
      c[3] = floor(8388608.0 * m);
      
      //Unpack exponent
      highp float ebias = e + 127.0;
      c[0] = floor(ebias / 2.0);
      ebias -= c[0] * 2.0;
      c[1] += floor(ebias) * 128.0; 

      //Unpack sign bit
      c[0] += 128.0 * step(0.0, -v);

      //Scale back to range
      return c / 255.0;
    }

    // note: the 0.1s here an there are voodoo related to precision
    float decode_float(vec4 v) {
      vec4 bits = v * 255.0;
      float sign = mix(-1.0, 1.0, step(bits[3], 128.0));
      float expo = floor(mod(bits[3] + 0.1, 128.0)) * 2.0 +
                  floor((bits[2] + 0.1) / 128.0) - 127.0;
      float sig = bits[0] +
                  bits[1] * 256.0 +
                  floor(mod(bits[2] + 0.1, 128.0)) * 256.0 * 256.0;
      return sign * (1.0 + sig / 8388607.0) * pow(2.0, expo);
    }

    // float orthographicDepthToViewZ( const in float linearClipZ, const in float near, const in float far ) {
    //   return linearClipZ * ( near - far ) - near;
    // }
    float perspectiveDepthToViewZ( const in float depth, const in float near, const in float far ) {
      // maps perspective depth in [ 0, 1 ] to viewZ
      return ( near * far ) / ( ( far - near ) * depth - far );
    }

    void main() {
      // Calculate the distance from the vertex to the camera
      float distance = length(cameraPosition - vWorldPosition.xyz);
      
      // Normalize the distance (assuming 1000 is the maximum distance to visualize)
      // float normalizedDepth = clamp(distance / 50.0, 0.0, 1.0);
      
      // Assign color based on depth
      // gl_FragColor = vec4(vec3(normalizedDepth), 1.0);
      // gl_FragColor = encode_float(distance).abgr;
      gl_FragColor = vec4(vec3(distance), 1.);
    }

    // void main() {
    //   // float d = gl_FragCoord.z/gl_FragCoord.w;
    //   float d = gl_FragCoord.z/gl_FragCoord.w;
    //   // float viewZ = orthographicDepthToViewZ(d, cameraNear, cameraFar);
    //   float viewZ = perspectiveDepthToViewZ(d, cameraNear, cameraFar);
    //   // gl_FragColor = encode_float(viewZ).abgr;
    //   gl_FragColor = vec4(viewZ, 0., 0., 1.0);
    // }
  `;

  //

  // const depthTexture = new THREE.DepthTexture();
  const renderTarget = new THREE.WebGLCubeRenderTarget(512, {
    minFilter: THREE.NearestFilter,
    magFilter: THREE.NearestFilter,
    generateMipmaps: false,
    encoding: THREE.sRGBEncoding,
    // type: THREE.FloatType,
    // depthTexture,
  });
  renderTarget.texture.mapping = THREE.CubeRefractionMapping;
  // renderTarget.texture.mapping = THREE.CubeUVReflectionMapping;
  const cubeCamera = new THREE.CubeCamera(camera.near, camera.far, renderTarget);

  // const depthTexture = new THREE.DepthTexture();
  const renderTargetDepth = new THREE.WebGLCubeRenderTarget(512, {
    minFilter: THREE.NearestFilter,
    magFilter: THREE.NearestFilter,
    generateMipmaps: false,
    type: THREE.FloatType,
    // depthTexture,
  });
  renderTargetDepth.texture.mapping = THREE.CubeRefractionMapping;
  // renderTarget.texture.mapping = THREE.CubeUVReflectionMapping;
  const cubeCameraDepth = new THREE.CubeCamera(camera.near, camera.far, renderTargetDepth);

  cubeCamera.position.copy(position);
  scene.add(cubeCamera);
  cubeCamera.updateMatrixWorld();
  
  cubeCameraDepth.position.copy(position);
  scene.add(cubeCameraDepth);
  cubeCameraDepth.updateMatrixWorld();

  // Temporarily hide the localPlayer so it won't be visible in the cube camera render
  // localPlayer.visible = false;
  const oldParent = localPlayer.appManager.parent;
  oldParent.remove(localPlayer.appManager);

  // Render the scene to the cube camera
  cubeCamera.update(renderer, scene);

  // read the cube camera pixels (Uint8Array)
  const uint8Arrays = [];
  // const {texture} = renderTarget;
  // const {image} = texture;
  for (let i = 0; i < 6; i++) {
    // use readRenderTargetPixels
    const uint8Array = new Uint8Array(512 * 512 * 4);
    renderer.readRenderTargetPixels(renderTarget, 0, 0, 512, 512, uint8Array, i);
    uint8Arrays.push(uint8Array);
  }
  // globalThis.uint8Arrays = uint8Arrays;

  // render the scene to the depth cube camera
  scene.overrideMaterial = new THREE.ShaderMaterial({
    uniforms: {
      cameraNear: {
        value: camera.near,
        needsUpdate: true,
      },
      cameraFar: {
        value: camera.far,
        needsUpdate: true,
      },
      // cameraPosition: {
      //   value: camera.position,
      //   needsUpdate: true,
      // },
    },
    vertexShader: depthVertexShader,
    fragmentShader: depthFragmentShader,
  });
  cubeCameraDepth.update(renderer, scene);

  // read the cube camera pixels (Float32Array)
  const float32DepthArrays = [];
  // const {texture} = renderTarget;
  // const {image} = texture;
  for (let i = 0; i < 6; i++) {
    // use readRenderTargetPixels
    const float32Array = new Float32Array(512 * 512 * 4);
    renderer.readRenderTargetPixels(renderTargetDepth, 0, 0, 512, 512, float32Array, i);
    float32DepthArrays.push(float32Array);
  }
  // globalThis.float32DepthArrays = float32DepthArrays;

  //

  // order:
  // right Positive X (px)
  // left Negative X (nx)
  // up Positive Y (py)
  // down Negative Y (ny)
  // back Positive Z (pz)
  // forward Negative Z (nz)

  // Make the localPlayer visible again
  // localPlayer.visible = true;
  scene.overrideMaterial = null;
  oldParent.add(localPlayer.appManager);
  scene.updateMatrixWorld();

  //

  const faceOrder = [
    [ // top
      [2, -Math.PI / 2],
      [2, Math.PI],
      [2, Math.PI / 2],
      [2, 0],
      [2, -Math.PI / 2],
    ],
    [ // center
      [1, 0], // left
      [5, 0], // forward
      [0, 0], // right
      [4, 0], // back
      [1, 0], // left
    ],
    [ // bottom
      [3, Math.PI / 2],
      [3, Math.PI],
      [3, -Math.PI / 2],
      [3, 0],
      [3, Math.PI / 2],
    ],
  ];
  const numRows = faceOrder.length;
  const numCols = faceOrder[0].length;
  const faceOrderInverse = Array(6).fill(null);
  for (let y = 0; y < numRows; y++) {
    const xs = [1, 0, 2, 3, 4];
    for (const x of xs) {
      const [faceIndex, rotationY] = faceOrder[y][x];
      if (faceOrderInverse[faceIndex] === null) {
        const k = [x, y, rotationY].join(',');
        faceOrderInverse[faceIndex] = k;
      }
    }
  }
  // globalThis.faceOrderInverse = faceOrderInverse;
  if (faceOrderInverse.some(n => n === null)) {
    debugger;
  }

  const applyRotationZ = (innerX, innerY, w, h, rotationZ) => {
    switch (rotationZ) {
      case 0: {
        // nothing
        break;
      }
      case Math.PI/2: {
        [innerX, innerY] = [innerY, h - innerX];
        break;
      }
      case Math.PI: {
        [innerX, innerY] = [w - innerX, h - innerY];
        break;
      }
      case -Math.PI/2: {
        [innerX, innerY] = [w - innerY, innerX];
        break;
      }
      default: {
        throw new Error(`invalid rotationZ: ${rotationZ}`);
      }
    }
    return [innerX, innerY];
  }

  /**
   * Map sphere UV to cube render target face.
   *
   * @param {number} u - The u coordinate of the sphere UV.
   * @param {number} v - The v coordinate of the sphere UV.
   * @returns {Object} An object containing the cubeRenderTargetIndex, newU, and newV.
   */
  function mapSphereUVToCube(u, v, radius = 1) {
    // Step 1: Convert spherical UV to 3D Cartesian coordinates
    const phi = 2 * Math.PI * u;
    const theta = Math.PI * v;
  
    const x = -radius * Math.cos(phi) * Math.sin(theta);
    const y = radius * Math.cos(theta);
    const z = radius * Math.sin(phi) * Math.sin(theta);
  
    // Step 2: Determine which face of the cube the point belongs to
    const absX = Math.abs(x);
    const absY = Math.abs(y);
    const absZ = Math.abs(z);
    
    let cubeRenderTargetIndex;
    let axisMax = Math.max(absX, absY, absZ);
  
    if (axisMax === absX) {
      cubeRenderTargetIndex = x > 0 ? 0 : 1;  // px or nx
    } else if (axisMax === absY) {
      cubeRenderTargetIndex = y > 0 ? 2 : 3;  // py or ny
    } else if (axisMax === absZ) {
      cubeRenderTargetIndex = z > 0 ? 4 : 5;  // pz or nz
    }
  
    // Step 3: Find the UV coordinate on that face
    let uCube, vCube;

    switch (cubeRenderTargetIndex) {
      case 0:  // px
        uCube = -z / absX;
        vCube = y / absX;
        break;
      case 1:  // nx
        uCube = z / absX;
        vCube = y / absX;
        break;
      case 2:  // py
        uCube = x / absY;
        vCube = z / absY;
        break;
      case 3:  // ny
        uCube = x / absY;
        vCube = -z / absY;
        break;
      case 4:  // pz
        uCube = x / absZ;
        vCube = y / absZ;
        break;
      case 5:  // nz
        uCube = -x / absZ;
        vCube = y / absZ;
        break;
    }
    function uvToSpherical(uv) {
      // Ensure uv coordinates are normalized to [0, 1]
      let u = uv.x % 1;
      let v = uv.y % 1;
  
      // Handle negative uv coordinates
      u = u < 0 ? u + 1 : u;
      v = v < 0 ? v + 1 : v;
  
      const theta = 2 * Math.PI * u;
      const phi = Math.PI * (1 - v);
  
      return {
        theta,
        phi,
      };
    }
  
    // Remap uCube and vCube to [0, 1]
    uCube = uCube * 0.5 + 0.5;
    vCube = -vCube * 0.5 + 0.5;

    const cubeRenderTargetIndexInverseString = faceOrderInverse[cubeRenderTargetIndex];
    const match = cubeRenderTargetIndexInverseString.match(/^(.+?),(.+?),(.+?)$/);
    const indexU = parseInt(match[1], 10);
    const indexV = parseInt(match[2], 10);
    const rotationZ = parseFloat(match[3]);

    return {
      cubeRenderTargetIndex,
      indexU,
      indexV,
      rotationZ,
      u: uCube,
      v: vCube
    };
  }

  // lay out the image in a canvas, in the order [right, back, left, forward, up, down]
  const canvas = document.createElement('canvas');
  canvas.width = 512 * numCols;
  canvas.height = 512 * numRows;
  const ctx = canvas.getContext('2d');
  for (let y = 0; y < numRows; y++) {
    for (let x = 0; x < numCols; x++) {
      const baseX = x * 512;
      const baseY = y * 512;

      const [imageIndex, rotationZ] = faceOrder[y][x];

      const uint8Array = uint8Arrays[imageIndex];
      const imageData = ctx.createImageData(512, 512);
      // read the data, flipping in the x direction
      for (let y = 0; y < 512; y++) {
        for (let x = 0; x < 512; x++) {
          const [x2, y2] = applyRotationZ(x, y, 512, 512, rotationZ);

          const index = (y2 * 512 + x2) * 4;
          const index2 = (y * 512 + (511 - x)) * 4;
          imageData.data[index2 + 0] = uint8Array[index + 0];
          imageData.data[index2 + 1] = uint8Array[index + 1];
          imageData.data[index2 + 2] = uint8Array[index + 2];
          imageData.data[index2 + 3] = uint8Array[index + 3];
        }
      }
      ctx.putImageData(imageData, baseX, baseY);
    }
  }
  const renderWidth = 500;
  canvas.style.cssText = `\
    position: fixed;
    top: 0;
    left: 0;
    width: ${renderWidth}px;
  `;
  document.body.appendChild(canvas);

  // same for depth
  const canvasDepth = document.createElement('canvas');
  canvasDepth.width = 512 * numCols;
  canvasDepth.height = 512 * numRows;
  const ctxDepth = canvasDepth.getContext('2d');
  for (let y = 0; y < numRows; y++) {
    for (let x = 0; x < numCols; x++) {
      const baseX = x * 512;
      const baseY = y * 512;

      const [imageIndex, rotationZ] = faceOrder[y][x];

      const float32DepthArray = float32DepthArrays[imageIndex];
      const imageData = ctxDepth.createImageData(512, 512);
      // read the data, flipping in the x direction
      for (let y = 0; y < 512; y++) {
        for (let x = 0; x < 512; x++) {
          const [x2, y2] = applyRotationZ(x, y, 512, 512, rotationZ);

          const index = (y2 * 512 + x2) * 4;
          const index2 = (y * 512 + (511 - x)) * 4;
          const depth = float32DepthArray[index + 0] / 10;
          const depth2 = Math.min(Math.max(depth, 0), 1);
          const depth3 = Math.floor(depth2 * 255);
          imageData.data[index2 + 0] = depth3;
          imageData.data[index2 + 1] = 0;
          imageData.data[index2 + 2] = 0;
          imageData.data[index2 + 3] = 255;
        }
      }
      ctxDepth.putImageData(imageData, baseX, baseY);
    }
  }
  canvasDepth.style.cssText = `\
    position: fixed;
    top: 300px;
    left: 0;
    width: ${renderWidth}px;
  `;
  document.body.appendChild(canvasDepth);

  // convert to panorama
  const equiUnmanaged = new CubemapToEquirectangular(renderer, false);
  const imageData = equiUnmanaged.convert(cubeCamera, false);
  const panoramaCanvas = document.createElement('canvas');
  panoramaCanvas.width = imageData.width;
  panoramaCanvas.height = imageData.height;
  const panoramaCtx = panoramaCanvas.getContext('2d');
  panoramaCtx.putImageData(imageData, 0, 0);

  // right now left is in the middle of the panorama
  // therefore, shift the panorama to the right by 0.25 of width, so forward is in the middle
  // carefully redraw the result in two slices, since the panorama loops in the x direction
  const panoramaCanvas2 = document.createElement('canvas');
  panoramaCanvas2.width = imageData.width;
  panoramaCanvas2.height = imageData.height;
  const panoramaCtx2 = panoramaCanvas2.getContext('2d');
  panoramaCtx2.drawImage(panoramaCanvas, imageData.width * 0.25, 0, imageData.width * 0.75, imageData.height, 0, 0, imageData.width * 0.75, imageData.height);
  panoramaCtx2.drawImage(panoramaCanvas, 0, 0, imageData.width * 0.25, imageData.height, imageData.width * 0.75, 0, imageData.width * 0.25, imageData.height);
  panoramaCanvas2.style.cssText = `\
    position: fixed;
    top: 0;
    left: 0;
    width: ${renderWidth}px;
    z-index: 1;
  `;
  document.body.appendChild(panoramaCanvas2);
  const panoramaBlob = await new Promise((accept, reject) => {
    panoramaCanvas2.toBlob(accept, 'image/png');
  });

  // get the middle strip (the horizontal one)
  // const canvas2 = document.createElement('canvas');
  // canvas2.width = 512 * numCols;
  // canvas2.height = 512;
  // const ctx2 = canvas2.getContext('2d');
  // ctx2.drawImage(canvas, 0, 512, 512 * numCols, 512, 0, 0, 512 * numCols, 512);
  // const canvasBlob = await new Promise((accept, reject) => {
  //   canvas2.toBlob(accept, 'image/png');
  // });

  // read the llava caption
  {
    const response = await llava(panoramaBlob, 'What is in this image? Output the result as a JSON array of exactly 4 different strings.');
    console.log('got objects 1', {
      response,
    });

    try {
      let labels = JSON.parse(response);
      // get the unique set of labels
      labels = labels.filter((v, i, a) => a.indexOf(v) === i);
      const locations = [];
      for (const label of labels) {
        const location = await imageFind(panoramaBlob, label);
        console.log('got location', location);
        locations.push(location);
      }
    } catch(err) {
      console.warn(err.stack);
    }

    // const response2 = await llava(panoramaBlob, 'What is the text in this image, if any?');
    // console.log('got objects 2', {
    //   response2,
    // });
  }

  /* // read the vqa segmentation
  {
    const canvasBlob = await new Promise((accept, reject) => {
      canvas2.toBlob(accept, 'image/png');
    });
    const segments = await imageSegmentation(
      canvasBlob,
      (segmentationResult) => getTopSegmentBoxes(segmentationResult, {
        maxBoxes: 10,
      }),
    );
    console.log('got segments', segments);
    const boundingBoxLayers = segments.map(bbox => {
      const [
        x, y,
        width, height,
      ] = bbox;
      const bbox2 = [
        x, y,
        x + width, y + height,
      ];
      return {
        bbox: bbox2,
        label: '',
      };
    });
    const imageSegmentationSpec = {
      boundingBoxLayers,
    };
    const imageSegmentationCanvas = drawImageSegments(imageSegmentationSpec, canvas2, canvas2.width, canvas2.height);
    imageSegmentationCanvas.style.cssText = `\
      position: fixed;
      top: 0;
      left: 0;
      width: ${renderWidth}px;
    `;
    console.log('got segmentation canvas', {
      imageSegmentationCanvas,
    });
  } */

  // Create a sphere mesh to display the cube camera's render
  const sphereGeometry = new THREE.SphereGeometry(1, 128, 128/*, 0, Math.PI * 4*/);
  // sample the depth and apply it to the sphereGeometry
  const localVector = new THREE.Vector3();
  const localVector2D = new THREE.Vector2();
  for (let i = 0; i < sphereGeometry.attributes.position.count; i++) {
    // compute UV
    let u = sphereGeometry.attributes.uv.array[i * 2 + 0];
    // u = 1 - u; // flip u to match the cube camera
    let v = sphereGeometry.attributes.uv.array[i * 2 + 1];
    v = 1 - v; // flip v to match the cube camera
    let {
      cubeRenderTargetIndex,
      indexU,
      indexV,
      rotationZ,
      u: newU,
      v: newV,
    } = mapSphereUVToCube(u, v);
    // if (rotationZ !== 0) {
    //   debugger;
    // }

    // console.log('got', [
    //   indexU,
    //   indexV,
    // ].join(':'));

    // sample depth
    const float32DepthArray = float32DepthArrays[cubeRenderTargetIndex];

    let innerX = newU * 512;
    let innerY = newV * 512;
    [innerX, innerY] = applyRotationZ(innerX, innerY, 512, 512, rotationZ);
    innerX = Math.min(Math.max(Math.floor(innerX), 0), 512 - 1);
    innerY = Math.min(Math.max(Math.floor(innerY), 0), 512 - 1);

    const indexPx = ((innerY * 512) + innerX);
    const index4 = indexPx * 4;
    let depth = float32DepthArray[index4];
    if (depth === undefined) {
      debugger;
    }
    if (depth === 0) {
      depth = camera.far;
    }
    localVector.fromArray(sphereGeometry.attributes.position.array, i * 3);
    localVector.multiplyScalar(depth);
    localVector.toArray(sphereGeometry.attributes.position.array, i * 3);
    if (isNaN(localVector.x) || isNaN(localVector.y) || isNaN(localVector.z)) {
      debugger;
    }

    // set color UV
    const u2 = (
      (indexU * 512) +
      (512 - 1 - innerX)
    ) / (512 * numCols);
    const v2 = (
      ((3 - 1 - indexV) * 512) +
      (indexV !== 1 ? innerY : (512 - 1 - innerY))
    ) / (512 * numRows);
    localVector2D.set(u2, v2);
    localVector2D.toArray(sphereGeometry.attributes.uv.array, i * 2);
    // globalThis.uvs = sphereGeometry.attributes.uv.array;
  }
  const map = new THREE.Texture(canvas);
  map.minFilter = THREE.NearestFilter;
  map.magFilter = THREE.NearestFilter;
  map.needsUpdate = true;
  const cubeMaterial = new THREE.MeshBasicMaterial({
    // envMap: renderTarget.texture,
    map,
    // color: 0x200000,
    side: THREE.BackSide,
  });
  const cubeMesh = new THREE.Mesh(sphereGeometry, cubeMaterial);
  cubeMesh.position.copy(position);
  scene.add(cubeMesh);
  cubeMesh.updateMatrixWorld();
};

/* export default function PicketApp({
  Component,
  pageProps,
}) {
  return (
    <PicketProvider apiKey={process.env.PICKET_PUBLISHABLE_KEY}>
      <Component {...pageProps} />
    </PicketProvider>
  )
} */

/* globalThis.testSilkClaim = async (
  to = '0x08E242bB06D85073e69222aF8273af419d19E4f6',
  amount = 10,
  nonce = Math.floor(Math.random() * 0xFFFFFFFF),
) => {
  const {
    silk,
  } = await getSilkContractsAsync();

  // encode the above, packed, and hashed
  const encoded = ethers.solidityPacked(["address", "uint256", "uint256"], [to, amount, nonce]);
  // const hash = ethers.keccak256(encoded);
  console.log('got encoded', encoded);

  // the above is in 0x hex format, so we need to convert it to a Uint8Array
  // const buffer = Buffer.from(encoded.slice(2), 'hex');
  const buffer = Buffer.from(encoded.slice(2), 'hex');
  console.log('got buffer', buffer);

  const res = await fetch('https://silk-api.upstreet.ai/', {
    method: 'POST',
    body: buffer,
  });
  const json = await res.json();
  const {v, r, s} = json;
  console.log('got json', {v, r, s});

  const hash = ethers.keccak256(ethers.concat([
    ethers.toUtf8Bytes("\x19Ethereum Signed Message:\n84"),
    buffer,
  ]));
  console.log('recover', ethers.recoverAddress(hash, {v, r, s}));

  const result = await silk.claim(to, amount, nonce, v, r, s);
  console.log('got result', result);
}; */

/* globalThis.testVrmSpritesheetRender = async () => {
  const srcUrls = [
    '/public/avatars/male.vrm',
    '/public/avatars/female.vrm',
  ];
  await Promise.all(srcUrls.map(async (srcUrl, i) => {
    const res = await fetch(srcUrl);
    const arrayBuffer = await res.arrayBuffer();

    const size = 128;

    const spriteImages = await renderSpriteImages(arrayBuffer, srcUrl);
    // console.log('sprite images', spriteImages);
    [0, 1, 5].forEach((j, k) => {
      const canvas = spriteImages[j];
      // console.log('got name', canvas.name);
      canvas.style.cssText = `\
        position: fixed;
        top: 62px;
        left: ${(i * 3 + k) * size}px;
        width: ${size}px;
      `;
      document.body.appendChild(canvas);
    });
  }));
}; */

/* globalThis.testControlNetPoseGeneration = async () => {
  const blobs = await Promise.all([
    (async () => {
      // const res = await fetch('/images/poses/sd-controlnet/male-stand.png');
      const res = await fetch('/images/poses/sd-controlnet/male-walk.png');
      // const res = await fetch('/images/poses/sd-controlnet/male-run.png');
      const blob = await res.blob();
      return blob;
    })(),
    (async () => {
      // const res = await fetch('/images/poses/sd-controlnet/female-stand.png');
      const res = await fetch('/images/poses/sd-controlnet/female-walk.png');
      // const res = await fetch('/images/poses/sd-controlnet/female-run.png');
      const blob = await res.blob();
      return blob;
    })(),
  ]);

  let cleanup = null;
  for (let i = 0; i < blobs.length; i++) {
    const vrmBlob = blobs[i];
    const vrmImageBitmap = await createImageBitmap(vrmBlob);
    const vrmFullCanvas = document.createElement('canvas');
    vrmFullCanvas.width = vrmImageBitmap.width;
    vrmFullCanvas.height = vrmImageBitmap.height;
    const vrmFullCtx = vrmFullCanvas.getContext('2d');
    vrmFullCtx.drawImage(vrmImageBitmap, 0, 0);

    const fullCanvas = document.createElement('canvas');
    fullCanvas.width = vrmImageBitmap.width;
    fullCanvas.height = vrmImageBitmap.height;
    fullCanvas.style.cssText = `\
      position: fixed;
      top: 62px;
      left: ${(1 + i) * displaySize}px;
      width: ${displaySize}px;
    `;
    document.body.appendChild(fullCanvas);
    const fullCtx = fullCanvas.getContext('2d');
    // globalThis.fullCanvas = fullCanvas;
    
    // now perform the real capture
    let angleIndex = 0;
    for (let angle = 0; angle < Math.PI * 2; angle += (Math.PI * 2) / numAngles) {
      for (let k = 0; k < numFrames; k++, angleIndex++) {
        const [x, y] = getFrameOffsetCoords(angleIndex);
        const [w, h] = getFrameSize();

        const vrmSliceCanvas = (() => {
          const canvas = document.createElement('canvas');
          canvas.width = w;
          canvas.height = h;
          const ctx = canvas.getContext('2d');
          ctx.drawImage(vrmFullCanvas, x, y, w, h, 0, 0, w, h);
          return canvas;
        })();
        const dataUrl = vrmSliceCanvas.toDataURL();
        
        const controlnet_input_images = [
          dataUrl,
        ];
        const j = {
          controlnet_module: 'openpose',
          controlnet_input_images,
          controlnet_processor_res: 512,
          controlnet_threshold_a: 64,
          controlnet_threshold_b: 64,
        };
      
        const aiProxyHost = 'ai-proxy.isekai.chat';
        const res2 = await fetch(`https://${aiProxyHost}/controlnet/detect`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(j),
        });
      
        const j2 = await res2.json();
        const {
          images,
        } = j2;
        if (images.length > 0) {
          console.log('got images', images);
      
          const res3 = await fetch(`data:image/png;base64,${images[0]}`);
          const blob3 = await res3.blob();
          const imageBitmap = await createImageBitmap(blob3);
      
          const sliceCanvas = document.createElement('canvas');
          sliceCanvas.width = imageBitmap.width;
          sliceCanvas.height = imageBitmap.height;
          const ctx = sliceCanvas.getContext('2d');
          ctx.drawImage(imageBitmap, 0, 0);

          cleanup && cleanup();

          vrmSliceCanvas.style.cssText = `\
            position: fixed;
            top: 62px;
            left: 0;
            width: ${displaySize}px;
          `;
          document.body.appendChild(vrmSliceCanvas);
          
          sliceCanvas.style.cssText = `\
            position: fixed;
            top: 62px;
            left: 0;
            width: ${displaySize}px;
            opacity: 0.5;
          `;
          document.body.appendChild(sliceCanvas);

          // drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
          fullCtx.drawImage(sliceCanvas, 0, 0, sliceCanvas.width, sliceCanvas.height, x, y, w, h);

          cleanup = () => {
            document.body.removeChild(vrmSliceCanvas);
            document.body.removeChild(sliceCanvas);
          };
        } else {
          console.log('no images', {
            angle,
            k,
          });
        }
      }
    }
  }
}; */

/* globalThis.testControlNetPoseDetection = async () => {
  // const res = await fetch('https://local.isekai.chat:4443/images/dress.png');
  const res = await fetch('https://local.isekai.chat:4443/images/pokemon-trainer-oc.png');
  // const res = await fetch('https://i.imgur.com/AyGhz4H.png');
  const blob = await res.blob();
  // http://cloud.webaverse.com:61647/
  const dataUrl = await new Promise((accept, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      accept(reader.result);
    };
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });

  const controlnet_input_images = [
    dataUrl,
  ];
  const j = {
    controlnet_module: 'openpose',
    controlnet_input_images,
    controlnet_processor_res: 512,
    controlnet_threshold_a: 64,
    controlnet_threshold_b: 64,
  };

  const aiProxyHost = 'ai-proxy.isekai.chat';
  const res2 = await fetch(`https://${aiProxyHost}/controlnet/detect`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(j),
  });

  // const blob2 = await res2.blob();
  // const text = await res2.text();
  // console.log('got blob2', blob2);
  const j2 = await res2.json();
  // console.log('got j', j2);
  const {
    images,
  } = j2;
  if (images.length > 0) {
    console.log('got images', images);

    const res3 = await fetch(`data:image/png;base64,${images[0]}`);
    const blob3 = await res3.blob();
    const imageBitmap = await createImageBitmap(blob3);

    const canvas = document.createElement('canvas');
    canvas.width = imageBitmap.width;
    canvas.height = imageBitmap.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(imageBitmap, 0, 0);
    canvas.style.cssText = `\
      position: fixed;
      top: 62px;
      left: 0;
      width: 512px;
      height: auto;
      z-inddex: 1;
    `;
    document.body.appendChild(canvas);
  }
}; */

// globalThis.testSplit = async () => {
//   let classifier = await pipeline('token-classification', 'benjamin/wtp-bert-mini');
//   let output = await classifier('My name is Sarah and I live in London');
//   console.log('got output', output);
// };

// globalThis.testStory = {
// You are an AI writer for an anime inspired extraction shooter battle royale video game. Generate some lore, characters, and a script in a modern and unique style -- try to avoid typical cliches of the genre, and try to mix in ideas from other styles and mediums. Focus on comedy and character relationships. Make the dialogue something that would appear in a highly rated battle anime.

// First, generate 4 characters, with their age and gender, and a short bio for each, including the relationships between the characters, and then generate a 10-minute episode screenplay in which they interact.

// Then, come up with an overall story arc that this episode belongs to. Write a paragraph about the current status of the plot and where this episode fits in with the overall lore.

// Then, write the scenes. For each scene, begin by listing the plot actions that happen in the scene, and the key information revealed. After that, write the action lines and dialogue. Each scene should be a couple of pages long (at least 50 lines). Before ending each scene, reflect on whether the scene is complete. If it is not, continue with the current scene instead of moving onto the next one.
// };

globalThis.testDiscordConnect = async (
  token = '',
  channelWhitelist = [
    'Upstreet:discord-bot',
    'Upstreet:🔊 Dev',
  ],
  userWhitelist = ['avaer'],

  model = 'elevenlabs',
  // voiceId = 'VsIWLW6b9PNzVO4nSUKQ',
  voiceId = 'PSAakCTPE63lB4tP9iNQ',
) => {
  console.log('connect to discord client 1');
  const discordClient = new DiscordClient({
    token,
    channelWhitelist,
    userWhitelist,
  });
  await discordClient.connect();
  console.log('connect to discord client 2');
  await new Promise((accept, reject) => {
    setTimeout(accept, 2000);
  });
  discordClient.input.writeText('test biach');
  console.log('connect to discord client 3');

  const voiceEndpoint = new AutoVoiceEndpoint({
    model,
    voiceId,
  });
//   const stream = await voiceEndpoint.getStream(`\
// In a digital realm, where dreams converge,
// With THREE.js the visuals emerge.
// In the vastness of the code's vast sea,
// SupabaseClient is the key.

// Bridging the worlds with a vibrant glow,
// imageCaptioning tells what we need to know.
// To manage the tasks, not a dance or a prance,
// The QueueManager takes its chance.

// Glistening lights, as the scenes unfold,
// generateGif brings stories untold.
// In the cosmic dance, where dimensions expand,
// CubemapToEquirectangular takes a stand.

// Through the lens of llava and imageFind,
// Secrets of images, we're destined to bind.
// Crafting the scenes, where dreams intertwine,
// drawImageSegments makes it all shine.

// Mystical powers, the Alchemy holds,
// In the blockchain tales, where legends are told.
// A unique identity, a mark so profound,
// With uuidByString, it's easily found.

// To strip away the noise, to see what's beneath,
// remark and strip-markdown unsheath.
// In the world of voices, where whispers begin,
// WHIPClient and AutoVoiceEndpoint join in.
//   `);
//   console.log('connect to discord client 4');
//   const abortController = new AbortController();
//   const {signal} = abortController;
//   await discordClient.input.pushStream(stream, {
//     signal,
//   });
  console.log('connect to discord client 5');
  discordClient.output.addEventListener('usermessage', async e => {
    const {
      username,
      text,
    } = e.data;
    console.log('got user message 1', {username, text});
    const stream = await voiceEndpoint.getStream(`${username} just said ${text}`);
    console.log('got user message 2', text);
    const abortController = new AbortController();
    const {signal} = abortController;
    await discordClient.input.pushStream(stream, {
      signal,
    });
    console.log('got user message 3', text);
  });
  console.log('connect to discord client 6');
};

//

globalThis.testTwitchStream = async (
  streamKey = 'live_',
) => {
  const videoIngest = document.createElement('video');
  videoIngest.style.cssText = `\
    position: fixed;
    top: 0;
    left: 0;
    width: 300px;
    z-index: 10;
  `;
  document.body.appendChild(videoIngest);
  const mediaStream = await navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true,
  });
  videoIngest.srcObject = mediaStream;

  const twitchVideoClient = new TwitchVideoClient();
  await twitchVideoClient.connect({
    streamKey,
    mediaStreams: [
      mediaStream,
    ],
  });
};
globalThis.testTwitchChat = async (
  username = 'avaer',
  password = 'oauth:',
  channels = [
    'avaer',
  ],
) => {
  console.log('twitch chat connect 1');
  const twitchChatClient = new TwitchChatClient();
  await twitchChatClient.connect({
    username,
    password,
    channels,
  });
  console.log('twitch chat connect 2');
  twitchChatClient.addEventListener('message', e => {
    console.log('got twitch chat message', e.data);
  });
};
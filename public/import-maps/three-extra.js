const {THREEExtra} = globalThis.Metaversefile.exports;

const {
  // GLTFLoader,

  ChunkedBatchedMesh,
  ChunkedGeometryAllocator,
  
  InstancedBatchedMesh,
  InstancedGeometryAllocator,
} = THREEExtra;

export {
  // GLTFLoader,

  ChunkedBatchedMesh,
  ChunkedGeometryAllocator,

  InstancedBatchedMesh,
  InstancedGeometryAllocator,
};
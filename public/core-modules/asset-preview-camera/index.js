import * as THREE from 'three';

export default ctx => {
  const {
    useApp,
    useEngineRenderer,
    useCameraManager,
  } = ctx;

  const app = useApp();
  app.name = 'Asset preview camera';

  const engineRenderer = useEngineRenderer();
  const cameraManager = useCameraManager();
  cameraManager.setControllerFn(() => {
    const {
      camera,
    } = engineRenderer;

    const now = performance.now();
    const rotationTime = 10 * 1000;
    const radius = 2;
    camera.position.set(
      Math.cos(now / rotationTime * Math.PI * 2) * radius,
      1.6,
      Math.sin(now / rotationTime * Math.PI * 2) * radius,
    );
    camera.lookAt(0, 1.6, 0);
    camera.updateMatrixWorld();
  });
};
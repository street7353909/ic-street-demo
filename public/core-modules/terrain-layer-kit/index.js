import * as THREE from 'three';
import {
  LitterMetaMesh,
} from './layers/litter-mesh.js';

const chunkSize = 64;

const localUpVector = new THREE.Vector3(0, 1, 0);
const localQuaternion = new THREE.Quaternion();

export default ctx => {
  const {
    useApp,
    useCamera,
    useEngineRenderer,
    usePhysics,
    useLoaders,
    useTextureAtlasLoader,
    useSpriteLoader,
    useAppTracker,
    useFrame,
  } = ctx;
  const app = useApp();
  const camera = useCamera();
  const engineRenderer = useEngineRenderer();
  const {renderer} = engineRenderer;
  const physics = usePhysics();
  const {
    gltfLoader,
  } = useLoaders();
  const textureAtlasLoader = useTextureAtlasLoader();
  const spriteLoader = useSpriteLoader();
  const appTracker = useAppTracker();

  ctx.waitUntil((async () => {
    const instance = {
      chunkSize,
    };
    const litterMesh = new LitterMetaMesh({
      camera,

      renderer,
      instance,

      physics,
      gltfLoader,
      textureAtlasLoader,
      spriteLoader,
    });

    await litterMesh.waitForLoad();

    useFrame(() => {
      litterMesh.update();
    });

    appTracker.registerAppTracker({
      query: /terrain$/,
      addCb: async terrainApp => {
        const numGeometries = litterMesh.polygonMesh.allocator.geometryRegistry.size;

        await terrainApp.waitForLoad();
        terrainApp.listenTerrains(e => {
          const {
            terrain,
          } = e;
            const {layer4Positions, layer4Quaternions, layer4Infos} = terrain;

            if (layer4Positions.length > 0) {
              // console.log('got chunk', chunk.x, chunk.z);
              const chunk2 = {
                min: new THREE.Vector2(terrain.x, terrain.z)
                  .divideScalar(chunkSize),
                lod: terrain.halfSize * 2 / chunkSize,
              };
              // console.log('got lod', chunk2.lod);
              const ps = layer4Positions;

              const qs = new Float32Array(layer4Quaternions.length);
              for (let i = 0; i < layer4Quaternions.length; i += 4) {
                localQuaternion
                  .setFromAxisAngle(localUpVector, layer4Quaternions[i + 0])
                  .toArray(qs, i);
              }

              const instancesSrc = new Uint32Array(layer4Infos.buffer, layer4Infos.byteOffset, layer4Infos.byteLength / 4);
              const instances = new Uint32Array(instancesSrc.length / 3);
              for (let i = 0; i < instances.length; i++) {
                instances[i] = instancesSrc[i * 3] % numGeometries;
              }

              // resolve the instances
              const instanceMap = new Map();
              for (let i = 0; i < instances.length; i++) {
                const instanceId = instances[i];
                let instance = instanceMap.get(instanceId);
                if (!instance) {
                  instance = {
                    instanceId,
                    ps: [],
                    qs: [],
                  };
                  instanceMap.set(instanceId, instance);
                }
                instance.ps.push(
                  ps[i * 3 + 0],
                  ps[i * 3 + 1],
                  ps[i * 3 + 2],
                );
                instance.qs.push(
                  qs[i * 4 + 0],
                  qs[i * 4 + 1],
                  qs[i * 4 + 2],
                  qs[i * 4 + 3],
                );
              }
              const chunkResult2 = Array.from(instanceMap.values());

              litterMesh.addChunk(chunk2, chunkResult2);
            }
        }, e => {
          const {
            terrain,
          } = e;
          // if (!chunk.customType) {
            // const {terrain} = chunk;
            // if (terrain) {
              const chunk2 = {
                min: new THREE.Vector2(terrain.x, terrain.z)
                  .divideScalar(chunkSize),
                lod: terrain.halfSize * 2 / chunkSize,
              };
              // console.log('remove ore chunk', terrain, chunk2);
              litterMesh.removeChunk(chunk2);
            // }
          // }
        });
      },
      removeCb: terrainApp => {
        // console.log('remove app', terrainApp);
      },
    });

    app.add(litterMesh);
    litterMesh.updateMatrixWorld();
  })());

  return app;
};
import * as THREE from 'three';
import {
  urlSpecs,
} from '../assets.js';
import {
  getNodeHash,
} from '../utils.js';

const maxLod = 1;

export class NpcMesh extends THREE.Object3D {
  constructor({
    gltfLoader,
    importManager,
    assetCache,
  }) {
    super();

    this.gltfLoader = gltfLoader;
    this.importManager = importManager;
    this.assetCache = assetCache;

    this.vrms = [];
    this.chunkNpcs = new Map();
  }
  update() {
    // XXX
  }
  async waitForLoad() {
    const {gltfLoader} = this;
    const _loadModel = u => new Promise((accept, reject) => {
      gltfLoader.load(u, accept, function onProgress() {}, reject);
    });
    const vrms = await Promise.all(urlSpecs.vrms.map(async u => {
      const res = await fetch(u);
      const arrayBuffer = await res.arrayBuffer();

      this.assetCache.addAsset(u, {
        arrayBuffer,
      });

      const name = u.match(/([^\/]*)$/)[1];
      return {
        name,
        start_url: u,
      };
    }));
    this.vrms = vrms;
  }
  addChunk(chunk, chunkResult) {
    if (chunk.lod <= maxLod && chunkResult.ps.length > 0) {
      // this.chunkNpcs.set(key, chunkResult);

      // console.log('add npc chunk', chunk, chunkResult);

      (async () => {
        const npcs = [];
        const promises = [];
        for (let i = 0; i < chunkResult.ps.length; i += 3) {
          const position = new THREE.Vector3().fromArray(chunkResult.ps, i);
          position.y += 10;
          const instance = chunkResult.instances[i / 3];

          const vrmIndex = Math.floor(instance % this.vrms.length);
          const vrm = this.vrms[vrmIndex];
          const {
            name,
            start_url,
          } = vrm;

          const type = 'application/npc';
          const content = {
            name,
            avatarUrl: start_url,
            voicePack: 'ShiShi voice pack',
            bio: 'An NPC',
          };
          console.log('add npc', content);

          const npcApp = this.importManager.createApp();
          this.add(npcApp);
          npcApp.updateMatrixWorld();
          npcs.push(npcApp);

          const promise = this.importManager.createAppAsync({
            type,
            content,
            app: npcApp,
            position,
          });
          promises.push(promise);
        }

        const key = getNodeHash(chunk);
        this.chunkNpcs.set(key, npcs);

        await Promise.all(promises);
      })();
    }
  }
  removeChunk(chunk) {
    const key = getNodeHash(chunk);
    const npcs = this.chunkNpcs.get(key);
    if (npcs) {
      for (const npcApp of npcs) {
        console.log('remove npc chunk app', npcApp);

        npcApp.parent.remove(npcApp);
        npcApp.destroy();
      }
      this.chunkNpcs.delete(key);
    }
  }
}
import * as THREE from 'three';
// import {
//   HudMesh,
// } from './layers/hud-mesh.js';
import {
  NpcMesh,
} from './layers/npc-mesh.js';
// import {
//   urlSpecs,
// } from './assets.js';

const chunkSize = 64;

export default ctx => {
  const {
    useApp,
    // useCamera,
    // useEngineRenderer,
    useLoaders,
    useImportManager,
    useAssetCache,
    useAppTracker,
    useFrame,
  } = ctx;
  const app = useApp();
  // const camera = useCamera();
  // const engineRenderer = useEngineRenderer();
  // const {renderer} = engineRenderer;
  const {gltfLoader} = useLoaders();
  const importManager = useImportManager();
  const assetCache = useAssetCache();
  const appTracker = useAppTracker();

  app.name = 'npc';

  ctx.waitUntil((async () => {
    const npcMesh = new NpcMesh({
      gltfLoader,
      importManager,
      assetCache,
    });

    await npcMesh.waitForLoad();

    useFrame(() => {
      npcMesh.update();
    });

    appTracker.registerAppTracker({
      query: /terrain$/,
      addCb: terrainApp => {
        terrainApp.listenTerrains(e => {
          const {
            terrain,
          } = e;
          const {layer3Positions, layer3Infos} = terrain;
          if (layer3Positions.length > 0) {
            const chunk2 = {
              min: new THREE.Vector2(terrain.x, terrain.z)
                .divideScalar(chunkSize),
              lod: terrain.halfSize * 2 / chunkSize,
            };
            const ps = layer3Positions
              .slice(0, 3);
            const instancesSrc = new Uint32Array(layer3Infos.buffer, layer3Infos.byteOffset, layer3Infos.byteLength / 4)
              .slice(0, 3);
            const instances = new Uint32Array(instancesSrc.length / 3);
            for (let i = 0; i < instances.length; i++) {
              instances[i] = instancesSrc[i * 3];
            }
            const chunkResult2 = {
              ps,
              instances,
            };
            npcMesh.addChunk(chunk2, chunkResult2);
          }
        }, e => {
          const {
            terrain,
          } = e;

          const {layer3Positions, layer3Infos} = terrain;
          if (layer3Positions.length > 0) {
            const chunk2 = {
              min: new THREE.Vector2(terrain.x, terrain.z)
                .divideScalar(chunkSize),
              lod: terrain.halfSize * 2 / chunkSize,
            };
            npcMesh.removeChunk(chunk2);
          }
        });
      },
      removeCb: terrainApp => {
        // console.log('remove app', terrainApp);
      },
    });

    app.add(npcMesh);
    npcMesh.updateMatrixWorld();
  })());

  return app;
};
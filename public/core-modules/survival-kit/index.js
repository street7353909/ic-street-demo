import * as THREE from 'three';

const baseUrl = import.meta.url.replace(/(\/)[^\/\\]*$/, '$1');

export default ctx => {
  const {
    useApp,
    useFrame,
    usePhysics,
    useEngine,
    useCleanup,
    useImportManager,
  } = ctx;

  //

  const app = useApp();
  //   const engine = useEngine();
  //   const physics = usePhysics();
  const importManager = useImportManager();

  let survival = null;
  (async () => {
    const survivalPromise = (async () => {
      survival = await importManager.createAppAsync({
        start_url: `${baseUrl}survival2.glb`,
        components: {
          physics: false,
        },
      });
      survival.frustumCulled = false;
      app.add(survival);
      survival.updateMatrixWorld();

      for (let i = 0; i < survival.children[0].children.length; i++) {
        const mesh = survival.children[0].children[i];
        mesh.position.x = i;
        mesh.position.y = 0;
        mesh.position.z = 0;
        mesh.updateMatrixWorld();
      }
    })();
    const vipeCityKitPromise = (async () => {
      const vipeCityKit = await importManager.createAppAsync({
        start_url: `${baseUrl}VipeCityKit.glb`,
        components: {
          physics: false,
        },
      });
      vipeCityKit.frustumCulled = false;
      app.add(vipeCityKit);
      vipeCityKit.updateMatrixWorld();

    })();
    await Promise.all([
      survivalPromise,
      vipeCityKitPromise,
    ]);
  })();

  useCleanup(() => {
    // for (const physicsId of physicsIds) {
    //   physics.removeGeometry(physicsId);
    // }

    survival.destroy();
  });

  return app;
};
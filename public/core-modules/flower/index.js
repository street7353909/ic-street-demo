import * as THREE from 'three';

const baseUrl = import.meta.url.replace(/(\/)[^\/\\]*$/, '$1');

// const emptyArray = [];
// const fnEmptyArray = () => emptyArray;

// const localVector = new THREE.Vector3();

const urlPrefix = `https://avaer.github.io/fx-textures/`;
const fileIndexUrl = `${urlPrefix}fx-files.json`;

// const canvasSize = 4096;
// const frameSize = 512;
// const rowSize = Math.floor(canvasSize/frameSize); // 8

const particleName = 'Elements - Energy 017 Charge Up noCT noRSZ.ktx2';
const explosionName = 'Elements - Energy 119 Dissapear noCT noRSZ.ktx2';
const explosion2Name = 'Elements - Explosion 014 Hit Radial MIX noCT noRSZ.ktx2';
const particleNames = [
  particleName,
  explosionName,
  explosion2Name,
];

const components = [
  // {
  //   "key": "physics",
  //   "value": true,
  // },
  {
    "key": "wear",
    "value": {
      "boneAttachment": "leftHand",
      "position": [0, 0, 0],
      "quaternion": [0.4999999999999999, -0.5, -0.5, 0.5000000000000001]
    }
  },
  {
    "key": "aim",
    "value": {}
  },
  {
    "key": "use",
    "value": {
      "animation": "magic",
      "boneAttachment": "leftHand",
      "position": [0, 0, 0],
      "quaternion": [0.4999999999999999, -0.5, -0.5, 0.5000000000000001],
      "scale": [1, 1, 1]
    }
  },
];

export default ctx => {
  const {
    useApp,
    useScene,
    // usePhysics,
    // useActivate,
    useEngine,
    useParticleSystem,
    useImportManager,
    useLocalPlayer,
    useFrame,
    // useWear,
    useUse,
    useCleanup,
    // getNextInstanceId,
  } = ctx;
  const app = useApp();
  const scene = useScene();
  const engine = useEngine();
  const particleSystemManager = useParticleSystem();
  const importManager = useImportManager();

  app.name = 'flower';
  app.description = 'A magical flower.';

  app.setComponents(components);

  class ParticleEmitter2 extends THREE.Object3D {
    constructor(particleSystem) {
      super();

      this.particleSystem = particleSystem;

      this.timeout = null;
      const now = performance.now()
      this.resetNextUpdate(now);
      this.particles = [];
    }
    resetNextUpdate(now) {
      this.lastParticleTimestamp = now;
      this.nextParticleDelay = Math.random() * 100;
    }
    update(timestamp) {
      const localPlayer = useLocalPlayer();
      const now = timestamp;
      const timeDiff = now - this.lastParticleTimestamp;
      const duration = 1000;

      const _removeParticles = () => {
        this.particles = this.particles.filter(particle => {
          const timeDiff = now - particle.startTime;
          if (timeDiff < duration) {
            return true;
          } else {
            particle.destroy();
            return false;
          }
        });
      };
      _removeParticles();
      if (wearing) {
        const _addParticles = () => {
          if (timeDiff >= this.nextParticleDelay) {
            const particleName = particleNames[Math.floor(Math.random() * particleNames.length)];
            const particle = this.particleSystem.addParticle(particleName, {
              duration,
            });
            particle.offset = new THREE.Vector3((-0.5 + Math.random()) * 2, (-0.5 + Math.random()) * 2, (-0.5 + Math.random()) * 2);
            this.particles.push(particle);

            this.resetNextUpdate(timestamp);
          }
        };
        _addParticles();
        const _updateParticles = () => {
          if (this.particles.length > 0) {
            for (const particle of this.particles) {
              particle.position.copy(localPlayer.position)
                .add(particle.offset)
              particle.update();
            }
          }
        };
        _updateParticles();
      }
    }
  }

  let particleSystem = null;
  let particleEmitter2 = null;
  ((async () => {
    particleSystem = particleSystemManager.createParticleSystem();
    globalThis.particleSystem = particleSystem; // XXX

    const fileIndexJson = await (async () => {
      const res = await fetch(fileIndexUrl);
      const j = await res.json();
      return j;
    })();

    const promises = [];
    for (const name of particleNames) {
      const fileSpec = fileIndexJson.find(fs => fs.name === name);
      const {
        numFrames,
        rowSize,
      } = fileSpec;
      const textureUrl = `${urlPrefix}${name}`;

      const p = particleSystem.loadTexture(name, textureUrl, numFrames, rowSize);
      promises.push(p);
    }
    await Promise.all(promises);
    
    scene.add(particleSystem);
    particleSystem.updateMatrixWorld();

    particleEmitter2 = new ParticleEmitter2(particleSystem);

    useCleanup(() => {
      scene.remove(particleSystem);
      particleSystem.destroy();
    });
  })());

  let flowerApp = null;
  ctx.waitUntil((async () => {
    let u2 = `${baseUrl}Group2_Orchid.glb`;

    flowerApp = importManager.createApp();
    flowerApp.name = app.name;
    flowerApp.description = app.description;
    app.add(flowerApp);
    flowerApp.updateMatrixWorld();

    const appContext = engine.engineAppContextFactory.makeAppContext({
      app: flowerApp,
    });

    await importManager.createAppAsync({
      app: flowerApp,
      appContext,
      start_url: u2,
      components,
    });

    // flowerApp.addEventListener('use', e => {
    //   console.log('flower use');
    // });

    useCleanup(() => {
      if (flowerApp) {
        scene.remove(flowerApp);
        flowerApp.destroy();
      }
    });
  })());

  // app.getPhysicsObjects = () => {
  //   return flowerApp ? flowerApp.getPhysicsObjectsOriginal() : [];
  // };
  
  let wearing = false;
  app.addEventListener('wearupdate', e => {
    const {wear} = e;

    // flowerApp.position.copy(app.position);
    // flowerApp.quaternion.copy(app.quaternion);
    // flowerApp.scale.copy(app.scale);
    // flowerApp.updateMatrixWorld();
    
    // flowerApp.dispatchEvent({
    //   ...e,
    // });

    wearing = wear;
  });

  // useActivate(e => {
  //   if (e.use && flowerApp) {
  //     flowerApp.use();
  //   }
  // });

  useFrame((timestamp) => {
    // if (!wearing) {
    //   if (flowerApp) {
    //     flowerApp.position.copy(app.position);
    //     flowerApp.quaternion.copy(app.quaternion);
    //     flowerApp.updateMatrixWorld();
    //   }
    // } else {
    //   if (flowerApp) {
    //     app.position.copy(flowerApp.position);
    //     app.quaternion.copy(flowerApp.quaternion);
    //     app.updateMatrixWorld();
    //   }
    // }

    // particleEmitter.update(timestamp);
    particleEmitter2 && particleEmitter2.update(timestamp);
  });
  
  return app;
};
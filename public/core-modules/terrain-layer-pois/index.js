import * as THREE from 'three';
import {
  HudMesh,
} from './layers/hud-mesh.js';
import {
  urlSpecs,
} from './assets.js';

const chunkSize = 64;

export default ctx => {
  const {
    useApp,
    useCamera,
    useEngineRenderer,
    useAppTracker,
    useFrame,
  } = ctx;
  const app = useApp();
  const camera = useCamera();
  const engineRenderer = useEngineRenderer();
  const {renderer} = engineRenderer;
  const appTracker = useAppTracker();

  app.name = 'poi';
  // console.log('poi app', app);

  ctx.waitUntil((async () => {
    const instance = {
      chunkSize,
    };
    const hudMesh = new HudMesh({
      camera,
      renderer,
      instance,
    });

    await hudMesh.waitForLoad();

    useFrame(() => {
      hudMesh.update();
    });

    const addedHashes = new Set();
    appTracker.registerAppTracker({
      query: /terrain$/,
      addCb: async terrainApp => {
        // console.log('got terrain app', terrainApp);

        await terrainApp.waitForLoad();
        terrainApp.listenTerrains(e => {
          const {
            terrain,
          } = e;
          // if (!chunk.customType) {
            // chunk.events.on('ready', () => {
              const chunk2 = {
                min: new THREE.Vector2(terrain.x, terrain.z)
                  .divideScalar(chunkSize),
                lod: terrain.halfSize * 2 / chunkSize,
              };
              // console.log('add poi terrain', terrain, chunk2);

              // if (terrain) {
                const {layer1Positions, layer1Infos} = terrain;
                // if (!layer1Positions) {
                //   debubgger;
                // }

                if (layer1Positions.length > 0) {
                  const ps = layer1Positions;
                  const instancesSrc = new Uint32Array(layer1Infos.buffer, layer1Infos.byteOffset, layer1Infos.byteLength / 4);
                  const instances = new Uint32Array(instancesSrc.length / 3);
                  for (let i = 0; i < instances.length; i++) {
                    instances[i] = instancesSrc[i * 4] % urlSpecs.huds.length;
                  }
                  const chunkResult2 = {
                    ps,
                    instances,
                  };
                  // console.log('add chunk', {
                  //   chunk2,
                  //   chunkResult2,
                  // });
                  hudMesh.addChunk(chunk2, chunkResult2);
                // } else {
                //   const coords = new THREE.Vector2(terrain.x, terrain.z)
                //     .divideScalar(chunkSize);
                //   console.warn('do not add chunk 1', terrain, `${coords.x},${coords.y}:${terrain.halfSize * 2 / chunkSize}`);
                }
              // } else {
              //   console.warn('do not add chunk 2', chunk, chunk?.terrain);
              // }
            // });
          // }
        }, e => {
          const {
            terrain,
          } = e;
          // if (!chunk.customType) {
            // const {terrain} = chunk;
            // if (terrain) {

            const chunk2 = {
              min: new THREE.Vector2(terrain.x, terrain.z)
                .divideScalar(chunkSize),
              lod: terrain.halfSize * 2 / chunkSize,
            };

            const {layer1Positions, layer1Infos} = terrain;
            if (layer1Positions.length > 0) {
              // console.log('remove poi chunk', terrain, chunk2);
              hudMesh.removeChunk(chunk2);
            }
          // }
        });
      },
      removeCb: terrainApp => {
        console.log('remove app', terrainApp);
        debugger;
      },
    });

    app.add(hudMesh);
    hudMesh.updateMatrixWorld();
  })());

  return app;
};
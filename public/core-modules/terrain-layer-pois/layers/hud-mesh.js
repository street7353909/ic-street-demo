import * as THREE from 'three';
import {IconPackage, IconMesh} from '../meshes/icon-mesh.js';
import {urlSpecs} from '../assets.js';

//

export const hudUrls = urlSpecs.huds;

//

const hudLodDistanceCutoff = 4;

//

export class HudMesh extends THREE.Object3D {
  constructor({
    camera,
    renderer,
    instance,
  }) {
    super();

    this.iconMesh = new IconMesh({
      camera,
      renderer,
      instance,
      lodCutoff: hudLodDistanceCutoff,
    });
    this.add(this.iconMesh);
  }
  addChunk(chunk, chunkResult) {
    this.iconMesh.addChunk(chunk, chunkResult);
  }
  removeChunk(chunk) {
    this.iconMesh.removeChunk(chunk);
  }
  update() {
    this.iconMesh.update();
  }
  async waitForLoad() {
    const iconPackage = await IconPackage.loadUrls(hudUrls);
    this.iconMesh.setPackage(iconPackage);
  }
}
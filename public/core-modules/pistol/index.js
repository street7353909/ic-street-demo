import * as THREE from 'three';
// import metaversefile from 'metaversefile';
import {
  MathUtils,
} from 'three';

//

const {clamp} = MathUtils;
// const baseUrl = import.meta.url.replace(/(\/)[^\/\\]*$/, '$1');

//

const localVector = new THREE.Vector3();
const localMatrix = new THREE.Matrix4();

const upVector = new THREE.Vector3(0, 1, 0);
const z180Quaternion = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI);
const muzzleOffset = new THREE.Vector3(0, 0.1, 0.25);
const muzzleFlashTime = 300;
const bulletSparkTime = 300;

const emptyArray = [];
const fnEmptyArray = () => emptyArray;

//

const numSmokes = 20;
const numZs = 10;
const explosionCubeGeometry = new THREE.BoxBufferGeometry(0.04, 0.04, 0.04);
const explosionCubeMaterial = new THREE.ShaderMaterial({
  uniforms: {
    uAnimation: {
      type: 'f',
      value: 0,
      needsUpdate: true,
    },
    uColor1: {
      type: 'v3',
      value: new THREE.Color(0x9ccc65),
      needsUpdate: true,
    },
    uColor2: {
      type: 'v3',
      value: new THREE.Color(0x7e57c2),
      needsUpdate: true,
    },
    uGravity: {
      type: 'f',
      value: 0,
      needsUpdate: true,
    },
  },
  vertexShader: `\
    ${THREE.ShaderChunk.common}

    uniform float uAnimation;
    uniform float uGravity;
    attribute float z;
    attribute float maxZ;
    attribute vec4 q;
    attribute vec4 phase;
    attribute float scale;
    varying float vZ;
    varying float vMaxZ;
    varying vec4 vPhase;

    ${THREE.ShaderChunk.logdepthbuf_pars_vertex}

    vec3 applyQuaternion(vec3 v, vec4 q) {
      return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
    }
    /* float easeBezier(float p, vec4 curve) {
      float ip = 1.0 - p;
      return (3.0 * ip * ip * p * curve.xy + 3.0 * ip * p * p * curve.zw + p * p * p).y;
    }
    float ease(float p) {
      return easeBezier(p, vec4(0., 1., 0., 1.));
    } */

    void main() {
      vZ = z;
      vMaxZ = maxZ;
      vPhase = phase;
      float forwardFactor = pow(uAnimation, 0.5);
      vec2 sideFactor = vec2(sin(uAnimation*PI*2.*phase.z), sin(uAnimation*PI*2.*phase.w));
      vec3 p = applyQuaternion(position * scale * (1.-z*maxZ) * (1.0-uAnimation) + vec3(0., 0., pow(z*maxZ*forwardFactor, 0.5)), q) +
        vec3(uAnimation * sideFactor.x, uAnimation * sideFactor.y, 0.)*0.1 +
        vec3(0., uAnimation * 0.1 * uGravity * phase.x, 0.);
      gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
      
      ${THREE.ShaderChunk.logdepthbuf_vertex}
    }
  `,
  fragmentShader: `\
    #define PI 3.1415926535897932384626433832795
    
    uniform float uAnimation;
    uniform vec3 uColor1;
    uniform vec3 uColor2;
    varying float vZ;
    varying float vMaxZ;
    varying vec4 vPhase;

    // vec3 c = vec3(${new THREE.Color(0x9ccc65).toArray().join(', ')});
    // vec3 s = vec3(${new THREE.Color(0x7e57c2).toArray().join(', ')});

    ${THREE.ShaderChunk.logdepthbuf_pars_fragment}
    
    void main() {
      float factor = min(pow(vZ*vMaxZ, 0.2) + pow(uAnimation, 2.), 1.0);
      gl_FragColor = vec4(mix(uColor1, uColor2, factor) * (2.5 - pow(uAnimation, 0.2)) * 0.6 + vec3(0.03 * vPhase.x), 1.0);

      ${THREE.ShaderChunk.logdepthbuf_fragment}
    }
  `,
  // transparent: true,
});
const _makeExplosionMesh = (color1Hex, color2Hex, gravity, rate) => {
  const numPositions = explosionCubeGeometry.attributes.position.array.length * numSmokes * numZs;
  const numIndices = explosionCubeGeometry.index.array.length * numSmokes * numZs;
  const arrayBuffer = new ArrayBuffer(
    numPositions * Float32Array.BYTES_PER_ELEMENT + // position
    numPositions/3 * Float32Array.BYTES_PER_ELEMENT + // z
    numPositions/3 * Float32Array.BYTES_PER_ELEMENT + // maxZ
    numPositions/3*4 * Float32Array.BYTES_PER_ELEMENT + // q
    numPositions/3*4 * Float32Array.BYTES_PER_ELEMENT + // phase
    numPositions/3 * Float32Array.BYTES_PER_ELEMENT + // scale
    numIndices * Int16Array.BYTES_PER_ELEMENT // index
  );
  let index = 0;
  const positions = new Float32Array(arrayBuffer, index, numPositions);
  index += numPositions*Float32Array.BYTES_PER_ELEMENT;
  const zs = new Float32Array(arrayBuffer, index, numPositions/3);
  index += numPositions/3*Float32Array.BYTES_PER_ELEMENT;
  const maxZs = new Float32Array(arrayBuffer, index, numPositions/3);
  index += numPositions/3*Float32Array.BYTES_PER_ELEMENT;
  const qs = new Float32Array(arrayBuffer, index, numPositions/3*4);
  index += numPositions/3*4*Float32Array.BYTES_PER_ELEMENT;
  const phases = new Float32Array(arrayBuffer, index, numPositions/3*4);
  index += numPositions/3*4*Float32Array.BYTES_PER_ELEMENT;
  const scales = new Float32Array(arrayBuffer, index, numPositions/3);
  index += numPositions/3*Float32Array.BYTES_PER_ELEMENT;
  const indices = new Uint16Array(arrayBuffer, index, numIndices);
  index += numIndices*Uint16Array.BYTES_PER_ELEMENT;

  const numPositionsPerSmoke = numPositions/numSmokes;
  const numPositionsPerZ = numPositionsPerSmoke/numZs;
  const numIndicesPerSmoke = numIndices/numSmokes;
  const numIndicesPerZ = numIndicesPerSmoke/numZs;

  for (let i = 0; i < numSmokes; i++) {
    const q = new THREE.Quaternion().setFromEuler(
      new THREE.Euler((-1+Math.random()*2)*Math.PI*2*0.05, (-1+Math.random()*2)*Math.PI*2*0.05, (-1+Math.random()*2)*Math.PI*2*0.05, 'YXZ')
    );
    for (let j = 0; j < numPositionsPerSmoke/3*4; j += 4) {
      q.toArray(qs, i*numPositionsPerSmoke/3*4 + j);
    }
    const maxZ = Math.random();
    for (let j = 0; j < numZs; j++) {
      positions.set(explosionCubeGeometry.attributes.position.array, i*numPositionsPerSmoke + j*numPositionsPerZ);
      const indexOffset = i*numPositionsPerSmoke/3 + j*numPositionsPerZ/3;
      for (let k = 0; k < numIndicesPerZ; k++) {
        indices[i*numIndicesPerSmoke + j*numIndicesPerZ + k] = explosionCubeGeometry.index.array[k] + indexOffset;
      }

      const z = j/numZs;
      for (let k = 0; k < numPositionsPerZ/3; k++) {
        zs[i*numPositionsPerSmoke/3 + j*numPositionsPerZ/3 + k] = z;
      }
      for (let k = 0; k < numPositionsPerZ/3; k++) {
        maxZs[i*numPositionsPerSmoke/3 + j*numPositionsPerZ/3 + k] = maxZ;
      }
      const phase = new THREE.Vector4(Math.random()*Math.PI*2, Math.random()*Math.PI*2, 0.1+Math.random()*0.2, 0.1+Math.random()*0.2);
      for (let k = 0; k < numPositionsPerZ/3*4; k += 4) {
        phase.toArray(phases, i*numPositionsPerSmoke/3*4 + j*numPositionsPerZ/3*4 + k);
      }
      const scale = 0.9 + Math.random()*0.2;
      for (let k = 0; k < numPositionsPerZ/3; k++) {
        scales[i*numPositionsPerSmoke/3*4 + j*numPositionsPerZ/3*4 + k] = scale;
      }
    }
  }
  const geometry = new THREE.BufferGeometry();
  geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));
  geometry.setAttribute('z', new THREE.BufferAttribute(zs, 1));
  geometry.setAttribute('maxZ', new THREE.BufferAttribute(maxZs, 1));
  geometry.setAttribute('q', new THREE.BufferAttribute(qs, 4));
  geometry.setAttribute('phase', new THREE.BufferAttribute(phases, 4));
  geometry.setAttribute('scale', new THREE.BufferAttribute(scales, 1));
  geometry.setIndex(new THREE.BufferAttribute(indices, 1));

  const material = explosionCubeMaterial.clone();
  if (typeof color1Hex === 'number') {
    material.uniforms.uColor1.value.setHex(color1Hex);
  }
  if (typeof color2Hex === 'number') {
    material.uniforms.uColor2.value.setHex(color2Hex);
  }
  if (typeof gravity === 'number') {
    material.uniforms.uGravity.value = gravity;
  }
  if (typeof rate === 'number') {
    // nothing
  } else {
    rate = 1;
  }

  const mesh = new THREE.Mesh(geometry, material);
  mesh.frustumCulled = false;
  mesh.update = timeDiff => {
    material.uniforms.uAnimation.value += timeDiff/1000*rate;
    material.uniforms.uAnimation.needsUpdate = true;
    return material.uniforms.uAnimation.value < 1;
  };
  /* mesh.trigger = (position, quaternion) => {
    material.uniform.uAnimation = 0;
  }; */
  return mesh;
};

//

const components = [
  // {
  //   "key": "instanceId",
  //   "value": getNextInstanceId(),
  // },
  // {
  //   "key": "contentId",
  //   "value": u2,
  // },
  {
    "key": "physics",
    "value": true,
  },
  {
    "key": "wear",
    "value": {
      "boneAttachment": "leftHand",
      "position": [-0.04, -0.03, -0.01],
      "quaternion": [0.5, -0.5, -0.5, 0.5],
      "scale": [1, 1, 1]
    }
  },
  {
    "key": "aim",
    "value": {
      "ikHand": "right"
    }
  },
  {
    "key": "use",
    "value": {
      "ik": "pistol"
    }
  }
];

//

export default ctx => {
  const {
    useApp,
    useFrame,
    useActivate,
    // useWear,
    // useUse,
    useLocalPlayer,
    usePhysics,
    useScene,
    // getNextInstanceId,
    getAppByPhysicsId,
    // useWorld,
    // useDefaultModules,
    useCleanup,
    useSounds,
    useImportManager,
    useEngine,
  } = ctx;

  const app = useApp();
  // const appSubApps = [];
  const physics = usePhysics();
  const scene = useScene();
  const importManager = useImportManager();
  const engine = useEngine();
  
  app.name = 'pistol';
  app.description = 'A pistol that seems like it would be used by a gang.';

  app.setComponents(components);

  // const worldLights = app;
  /* const _updateSubAppMatrix = subApp => {
    subApp.updateMatrixWorld();
    app.position.copy(subApp.position);
    app.quaternion.copy(subApp.quaternion);
    app.scale.copy(subApp.scale);
    app.matrix.copy(subApp.matrix);
    app.matrixWorld.copy(subApp.matrixWorld);
  }; */
  const sounds = useSounds();
  const soundFiles = sounds.getSoundFiles();
  const soundIndex = soundFiles.combat.map(sound => sound.name).indexOf('combat/Colt45_Shot2.wav');

  let pointLights = [];
  const gunPointLight = new THREE.PointLight(0xFFFFFF, 5);
  gunPointLight.castShadow = false; 
  gunPointLight.startTime = 0;
  gunPointLight.endTime = 0;
  gunPointLight.initialIntensity = gunPointLight.intensity;
  // const world = useWorld();
  // const worldLights = world.getLights();
  // worldLights.add(gunPointLight);
  pointLights.push(gunPointLight);
  
  const bulletPointLight = new THREE.PointLight(0xef5350, 5, 10);
  bulletPointLight.castShadow = false;
  bulletPointLight.startTime = 0;
  bulletPointLight.endTime = 0;
  bulletPointLight.initialIntensity = bulletPointLight.intensity;
  // worldLights.add(bulletPointLight);
  pointLights.push(bulletPointLight);

  const textureLoader = new THREE.TextureLoader();

  const debugGeo = new THREE.BoxGeometry( 0.01, 0.01, 0.01);
  const debugMat = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
  const decalTextureName = "bulletHole.jpg";
  const decalTexture = textureLoader.load(`${import.meta.url.replace(/(\/)[^\/]*$/, '$1')}${ decalTextureName}`);
  // decalTexture.needsUpdate = true;
  const decalMaterial = new THREE.MeshPhysicalMaterial({
    // color: 0xFF0000,
    map: decalTexture,
    alphaMap: decalTexture,
    transparent: true,
    alphaTest: 0.01,
    // depthWrite: true,
    // depthTest: true,
  });
  decalMaterial.needsUpdate = true;
  // const debugMesh = [];
  const debugDecalVertPos = false;

  const maxNumDecals = 128;
  const decalGeometry = new THREE.PlaneBufferGeometry(0.5, 0.5, 8, 8).toNonIndexed();
  const _makeDecalMesh = () => {
    const geometry = new THREE.BufferGeometry();
    const positions = new Float32Array(decalGeometry.attributes.position.array.length * maxNumDecals);
    const positionsAttribute = new THREE.BufferAttribute(positions, 3);
    geometry.setAttribute('position', positionsAttribute);
    const normals = new Float32Array(decalGeometry.attributes.normal.array.length * maxNumDecals);
    const normalsAttribute = new THREE.BufferAttribute(normals, 3);
    geometry.setAttribute('normal', normalsAttribute);
    const uvs = new Float32Array(decalGeometry.attributes.uv.array.length * maxNumDecals);
    const uvsAttribute = new THREE.BufferAttribute(uvs, 2);
    geometry.setAttribute('uv', uvsAttribute);
    // const indices = new Uint16Array(decalGeometry.index.array.length * maxNumDecals);
    // const indicesAttribute = new THREE.BufferAttribute(indices, 1);
    // geometry.setIndex(indicesAttribute);

    const decalMesh = new THREE.Mesh(geometry, decalMaterial);
    decalMesh.name = 'DecalMesh';
    decalMesh.frustumCulled = false;
    decalMesh.offset = 0;

    return decalMesh;
  };

  const appDecalMeshes = [];
  const decalMeshMap = new Map();

  const decalMeshCleanup = (event) => {
    const destroyingApp = event.target;
    const destroyingDecalMesh = decalMeshMap.get(destroyingApp);
    scene.remove(destroyingDecalMesh);
  };

  const baseUrl = import.meta.url
    .replace(/\/([^\/]*)$/, '');
  // const baseUrl2 = baseUrl
  //   .replace(/\/([^\/]*)$/, '');

  //

  let explosionMeshes = [];

  //

  let gunApp = null;
  // let explosionApp = null;
  // let subApps = [null, null];
  ctx.waitUntil((async () => {
    {
      // let u2 = `https://webaverse.github.io/pixelsplosion/`;
      // let u2 = `${baseUrl2}/pixelsplosion/index.js`;
      // if (/^https?:/.test(u2)) {
      //   u2 = '/@proxy/' + u2;
      // }
      // const appContext = engine.engineAppContextFactory.makeAppContext({
      //   app,
      // });



      /* explosionApp = await importManager.createAppAsync({
        start_url: u2,

        position: app.position,
        quaternion: app.quaternion,
        scale: app.scale,

        appContext,
      }); */

      // const explosionRate = 2000;

      // console.log('group objects 3', u2, m);
      // explosionApp.contentId = u2;
      // explosionApp.instanceId = getNextInstanceId();
      // explosionApp.position.copy(app.position);
      // explosionApp.quaternion.copy(app.quaternion);
      // explosionApp.scale.copy(app.scale);
      // explosionApp.updateMatrixWorld();

      // explosionApp.name = 'explosion';
      // subApps[0] = explosionApp;

      // await explosionApp.addModule(m);
      // scene.add(explosionApp);
      // appSubApps.push(explosionApp);
      // explosionApp.add(bulletPointLight);
      // // metaversefile.addApp(explosionApp);
      // explosionApp.updateMatrixWorld();
    }

    {
      let u2 = `${baseUrl}/military.glb`;

      gunApp = importManager.createApp();
      // gunApp.name = app.name;
      // gunApp.description = app.description;
      app.add(gunApp);
      gunApp.updateMatrixWorld();

      const appContext = engine.engineAppContextFactory.makeAppContext({
        app: gunApp,
      });

      gunApp = await importManager.createAppAsync({
        app: gunApp,
        appContext,
        start_url: u2,
      });
      // gunApp.name = app.name;
      // gunApp.description = app.description;

      // gunApp.position.copy(app.position);
      // gunApp.quaternion.copy(app.quaternion);
      // gunApp.scale.copy(app.scale);
      // gunApp.updateMatrixWorld();
      // gunApp.name = 'gun';
      // gunApp.getPhysicsObjectsOriginal = gunApp.getPhysicsObjects;
      // gunApp.getPhysicsObjects = fnEmptyArray;
      // subApps[1] = gunApp;

      gunApp.add(gunPointLight);
      // gunApp.updateMatrixWorld();

      // await gunApp.addModule(m);
      app.add(gunApp);
      gunApp.updateMatrixWorld();

      app.addEventListener('use', e => {
        const {use} = e;
        if (use) {
          // explosion mesh
          {
            const color1 = 0xef5350;
            const color2 = 0x000000;
            const gravity = -0.5;
            const rate = 0.5;
    
            const explosionMesh = _makeExplosionMesh(color1, color2, gravity, rate);
            explosionMesh.position.copy(app.position)
              .add(
                new THREE.Vector3(0, 0.1, 0.25).applyQuaternion(gunApp.quaternion)
              );
            explosionMesh.quaternion.copy(app.quaternion);
            explosionMesh.scale.copy(app.scale);
    
            scene.add(explosionMesh);
            explosionMesh.updateMatrixWorld();

            explosionMeshes.push(explosionMesh);
          }

          // muzzle flash
          {
            gunPointLight.startTime = performance.now();
            gunPointLight.endTime = gunPointLight.startTime + muzzleFlashTime;
          }

          // bullet hit
          {
            const result = physics.raycast(
              gunApp.position,
              gunApp.quaternion.clone().multiply(z180Quaternion)
            );
            if (result) {
              const targetApp = getAppByPhysicsId(result.objectId);
              if (targetApp) {
                const hasTargetApp = decalMeshMap.has(targetApp);
                if (!hasTargetApp) {
                  const newDecalMesh = _makeDecalMesh();
                  scene.add(newDecalMesh);
                  // appSubApps.push(newDecalMesh)
                  appDecalMeshes.push(newDecalMesh);
                  decalMeshMap.set(targetApp, newDecalMesh);
                  // listening for destroy event on the hit app
                  targetApp.addEventListener('destroy', decalMeshCleanup);
                }
              }

              const appDecalMesh = decalMeshMap.get(targetApp);

              const normal = new THREE.Vector3().fromArray(result.normal);
              const newPointVec = new THREE.Vector3().fromArray(result.point);
              const modiPoint = newPointVec
                .clone()
                .add(normal.clone().multiplyScalar(0.01));

              const pos = modiPoint;
              const q = new THREE.Quaternion().setFromRotationMatrix(
                new THREE.Matrix4().lookAt(pos, pos.clone().sub(normal), upVector)
              );
              const s = new THREE.Vector3(1, 1, 1);
              const planeMatrix = new THREE.Matrix4().compose(pos, q, s);
              const planeMatrixInverse = planeMatrix.clone().invert();

              const localDecalGeometry = decalGeometry.clone();
              const positions = localDecalGeometry.attributes.position.array;
              for (let i = 0; i < positions.length; i++) {
                const p = new THREE.Vector3(
                  positions[i * 3],
                  positions[i * 3 + 1],
                  positions[i * 3 + 2]
                );
                const pToWorld = p.clone().applyMatrix4(planeMatrix);
                const vertexRaycast = physics.raycast(pToWorld, q.clone());

                if (vertexRaycast) {
                  const vertextHitnormal = new THREE.Vector3().fromArray(
                    vertexRaycast.normal
                  );

                  /* const dummyPosition = new THREE.Object3D();
                  scene.add( dummyPosition );
                  const offSet = 14;
                  const pointVec =  dummyPosition.localToWorld(new THREE.Vector3().fromArray(vertexRaycast.point).add(
                    new Vector3(0, vertextHitnormal.y / offSet,0 )
                  )); */
                  const pointVec = new THREE.Vector3()
                    .fromArray(vertexRaycast.point)
                    .add(vertextHitnormal.clone().multiplyScalar(0.01));
                  pointVec.applyMatrix4(planeMatrixInverse);
                  const minClamp = -0.25;
                  const maxClamp = 0.25;
                  pointVec.sub(p);
                  // pointVec.x = clamp(pointVec.x, minClamp, maxClamp);
                  // pointVec.y = clamp(pointVec.y, minClamp, maxClamp);
                  pointVec.z = clamp(pointVec.z, minClamp, maxClamp);
                  pointVec.add(p);
                  pointVec.applyMatrix4(planeMatrix);
                  // const clampedPos = new Vector3(clamp(worldToLoc.x, minClamp, maxClamp),
                  // clamp(worldToLoc.y, minClamp, maxClamp), clamp(worldToLoc.z, minClamp, maxClamp));

                  if (debugDecalVertPos) {
                    const debugMesh = new THREE.Mesh(debugGeo, debugMat);
                    debugMesh.position.set(pointVec.x, pointVec.y, pointVec.z);
                    debugMesh.updateWorldMatrix();
                    scene.add(debugMesh);
                    // appSubApps.push(debugMesh);
                  }

                  // dummyPosition.position.set(pointVec.x, pointVec.y, pointVec.z);
                  // dummyPosition.updateWorldMatrix();
                  // const worldToLoc = pointVec.clone().applyMatrix4(planeMatrixInverse);

                  pointVec.toArray(positions, i * 3);
                  // decalGeometry.attributes.position.setXYZ( i, clampedPos.x, clampedPos.y, clampedPos.z );
                } else {
                  pToWorld.toArray(positions, i * 3);
                }
              }

              localDecalGeometry.computeVertexNormals();
              // now, we copy the localDecalGeometry into the decalMesh.geometry at the appropriate position
              // we make sure to copy the position, uv, normal, and index. all of these attributes should be correctly offset
              if (appDecalMesh) {
                const offset = appDecalMesh.offset;
                // console.log('offset', appDecalMesh.offset);
                for (
                  let i = 0;
                  i < localDecalGeometry.attributes.position.count;
                  i++
                ) {
                  appDecalMesh.geometry.attributes.position.setXYZ(
                    i + offset,
                    localDecalGeometry.attributes.position.getX(i),
                    localDecalGeometry.attributes.position.getY(i),
                    localDecalGeometry.attributes.position.getZ(i)
                  );
                  appDecalMesh.geometry.attributes.uv.setXY(
                    i + offset,
                    localDecalGeometry.attributes.uv.getX(i),
                    localDecalGeometry.attributes.uv.getY(i)
                  );
                  appDecalMesh.geometry.attributes.normal.setXYZ(
                    i + offset,
                    localDecalGeometry.attributes.normal.getX(i),
                    localDecalGeometry.attributes.normal.getY(i),
                    localDecalGeometry.attributes.normal.getZ(i)
                  );
                  // appDecalMesh.geometry.index.setX( i + offset, localDecalGeometry.index.getX(i) );
                }
                // flag geometry attributes for update
                appDecalMesh.geometry.attributes.position.updateRange = {
                  offset: offset * 3,
                  count: localDecalGeometry.attributes.position.array.length,
                };
                appDecalMesh.geometry.attributes.position.needsUpdate = true;
                appDecalMesh.geometry.attributes.uv.updateRange = {
                  offset: offset * 2,
                  count: localDecalGeometry.attributes.uv.array.length,
                };
                appDecalMesh.geometry.attributes.uv.needsUpdate = true;
                appDecalMesh.geometry.attributes.normal.updateRange = {
                  offset: offset * 3,
                  count: localDecalGeometry.attributes.normal.array.length,
                };
                appDecalMesh.geometry.attributes.normal.needsUpdate = true;
                // appDecalMesh.geometry.index.updateRange = {
                //   offset,
                //   count: localDecalGeometry.index.count,
                // };
                //appDecalMesh.geometry.index.needsUpdate = true;
                // update geometry attribute offset
                appDecalMesh.offset +=
                  localDecalGeometry.attributes.position.count;
                appDecalMesh.offset =
                  appDecalMesh.offset %
                  appDecalMesh.geometry.attributes.position.count;

                explosionApp.position.fromArray(result.point);
                explosionApp.quaternion.setFromRotationMatrix(
                  new THREE.Matrix4().lookAt(
                    explosionApp.position,
                    explosionApp.position.clone().sub(normal),
                    upVector
                  )
                );
                // explosionApp.scale.copy(gunApp.scale);
                explosionApp.updateMatrixWorld();
                // explosionApp.setComponent('color1', 0xef5350);
                // explosionApp.setComponent('color2', 0x000000);
                // explosionApp.setComponent('gravity', -0.5);
                // explosionApp.setComponent('rate', 0.5);
                // explosionApp.use();

                // bulletPointLight.position.copy(explosionApp.position);
                bulletPointLight.startTime = performance.now();
                bulletPointLight.endTime = bulletPointLight.startTime + bulletSparkTime;

                if (targetApp) {
                  const localPlayer = useLocalPlayer();
                  const damage = 2;

                  const hitPosition = new THREE.Vector3().fromArray(result.point);
                  const hitQuaternion =
                    new THREE.Quaternion().setFromRotationMatrix(
                      localMatrix.lookAt(
                        localPlayer.position,
                        hitPosition,
                        localVector.set(0, 1, 0)
                      )
                    );

                  const hitDirection = targetApp.position
                    .clone()
                    .sub(localPlayer.position);
                  // hitDirection.y = 0;
                  hitDirection.normalize();

                  // const willDie = targetApp.willDieFrom(damage);
                  targetApp.hit(damage, {
                    collisionId: result.objectId,
                    hitPosition,
                    hitDirection,
                    hitQuaternion,
                    // willDie,
                  });
                }
              } else {
                console.warn('no app with physics id', result.objectId);
              }
            }
          }
        }
      });
    }
  })());
  
  // let timePassed = explosionRate;
  useFrame(({timeDiff}) => {
    /* timePassed += timeDiff;
    while (timePassed >= explosionRate) {
      const explosionMesh = _makeExplosionMesh();
      explosionMesh.position.set((-0.5+Math.random())*2, 0, (-0.5+Math.random())*2);
      app.add(explosionMesh);
      explosionMeshes.push(explosionMesh);
      timePassed -= explosionRate;
    } */
    
    explosionMeshes = explosionMeshes.filter(explosionMesh => {
      const result = explosionMesh.update(timeDiff);
      if (!result) {
        explosionMesh.parent.remove(explosionMesh);
      }
      return result;
    });
  });

  // useActivate(() => {
  //   const localPlayer = useLocalPlayer();
  //   localPlayer.wear(app);
  // });

  let wearing = false;
  app.addEventListener('wearupdate', e => {
    const {wear, player} = e;
    // for (const subApp of subApps) {
    //   subApp.position.copy(app.position);
    //   subApp.quaternion.copy(app.quaternion);
    //   subApp.scale.copy(app.scale);
    //   subApp.updateMatrixWorld();
      
    //   subApp.dispatchEvent({
    //     type: 'wearupdate',
    //     wear,
    //     player: player
    //   });
    // }
    wearing = wear;
  });
  app.addEventListener('use', e => {
    const {
      use,
    } = e;
    if (use) {
      sounds.playSound(soundFiles.combat[soundIndex]);
      // gunApp.use();
    }
  });

  useFrame((timestamp) => {
    // if (!wearing) {
    //   if (gunApp) {
    //     gunApp.position.copy(app.position);
    //     gunApp.quaternion.copy(app.quaternion);
    //     gunApp.updateMatrixWorld();
    //   }
    // } else {
    //   if (gunApp) {
    //     app.position.copy(gunApp.position);
    //     app.quaternion.copy(gunApp.quaternion);
    //     app.updateMatrixWorld();
    //   }
    // }
    
    // if (gunApp) {
    //   gunPointLight.position.set(0,0,0)
    //     .add(localVector.copy(muzzleOffset).applyQuaternion(gunApp.quaternion));
    //   gunPointLight.updateMatrixWorld();
    // }
     
    for (const pointLight of pointLights) {
      const factor = Math.min(Math.max((timestamp - pointLight.startTime) / (pointLight.endTime - pointLight.startTime), 0), 1);
      pointLight.intensity = pointLight.initialIntensity * (1 - Math.pow(factor, 0.5));
    }
  });
  
  useCleanup(() => {
    for (const [targetApp, decalMesh] of decalMeshMap.entries()) {
      targetApp.removeEventListener('destroy', decalMeshCleanup);
      scene.remove(decalMesh);
      decalMeshMap.delete(targetApp);
    }
    for (const decalMesh of appDecalMeshes) {
      scene.remove(decalMesh);
    }
    // for (const subApp of subApps) {
    //   if (subApp) {
    //     // metaversefile.removeApp(subApp);
    //     scene.remove(subApp);
    //     subApp.destroy();
    //   }
    // }
  });

  return app;
};


import Terrains from './terrains.js'
import Chunks from './chunks.js'

export default class State
{
  static instance

  static getInstance() {
    return State.instance
  }

  constructor(app, camera, player, terrainWorker) {
    if(State.instance)
      return State.instance

    State.instance = this
    this.app = app;
    this.camera = camera;
    this.player = player;
    this.terrainWorker = terrainWorker;
    this.terrains = new Terrains();
    this.chunks = new Chunks();

    this.chunks.events.on('create', chunk => {
      app.dispatchEvent({
        type: 'chunkadd',
        chunk,
      });
    });
    this.chunks.events.on('destroy', chunk => {
      app.dispatchEvent({
        type: 'chunkremove',
        chunk,
      });
    });

    this.terrains.events.on('create', terrain => {
      app.dispatchEvent({
        type: 'terrainadd',
        terrain,
      });
    });
    this.terrains.events.on('destroy', terrain => {
      app.dispatchEvent({
        type: 'terrainremove',
        terrain,
      });
    });
  }
  
  update() {
    this.chunks.update()
  }
}